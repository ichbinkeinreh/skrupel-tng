package org.skrupeltng.modules.ai.easy.ships;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.TotalGoodCalculator;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteResourceAction;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.skrupeltng.modules.masterdata.database.Resource;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataConstants;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_EASY")
public class AIEasyFreighters {

	@Autowired
	protected PlanetRepository planetRepository;

	@Autowired
	protected ShipRepository shipRepository;

	@Autowired
	protected ShipRouteEntryRepository shipRouteEntryRepository;

	@Autowired
	protected MasterDataService masterDataService;

	@Autowired
	protected ShipService shipService;

	public void processFreighter(long playerId, List<Ship> allPlayerShips, List<Planet> targetPlanets, List<Planet> ownedPlanets,
			List<Planet> highPopulationPlanets, Ship ship) {
		if (ship.getCurrentRouteEntry() != null) {
			checkRouteStatus(ship);
			return;
		}

		Planet planet = ship.getPlanet();

		if (planet == null) {
			if (ship.getTravelSpeed() == 0) {
				Optional<Planet> nextPlanetOpt = ownedPlanets.stream().min(new CoordinateDistanceComparator(ship));

				if (nextPlanetOpt.isPresent()) {
					ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
					Planet nextPlanet = nextPlanetOpt.get();
					ship.setDestinationX(nextPlanet.getX());
					ship.setDestinationY(nextPlanet.getY());
					shipRepository.save(ship);
				}
			}

			return;
		}

		ShipTransportRequest transportRequest = new ShipTransportRequest(ship);

		if (planet.getPlayer() == null || planet.getPlayer().getId() != playerId) {
			colonize(ship, transportRequest);
		} else {
			List<Long> planetIds = planetRepository.findOwnedPlanetIdsWithoutRoute(ship.getPlayer().getId());

			if (checkRoute(ship, allPlayerShips, planetIds)) {
				Pair<Ship, Planet> result = shipService.transportWithoutPermissionCheck(new ShipTransportRequest(ship), ship, planet);
				ship = result.getFirst();
				setupRoute(ship, planetIds, highPopulationPlanets, ownedPlanets);
				return;
			}

			int storageSpace = ship.retrieveStorageSpace();
			int targetColonists = (int)Math.min((storageSpace * 0.667f) * MasterDataConstants.COLONIST_STORAGE_FACTOR,
					getColonistsForNewColony() * 4);

			if (checkRaid(ship, targetColonists)) {
				raidResources(ship, transportRequest);
			} else if (!loadNewColonists(ship, transportRequest, targetColonists)) {
				return;
			}
		}

		ship = setNextFreighterDestination(targetPlanets, highPopulationPlanets, ship, transportRequest);

		shipService.transportWithoutPermissionCheck(transportRequest, ship, planet);
	}

	protected void checkRouteStatus(Ship ship) {
		if (ship.getTravelSpeed() == 0 || ship.getDestinationX() < 0 || ship.getDestinationY() < 0) {
			ship.setTravelSpeed(ship.getRouteTravelSpeed());

			Planet destinationPlanet = ship.getCurrentRouteEntry().getPlanet();
			ship.setDestinationX(destinationPlanet.getX());
			ship.setDestinationY(destinationPlanet.getY());

			Planet planet = ship.getPlanet();

			if (planet != null && ship.getFuel() < ship.getRouteMinFuel()) {
				ShipTransportRequest request = new ShipTransportRequest(ship);
				request.setFuel(Math.min(ship.getRouteMinFuel(), ship.getFuel() + planet.getFuel()));
				ship = shipService.transportWithoutPermissionCheck(request, ship, planet).getFirst();
			}

			shipRepository.save(ship);
		}
	}

	protected int getColonistsForNewColony() {
		return 1500;
	}

	protected int getSuppliesForNewColony() {
		return 3;
	}

	protected void colonize(Ship ship, final ShipTransportRequest transportRequest) {
		int colonists = ship.getColonists();
		int countForNewColony = getColonistsForNewColony();

		if (colonists >= countForNewColony) {
			transportRequest.setColonists(colonists - countForNewColony);

			float ratio = (float)transportRequest.getColonists() / colonists;
			transportRequest.setMoney((int)Math.floor(ship.getMoney() * ratio));
			transportRequest.setSupplies((int)Math.floor(ship.getSupplies() * ratio));

			if (transportRequest.getSupplies() < 1 && ship.getSupplies() > 1) {
				transportRequest.setSupplies(0);
			}
		}
	}

	protected boolean checkRoute(Ship ship, List<Ship> ships, List<Long> planetIds) {
		boolean otherShipsColonize = ships.stream().anyMatch(s -> s.getId() != ship.getId() && s.getColonists() > 0);
		int min = getPlanetCountForRoute(ship);
		return otherShipsColonize && planetIds.size() > min;
	}

	protected int getPlanetCountForRoute(Ship ship) {
		int storageSpace = ship.retrieveStorageSpace();
		int min = 2;

		if (storageSpace > 300) {
			min = 3;
		}

		if (storageSpace > 1000) {
			min = 4;
		}

		if (storageSpace > 2000) {
			min = 5;
		}

		return min;
	}

	protected void setupRoute(Ship ship, List<Long> planetIds, List<Planet> highPopulationPlanets, List<Planet> ownedPlanets) {
		List<Planet> planets = planetRepository.findByIds(planetIds);
		int count = getPlanetCountForRoute(ship);

		Planet current = planets.get(0);
		Optional<Planet> nearestDropOffOpt = highPopulationPlanets.stream().min(new CoordinateDistanceComparator(current));
		Planet nearestDropOff = null;

		if (nearestDropOffOpt.isPresent()) {
			nearestDropOff = nearestDropOffOpt.get();
		} else {
			nearestDropOff = ownedPlanets.stream().max(Comparator.comparing(Planet::getColonists)).get();
		}

		List<ShipRouteEntry> route = new ArrayList<>(count + 1);

		ShipRouteEntry entry = new ShipRouteEntry();
		route.add(entry);

		entry.setShip(ship);
		entry.setPlanet(nearestDropOff);
		entry.setFuelAction(ShipRouteResourceAction.LEAVE);
		entry.setMoneyAction(ShipRouteResourceAction.LEAVE);
		entry.setSupplyAction(ShipRouteResourceAction.LEAVE);
		entry.setMineral1Action(ShipRouteResourceAction.LEAVE);
		entry.setMineral2Action(ShipRouteResourceAction.LEAVE);
		entry.setMineral3Action(ShipRouteResourceAction.LEAVE);

		Set<Long> planetIdsInRoute = new HashSet<>(count + 1);
		planetIdsInRoute.add(nearestDropOff.getId());

		int maxDist = (int)CoordHelper.getDistance(nearestDropOff, current);

		for (int i = 1; i <= count; i++) {
			entry = new ShipRouteEntry();
			entry.setShip(ship);
			entry.setPlanet(current);
			entry.setOrderId(i);
			entry.setFuelAction(ShipRouteResourceAction.TAKE);
			entry.setMoneyAction(ShipRouteResourceAction.TAKE);
			entry.setSupplyAction(ShipRouteResourceAction.TAKE);
			entry.setMineral1Action(ShipRouteResourceAction.TAKE);
			entry.setMineral2Action(ShipRouteResourceAction.TAKE);
			entry.setMineral3Action(ShipRouteResourceAction.TAKE);
			route.add(entry);

			planetIdsInRoute.add(current.getId());

			Optional<Planet> nextOpt = planets.stream().filter(p -> p.getLog() == null && !planetIdsInRoute.contains(p.getId()))
					.min(new CoordinateDistanceComparator(current));

			if (nextOpt.isPresent()) {
				Planet last = current;
				current = nextOpt.get();

				int dist = (int)CoordHelper.getDistance(last, current);
				if (dist > maxDist) {
					maxDist = dist;
				}
			} else {
				break;
			}
		}

		if (route.size() > 1) {
			int dist = (int)CoordHelper.getDistance(entry.getPlanet(), nearestDropOff);
			if (dist > maxDist) {
				maxDist = dist;
			}

			int routeMinFuel = getRouteMinFuel(ship, maxDist);

			if (routeMinFuel <= ship.getPlanet().getFuel()) {
				route = shipRouteEntryRepository.saveAll(route);

				ship.updateRoute(route.get(1));
				ship.setRouteTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
				ship.setRoutePrimaryResource(Resource.SUPPLIES);
				ship.setRouteMinFuel(routeMinFuel);

				shipRepository.save(ship);
			}
		}
	}

	protected int getRouteMinFuel(Ship ship, int maxDist) {
		int routeTravelSpeed = ship.getPropulsionSystemTemplate().getWarpSpeed();
		double duration = masterDataService.calculateTravelDuration(routeTravelSpeed, maxDist, 1);
		int fuelPerMonth = masterDataService.calculateFuelConsumptionPerMonth(ship, routeTravelSpeed, maxDist, duration, ship.getShipTemplate().getMass());
		int routeMinFuel = Math.min((int)(duration * fuelPerMonth * 3), ship.getShipTemplate().getFuelCapacity());
		return routeMinFuel;
	}

	protected boolean checkRaid(Ship ship, int targetColonists) {
		Planet planet = ship.getPlanet();
		long playerId = ship.getPlayer().getId();

		return targetColonists < ship.getColonists() || planet.getPlayer() == null && planet.getPlayer().getId() != playerId ||
				planet.getColonists() < 20000;
	}

	protected boolean loadNewColonists(Ship ship, ShipTransportRequest transportRequest, int targetColonists) {
		Planet planet = ship.getPlanet();
		int storageSpace = ship.retrieveStorageSpace();

		transportRequest.setMoney(0);
		transportRequest.setSupplies(0);
		transportRequest.setColonists(targetColonists - ship.getColonists());

		int targetSupplies = (int)Math.min(storageSpace / 6f * 1f, 4 * getSuppliesForNewColony());

		if (targetSupplies > ship.getSupplies()) {
			transportRequest.setSupplies(Math.min(planet.getSupplies(), targetSupplies));
		}

		if (400 > ship.getMoney()) {
			transportRequest.setMoney(Math.min(planet.getMoney(), 400));
		}

		transportRequest.setMineral1(0);
		transportRequest.setMineral2(0);
		transportRequest.setMineral3(0);

		return true;
	}

	protected void raidResources(Ship ship, ShipTransportRequest transportRequest) {
		Planet planet = ship.getPlanet();
		ShipTemplate shipTemplate = ship.getShipTemplate();

		transportRequest.setFuel(Math.min(shipTemplate.getFuelCapacity(), ship.getFuel() + planet.getFuel()));
		int emptySpace = ship.retrieveStorageSpace() - TotalGoodCalculator.retrieveSum(transportRequest);

		if (emptySpace > 0) {
			float quantity = emptySpace / 3f;

			if (quantity >= 1f) {
				float checkSum = emptySpace;
				int quantityInt = (int)Math.floor(quantity);

				checkSum -= quantityInt;
				transportRequest.setMineral1(Math.min(planet.getMineral1(), quantityInt));

				checkSum -= quantityInt;
				transportRequest.setMineral2(Math.min(planet.getMineral2(), quantityInt));

				transportRequest.setMineral3(Math.min(planet.getMineral3(), (int)checkSum));
			} else {
				transportRequest.setMineral3(Math.min(planet.getMineral3(), emptySpace));
			}
		}
	}

	protected Ship setNextFreighterDestination(List<Planet> targetPlanets, List<Planet> highPopulationPlanets, Ship ship,
			ShipTransportRequest transportRequest) {
		Planet planet = ship.getPlanet();

		Collection<Planet> planets = transportRequest.getColonists() >= 1500 ? targetPlanets : highPopulationPlanets;

		Optional<Planet> nearestPlanet = planets.stream()
				.filter(p -> p.getId() != planet.getId())
				.min((a, b) -> Double.compare(CoordHelper.getDistance(ship, a), CoordHelper.getDistance(ship, b)));

		if (nearestPlanet.isPresent()) {
			Planet target = nearestPlanet.get();
			int bestTravelSpeed = ship.getPropulsionSystemTemplate().getWarpSpeed();

			if (planet != null) {
				ShipTemplate shipTemplate = ship.getShipTemplate();

				double distance = CoordHelper.getDistance(ship, target);
				double duration = masterDataService.calculateTravelDuration(bestTravelSpeed, distance, 0d);
				int mass = shipTemplate.getMass();
				int fuelConsumption = masterDataService.calculateFuelConsumptionPerMonth(ship, bestTravelSpeed, distance, duration, mass);
				int targetFuel = Math.min(shipTemplate.getFuelCapacity(), fuelConsumption * 10);

				if (ship.getFuel() < targetFuel) {
					int transportedFuel = Math.min(planet.getFuel(), targetFuel - ship.getFuel());
					transportRequest.setFuel(ship.getFuel() + transportedFuel);
				}
			}

			targetPlanets.remove(target);

			ship.setDestinationX(target.getX());
			ship.setDestinationY(target.getY());
			ship.setTravelSpeed(bestTravelSpeed);

			return shipRepository.save(ship);
		}

		return ship;
	}
}