package org.skrupeltng.modules.ai.easy.ships;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_EASY")
public class AIEasyBombers {

	@Autowired
	protected ShipRepository shipRepository;

	@Autowired
	protected ShipService shipService;

	protected void processBomber(long playerId, Ship ship, List<Planet> targetPlanets, List<Planet> visiblePlanets) {
		if ((ship.getDestinationX() != 0 || ship.getDestinationX() != 0) &&
				CoordHelper.getDistance(ship, new CoordinateImpl(ship.getDestinationX(), ship.getDestinationY(), 0)) < 70) {
			return;
		}

		Optional<Planet> targetPlanet = setTargetPlanet(playerId, ship, targetPlanets, visiblePlanets);

		ship.setActiveAbility(null);
		ship.setTaskType(ShipTaskType.PLANET_BOMBARDMENT);

		Optional<ShipAbility> viralInvasion = ship.getAbility(ShipAbilityType.VIRAL_INVASION);

		if (viralInvasion.isPresent()) {
			ship.setActiveAbility(viralInvasion.get());
			ship.setTaskValue("VIRAL_INVASION_COLONISTS");
			ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
		}

		Optional<ShipAbility> destabilizer = ship.getAbility(ShipAbilityType.DESTABILIZER);

		if (targetPlanet.isPresent() && ship.getPlanet() == targetPlanet.get() && destabilizer.isPresent()) {
			ship.setActiveAbility(destabilizer.get());
			ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
		}

		ship = shipRepository.save(ship);

		Planet planet = ship.getPlanet();

		if (planet != null) {
			ShipTransportRequest request = new ShipTransportRequest(ship);
			request.setFuel(Math.min(ship.getFuel() + planet.getFuel(), ship.getShipTemplate().getFuelCapacity()));
			shipService.transportWithoutPermissionCheck(request, ship, planet);
		}
	}

	protected Optional<Planet> setTargetPlanet(long playerId, Ship ship, List<Planet> targetPlanets, List<Planet> visiblePlanets) {
		List<Planet> planets = targetPlanets.stream().filter(p -> p.getPlayer() != null && p.getPlayer().getId() != playerId).collect(Collectors.toList());

		if (targetPlanets.isEmpty()) {
			planets = visiblePlanets.stream().filter(p -> p.getPlayer() != null && p.getPlayer().getId() != playerId).collect(Collectors.toList());
		}

		if (planets.isEmpty()) {
			return Optional.empty();
		}

		Collections.sort(planets, new CoordinateDistanceComparator(ship));
		Planet targetPlanet = planets.get(0);

		ship.setDestinationShip(null);
		ship.setDestinationX(targetPlanet.getX());
		ship.setDestinationY(targetPlanet.getY());
		ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());

		return Optional.of(targetPlanet);
	}
}