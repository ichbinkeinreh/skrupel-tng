package org.skrupeltng.modules.ai.easy.ships;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_EASY")
public class AIEasyAmbushers {

	@Autowired
	protected ShipRepository shipRepository;

	@Autowired
	protected ShipService shipService;

	@Autowired
	protected VisibleObjects visibleObjects;

	@Autowired
	protected GameRepository gameRepository;

	@Autowired
	protected PlayerRepository playerRepository;

	@Autowired
	protected ConfigProperties configProperties;

	protected void processAmbushers(long playerId, long gameId, Ship ship) {
		Set<CoordinateImpl> visibilityCoordinates = visibleObjects.getVisibilityCoordinates(playerId);
		List<Ship> visibleShips = visibleObjects.getVisibleShipsCached(gameId, visibilityCoordinates, playerId, false);
		Optional<Ship> targetOpt = visibleShips.stream().filter(s -> s.getPlayer().getId() != playerId).min(new CoordinateDistanceComparator(ship));

		if (!targetOpt.isPresent()) {
			return;
		}

		Ship targetShip = targetOpt.get();

		ship.setDestinationShip(targetShip);
		ship.setDestinationX(targetShip.getX());
		ship.setDestinationY(targetShip.getY());
		ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());

		ship.setActiveAbility(null);
		ship.setTaskType(ShipTaskType.NONE);

		if (ship.hasActiveAbility(ShipAbilityType.SIGNATURE_MASK)) {
			ship.setActiveAbility(ship.getAbility(ShipAbilityType.SIGNATURE_MASK).get());
			String color = getSignaturMaskColor(playerId, gameId);
			ship.setTaskValue(color);
			ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
		}

		boolean subSpaceDistortionActive = checkSubSpaceDistortion(ship);
		boolean hasCloaking = false;

		if (!subSpaceDistortionActive) {
			hasCloaking = checkCloaking(ship);
		}

		ship = shipRepository.save(ship);

		if (!subSpaceDistortionActive) {
			Planet planet = ship.getPlanet();

			if (planet != null) {
				ShipTransportRequest request = new ShipTransportRequest(ship);
				request.setFuel(Math.min(ship.getFuel() + planet.getFuel(), ship.getShipTemplate().getFuelCapacity()));

				if (hasCloaking) {
					request.setMineral2(Math.min(ship.getMineral2() + planet.getMineral2(), ship.retrieveStorageSpace()));
				}

				shipService.transportWithoutPermissionCheck(request, ship, planet);
			}
		}
	}

	protected String getSignaturMaskColor(long playerId, long gameId) {
		List<String> playerColors = gameRepository.getPlayerColors(gameId);
		String playerColor = playerRepository.getPlayerColor(playerId);
		playerColors.remove(playerColor);
		Collections.shuffle(playerColors);
		return playerColors.get(0);
	}

	protected boolean checkSubSpaceDistortion(Ship ship) {
		Optional<ShipAbility> ability = ship.getAbility(ShipAbilityType.SUB_SPACE_DISTORTION);

		if (ability.isPresent()) {
			double distance = CoordHelper.getDistance(ship, ship.getDestinationShip());

			if (distance < configProperties.getSubSpaceDistortionRange()) {
				ship.setActiveAbility(ability.get());
				ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
				return true;
			}
		}

		return false;
	}

	protected boolean checkCloaking(Ship ship) {
		Optional<ShipAbility> ability = ship.getAbility(ShipAbilityType.CLOAKING_PERFECT);

		if (ability.isPresent()) {
			ship.setActiveAbility(ability.get());
			ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
			ship.setTaskValue(null);
			return true;
		}

		ability = ship.getAbility(ShipAbilityType.CLOAKING_RELIABLE);

		if (ability.isPresent()) {
			ship.setActiveAbility(ability.get());
			ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
			ship.setTaskValue(null);
			return true;
		}

		ability = ship.getAbility(ShipAbilityType.CLOAKING_UNRELIABLE);

		if (ability.isPresent()) {
			ship.setActiveAbility(ability.get());
			ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
			ship.setTaskValue(null);
			return true;
		}

		return false;
	}
}