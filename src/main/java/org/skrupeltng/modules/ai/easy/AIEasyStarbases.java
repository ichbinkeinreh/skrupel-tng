package org.skrupeltng.modules.ai.easy;

import static org.skrupeltng.modules.ai.AIConstants.AI_AMBUSHER_NAME;
import static org.skrupeltng.modules.ai.AIConstants.AI_BOMBER_NAME;
import static org.skrupeltng.modules.ai.AIConstants.AI_FREIGHTER_NAME;
import static org.skrupeltng.modules.ai.AIConstants.AI_QUARKREORGANIZER_NAME;
import static org.skrupeltng.modules.ai.AIConstants.AI_SCOUT_NAME;
import static org.skrupeltng.modules.ai.AIConstants.AI_SUBPARTICLECLUSTER_NAME;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.skrupeltng.modules.ai.ShipTemplateScore;
import org.skrupeltng.modules.ai.ShipTemplateScoringData;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.starbase.controller.StarbaseProductionRequest;
import org.skrupeltng.modules.ingame.modules.starbase.controller.StarbaseShipConstructionRequest;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplate;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplateRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseHullStock;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbasePropulsionStock;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseWeaponStock;
import org.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.StarbaseProducable;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.ProducableTechLevelComparator;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.database.ShipTemplateStorageSpaceComparator;
import org.skrupeltng.modules.masterdata.database.WeaponTemplate;
import org.skrupeltng.modules.masterdata.database.WeaponTemplateRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.skrupeltng.modules.masterdata.service.StarbaseUpgradeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_EASY")
public class AIEasyStarbases {

	protected static final Comparator<ShipTemplate> SHIP_STORAGE_COMPARATOR_REVERSED = new ShipTemplateStorageSpaceComparator().reversed();
	protected static final Comparator<ShipTemplate> SHIP_STORAGE_COMPARATOR = new ShipTemplateStorageSpaceComparator().reversed();
	protected static final Comparator<StarbaseProducable> PRODUCABLE_TECHLEVEL_COMPARATOR = new ProducableTechLevelComparator().reversed();

	@Autowired
	protected PlanetRepository planetRepository;

	@Autowired
	protected StarbaseRepository starbaseRepository;

	@Autowired
	protected ShipRepository shipRepository;

	@Autowired
	protected MasterDataService masterDataService;

	@Autowired
	protected StarbaseService starbaseService;

	@Autowired
	protected PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Autowired
	protected WeaponTemplateRepository weaponTemplateRepository;

	@Autowired
	protected AquiredShipTemplateRepository aquiredShipTemplateRepository;

	@Autowired
	protected VisibleObjects visibleObjects;

	public void processStarbases(long playerId) {
		List<Starbase> starbases = starbaseRepository.findByPlayerId(playerId);

		for (Starbase starbase : starbases) {
			starbase = upgradeStarbase(starbase);
			buildShip(starbase, playerId);
		}
	}

	protected Starbase upgradeStarbase(Starbase starbase) {
		if (starbase.getHullLevel() < 6 || starbase.getPropulsionLevel() < 6) {
			upgradeStarbase(starbase, 6, StarbaseUpgradeType.HULL, StarbaseUpgradeType.PROPULSION);

			if (starbase.getEnergyLevel() == 0) {
				upgradeStarbase(starbase, 1, StarbaseUpgradeType.ENERGY);
			}

			if (starbase.getProjectileLevel() == 0) {
				upgradeStarbase(starbase, 1, StarbaseUpgradeType.PROJECTILE);
			}
		} else {
			upgradeStarbase(starbase, null, StarbaseUpgradeType.values());
		}

		planetRepository.save(starbase.getPlanet());

		return starbaseRepository.save(starbase);
	}

	protected void upgradeStarbase(Starbase starbase, Integer max, StarbaseUpgradeType... types) {
		if (max == null) {
			max = 10;
		}

		for (StarbaseUpgradeType type : types) {
			int techLevel = starbase.retrieveTechLevel(type);

			if (techLevel == 10) {
				continue;
			}

			for (int i = techLevel + 1; i <= max; i++) {
				if (!upgradeStarbase(starbase, type, i)) {
					break;
				}
			}
		}
	}

	protected boolean upgradeStarbase(Starbase starbase, StarbaseUpgradeType type, int targetLevel) {
		Planet planet = starbase.getPlanet();

		int costs = masterDataService.calculateStarbaseUpgradeMoney(type, targetLevel - 1, targetLevel);

		if (costs <= planet.getMoney()) {
			planet.spendMoney(costs);
			starbase.upgrade(type);

			return true;
		}

		return false;
	}

	protected void buildShip(Starbase starbase, long playerId) {
		buildHull(starbase, playerId);

		if (!starbase.getHullStocks().isEmpty()) {
			StarbaseHullStock hullStock = starbase.getHullStocks().get(0);
			ShipTemplate shipTemplate = hullStock.getShipTemplate();

			Optional<StarbasePropulsionStock> propulsionStockResult = getPropulsionStock(starbase, shipTemplate);

			if (propulsionStockResult.isPresent()) {
				Long energyStockId = null;
				Long projectileStockId = null;

				String shipName = starbase.getLog();

				int maxWeaponLevel = getMaxWeaponTechLevel(shipName);

				if (shipTemplate.getEnergyWeaponsCount() > 0) {
					Optional<StarbaseWeaponStock> energyStock = getWeaponStock(starbase, shipTemplate.getEnergyWeaponsCount(), StarbaseUpgradeType.ENERGY,
							maxWeaponLevel);

					if (!energyStock.isPresent()) {
						return;
					}

					energyStockId = energyStock.get().getId();
				}

				if (shipTemplate.getProjectileWeaponsCount() > 0) {
					Optional<StarbaseWeaponStock> projectileStock = getWeaponStock(starbase, shipTemplate.getProjectileWeaponsCount(),
							StarbaseUpgradeType.PROJECTILE, maxWeaponLevel);

					if (!projectileStock.isPresent()) {
						return;
					}

					projectileStockId = projectileStock.get().getId();
				}

				StarbaseShipConstructionRequest request = new StarbaseShipConstructionRequest();
				request.setName(shipName);
				request.setHullStockId(hullStock.getId());
				request.setPropulsionSystemStockId(propulsionStockResult.get().getId());
				request.setEnergyStockId(energyStockId);
				request.setProjectileStockId(projectileStockId);
				starbase.setLog(null);

				starbaseService.addShipConstructionJobWithoutPermissionCheck(starbase.getId(), request);
			}
		}

		starbaseRepository.save(starbase);
	}

	protected int getMaxWeaponTechLevel(String shipName) {
		return shipName.contains(AI_FREIGHTER_NAME) || shipName.contains(AI_SUBPARTICLECLUSTER_NAME) || shipName.contains(AI_QUARKREORGANIZER_NAME) ? 1 : 10;
	}

	protected void buildHull(Starbase starbase, long playerId) {
		if (starbase.getHullStocks().isEmpty()) {
			List<Ship> ships = shipRepository.findByPlayerId(playerId);
			long gameId = starbase.getPlanet().getPlayer().getGame().getId();

			if (checkBuildFreighter(playerId, ships)) {
				buildFreighter(starbase);
			} else if (checkBuildScout(playerId, ships)) {
				buildScout(starbase);
			} else if (checkBuildSubParticleCluster(starbase)) {
				buildSubParticleCluster(starbase);
			} else if (checkBuildQuarkReorganizer(starbase)) {
				buildQuarkReorganizer(starbase);
			} else if (checkBuildBomber(gameId, playerId, ships)) {
				buildBomber(starbase);
			} else if (checkBuildAmbusher(gameId, playerId, ships)) {
				buildAmbusher(starbase);
			}
		}
	}

	protected int getFreighterScore(ShipTemplateScoringData data) {
		ShipTemplate s = data.getShipTemplate();

		if (s.getAbility(ShipAbilityType.QUARK_REORGANIZER).isPresent() || s.getAbility(ShipAbilityType.SUB_PARTICLE_CLUSTER).isPresent()) {
			return 0;
		}

		return s.getStorageSpace() - (s.getEnergyWeaponsCount() + s.getProjectileWeaponsCount() + s.getHangarCapacity()) * 10;
	}

	protected int getSubParticleClusterScore(ShipTemplateScoringData data) {
		ShipTemplate s = data.getShipTemplate();
		return s.getAbility(ShipAbilityType.SUB_PARTICLE_CLUSTER).isPresent() ? 1 : 0;
	}

	protected int getQuarkReorganizerScore(ShipTemplateScoringData data) {
		ShipTemplate s = data.getShipTemplate();
		return s.getAbility(ShipAbilityType.QUARK_REORGANIZER).isPresent() ? 1 : 0;
	}

	protected int getScoutScore(ShipTemplateScoringData data) {
		ShipTemplate s = data.getShipTemplate();

		if (s.getAbility(ShipAbilityType.QUARK_REORGANIZER).isPresent() || s.getAbility(ShipAbilityType.SUB_PARTICLE_CLUSTER).isPresent()) {
			return 0;
		}

		int score = s.getStorageSpace() > 0 ? 10 / s.getStorageSpace() : 20;
		score += 10 / s.getMass();
		score += s.getAbility(ShipAbilityType.ASTRO_PHYSICS_LAB).isPresent() ? 5 : 0;
		score += s.getAbility(ShipAbilityType.EXTENDED_SENSORS).isPresent() ? 5 : 0;
		score += s.getAbility(ShipAbilityType.JUMP_ENGINE).isPresent() ? 5 : 0;
		score += s.getAbility(ShipAbilityType.STRUCTUR_SCANNER).isPresent() ? 15 : 0;
		return score;
	}

	protected int getBomberScore(ShipTemplateScoringData data) {
		ShipTemplate s = data.getShipTemplate();

		if (s.getAbility(ShipAbilityType.QUARK_REORGANIZER).isPresent() || s.getAbility(ShipAbilityType.SUB_PARTICLE_CLUSTER).isPresent()) {
			return 0;
		}

		int score = s.getStorageSpace() > 0 ? 10 / s.getStorageSpace() : 20;
		score += (s.getEnergyWeaponsCount() + s.getProjectileWeaponsCount() + s.getHangarCapacity()) * 10;
		score += s.getAbility(ShipAbilityType.VIRAL_INVASION).isPresent() ? 60 : 0;
		score += s.getAbility(ShipAbilityType.DESTABILIZER).isPresent() ? 70 : 0;
		score += s.getAbility(ShipAbilityType.ORBITAL_SHIELD).isPresent() ? 20 : 0;
		return score;
	}

	protected int getAmbusherScore(ShipTemplateScoringData data) {
		ShipTemplate s = data.getShipTemplate();

		if (s.getAbility(ShipAbilityType.QUARK_REORGANIZER).isPresent() || s.getAbility(ShipAbilityType.SUB_PARTICLE_CLUSTER).isPresent()) {
			return 0;
		}

		int score = s.getStorageSpace() > 0 ? 10 / s.getStorageSpace() : 20;
		score += (s.getEnergyWeaponsCount() + s.getProjectileWeaponsCount() + s.getHangarCapacity()) * 10;
		score += s.getAbility(ShipAbilityType.CLOAKING_PERFECT).isPresent() ? 50 : 0;
		score += s.getAbility(ShipAbilityType.CLOAKING_RELIABLE).isPresent() ? 45 : 0;
		score += s.getAbility(ShipAbilityType.CLOAKING_UNRELIABLE).isPresent() ? 40 : 0;
		score += s.getAbility(ShipAbilityType.KAMIKAZE).isPresent() ? 40 : 0;
		score += s.getAbility(ShipAbilityType.LUCKY_SHOT).isPresent() ? 60 : 0;
		score += s.getAbility(ShipAbilityType.SHIELD_DAMPER).isPresent() ? 90 : 0;
		score += s.getAbility(ShipAbilityType.STRUCTUR_SCANNER).isPresent() ? 5 : 0;
		score += s.getAbility(ShipAbilityType.SIGNATURE_MASK).isPresent() ? 2 : 0;
		score += s.getAbility(ShipAbilityType.SUB_SPACE_DISTORTION).isPresent() ? 50 : 0;
		return score;
	}

	protected boolean checkBuildFreighter(long playerId, List<Ship> ships) {
		if (!ships.isEmpty()) {
			long colonizingFreighters = ships.stream().filter(s -> s.getName().contains(AI_FREIGHTER_NAME) && s.getCurrentRouteEntry() == null)
					.count();
			int planetsWithoutRoute = planetRepository.findOwnedPlanetIdsWithoutRoute(playerId).size();

			return planetsWithoutRoute > 2 && colonizingFreighters <= 1;
		}

		return true;
	}

	protected void buildFreighter(Starbase starbase) {
		Optional<ShipTemplate> shipTemplateOpt = getBestMatchingShipTemplate(starbase, s -> getFreighterScore(s));
		buildShip(starbase, shipTemplateOpt);
		starbase.setLog(AI_FREIGHTER_NAME);
	}

	protected boolean checkBuildScout(long playerId, List<Ship> ships) {
		long scoutCount = ships.stream().filter(s -> s.getName().contains(AI_SCOUT_NAME)).count();
		return scoutCount < 2;
	}

	protected void buildScout(Starbase starbase) {
		Optional<ShipTemplate> shipTemplateOpt = getBestMatchingShipTemplate(starbase, s -> getScoutScore(s));
		buildShip(starbase, shipTemplateOpt);
		starbase.setLog(AI_SCOUT_NAME);
	}

	protected boolean checkBuildBomber(long gameId, long playerId, List<Ship> ships) {
		Set<CoordinateImpl> visibilityCoordinates = visibleObjects.getVisibilityCoordinates(playerId);
		List<PlanetEntry> visiblePlanets = visibleObjects.getVisiblePlanetsCached(gameId, visibilityCoordinates, playerId);
		long planetCount = visiblePlanets.stream().filter(p -> p.getPlayerId() != null && p.getPlayerId() != playerId).count();

		if (planetCount == 0) {
			return false;
		}

		long bomberCount = ships.stream().filter(s -> s.getName().contains(AI_BOMBER_NAME)).count();

		if (bomberCount <= planetCount) {
			return true;
		}

		return bomberCount / planetCount < 6;
	}

	protected void buildBomber(Starbase starbase) {
		Optional<ShipTemplate> shipTemplateOpt = getBestMatchingShipTemplate(starbase, s -> getBomberScore(s));
		buildShip(starbase, shipTemplateOpt);
		starbase.setLog(AI_BOMBER_NAME);
	}

	protected boolean checkBuildAmbusher(long gameId, long playerId, List<Ship> ships) {
		Set<CoordinateImpl> visibilityCoordinates = visibleObjects.getVisibilityCoordinates(playerId);
		List<Ship> visibleShips = visibleObjects.getVisibleShipsCached(gameId, visibilityCoordinates, playerId, false);
		long shipCount = visibleShips.stream().filter(s -> s.getPlayer() != null && s.getPlayer().getId() != playerId).count();

		if (shipCount == 0) {
			return false;
		}

		long ambusherCount = ships.stream().filter(s -> s.getName().contains(AI_AMBUSHER_NAME)).count();

		if (ambusherCount <= shipCount) {
			return true;
		}

		return ambusherCount / shipCount < 2;
	}

	protected void buildAmbusher(Starbase starbase) {
		Optional<ShipTemplate> shipTemplateOpt = getBestMatchingShipTemplate(starbase, s -> getAmbusherScore(s));
		buildShip(starbase, shipTemplateOpt);
		starbase.setLog(AI_AMBUSHER_NAME);
	}

	protected boolean checkBuildSubParticleCluster(Starbase starbase) {
		if (starbase.getHullLevel() < 10) {
			return false;
		}

		Planet planet = starbase.getPlanet();
		long gameId = planet.getGame().getId();

		List<Ship> ships = shipRepository.findByGameIdAndPlanetId(gameId, planet.getId());
		return !ships.stream().anyMatch(s -> s.getAbility(ShipAbilityType.SUB_PARTICLE_CLUSTER).isPresent());
	}

	protected boolean checkBuildQuarkReorganizer(Starbase starbase) {
		if (starbase.getHullLevel() < 9) {
			return false;
		}

		Planet planet = starbase.getPlanet();
		long gameId = planet.getGame().getId();

		List<Ship> ships = shipRepository.findByGameIdAndPlanetId(gameId, planet.getId());
		return !ships.stream().anyMatch(s -> s.getAbility(ShipAbilityType.QUARK_REORGANIZER).isPresent());
	}

	protected void buildSubParticleCluster(Starbase starbase) {
		Optional<ShipTemplate> shipTemplateOpt = getBestMatchingShipTemplate(starbase, s -> getSubParticleClusterScore(s));
		buildShip(starbase, shipTemplateOpt);
		starbase.setLog(AI_SUBPARTICLECLUSTER_NAME);
	}

	protected void buildQuarkReorganizer(Starbase starbase) {
		Optional<ShipTemplate> shipTemplateOpt = getBestMatchingShipTemplate(starbase, s -> getQuarkReorganizerScore(s));
		buildShip(starbase, shipTemplateOpt);
		starbase.setLog(AI_QUARKREORGANIZER_NAME);
	}

	protected void buildShip(Starbase starbase, Optional<ShipTemplate> shipTemplateOpt) {
		if (shipTemplateOpt.isPresent()) {
			ShipTemplate shipTemplate = shipTemplateOpt.get();

			if (starbase.canBeProduced(shipTemplate, 1)) {
				StarbaseProductionRequest request = new StarbaseProductionRequest();
				request.setQuantity(1);
				String shipTemplateId = shipTemplate.getId();
				request.setTemplateId(shipTemplateId);
				request.setType(StarbaseUpgradeType.HULL.name());
				starbaseService.produceWithoutPermissionCheck(request, starbase.getId());
			}
		}
	}

	protected Optional<StarbasePropulsionStock> getPropulsionStock(Starbase starbase, ShipTemplate template) {
		int propulsionSystemsCount = template.getPropulsionSystemsCount();

		Optional<StarbasePropulsionStock> propulsionStockResult = getPropulsionSystemStock(starbase, propulsionSystemsCount);

		if (!propulsionStockResult.isPresent()) {
			Optional<PropulsionSystemTemplate> propulsionSystemResult = propulsionSystemTemplateRepository.findAll().stream()
					.filter(p -> p.getTechLevel() <= starbase.getPropulsionLevel() && starbase.canBeProduced(p, propulsionSystemsCount))
					.sorted(PRODUCABLE_TECHLEVEL_COMPARATOR)
					.findFirst();

			if (propulsionSystemResult.isPresent()) {
				PropulsionSystemTemplate propulsionSystemTemplate = propulsionSystemResult.get();

				StarbaseProductionRequest request = new StarbaseProductionRequest();
				request.setTemplateId(propulsionSystemTemplate.getName());
				request.setType(StarbaseUpgradeType.PROPULSION.name());
				request.setQuantity(propulsionSystemsCount);
				starbaseService.produceWithoutPermissionCheck(request, starbase.getId());

				propulsionStockResult = getPropulsionSystemStock(starbase, propulsionSystemsCount);
			}
		}

		return propulsionStockResult;
	}

	protected Optional<StarbaseWeaponStock> getWeaponStock(Starbase starbase, int weaponCount, StarbaseUpgradeType type, int maxLevel) {
		boolean projectiles = type == StarbaseUpgradeType.PROJECTILE;

		Optional<StarbaseWeaponStock> weaponStockResult = getWeaponStock(starbase, weaponCount, projectiles);

		if (!weaponStockResult.isPresent()) {
			int starbaseWeaponLevel = projectiles ? starbase.getProjectileLevel() : starbase.getEnergyLevel();

			Optional<WeaponTemplate> weaponTemplateResult = weaponTemplateRepository.findAll().stream()
					.filter(p -> p.getTechLevel() <= starbaseWeaponLevel && starbase.canBeProduced(p, weaponCount) && p.getTechLevel() <= maxLevel &&
							p.isUsesProjectiles() == projectiles)
					.sorted(PRODUCABLE_TECHLEVEL_COMPARATOR)
					.findFirst();

			if (weaponTemplateResult.isPresent()) {
				WeaponTemplate weaponTemplate = weaponTemplateResult.get();

				StarbaseProductionRequest request = new StarbaseProductionRequest();
				request.setTemplateId(weaponTemplate.getName());
				request.setType(type.name());
				request.setQuantity(weaponCount);
				starbaseService.produceWithoutPermissionCheck(request, starbase.getId());

				weaponStockResult = getWeaponStock(starbase, weaponCount, projectiles);
			}
		}

		return weaponStockResult;
	}

	protected Stream<ShipTemplate> getPossibleShipTemplates(Starbase starbase, List<ShipTemplate> shipTemplates) {
		return shipTemplates.stream()
				.filter(s -> s.getTechLevel() <= starbase.getHullLevel())
				.sorted();
	}

	protected Optional<ShipTemplate> getBestMatchingShipTemplate(Starbase starbase, Function<ShipTemplateScoringData, Integer> scoreProvider) {
		Player player = starbase.getPlanet().getPlayer();
		Faction faction = player.getFaction();

		List<ShipTemplate> factionTemplates = faction.getShips();
		List<ShipTemplate> allTemplates = new ArrayList<>(factionTemplates);

		if (faction.getId().equals("kuatoh")) {
			List<AquiredShipTemplate> aquired = aquiredShipTemplateRepository.findByGameId(player.getGame().getId());
			allTemplates.addAll(aquired.stream().map(AquiredShipTemplate::getShipTemplate).collect(Collectors.toList()));
		}

		allTemplates = getPossibleShipTemplates(starbase, allTemplates).collect(Collectors.toList());

		Optional<ShipTemplateScore> resultOpt = allTemplates.stream()
				.map(s -> new ShipTemplateScoringData(s, player, starbase))
				.map(s -> new ShipTemplateScore(s, scoreProvider))
				.max(Comparator.naturalOrder());

		if (resultOpt.isPresent()) {
			ShipTemplateScore result = resultOpt.get();

			if (result.getScore() > 0) {
				return Optional.of(result.getShipTemplate());
			}
		}

		return Optional.empty();
	}

	protected Optional<StarbasePropulsionStock> getPropulsionSystemStock(Starbase starbase, int stock) {
		return starbase.getPropulsionStocks().stream()
				.filter(p -> p.getStock() >= stock)
				.max((a, b) -> PRODUCABLE_TECHLEVEL_COMPARATOR.compare(a.getPropulsionSystemTemplate(), b.getPropulsionSystemTemplate()));
	}

	protected Optional<StarbaseWeaponStock> getWeaponStock(Starbase starbase, int stock, boolean projectiles) {
		return starbase.getWeaponStocks().stream()
				.filter(p -> p.getStock() >= stock && p.getWeaponTemplate().isUsesProjectiles() == projectiles)
				.max((a, b) -> PRODUCABLE_TECHLEVEL_COMPARATOR.compare(a.getWeaponTemplate(), b.getWeaponTemplate()));
	}
}