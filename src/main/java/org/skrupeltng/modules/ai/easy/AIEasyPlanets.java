package org.skrupeltng.modules.ai.easy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ingame.modules.orbitalsystem.service.OrbitalSystemService;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.service.PlanetService;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_EASY")
public class AIEasyPlanets {

	@Autowired
	protected PlanetRepository planetRepository;

	@Autowired
	protected PlanetService planetService;

	@Autowired
	protected StarbaseRepository starbaseRepository;

	@Autowired
	protected ShipRouteEntryRepository shipRouteEntryRepository;

	@Autowired
	protected ShipRepository shipRepository;

	@Autowired
	protected OrbitalSystemService orbitalSystemService;

	public void processPlanets(long playerId) {
		List<Planet> planets = planetRepository.findByPlayerId(playerId);

		for (Planet planet : planets) {
			processPlanet(planet, planets, playerId);
		}

		List<Starbase> starbases = starbaseRepository.findByPlayerId(playerId);

		if (checkNewStarbaseNecessary(planets, starbases)) {
			tryToBuildNewStarbase(planets, starbases, playerId);
		}
	}

	protected Planet processPlanet(Planet planet, List<Planet> planets, long playerId) {
		planet = buildFactories(planet);
		planet = buildMines(planet);
		return buildOrbitalSystems(planet);
	}

	protected Planet buildMines(Planet planet) {
		int minesToBeBuild = planet.retrieveMaxMines() - planet.getMines();

		if (minesToBeBuild > 0) {
			planet = planetService.buildMinesWithoutPermissionCheck(planet.getId(), minesToBeBuild);
		}

		return planet;
	}

	protected Planet buildFactories(Planet planet) {
		int factoriesToBeBuild = planet.retrieveMaxFactories() - planet.getFactories();

		if (factoriesToBeBuild > 0) {
			planet = planetService.buildFactoriesWithoutPermissionCheck(planet.getId(), factoriesToBeBuild);
		}

		return planet;
	}

	protected boolean checkNewStarbaseNecessary(List<Planet> planets, List<Starbase> starbases) {
		boolean starbasePrepared = planets.stream().anyMatch(p -> p.getLog() != null);

		if (starbasePrepared) {
			return true;
		}

		int starbaseCount = starbases.size();
		int planetCount = planets.size();
		return planetCount / 5 > starbaseCount;
	}

	protected void tryToBuildNewStarbase(List<Planet> planets, List<Starbase> starbases, long playerId) {
		Optional<Planet> result = findStarbaseCandidate(planets, starbases);

		if (result.isPresent()) {
			Planet planet = result.get();
			long planetId = planet.getId();

			shipRepository.clearCurrentRouteEntryByRoutePlanetId(planetId);
			shipRouteEntryRepository.deleteByPlanetIdAndPlayerId(planetId, playerId);

			if (!planet.canConstructStarbase(StarbaseType.STAR_BASE)) {
				planet.setLog(StarbaseType.STAR_BASE.name());
				planetRepository.save(planet);
			} else {
				buildStarbase(starbases, planet, playerId);
			}
		}
	}

	protected void buildStarbase(List<Starbase> starbases, Planet planet, long playerId) {
		planet.setLog(null);
		planet = planetRepository.save(planet);
		planetService.constructStarbaseWithoutPermissionCheck(planet.getId(), StarbaseType.STAR_BASE, "Starbase_" + (starbases.size() + 1));
	}

	protected Optional<Planet> findStarbaseCandidate(List<Planet> planets, List<Starbase> starbases) {
		Optional<Planet> preparedOpt = planets.stream().filter(p -> p.getLog() != null).findAny();

		if (preparedOpt.isPresent()) {
			return preparedOpt;
		}

		List<Planet> candidates = planets.stream().filter(p -> p.getStarbase() == null).sorted(Comparator.comparing(Planet::getColonists).reversed())
				.collect(Collectors.toList());

		int maxChecks = Math.min(candidates.size(), 3);
		Planet candidate = null;

		for (int i = 0; i < maxChecks; i++) {
			Planet planet = candidates.get(i);

			if (!starbases.isEmpty()) {
				double minDist = Long.MAX_VALUE;

				for (Starbase starbase : starbases) {
					double dist = CoordHelper.getDistance(planet, starbase);

					if (dist < minDist) {
						minDist = dist;
					}
				}

				if (minDist < 250) {
					continue;
				}
			}

			candidate = planet;
			break;
		}

		return Optional.ofNullable(candidate);
	}

	protected Planet buildOrbitalSystems(Planet planet) {
		List<OrbitalSystem> orbitalSystems = new ArrayList<>(planet.getOrbitalSystems());

		for (OrbitalSystem orbitalSystem : orbitalSystems) {
			planet = tryToBuildOrbitalSystem(planet, orbitalSystem, OrbitalSystemType.MEGA_FACTORY);
			planet = tryToBuildOrbitalSystem(planet, orbitalSystem, OrbitalSystemType.METROPOLIS);
			planet = tryToBuildOrbitalSystem(planet, orbitalSystem, OrbitalSystemType.RECREATIONAL_PARK);
			planet = tryToBuildOrbitalSystem(planet, orbitalSystem, OrbitalSystemType.PLANETARY_GUN_BATTERY);
			planet = tryToBuildOrbitalSystem(planet, orbitalSystem, OrbitalSystemType.BANK);
			planet = tryToBuildOrbitalSystem(planet, orbitalSystem, OrbitalSystemType.PLANETARY_SHIELDS);
		}

		return planet;
	}

	protected Planet tryToBuildOrbitalSystem(Planet planet, OrbitalSystem orbitalSystem, OrbitalSystemType type) {
		if (orbitalSystem.getType() == null && !planet.hasOrbitalSystem(type) && type.canBeBuild(planet) &&
				planet.getPlayer().getFaction().orbitalSystemAllowed(type)) {
			return orbitalSystemService.constructWithoutPermissionsCheck(type, orbitalSystem);
		}

		return planet;
	}
}
