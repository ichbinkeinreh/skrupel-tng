package org.skrupeltng.modules.ai.easy;

import java.util.List;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequest;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_EASY")
public class AIEasyPolitics {

	@Autowired
	protected PlayerRepository playerRepository;

	@Autowired
	protected PlayerRelationRepository playerRelationRepository;

	@Autowired
	protected PlayerRelationRequestRepository playerRelationRequestRepository;

	public void processPolitics(long playerId) {
		reactToRelationRequests(playerId);
	}

	protected void reactToRelationRequests(long playerId) {
		List<PlayerRelationRequest> requests = playerRelationRequestRepository.findIncomingRequests(playerId);

		for (PlayerRelationRequest request : requests) {
			reactToRelationRequest(request);
		}
	}

	protected void reactToRelationRequest(PlayerRelationRequest request) {
		acceptRelationRequest(request);
	}

	protected void acceptRelationRequest(PlayerRelationRequest request) {
		Player requestingPlayer = request.getRequestingPlayer();
		Player otherPlayer = request.getOtherPlayer();

		List<PlayerRelation> exitingRelation = playerRelationRepository.findByPlayerIds(requestingPlayer.getId(), otherPlayer.getId());

		if (!exitingRelation.isEmpty()) {
			return;
		}

		PlayerRelation relation = new PlayerRelation();
		relation.setPlayer1(requestingPlayer);
		relation.setPlayer2(otherPlayer);
		relation.setType(request.getType());
		playerRelationRepository.save(relation);

		playerRelationRequestRepository.delete(request);
	}
}
