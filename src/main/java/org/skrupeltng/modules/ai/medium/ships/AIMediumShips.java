package org.skrupeltng.modules.ai.medium.ships;

import java.util.List;

import org.skrupeltng.modules.ai.easy.ships.AIEasyBombers;
import org.skrupeltng.modules.ai.easy.ships.AIEasyFreighters;
import org.skrupeltng.modules.ai.easy.ships.AIEasyScouts;
import org.skrupeltng.modules.ai.easy.ships.AIEasyShips;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFoldRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.starbase.controller.SpaceFoldRequest;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_MEDIUM")
public class AIMediumShips extends AIEasyShips {

	@Autowired
	protected StarbaseRepository starbaseRepository;

	@Autowired
	protected SpaceFoldRepository spaceFoldRepository;

	@Autowired
	protected StarbaseService starbaseService;

	@Autowired
	@Qualifier("AI_MEDIUM")
	private AIMediumFreighters freighters;

	@Autowired
	@Qualifier("AI_MEDIUM")
	private AIMediumScouts scouts;

	@Autowired
	@Qualifier("AI_MEDIUM")
	private AIMediumBombers bombers;

	@Override
	protected AIEasyFreighters getFreighters() {
		return freighters;
	}

	@Override
	protected AIEasyScouts getScouts() {
		return scouts;
	}

	@Override
	protected AIEasyBombers getBombers() {
		return bombers;
	}

	@Override
	protected void processShip(long playerId, List<Ship> allPlayerShips, long gameId, List<Planet> targetPlanets, List<Planet> visiblesPlanets,
			List<Planet> ownedPlanets,
			List<Planet> highPopulationPlanets, Ship ship) {
		super.processShip(playerId, allPlayerShips, gameId, targetPlanets, visiblesPlanets, ownedPlanets, highPopulationPlanets, ship);

		if (checkStrandedShipInSpace(ship)) {
			sendSpaceFoldToStrandedShip(ship);
		}
	}

	protected boolean checkStrandedShipInSpace(Ship ship) {
		return ship.getPlanet() == null && ship.getTravelSpeed() == 0 && spaceFoldRepository.findByShipId(ship.getId()).isEmpty();
	}

	protected void sendSpaceFoldToStrandedShip(Ship ship) {
		long playerId = ship.getPlayer().getId();
		List<Starbase> starbases = starbaseRepository.findByPlayerId(playerId);

		for (Starbase starbase : starbases) {
			if (starbase.canSendSpaceFolds()) {
				Planet planet = starbase.getPlanet();

				int fuel = Math.min(40, ship.getShipTemplate().getFuelCapacity());

				if (planet.getFuel() >= fuel && planet.getMoney() >= 75 + (8 * fuel)) {
					SpaceFoldRequest request = new SpaceFoldRequest();
					request.setShipId(ship.getId());
					request.setFuel(fuel);

					starbaseService.initSpaceFoldWithoutPermissionCheck(starbase.getId(), request);
					break;
				}
			}
		}
	}
}