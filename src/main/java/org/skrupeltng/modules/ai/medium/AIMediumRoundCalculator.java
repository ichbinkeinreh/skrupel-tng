package org.skrupeltng.modules.ai.medium;

import org.skrupeltng.modules.ai.AIRoundCalculator;
import org.skrupeltng.modules.ai.medium.ships.AIMediumShips;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component("AI_MEDIUM")
public class AIMediumRoundCalculator implements AIRoundCalculator {

	@Autowired
	@Qualifier("AI_MEDIUM")
	private AIMediumShips ships;

	@Autowired
	@Qualifier("AI_MEDIUM")
	private AIMediumPlanets planets;

	@Autowired
	@Qualifier("AI_MEDIUM")
	private AIMediumStarbases starbases;

	@Autowired
	@Qualifier("AI_MEDIUM")
	private AIMediumPolitics politics;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void calculateRound(long playerId) {
		ships.processShips(playerId);
		planets.processPlanets(playerId);
		starbases.processStarbases(playerId);
		politics.processPolitics(playerId);
	}
}