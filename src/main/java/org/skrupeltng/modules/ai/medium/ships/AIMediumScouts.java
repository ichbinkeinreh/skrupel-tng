package org.skrupeltng.modules.ai.medium.ships;

import java.util.List;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ai.easy.ships.AIEasyScouts;
import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_MEDIUM")
public class AIMediumScouts extends AIEasyScouts {

	@Override
	protected int getNecessaryFuel(Ship ship) {
		return Math.min(ship.getShipTemplate().getFuelCapacity() / 2, 50);
	}

	@Override
	protected void processScout(long gameId, long playerId, Ship ship, List<Planet> targetPlanets) {
		ship = fillTank(ship, ship.getPlanet());

		if (ship.getFuel() < getNecessaryFuel(ship) && ship.getPlanet() == null &&
				CoordHelper.getDistance(ship, new CoordinateImpl(ship.getDestinationX(), ship.getDestinationY(), 0)) > 70) {
			List<Planet> nearPlanets = targetPlanets.stream().sorted(new CoordinateDistanceComparator(ship)).limit(5).collect(Collectors.toList());
			int nearPlanetCount = nearPlanets.size();

			if (nearPlanetCount > 0) {
				Planet nextTarget = nearPlanets.get(MasterDataService.RANDOM.nextInt(nearPlanetCount));
				ship.setDestinationX(nextTarget.getX());
				ship.setDestinationY(nextTarget.getY());
				ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
				shipRepository.save(ship);
			}
		} else {
			super.processScout(gameId, playerId, ship, targetPlanets);
		}
	}
}