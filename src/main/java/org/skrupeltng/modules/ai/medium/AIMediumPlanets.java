package org.skrupeltng.modules.ai.medium;

import java.util.List;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ai.easy.AIEasyPlanets;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_MEDIUM")
public class AIMediumPlanets extends AIEasyPlanets {

	@Autowired
	protected ShipRepository shipRepository;

	@Override
	protected Planet processPlanet(Planet planet, List<Planet> planets, long playerId) {
		planet = super.processPlanet(planet, planets, playerId);
		return buildDefense(planet);
	}

	protected Planet buildDefense(Planet planet) {
		int defenseToBeBuild = planet.retrieveMaxPlanetaryDefense() - planet.getPlanetaryDefense();

		if (defenseToBeBuild > 0) {
			planet = planetService.buildPlanetaryDefenseWithoutPermissionCheck(planet.getId(), defenseToBeBuild);
		}

		return planet;
	}

	@Override
	protected void buildStarbase(List<Starbase> starbases, Planet planet, long playerId) {
		List<ShipRouteEntry> routeEntries = shipRouteEntryRepository.findByPlayerId(playerId);

		List<Long> shipIds = routeEntries.stream().filter(s -> CoordHelper.getDistance(planet, s.getPlanet()) < 200).map(s -> s.getShip().getId())
				.collect(Collectors.toList());

		if (!shipIds.isEmpty()) {
			shipRepository.clearCurrentRouteEntry(shipIds);
			shipRouteEntryRepository.deleteByShipIds(shipIds);
		}

		super.buildStarbase(starbases, planet, playerId);
	}
}