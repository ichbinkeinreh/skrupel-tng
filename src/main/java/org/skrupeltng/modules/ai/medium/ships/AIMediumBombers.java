package org.skrupeltng.modules.ai.medium.ships;

import java.util.List;
import java.util.Optional;

import org.skrupeltng.modules.ai.AIConstants;
import org.skrupeltng.modules.ai.easy.ships.AIEasyBombers;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_MEDIUM")
public class AIMediumBombers extends AIEasyBombers {

	@Override
	protected Optional<Planet> setTargetPlanet(long playerId, Ship ship, List<Planet> targetPlanets, List<Planet> visiblePlanets) {
		if (ship.getDestinationShip() != null && ship.getDestinationShip().getPlayer().getId() == playerId) {
			return Optional.empty();
		}

		Planet planet = ship.getPlanet();

		if (planet != null && planet.getPlayer() != null && planet.getPlayer().getId() == playerId && planet.getStarbase() != null) {
			List<Ship> ships = shipRepository.findByGameIdAndPlanetId(planet.getGame().getId(), planet.getId());

			long otherBombers = ships.stream().filter(s -> s.getId() != ship.getId() && s.getName().contains(AIConstants.AI_BOMBER_NAME)).count();

			if (otherBombers == 0L) {
				ship.setLog(AIConstants.AI_BOMBER_NAME);
				ship.resetTravel();
				return Optional.empty();
			}

			if (otherBombers < 4) {
				ship.resetTravel();

				if (ship.getLog() == null) {
					Optional<Ship> mainShipOpt = ships.stream().filter(s -> AIConstants.AI_BOMBER_NAME.equals(s.getLog())).findAny();

					if (mainShipOpt.isPresent()) {
						Ship mainShip = mainShipOpt.get();
						ship.setDestinationShip(mainShip);
						ship.setDestinationX(mainShip.getX());
						ship.setDestinationY(mainShip.getY());
						ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
					}
				}

				return Optional.empty();
			}
		}

		return super.setTargetPlanet(playerId, ship, targetPlanets, visiblePlanets);
	}
}