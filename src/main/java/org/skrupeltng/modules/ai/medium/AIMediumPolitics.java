package org.skrupeltng.modules.ai.medium;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ai.easy.AIEasyPolitics;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.overview.service.PlayerSummaryEntry;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequest;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_MEDIUM")
public class AIMediumPolitics extends AIEasyPolitics {

	@Autowired
	protected GameRepository gameRepository;

	@Override
	public void processPolitics(long playerId) {
		super.processPolitics(playerId);

		int round = gameRepository.getRoundByPlayerId(playerId);

		if (round > 15) {
			Player currentPlayer = playerRepository.getOne(playerId);
			makeNewRequests(playerId, currentPlayer);
		}
	}

	@Override
	protected void reactToRelationRequest(PlayerRelationRequest request) {
		if (MasterDataService.RANDOM.nextBoolean()) {
			acceptRelationRequest(request);
		} else {
			rejectRelationRequest(request);
		}
	}

	protected void rejectRelationRequest(PlayerRelationRequest request) {
		playerRelationRequestRepository.delete(request);
	}

	protected void makeNewRequests(long playerId, Player currentPlayer) {
		Game game = currentPlayer.getGame();
		List<Player> players = game.getPlayers();

		Map<Long, PlayerSummaryEntry> playerSummaryMap = playerRepository.getSummaries(game.getId()).stream()
				.collect(Collectors.toMap(PlayerSummaryEntry::getPlayerId, p -> p));

		PlayerSummaryEntry currentPlayerSummary = playerSummaryMap.get(playerId);
		int currentPlayerRank = currentPlayerSummary.getRank();

		Set<Long> alreadyRequestedPlayerIds = playerRelationRequestRepository.findOutgoingRequests(playerId);

		List<PlayerRelation> exitingRelations = playerRelationRepository.findByPlayerId(playerId);
		Set<Long> playerIdsWithRelation = new HashSet<>();

		for (PlayerRelation playerRelation : exitingRelations) {
			if (playerId == playerRelation.getPlayer1().getId()) {
				playerIdsWithRelation.add(playerRelation.getPlayer2().getId());
			} else {
				playerIdsWithRelation.add(playerRelation.getPlayer1().getId());
			}
		}

		for (Player player : players) {
			if (player == currentPlayer || alreadyRequestedPlayerIds.contains(player.getId()) || playerIdsWithRelation.contains(player.getId())) {
				continue;
			}

			PlayerSummaryEntry summary = playerSummaryMap.get(player.getId());
			int rank = summary.getRank();

			if (Math.abs(currentPlayerRank - rank) <= 1 && MasterDataService.RANDOM.nextInt(100) <= 10) {
				PlayerRelationRequest request = new PlayerRelationRequest();
				request.setRequestingPlayer(currentPlayer);
				request.setOtherPlayer(player);

				PlayerRelationType type = MasterDataService.RANDOM.nextBoolean() ? PlayerRelationType.ALLIANCE : PlayerRelationType.NON_AGGRESSION_TREATY;
				request.setType(type);
				playerRelationRequestRepository.save(request);
			} else if (currentPlayerRank > rank && MasterDataService.RANDOM.nextInt(100) <= 10) {
				PlayerRelation request = new PlayerRelation();
				request.setPlayer1(currentPlayer);
				request.setPlayer2(player);
				request.setType(PlayerRelationType.WAR);
				playerRelationRepository.save(request);
			}
		}
	}
}
