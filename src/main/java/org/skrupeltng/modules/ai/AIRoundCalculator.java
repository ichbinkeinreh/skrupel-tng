package org.skrupeltng.modules.ai;

public interface AIRoundCalculator {

	void calculateRound(long playerId);
}