package org.skrupeltng.modules.ai;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;

public class ShipTemplateScoringData {

	private final ShipTemplate shipTemplate;
	private final Player player;
	private final Starbase starbase;

	public ShipTemplateScoringData(ShipTemplate shipTemplate, Player player, Starbase starbase) {
		this.shipTemplate = shipTemplate;
		this.player = player;
		this.starbase = starbase;
	}

	public ShipTemplate getShipTemplate() {
		return shipTemplate;
	}

	public Player getPlayer() {
		return player;
	}

	public Starbase getStarbase() {
		return starbase;
	}
}