package org.skrupeltng.modules;

public class SortFieldItem {

	private SortField sortField;
	private boolean ascending;

	public SortFieldItem(SortField sortField, boolean ascending) {
		this.sortField = sortField;
		this.ascending = ascending;
	}

	public SortField getSortField() {
		return sortField;
	}

	public void setSortField(SortField sortField) {
		this.sortField = sortField;
	}

	public boolean isAscending() {
		return ascending;
	}

	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}
}