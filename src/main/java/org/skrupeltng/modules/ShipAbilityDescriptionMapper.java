package org.skrupeltng.modules;

import java.util.Locale;
import java.util.Map.Entry;

import org.skrupeltng.modules.ingame.modules.ship.controller.ShipAbilityDescription;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class ShipAbilityDescriptionMapper {

	@Autowired
	private MessageSource messageSource;

	public ShipAbilityDescription mapShipAbilityDescription(ShipAbility ability) {
		Locale locale = LocaleContextHolder.getLocale();
		String abilityName = ability.getType().name();
		String name = messageSource.getMessage("ship_ability_" + abilityName, null, abilityName, locale);
		String description = messageSource.getMessage("ship_ability_description_" + abilityName, null, abilityName, locale);

		for (Entry<String, String> entry : ability.getValues().entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			description = description.replace("{" + key + "}", value);
		}

		return new ShipAbilityDescription(ability.getId(), name, description);
	}
}