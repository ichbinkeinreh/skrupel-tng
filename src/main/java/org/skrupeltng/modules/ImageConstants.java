package org.skrupeltng.modules;

public interface ImageConstants {

	String news = "/images/news/";
	String news_pirates = news + "piraten.jpg";
	String news_epidemie = news + "epedemie.jpg";
	String news_explode = news + "star_explode.jpg";
	String news_message = news + "subfunk.jpg";
	String news_mine_field = news + "minenfeld.jpg";
}