package org.skrupeltng.modules.mail.service;

import java.util.List;
import java.util.Locale;

import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.skrupeltng.modules.dashboard.database.InstallationDetailsRepository;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.IContext;

@Service("mailService")
public class MailService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired(required = false)
	private JavaMailSender javaMailSender;

	@Autowired
	private TemplateEngine templateEngine;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private InstallationDetailsRepository installationDetailsRepository;

	public boolean mailsEnabled() {
		return javaMailSender != null;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void sendAccountActivationMail(Login login) {
		Locale locale = LocaleContextHolder.getLocale();

		Context context = new Context(locale);
		context.setVariable("login", login);

		String baseUrl = createBaseUrl();
		context.setVariable("baseUrl", baseUrl);

		String subject = messageSource.getMessage("email_subject_account_activation", null, locale);

		sendMail(login.getEmail(), subject, MailConstants.account_activation, context);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void sendPlayerJoinedGameNotification(Login newPlayer, Game game) {
		Login login = game.getCreator();
		Locale locale = Locale.forLanguageTag(login.getLanguage());

		Context context = new Context(locale);
		context.setVariable("login", login);
		context.setVariable("newPlayer", newPlayer);
		context.setVariable("game", game);

		String baseUrl = createBaseUrl();
		context.setVariable("link", baseUrl + "/game?id=" + game.getId());

		String subject = messageSource.getMessage("email_subject_player_joined_notification", null, locale);

		sendMail(login.getEmail(), subject, MailConstants.player_joined_notification, context);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void sendGameFullNotification(Game game) {
		Login login = game.getCreator();
		Locale locale = Locale.forLanguageTag(login.getLanguage());

		Context context = new Context(locale);
		context.setVariable("login", login);
		context.setVariable("game", game);

		String baseUrl = createBaseUrl();
		context.setVariable("link", baseUrl + "/game?id=" + game.getId());

		Object[] args = { game.getName() };
		String subject = messageSource.getMessage("email_subject_game_full_notification", args, locale);

		sendMail(login.getEmail(), subject, MailConstants.game_full_notification, context);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void sendGameStartedNotificationEmails(long gameId, Long triggeringLoginId) {
		sendGameNotificationEmails(gameId, triggeringLoginId, "email_subject_game_started_notification", MailConstants.game_started_notification);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void sendRoundNotificationEmails(long gameId, Long triggeringLoginId) {
		sendGameNotificationEmails(gameId, triggeringLoginId, "email_subject_round_notification", MailConstants.round_notification);
	}

	protected void sendGameNotificationEmails(long gameId, Long triggeringLoginId, String subject, String template) {
		List<Player> players = playerRepository.findByGameId(gameId);

		for (Player player : players) {
			Login login = player.getLogin();

			if (login.isRoundNotificationsEnabled() && !player.isHasLost() &&
					(triggeringLoginId == null || login.getId() != triggeringLoginId && StringUtils.isNotBlank(login.getEmail()))) {
				sendGameNotificationMail(player.getId(), subject, template);
			}
		}
	}

	protected void sendGameNotificationMail(long playerId, String subject, String template) {
		Player player = playerRepository.getOne(playerId);
		Login login = player.getLogin();
		Game game = player.getGame();
		Locale locale = Locale.forLanguageTag(login.getLanguage());

		Context context = new Context(locale);
		context.setVariable("login", login);
		context.setVariable("game", game);

		String baseUrl = createBaseUrl();
		context.setVariable("baseUrl", baseUrl);

		Object[] args = { game.getName() };
		String subjectText = messageSource.getMessage(subject, args, locale);

		sendMail(login.getEmail(), subjectText, template, context);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void sendPasswordRecoveryMail(Login login) {
		Locale locale = Locale.forLanguageTag(login.getLanguage());

		Context context = new Context(locale);
		context.setVariable("login", login);

		String baseUrl = createBaseUrl();
		context.setVariable("baseUrl", baseUrl);

		String subject = messageSource.getMessage("email_subject_password_recovery", null, locale);

		sendMail(login.getEmail(), subject, MailConstants.password_recovery, context);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void sendPasswordRecoveryFailMail(String email) {
		Locale locale = LocaleContextHolder.getLocale();

		Context context = new Context(locale);
		context.setVariable("email", email);

		String baseUrl = createBaseUrl();
		context.setVariable("baseUrl", baseUrl);

		String subject = messageSource.getMessage("email_subject_password_recovery_fail", null, locale);

		sendMail(email, subject, MailConstants.password_recovery_fail, context);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void sendRoundErrorEmail(long gameId, Exception exception) {
		String contactEmail = installationDetailsRepository.getContactEmail();

		if (StringUtils.isNotBlank(contactEmail)) {
			Locale locale = Locale.ENGLISH;
			String stackTrace = ExceptionUtils.getStackTrace(exception);

			Context context = new Context(locale);
			context.setVariable("gameId", gameId);
			context.setVariable("stackTrace", stackTrace);

			sendMail(contactEmail, "Error while calculating round for game " + gameId, MailConstants.round_error, context);
		} else {
			log.warn("Unable to send round error email because no contactEmail set!");
		}
	}

	private String createBaseUrl() {
		return installationDetailsRepository.getDomainUrl();
	}

	private void sendMail(String to, String subject, String template, IContext context) {
		try {
			String mailContent = templateEngine.process("emails/" + template, context);

			MimeMessage mail = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mail, true);
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(mailContent, true);

			javaMailSender.send(mail);
		} catch (Exception e) {
			log.error("Error while sending email: ", e);
		}
	}
}