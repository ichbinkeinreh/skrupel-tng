package org.skrupeltng.modules;

public interface PageableResult {

	int getTotalElements();
}