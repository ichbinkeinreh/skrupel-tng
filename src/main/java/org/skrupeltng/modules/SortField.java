package org.skrupeltng.modules;

public enum SortField {

	EXISTING_GAMES_NAME("g.name", "name"),

	EXISTING_GAMES_GAME_MODE("g.win_condition", "win_condition"),

	EXISTING_GAMES_CREATED("g.created", "created"),

	EXISTING_GAMES_STATUS("g.started", "status"),

	EXISTING_GAMES_PLAYERCOUNT("g.player_count", "player_count"),

	EXISTING_GAMES_ROUND("g.round", "round"),

	USERS_USERNAME("l.username", "username"),

	USERS_GAMES_CREATED("\"gamesCreated\"", "games_created"),

	USERS_GAMES_PLAYED("\"gamesPlayed\"", "games_played"),

	USERS_GAMES_WON("\"gamesWon\"", "games_won"),

	USERS_GAMES_LOST("\"gamesLost\"", "games_lost"),

	COLONY_OVERVIEW_NAME("p.name", ""),

	COLONY_OVERVIEW_COLONISTS("p.colonists", ""),

	COLONY_OVERVIEW_MONEY("p.money", ""),

	COLONY_OVERVIEW_SUPPLIES("p.supplies", ""),

	COLONY_OVERVIEW_FUEL("p.fuel", ""),

	COLONY_OVERVIEW_MINERAL1("p.mineral1", ""),

	COLONY_OVERVIEW_MINERAL2("p.mineral2", ""),

	COLONY_OVERVIEW_MINERAL3("p.mineral3", ""),

	COLONY_OVERVIEW_MINES("p.mines", ""),

	COLONY_OVERVIEW_FACTORIES("p.factories", ""),

	COLONY_OVERVIEW_DEFENSE("p.planetary_defense", ""),

	COLONY_ORBITAL_SYSTEMS("\"builtOrbitalSystemCount\"", ""),

	SHIP_OVERVIEW_NAME("s.name", ""),

	SHIP_OVERVIEW_TEMPLATE_NAME("st.id", ""),

	SHIP_OVERVIEW_FLEET("f.name", ""),

	SHIP_OVERVIEW_FUEL_PERCENTAGE("\"fuelPercentage\"", ""),

	SHIP_OVERVIEW_CARGO_PERCENTAGE("\"cargoPercentage\"", ""),

	SHIP_OVERVIEW_CREW_PERCENTAGE("\"crewPercentage\"", ""),

	SHIP_OVERVIEW_DAMAGE("s.damage", ""),

	STARBASE_OVERVIEW_NAME("s.name", ""),

	STARBASE_OVERVIEW_HAS_SHIP_IN_PRODUCTION("\"hasShipInProduction\"", ""),

	STARBASE_OVERVIEW_HULL_LEVEL("s.hull_level", ""),

	STARBASE_OVERVIEW_PROPULSION_LEVEL("s.propulsion_level", ""),

	STARBASE_OVERVIEW_ENERGY_LEVEL("s.energy_level", ""),

	STARBASE_OVERVIEW_PROJECTILE_LEVEL("s.projectile_level", ""),

	FLEET_OVERVIEW_NAME("f.name", ""),

	FLEET_OVERVIEW_SHIP_COUNT("\"shipCount\"", ""),

	FLEET_OVERVIEW_FUEL_PERCENTAGE("\"fuelPercentage\"", ""),

	FLEET_OVERVIEW_PROJECTILE_PERCENTAGE("\"projectilePercentage\"", "");

	private final String databaseField;
	private final String i18nKey;

	private SortField(String databaseField, String i18nKey) {
		this.databaseField = databaseField;
		this.i18nKey = i18nKey;
	}

	public String getDatabaseField() {
		return databaseField;
	}

	public String getI18nKey() {
		return i18nKey;
	}
}