package org.skrupeltng.modules.dashboard;

public enum ResourceDensity {

	EXTREM(1000, 7000), HIGH(800, 5000), MEDIUM(500, 3500), LOW(100, 1500);

	private int min;
	private int max;

	private ResourceDensity(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}
}