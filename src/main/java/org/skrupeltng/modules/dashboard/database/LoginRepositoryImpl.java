package org.skrupeltng.modules.dashboard.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.Roles;
import org.skrupeltng.modules.dashboard.modules.admin.controller.UserListResultDTO;
import org.skrupeltng.modules.dashboard.modules.admin.controller.UserSearchParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

public class LoginRepositoryImpl extends RepositoryCustomBase implements LoginRepositoryCustom {

	private final List<String> aiUsernames = new ArrayList<>();

	@PostConstruct
	public void init() {
		for (AILevel aiLevel : AILevel.values()) {
			aiUsernames.add(aiLevel.name());
		}
	}

	@Override
	public Page<UserListResultDTO> searchUsers(UserSearchParameters parameters, Pageable pageRequest) {
		String username = parameters.getUsername();
		Boolean tutorial = parameters.getTutorial();
		Boolean createdGames = parameters.getCreatedGames();
		Boolean playedGames = parameters.getPlayedGames();

		String sql = "" +
				"SELECT \n" +
				"	l.id, \n" +
				"	l.username, \n" +
				"	COALESCE(SUM(COALESCE(lsf.games_created)), 0) as \"gamesCreated\", \n" +
				"	COALESCE(SUM(COALESCE(lsf.games_played)), 0) as \"gamesPlayed\", \n" +
				"	COALESCE(SUM(COALESCE(lsf.games_won)), 0) as \"gamesWon\", \n" +
				"	COALESCE(SUM(COALESCE(lsf.games_lost)), 0) as \"gamesLost\", \n" +
				"	(SELECT t.stage FROM tutorial t WHERE t.login_id = l.id LIMIT 1) as \"tutorialStage\", \n" +
				"	l.tutorial_finished as \"tutorialFinished\", \n" +
				"	count(*) OVER() AS \"totalElements\" \n" +
				"FROM \n" +
				"	login l \n" +
				"	LEFT OUTER JOIN login_stats_faction lsf \n" +
				"		ON lsf.login_id = l.id \n" +
				"	LEFT OUTER JOIN login_role lr \n" +
				"		ON lr.login_id = l.id AND lr.role_name = '" + Roles.ADMIN + "' \n";

		Map<String, Object> params = new HashMap<>();
		List<String> wheres = new ArrayList<>();
		List<String> havings = new ArrayList<>();

		wheres.add("lr.id IS NULL");

		wheres.add("l.username NOT IN (:aiUsernames)");
		params.put("aiUsernames", aiUsernames);

		if (StringUtils.isNotBlank(username)) {
			wheres.add("l.username ILIKE :username");
			params.put("username", "%" + username + "%");
		}

		if (tutorial != null) {
			if (tutorial) {
				wheres.add("l.tutorial_finished = true");
			} else {
				wheres.add("l.tutorial_finished = false");
				wheres.add(
						"COALESCE((SELECT t.id IS NOT NULL FROM player p INNER JOIN game t ON t.id = p.game_id AND p.login_id = l.id AND t.tutorial = true), false) = true");
			}
		}

		if (createdGames != null) {
			if (createdGames) {
				havings.add("COALESCE(SUM(COALESCE(lsf.games_created, 0)), 0) > 0");
			} else {
				havings.add("COALESCE(SUM(COALESCE(lsf.games_created, 0)), 0) = 0");
			}
		}

		if (playedGames != null) {
			if (playedGames) {
				havings.add("COALESCE(SUM(COALESCE(lsf.games_played, 0)), 0) > 0");
			} else {
				havings.add("COALESCE(SUM(COALESCE(lsf.games_played, 0)), 0) = 0");
			}
		}

		sql += where(wheres);

		sql += " " +
				"GROUP BY \n" +
				"	l.id, \n" +
				"	l.username ";

		sql += having(havings);

		Sort sort = pageRequest.getSort();
		String orderBy = createOrderBy(sort, "l.username ASC");

		sql += orderBy;

		RowMapper<UserListResultDTO> rowMapper = new BeanPropertyRowMapper<>(UserListResultDTO.class);

		return search(sql, params, pageRequest, rowMapper);
	}
}