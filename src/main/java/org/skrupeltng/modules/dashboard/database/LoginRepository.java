package org.skrupeltng.modules.dashboard.database;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LoginRepository extends JpaRepository<Login, Long>, LoginRepositoryCustom {

	Optional<Login> findByUsername(String username);

	Optional<Login> findByActivationCode(String activationCode);

	Optional<Login> findByEmail(String email);

	Optional<Login> findByPasswordRecoveryToken(String passwordRecoveryToken);

	@Query("SELECT l FROM Login l WHERE LOWER(l.username) LIKE ?1")
	List<Login> searchByUsername(String name);

	@Query("SELECT l.tutorialFinished FROM Login l WHERE l.id = ?1")
	boolean tutorialFinished(long loginId);
}