package org.skrupeltng.modules.dashboard.database;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "achievement")
public class Achievement implements Serializable {

	private static final long serialVersionUID = -2316878593537235327L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "earned_date")
	private Date earnedDate;

	@ManyToOne(fetch = FetchType.LAZY)
	private Login login;

	@Enumerated(EnumType.STRING)
	private AchievementType type;

	public Achievement() {

	}

	public Achievement(Login login, AchievementType type) {
		this.login = login;
		this.type = type;
		this.earnedDate = new Date();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getEarnedDate() {
		return earnedDate;
	}

	public void setEarnedDate(Date earnedDate) {
		this.earnedDate = earnedDate;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public AchievementType getType() {
		return type;
	}

	public void setType(AchievementType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Achievement [id=" + id + ", earnedDate=" + earnedDate + ", login=" + login + ", type=" + type + "]";
	}
}