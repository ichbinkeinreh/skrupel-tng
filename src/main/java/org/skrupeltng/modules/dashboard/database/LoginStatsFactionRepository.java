package org.skrupeltng.modules.dashboard.database;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface LoginStatsFactionRepository extends JpaRepository<LoginStatsFaction, Long>, LoginStatsFactionRepositoryCustom {

	@Modifying
	@Query("DELETE FROM LoginStatsFaction l WHERE l.login.id = ?1")
	void deleteByLoginId(long loginId);

	Optional<LoginStatsFaction> findByLoginIdAndFactionId(long loginId, String factionId);
}