package org.skrupeltng.modules.dashboard.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.dashboard.controller.LoginFactionStatsItem;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

public class LoginStatsFactionRepositoryImpl extends RepositoryCustomBase implements LoginStatsFactionRepositoryCustom {

	@Override
	public List<LoginFactionStatsItem> getLoginFactionStats(long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	i.value as \"factionName\", \n" +
				"	COALESCE(s.games_created, 0) as \"gamesCreated\", \n" +
				"	COALESCE(s.games_played, 0) as \"gamesPlayed\", \n" +
				"	COALESCE(s.games_won, 0) as \"gamesWon\", \n" +
				"	COALESCE(s.games_lost, 0) as \"gamesLost\", \n" +
				"	COALESCE(s.planets_colonized, 0) as \"planetsColonized\", \n" +
				"	COALESCE(s.planets_conquered, 0) as \"planetsConquered\", \n" +
				"	COALESCE(s.planets_mined_out, 0) as \"planetsMinedOut\", \n" +
				"	COALESCE(s.planets_lost, 0) as \"planetsLost\", \n" +
				"	COALESCE(s.ships_built, 0) as \"shipsBuilt\", \n" +
				"	COALESCE(s.ships_destroyed, 0) as \"shipsDestroyed\", \n" +
				"	COALESCE(s.ships_captured, 0) as \"shipsCaptured\", \n" +
				"	COALESCE(s.ships_lost, 0) as \"shipsLost\", \n" +
				"	COALESCE(s.starbases_created, 0) as \"starbasesCreated\", \n" +
				"	COALESCE(s.starbases_conquered, 0) as \"starbasesConquered\", \n" +
				"	COALESCE(s.starbases_lost, 0) as \"starbasesLost\", \n" +
				"	COALESCE(s.starbases_maxed, 0) as \"starbasesMaxed\" \n" +
				"FROM \n" +
				"	faction r \n" +
				"	INNER JOIN i18n i \n" +
				"		ON i.key = r.id AND i.language = :language \n" +
				"	LEFT OUTER JOIN login_stats_faction s \n" +
				"		ON s.login_id = :loginId AND r.id = s.faction_id \n" +
				"ORDER BY \n" +
				"	i.value ASC";

		Map<String, Object> params = new HashMap<>(2);
		params.put("loginId", loginId);
		params.put("language", LocaleContextHolder.getLocale().getLanguage());

		RowMapper<LoginFactionStatsItem> rowMapper = new BeanPropertyRowMapper<>(LoginFactionStatsItem.class);

		return jdbcTemplate.query(sql, params, rowMapper);
	}

	@Override
	public LoginFactionStatsItem getLoginFactionStatsSums(long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	SUM(COALESCE(s.games_created, 0)) as \"gamesCreated\", \n" +
				"	SUM(COALESCE(s.games_played, 0)) as \"gamesPlayed\", \n" +
				"	SUM(COALESCE(s.games_won, 0)) as \"gamesWon\", \n" +
				"	SUM(COALESCE(s.games_lost, 0)) as \"gamesLost\", \n" +
				"	SUM(COALESCE(s.planets_colonized, 0)) as \"planetsColonized\", \n" +
				"	SUM(COALESCE(s.planets_conquered, 0)) as \"planetsConquered\", \n" +
				"	SUM(COALESCE(s.planets_mined_out, 0)) as \"planetsMinedOut\", \n" +
				"	SUM(COALESCE(s.planets_lost, 0)) as \"planetsLost\", \n" +
				"	SUM(COALESCE(s.ships_built, 0)) as \"shipsBuilt\", \n" +
				"	SUM(COALESCE(s.ships_destroyed, 0)) as \"shipsDestroyed\", \n" +
				"	SUM(COALESCE(s.ships_captured, 0)) as \"shipsCaptured\", \n" +
				"	SUM(COALESCE(s.ships_lost, 0)) as \"shipsLost\", \n" +
				"	SUM(COALESCE(s.starbases_created, 0)) as \"starbasesCreated\", \n" +
				"	SUM(COALESCE(s.starbases_conquered, 0)) as \"starbasesConquered\", \n" +
				"	SUM(COALESCE(s.starbases_lost, 0)) as \"starbasesLost\", \n" +
				"	SUM(COALESCE(s.starbases_maxed, 0)) as \"starbasesMaxed\" \n" +
				"FROM \n" +
				"	faction r \n" +
				"	INNER JOIN i18n i \n" +
				"		ON i.key = r.id AND i.language = :language \n" +
				"	LEFT OUTER JOIN login_stats_faction s \n" +
				"		ON s.login_id = :loginId AND r.id = s.faction_id \n";

		Map<String, Object> params = new HashMap<>(2);
		params.put("loginId", loginId);
		params.put("language", LocaleContextHolder.getLocale().getLanguage());

		RowMapper<LoginFactionStatsItem> rowMapper = new BeanPropertyRowMapper<>(LoginFactionStatsItem.class);

		return jdbcTemplate.query(sql, params, rowMapper).get(0);
	}

	@Override
	public long getSum(long loginId, String fieldName) {
		String sql = "" +
				"SELECT \n" +
				"	SUM(COALESCE(lsf." + fieldName + ", 0)) \n" +
				"FROM \n" +
				"	login_stats_faction lsf \n" +
				"WHERE \n" +
				"	lsf.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("loginId", loginId);

		Long result = jdbcTemplate.queryForObject(sql, params, Long.class);

		if (result == null) {
			return 0L;
		}

		return result;
	}
}
