package org.skrupeltng.modules.dashboard.database;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DataPrivacyStatementRepository extends JpaRepository<DataPrivacyStatement, Long> {

	Optional<DataPrivacyStatement> findByLanguage(String language);
}