package org.skrupeltng.modules.dashboard.database;

public enum AchievementType {

	TUTORIAL_FINISHED("fas fa-graduation-cap", true),

	GAMES_WON_1("fas fa-crown", false, 1, "games_won"),

	GAMES_WON_10("fas fa-crown", false, 10, "games_won"),

	GAMES_WON_KUATOH("fas fa-crown", false),

	GAMES_WON_NIGHTMARE("fas fa-crown", false),

	GAMES_WON_ORION("fas fa-crown", false),

	GAMES_WON_TRODAN("fas fa-crown", false),

	GAMES_WON_SILVERSTARAG("fas fa-crown", false),

	PLANETS_COLONIZED_1("fas fa-globe", false, 1, "planets_colonized"),

	PLANETS_COLONIZED_20("fas fa-globe", false, 20, "planets_colonized"),

	PLANETS_COLONIZED_100("fas fa-globe", false, 100, "planets_colonized"),

	PLANETS_CONQUERED_1("fas fa-globe", false, 1, "planets_conquered"),

	PLANETS_CONQUERED_10("fas fa-globe", false, 10, "planets_conquered"),

	PLANETS_MINED_OUT("fas fa-monument", false, 1, "planets_mined_out"),

	SHIPS_BUILT_1("fas fa-space-shuttle", true, 1, "ships_built"),

	SHIPS_BUILT_20("fas fa-space-shuttle", true, 20, "ships_built"),

	SHIPS_BUILT_100("fas fa-space-shuttle", true, 100, "ships_built"),

	SHIPS_DESTROYED_1("fas fa-space-shuttle", false, 1, "ships_destroyed"),

	SHIPS_DESTROYED_10("fas fa-space-shuttle", false, 10, "ships_destroyed"),

	SHIPS_CAPTURED("fas fa-space-shuttle", false, 1, "ships_captured"),

	STARBASES_CREATED_1("fas fa-project-diagram", true, 1, "starbases_created"),

	STARBASES_CREATED_10("fas fa-project-diagram", true, 10, "starbases_created"),

	STARBASES_MAXED_1("fas fa-project-diagram", true, 1, "starbases_maxed"),

	STARBASES_MAXED_10("fas fa-project-diagram", true, 10, "starbases_maxed"),

	DESTROY_SHIP_BY_MINE_FIELD("fas fa-project-diagram", false);

	private final String icon;
	private final int statsValue;
	private final boolean sendDesktopNotification;
	private final String fieldName;

	private AchievementType(String icon, boolean sendDesktopNotification) {
		this(icon, sendDesktopNotification, 0, null);
	}

	private AchievementType(String icon, boolean sendDesktopNotification, int statsValue, String fieldName) {
		this.icon = icon;
		this.sendDesktopNotification = sendDesktopNotification;
		this.statsValue = statsValue;
		this.fieldName = fieldName;
	}

	public String getIcon() {
		return icon;
	}

	public int getStatsValue() {
		return statsValue;
	}

	public boolean isSendDesktopNotification() {
		return sendDesktopNotification;
	}

	public String getFieldName() {
		return fieldName;
	}
}