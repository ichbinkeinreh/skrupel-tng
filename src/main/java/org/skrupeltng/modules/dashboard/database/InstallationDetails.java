package org.skrupeltng.modules.dashboard.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "installation_details")
public class InstallationDetails implements Serializable {

	private static final long serialVersionUID = -4805793030150222119L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "legal_text")
	private String legalText;

	@Column(name = "domain_url")
	private String domainUrl;

	@Column(name = "contact_email")
	private String contactEmail;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLegalText() {
		return legalText;
	}

	public void setLegalText(String legalText) {
		this.legalText = legalText;
	}

	public String getDomainUrl() {
		return domainUrl;
	}

	public void setDomainUrl(String domainUrl) {
		this.domainUrl = domainUrl;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
}