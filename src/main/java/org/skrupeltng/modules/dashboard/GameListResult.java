package org.skrupeltng.modules.dashboard;

import java.io.Serializable;
import java.util.Date;

import org.skrupeltng.modules.PageableResult;

public class GameListResult implements Serializable, PageableResult {

	private static final long serialVersionUID = -6813910842222539718L;

	private long id;
	private String name;
	private String winCondition;
	private Date created;
	private boolean started;
	private boolean finished;
	private int currentPlayerCount;
	private int playerCount;
	private int round;
	private boolean turnFinished;
	private boolean playerOfGame;
	private boolean lost;
	private boolean factionSelected;

	private int totalElements;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWinCondition() {
		return winCondition;
	}

	public void setWinCondition(String winCondition) {
		this.winCondition = winCondition;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public int getCurrentPlayerCount() {
		return currentPlayerCount;
	}

	public void setCurrentPlayerCount(int currentPlayerCount) {
		this.currentPlayerCount = currentPlayerCount;
	}

	public int getPlayerCount() {
		return playerCount;
	}

	public void setPlayerCount(int playerCount) {
		this.playerCount = playerCount;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public boolean isTurnFinished() {
		return turnFinished;
	}

	public void setTurnFinished(boolean turnFinished) {
		this.turnFinished = turnFinished;
	}

	public boolean isPlayerOfGame() {
		return playerOfGame;
	}

	public void setPlayerOfGame(boolean playerOfGame) {
		this.playerOfGame = playerOfGame;
	}

	public boolean isLost() {
		return lost;
	}

	public void setLost(boolean lost) {
		this.lost = lost;
	}

	public boolean isFactionSelected() {
		return factionSelected;
	}

	public void setFactionSelected(boolean factionSelected) {
		this.factionSelected = factionSelected;
	}

	@Override
	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}
}