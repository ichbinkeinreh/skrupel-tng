package org.skrupeltng.modules.dashboard;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.URL;
import org.skrupeltng.modules.dashboard.controller.ValidPassword;

public class InitSetupRequest implements Serializable {

	private static final long serialVersionUID = 8804636958787247925L;

	@NotNull
	@NotBlank
	private String username;

	private String email;

	@NotNull
	@NotBlank
	@Email
	private String contactEmail;

	@ValidPassword
	private String password;

	private String legalText;

	@NotNull
	@NotBlank
	@URL
	private String domainUrl;

	@NotNull
	@NotBlank
	private String dataPrivacyStatementEnglish;

	@NotNull
	@NotBlank
	private String dataPrivacyStatementGerman;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLegalText() {
		return legalText;
	}

	public void setLegalText(String legalText) {
		this.legalText = legalText;
	}

	public String getDomainUrl() {
		return domainUrl;
	}

	public void setDomainUrl(String domainUrl) {
		this.domainUrl = domainUrl;
	}

	public String getDataPrivacyStatementEnglish() {
		return dataPrivacyStatementEnglish;
	}

	public void setDataPrivacyStatementEnglish(String dataPrivacyStatementEnglish) {
		this.dataPrivacyStatementEnglish = dataPrivacyStatementEnglish;
	}

	public String getDataPrivacyStatementGerman() {
		return dataPrivacyStatementGerman;
	}

	public void setDataPrivacyStatementGerman(String dataPrivacyStatementGerman) {
		this.dataPrivacyStatementGerman = dataPrivacyStatementGerman;
	}
}