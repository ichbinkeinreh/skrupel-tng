package org.skrupeltng.modules.dashboard;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.skrupeltng.modules.ingame.database.Game;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GameDetailsMapper {

	GameDetailsMapper INSTANCE = Mappers.getMapper(GameDetailsMapper.class);

	@Mapping(source = "creator.username", target = "creator")
	@Mapping(source = "creator.id", target = "creatorId")
	@Mapping(source = "autoTickSeconds", target = "autoTickItem")
	GameDetails newGameToGameDetails(Game request);

	default AutoTickItem newAutoTickItem(int seconds) {
		switch (seconds) {
			case 21600:
				return AutoTickItem.SIX_HOURS;
			case 43200:
				return AutoTickItem.TWELVE_HOURS;
			case 64800:
				return AutoTickItem.EIGHTTEEN_HOURS;
			case 86400:
				return AutoTickItem.TWENTYFOUR_HOURS;
			case 172800:
				return AutoTickItem.FORTYEIGHT_HOURS;
		}

		return AutoTickItem.NEVER;
	}

	@Mapping(source = "creator", target = "creator", ignore = true)
	@Mapping(source = "id", target = "id", ignore = true)
	@Mapping(source = "started", target = "started", ignore = true)
	@Mapping(source = "finished", target = "finished", ignore = true)
	@Mapping(source = "autoTickItem.seconds", target = "autoTickSeconds")
	void updateGame(GameDetails details, @MappingTarget Game game);

	@Mapping(source = "autoTickItem.seconds", target = "autoTickSeconds")
	Game newGameRequestToGame(NewGameRequest request);
}