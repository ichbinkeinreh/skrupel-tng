package org.skrupeltng.modules.dashboard.controller;

import org.skrupeltng.modules.dashboard.GameDetails;
import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class NewGameValidator implements Validator {

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private GameRepository gameRepository;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(NewGameRequest.class) || clazz.equals(GameDetails.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		NewGameRequest request = (NewGameRequest)target;

		boolean started = false;

		if (request instanceof GameDetails) {
			GameDetails gameDetails = (GameDetails)request;
			started = gameRepository.getOne(gameDetails.getId()).isStarted();
		}

		if (!started && !request.getWinCondition().equals(WinCondition.NONE.name()) && request.getPlayerCount() < 2) {
			String winConditionString = messageSource.getMessage("win_condition_" + request.getWinCondition(), null, request.getWinCondition(),
					LocaleContextHolder.getLocale());
			String message = messageSource.getMessage("win_condition_requires_two_players", new String[] { winConditionString },
					LocaleContextHolder.getLocale());
			errors.rejectValue("playerCount", null, message);
		}
	}
}
