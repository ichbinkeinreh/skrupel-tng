package org.skrupeltng.modules.dashboard.controller;

import java.io.Serializable;

public class CombatCalculatorRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7700441354468409690L;
	
	private String hull1;
	private String energy1;
	private String projectile1;
	private int damage1;
	private int shield1;
	private int projectile1cnt;
	
	private String hull2;
	private String energy2;
	private String projectile2;
	private int damage2;
	private int shield2;
	private int projectile2cnt;
	
	
	public int getProjectile1cnt() {
		return projectile1cnt;
	}
	public void setProjectile1cnt(int projectile1cnt) {
		this.projectile1cnt = projectile1cnt;
	}
	public int getProjectile2cnt() {
		return projectile2cnt;
	}
	public void setProjectile2cnt(int projectile2cnt) {
		this.projectile2cnt = projectile2cnt;
	}
	public String getHull1() {
		return hull1;
	}
	public void setHull1(String hull1) {
		this.hull1 = hull1;
	}
	public String getEnergy1() {
		return energy1;
	}
	public void setEnergy1(String energy1) {
		this.energy1 = energy1;
	}
	public String getProjectile1() {
		return projectile1;
	}
	public void setProjectile1(String projectile1) {
		this.projectile1 = projectile1;
	}
	public int getDamage1() {
		return damage1;
	}
	public void setDamage1(int damage1) {
		this.damage1 = damage1;
	}
	public int getShield1() {
		return shield1;
	}
	public void setShield1(int shield1) {
		this.shield1 = shield1;
	}
	public String getHull2() {
		return hull2;
	}
	public void setHull2(String hull2) {
		this.hull2 = hull2;
	}
	public String getEnergy2() {
		return energy2;
	}
	public void setEnergy2(String energy2) {
		this.energy2 = energy2;
	}
	public String getProjectile2() {
		return projectile2;
	}
	public void setProjectile2(String projectile2) {
		this.projectile2 = projectile2;
	}
	public int getDamage2() {
		return damage2;
	}
	public void setDamage2(int damage2) {
		this.damage2 = damage2;
	}
	public int getShield2() {
		return shield2;
	}
	public void setShield2(int shield2) {
		this.shield2 = shield2;
	}

}
