package org.skrupeltng.modules.dashboard.controller;

import javax.validation.Valid;

import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ChangeUserEmailSettingsController extends AbstractUserSettingsController {

	@Autowired
	private LoginService loginService;

	@Autowired
	private ChangeEmailSettingsValidator changeEmailSettingsValidator;

	@InitBinder
	protected void initBinder(final WebDataBinder binder) {
		binder.addValidators(changeEmailSettingsValidator);
	}

	@PostMapping("/user/email")
	public String changePassword(@Valid @ModelAttribute("emailSettingsRequest") EmailChangeRequest emailSettingsRequest, BindingResult bindingResult,
			Model model) {
		Login login = loginService.getCurrentLogin();
		model.addAttribute("request", new PasswordChangeRequest(login));
		addUserDetailsData(model, login, false);

		if (bindingResult.hasErrors()) {
			return "dashboard/user-settings";
		}

		loginService.changeEmailSettings(userDetailService.getLoginId(), emailSettingsRequest);

		return "redirect:/user-settings";
	}
}