package org.skrupeltng.modules.dashboard.controller;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ResetPasswordValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return ResetPasswordRequest.class.equals(clazz) || PasswordRecoveryRequest.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target instanceof ResetPasswordRequest) {
			ResetPasswordRequest request = (ResetPasswordRequest)target;

			if (request.getPassword() != null && !request.getPassword().equals(request.getPasswordRepeated())) {
				errors.rejectValue("passwordRepeated", "passwords_must_match");
			}
		}
	}
}