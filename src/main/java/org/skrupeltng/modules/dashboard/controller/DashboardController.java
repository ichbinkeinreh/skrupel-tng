package org.skrupeltng.modules.dashboard.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.config.LoginDetails;
import org.skrupeltng.exceptions.ResourceNotFoundException;
import org.skrupeltng.modules.SortField;
import org.skrupeltng.modules.SortFieldItem;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.GameDetails;
import org.skrupeltng.modules.dashboard.GameDetailsMapper;
import org.skrupeltng.modules.dashboard.GameListResult;
import org.skrupeltng.modules.dashboard.GameSearchParameters;
import org.skrupeltng.modules.dashboard.LoginSearchResultJSON;
import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.DataPrivacyStatement;
import org.skrupeltng.modules.dashboard.database.DataPrivacyStatementRepository;
import org.skrupeltng.modules.dashboard.database.InstallationDetailsRepository;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationConstants;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationService;
import org.skrupeltng.modules.dashboard.service.AchievementService;
import org.skrupeltng.modules.dashboard.service.GameFullException;
import org.skrupeltng.modules.dashboard.service.LoginService;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.service.IngameService;
import org.skrupeltng.modules.ingame.service.VisiblePlanetsAndShips;
import org.skrupeltng.modules.mail.service.MailService;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.FactionLandingpageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DashboardController extends AbstractGameController {

	@Autowired
	private IngameService gameService;

	@Autowired
	private GameDetailsMapper gameDetailsMapper;

	@Autowired
	private InstallationDetailsRepository installationDetailsRepository;

	@Autowired
	private DataPrivacyStatementRepository dataPrivacyStatementRepository;

	@Autowired
	private LoginService loginService;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private AchievementService achievementService;

	@Autowired
	private MailService mailService;

	private final List<SortFieldItem> existingGamesSortItems = new ArrayList<>();

	@PostConstruct
	public void init() {
		addToExistingGamesSortItems(SortField.EXISTING_GAMES_NAME);
		addToExistingGamesSortItems(SortField.EXISTING_GAMES_GAME_MODE);
		addToExistingGamesSortItems(SortField.EXISTING_GAMES_CREATED);
		addToExistingGamesSortItems(SortField.EXISTING_GAMES_STATUS);
		addToExistingGamesSortItems(SortField.EXISTING_GAMES_PLAYERCOUNT);
		addToExistingGamesSortItems(SortField.EXISTING_GAMES_ROUND);
	}

	private void addToExistingGamesSortItems(SortField sortField) {
		existingGamesSortItems.add(new SortFieldItem(sortField, true));
		existingGamesSortItems.add(new SortFieldItem(sortField, false));
	}

	@GetMapping("")
	public String getLandingPage(Model model) {
		if (loginService.noPlayerExist()) {
			return "redirect:init-setup";
		}

		Locale locale = LocaleContextHolder.getLocale();

		List<FactionLandingpageData> factions = new ArrayList<>();

		factions.add(masterDataService.getFactionLandingpageData("orion", "7", ShipAbilityType.SIGNATURE_MASK, "14", ShipAbilityType.VIRAL_INVASION, locale));
		factions.add(masterDataService.getFactionLandingpageData("nightmare", "10", ShipAbilityType.SUB_SPACE_DISTORTION, "12",
				ShipAbilityType.CLOAKING_RELIABLE, locale));
		factions.add(
				masterDataService.getFactionLandingpageData("trodan", "6", ShipAbilityType.CLOAKING_PERFECT, "3", ShipAbilityType.ASTRO_PHYSICS_LAB, locale));
		factions.add(masterDataService.getFactionLandingpageData("kuatoh", "1", ShipAbilityType.STRUCTUR_SCANNER, "2", ShipAbilityType.JUMP_ENGINE, locale));

		model.addAttribute("factions", factions);
		model.addAttribute("totalFactionCount", masterDataService.getFactionCount());

		return "dashboard/landing-page";
	}

	@GetMapping("/legal")
	public String getLegalText(Model model) {
		String legalText = installationDetailsRepository.getLegalText();
		model.addAttribute("legalText", legalText);
		return "dashboard/legal-text";
	}

	@GetMapping("/register-successfull")
	public String getRegisterSuccessfull() {
		return "dashboard/register-successfull";
	}

	@GetMapping("/activation-success")
	public String getActivationSuccess(Model model) {
		model.addAttribute("headline", "account_now_active");
		return "dashboard/registration-finished";
	}

	@GetMapping("/registration-finished")
	public String getRegistrationFinished(Model model) {
		model.addAttribute("headline", "registration_successfull");
		return "dashboard/registration-finished";
	}

	@GetMapping("/activate-account/{activationCode}")
	public String activateAccount(@PathVariable("activationCode") String activationCode, Model model) {
		Optional<Login> resultOpt = loginService.activateAccount(activationCode);

		if (resultOpt.isPresent()) {
			Login login = resultOpt.get();

			LoginDetails loginDetails = userDetailService.loadUserByUsername(login.getUsername());
			UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(loginDetails, null, loginDetails.getAuthorities());
			SecurityContext sc = SecurityContextHolder.getContext();
			sc.setAuthentication(authReq);

			return "redirect:/activation-success";
		}

		return "dashboard/activation-failure";
	}

	@GetMapping("/data-privacy")
	public String getDataPrivacyStatement(Model model) {
		Optional<DataPrivacyStatement> resultOpt = dataPrivacyStatementRepository.findByLanguage(LocaleContextHolder.getLocale().getLanguage());

		if (!resultOpt.isPresent()) {
			return "dashboard/no-data-privacy-statement";
		}

		String text = resultOpt.get().getText();

		if (StringUtils.isBlank(text)) {
			return "dashboard/no-data-privacy-statement";
		}

		model.addAttribute("dataPrivacyStatement", text);
		return "dashboard/data-privacy-statement";
	}

	@GetMapping("/my-games")
	public String getMyGames(
			@RequestParam(required = false) String name,
			@RequestParam(required = false) String winCondition,
			@RequestParam(required = false) Boolean started,
			@RequestParam(required = false) Boolean turnNotDone,

			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) String sortField,
			@RequestParam(required = false) Boolean sortDirection,
			Model model) {

		Pageable pageRequest = createPageable(page, 20, sortField, sortDirection);

		long loginId = userDetailService.getLoginId();
		boolean isAdmin = userDetailService.isAdmin();
		WinCondition condition = StringUtils.isNotBlank(winCondition) ? WinCondition.valueOf(winCondition) : null;

		GameSearchParameters params = new GameSearchParameters();
		params.setAdmin(isAdmin);
		params.setCurrentLoginId(loginId);
		params.setName(name);
		params.setStarted(started);
		params.setTurnNotDone(turnNotDone);
		params.setOnlyOwnGames(true);
		params.setWinCondition(condition);

		Page<GameListResult> results = dashboardService.searchGames(params, pageRequest);

		model.addAttribute("sortItems", existingGamesSortItems);
		model.addAttribute("results", results);
		model.addAttribute("winConditions", WinCondition.values());

		return "dashboard/my-games";
	}

	@GetMapping("/open-games")
	public String getOpenGames(
			@RequestParam(required = false) String name,
			@RequestParam(required = false) String winCondition,
			@RequestParam(required = false) Boolean allGames,

			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) String sortField,
			@RequestParam(required = false) Boolean sortDirection,
			Model model) {

		Pageable pageRequest = createPageable(page, 20, sortField, sortDirection);

		long loginId = userDetailService.getLoginId();
		boolean isAdmin = userDetailService.isAdmin();
		WinCondition condition = StringUtils.isNotBlank(winCondition) ? WinCondition.valueOf(winCondition) : null;

		GameSearchParameters params = new GameSearchParameters();
		params.setAdmin(isAdmin);
		params.setCurrentLoginId(loginId);
		params.setName(name);
		params.setOnlyOwnGames(false);

		if (allGames == null || !allGames) {
			params.setStarted(false);
		}

		params.setWinCondition(condition);

		Page<GameListResult> results = dashboardService.searchGames(params, pageRequest);

		model.addAttribute("sortItems", existingGamesSortItems);
		model.addAttribute("results", results);
		model.addAttribute("winConditions", WinCondition.values());

		return "dashboard/open-games";
	}

	@GetMapping("/new-game")
	public String getNewGame(Model model) {
		NewGameRequest request = NewGameRequest.createDefaultRequest();
		model.addAttribute("request", request);
		model.addAttribute("canEdit", true);
		model.addAttribute("started", false);

		prepareNewGameModel(model);
		return "dashboard/new-game";
	}

	@GetMapping("/game")
	public String getGame(@RequestParam(required = true) long id, Model model) {
		Optional<Game> optional = gameService.getGame(id);

		if (optional.isPresent()) {
			Game game = optional.get();
			GameDetails gameDetails = gameDetailsMapper.newGameToGameDetails(game);
			prepareNewGameModel(model);
			prepareGameDetailsModel(id, model, game, gameDetails);

			if (game.isFinished()) {
				VisiblePlanetsAndShips visiblePlanetsAndShips = gameService.getVisiblePlanetsAndShipsForFinishedGameMiniMap(id);
				model.addAttribute("planets", visiblePlanetsAndShips.getPlanets());
				model.addAttribute("singleShips", visiblePlanetsAndShips.getSingleShips());
				model.addAttribute("shipClusters", visiblePlanetsAndShips.getShipClusters());
			}

			return "dashboard/game";
		}

		throw new ResourceNotFoundException();
	}

	@DeleteMapping("/game")
	@ResponseBody
	public void deleteGame(@RequestParam(required = true) long id) {
		dashboardService.deleteGame(id);
	}

	@PostMapping("/game/faction")
	@ResponseBody
	public void selectFaction(@RequestParam(required = true) long gameId, @RequestParam(required = true) String faction) {
		long currentLoginId = userDetailService.getLoginId();
		dashboardService.selectFactionForLogin(gameId, faction, currentLoginId);

		Game game = gameService.getGame(gameId).get();
		List<Player> players = game.getPlayers();
		Login creator = game.getCreator();

		if (creator.getId() != currentLoginId && game.getPlayerCount() == players.size() && !players.stream().anyMatch(p -> p.getFaction() == null)) {
			notificationService.addNotification(creator, "/game?id=" + gameId, NotificationConstants.notification_game_can_be_started,
					game.getName());

			if (creator.isJoinNotificationsEnabled()) {
				mailService.sendGameFullNotification(game);
			}
		}
	}

	@PutMapping("/game/faction")
	@ResponseBody
	public void setFactionForPlayer(@RequestParam(required = true) String faction, @RequestParam(required = true) long playerId) {
		dashboardService.selectFactionForPlayer(faction, playerId);
	}

	@DeleteMapping("/game/player")
	@ResponseBody
	public void removePlayerFromGame(@RequestParam(required = true) long gameId, @RequestParam(required = true) long playerId) {
		if (gameService.getGame(gameId).get().getCreator().getId() != userDetailService.getLoginId()) {
			throw new IllegalArgumentException("Only game creators can remove other players!");
		}

		dashboardService.removePlayerFromGame(gameId, playerId);
	}

	@DeleteMapping("/game/player/self")
	@ResponseBody
	public void removeLoginFromGame(@RequestParam(required = true) long gameId) {
		dashboardService.removeLoginFromGame(gameId, userDetailService.getLoginId());
	}

	@PostMapping("/game/player")
	@ResponseBody
	public String joinGame(@RequestParam long gameId, @RequestParam(required = false) Integer teamIndex) {
		try {
			Player player = dashboardService.addPlayer(gameId, userDetailService.getLoginId(), teamIndex);

			Game game = gameService.getGame(gameId).get();
			Login creator = game.getCreator();
			String gameName = game.getName();
			String link = "/game?id=" + gameId;

			if (userDetailService.getLoginId() != creator.getId()) {
				notificationService.addNotification(creator, link, NotificationConstants.notification_player_joined, gameName);

				if (creator.isJoinNotificationsEnabled()) {
					mailService.sendPlayerJoinedGameNotification(player.getLogin(), game);
				}
			}

			return null;
		} catch (GameFullException e) {
			return getGameFullMessage(e);
		}
	}

	@PutMapping("/game/player")
	@ResponseBody
	public String addPlayer(@RequestParam long gameId, @RequestParam long loginId, @RequestParam(required = false) Integer teamIndex) {
		try {
			dashboardService.addPlayer(gameId, loginId, teamIndex);
			return null;
		} catch (GameFullException e) {
			return getGameFullMessage(e);
		}
	}

	private String getGameFullMessage(GameFullException e) {
		return messageSource.getMessage("game_full", new Integer[] { Integer.valueOf(e.getMaxPlayerCount()) }, LocaleContextHolder.getLocale());
	}

	@PostMapping("/start-game")
	@ResponseBody
	public void startGame(@RequestParam long gameId) {
		dashboardService.startGame(gameId);
	}

	@GetMapping("/game/{gameId}/players")
	@ResponseBody
	public List<LoginSearchResultJSON> searchPlayers(@PathVariable long gameId, @RequestParam String name) {
		Locale locale = LocaleContextHolder.getLocale();

		String originalName = name;

		if (StringUtils.isNotBlank(name)) {
			AILevel[] aiLevels = AILevel.values();
			int matchCount = 0;
			String match = null;

			for (AILevel aiLevel : aiLevels) {
				String aiLevelName = aiLevel.name();
				String aiName = messageSource.getMessage(aiLevelName, null, aiLevelName, locale);

				if (aiName.toLowerCase().contains(name.toLowerCase())) {
					matchCount++;
					match = aiLevelName;
				}
			}

			if (matchCount == 1) {
				name = match;
			} else if (matchCount > 1) {
				name = "AI";
			}
		}

		List<LoginSearchResultJSON> logins = searchPlayers(gameId, name, locale);

		if (!StringUtils.equals(name, originalName)) {
			Set<LoginSearchResultJSON> allLogins = new HashSet<>(logins);
			allLogins.addAll(searchPlayers(gameId, originalName, locale));
			logins = new ArrayList<>(allLogins);
		}

		Collections.sort(logins);

		return logins;
	}

	private List<LoginSearchResultJSON> searchPlayers(long gameId, String name, Locale locale) {
		List<LoginSearchResultJSON> logins = dashboardService.searchLogins(gameId, name);

		for (LoginSearchResultJSON login : logins) {
			String playerName = login.getPlayerName();
			playerName = messageSource.getMessage(playerName, null, playerName, locale);
			login.setPlayerName(playerName);
		}

		return logins;
	}

	@DeleteMapping("/user")
	@ResponseBody
	public void deleteCurrentUser() {
		loginService.deleteCurrentUser();
	}

	@PostMapping("/start-tutorial")
	@ResponseBody
	public long startTutorial() {
		return tutorialService.startTutorial();
	}

	@PostMapping("/user/language")
	@ResponseBody
	public void changeUserLanguage(@RequestParam String language) {
		dashboardService.changeUserLanguage(userDetailService.getLoginId(), language);
	}

	@GetMapping("/user-statistics")
	public String getUserStatistics(Model model) {
		List<LoginFactionStatsItem> stats = dashboardService.getLoginFactionStats(userDetailService.getLoginId());
		LoginFactionStatsItem sums = dashboardService.getLoginFactionStatsSums(userDetailService.getLoginId());
		sums.setFactionName(messageSource.getMessage("total", null, "Total", LocaleContextHolder.getLocale()));
		stats.add(sums);
		model.addAttribute("stats", stats);

		return "dashboard/user-statistics";
	}

	@GetMapping("/credits")
	public String getCredits() {
		return "dashboard/credits";
	}

	@GetMapping({ "/achievements", "/achievements/{type}" })
	public String getAchievements(@PathVariable(required = false, name = "type") String type, Model model) {
		List<AchievementItemDTO> items = achievementService.getAchievementFrontendData(userDetailService.getLoginId());
		model.addAttribute("items", items);

		if (StringUtils.isNotBlank(type)) {
			model.addAttribute("selection", AchievementType.valueOf(type));
		}

		return "dashboard/achievements";
	}
}