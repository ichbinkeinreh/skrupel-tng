package org.skrupeltng.modules.dashboard.controller;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.dashboard.AutoTickItem;
import org.skrupeltng.modules.dashboard.GameDetails;
import org.skrupeltng.modules.dashboard.PlanetDensity;
import org.skrupeltng.modules.dashboard.ResourceDensity;
import org.skrupeltng.modules.dashboard.ResourceDensityHomePlanet;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.service.DashboardService;
import org.skrupeltng.modules.ingame.database.FogOfWarType;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.HomePlanetSetup;
import org.skrupeltng.modules.ingame.database.LoseCondition;
import org.skrupeltng.modules.ingame.database.StableWormholeConfig;
import org.skrupeltng.modules.ingame.database.StartPositionSetup;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.service.OverviewService;
import org.skrupeltng.modules.ingame.modules.overview.service.PlayerSummaryEntry;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;

public class AbstractGameController extends AbstractController {

	@Autowired
	protected DashboardService dashboardService;

	@Autowired
	protected OverviewService overviewService;

	@Autowired
	protected MasterDataService masterDataService;

	protected void prepareNewGameModel(Model model) {
		model.addAttribute("planetDensities", PlanetDensity.values());
		model.addAttribute("resourceDensities", ResourceDensity.values());
		model.addAttribute("resourceDensityHomePlanets", ResourceDensityHomePlanet.values());
		model.addAttribute("winConditions", WinCondition.values());
		model.addAttribute("startPositionSetups", StartPositionSetup.values());
		model.addAttribute("homePlanetSetups", HomePlanetSetup.values());
		model.addAttribute("loseConditions", LoseCondition.values());
		model.addAttribute("galaxyConfigs", dashboardService.getGalaxyConfigsIds());
		model.addAttribute("stableWormholeConfigs", StableWormholeConfig.values());
		model.addAttribute("fogOfWarTypes", FogOfWarType.values());
		model.addAttribute("autoTickItems", AutoTickItem.values());
	}

	protected void prepareGameDetailsModel(long id, Model model, Game game, GameDetails gameDetails) {
		gameDetails.setGalaxyConfigId(game.getGalaxyConfig().getId());

		List<Player> players = game.getPlayers();
		Collections.sort(players);

		model.addAttribute("game", gameDetails);
		model.addAttribute("started", gameDetails.isStarted());
		model.addAttribute("gameFinished", gameDetails.isFinished());
		model.addAttribute("factions", masterDataService.getAllFactions());
		model.addAttribute("players", players);

		long currentLoginId = userDetailService.getLoginId();
		model.addAttribute("currentLoginId", currentLoginId);

		Login creator = game.getCreator();
		boolean isCreator = creator != null && creator.getId() == currentLoginId;
		model.addAttribute("canEdit", isCreator);

		Optional<Player> currentPlayerOpt = players.stream().filter(p -> p.getLogin().getId() == currentLoginId).findAny();
		boolean hasLost = false;

		if (currentPlayerOpt.isPresent()) {
			hasLost = currentPlayerOpt.get().isHasLost();

			if (hasLost) {
				String loseText = messageSource.getMessage("lose_text_" + game.getLoseCondition().name(), null, LocaleContextHolder.getLocale());
				model.addAttribute("loseText", loseText);

				addPlayerSummaries(id, model);
			}
		}

		model.addAttribute("playerLost", hasLost);

		boolean playerWithoutFactionExists = players.stream().anyMatch(p -> p.getFaction() == null);

		boolean showStartButton = isCreator && !game.isStarted() && !playerWithoutFactionExists;
		showStartButton &= (game.getWinCondition() == WinCondition.NONE || game.getPlayers().size() >= 2);
		model.addAttribute("showStartButton", showStartButton);

		boolean playerTakesPartInGame = game.getPlayers().stream().anyMatch(l -> l.getLogin().getId() == currentLoginId);
		model.addAttribute("showPlayButton", game.isStarted() && playerTakesPartInGame);

		WinCondition winCondition = game.getWinCondition();
		boolean isTeamDeathFoe = winCondition == WinCondition.TEAM_DEATH_FOE;
		model.addAttribute("isTeamDeathFoe", isTeamDeathFoe);

		boolean gameIsNotFull = game.getPlayerCount() > players.size();
		boolean showJoinGameButton = !showStartButton && !game.isStarted() && !playerTakesPartInGame && gameIsNotFull;
		model.addAttribute("showJoinGameButton", showJoinGameButton);

		boolean showLeaveGameButton = !isCreator && playerTakesPartInGame && !game.isStarted() && !game.isFinished();
		model.addAttribute("showLeaveGameButton", showLeaveGameButton);

		boolean showJoinTeamButtons = !game.isStarted() && !game.isFinished() && isTeamDeathFoe && gameIsNotFull;
		model.addAttribute("showJoinTeamButtons", showJoinTeamButtons);
		model.addAttribute("currentTeamIndex", -1);
		model.addAttribute("playerAloneInTeam", false);

		if (isTeamDeathFoe) {
			List<DeathFoeTeam> deathFoeTeams = dashboardService.getDeathFoeTeams(id);

			if (playerTakesPartInGame) {
				for (int i = 0; i < deathFoeTeams.size(); i++) {
					List<Player> playersOfTeam = deathFoeTeams.get(i).getPlayers();

					if (playersOfTeam.stream().anyMatch(p -> p.getLogin().getId() == currentLoginId)) {
						model.addAttribute("currentTeamIndex", i);
						model.addAttribute("playerAloneInTeam", playersOfTeam.size() == 1);
						break;
					}
				}
			}

			model.addAttribute("teams", deathFoeTeams);
			model.addAttribute("emptyList", Collections.emptyList());
		}

		model.addAttribute("gameFull", game.getPlayerCount() == game.getPlayers().size());

		boolean canBeDeleted = isCreator || userDetailService.isAdmin();
		model.addAttribute("canBeDeleted", canBeDeleted);

		if (game.isFinished()) {
			addPlayerSummaries(id, model);

			addGameFinishedText(model, players, winCondition);
		}
	}

	protected void addGameFinishedText(Model model, List<Player> players, WinCondition winCondition) {
		List<Player> winners = players.stream().filter(p -> !p.isHasLost()).collect(Collectors.toList());

		String gameFinishedTextPrefix = "game_finished_text_";
		String additionalText = null;
		Object[] args = null;

		if (winCondition == WinCondition.SURVIVE) {
			if (winners.isEmpty()) {
				String text = messageSource.getMessage("no_one_survived", null, LocaleContextHolder.getLocale());
				model.addAttribute("winnerText", text);
				return;
			}

			args = new Object[] { getPlayerNameWithColor(winners.get(0)) };
		} else if (winCondition == WinCondition.DEATH_FOE) {
			if (winners.size() == 0) {
				String text = messageSource.getMessage("mutually_destroyed_death_foes", null, LocaleContextHolder.getLocale());
				model.addAttribute("winnerText", text);
				return;
			} else if (winners.size() == 1) {
				args = new Object[] { getPlayerNameWithColor(winners.get(0)),
						getPlayerNameWithColor(winners.get(0).getDeathFoes().iterator().next().getDeathFoe()) };
			} else {
				gameFinishedTextPrefix = "game_finished_text_multiple_";
				args = new Object[] { joinPlayerNames(winners) };
			}
		} else if (winCondition == WinCondition.TEAM_DEATH_FOE) {
			List<Player> deathFoes = winners.get(0).getDeathFoes().stream().map(d -> d.getDeathFoe()).collect(Collectors.toList());

			additionalText = createAdditionalText(winners.get(0));

			if (winners.size() == 1) {
				gameFinishedTextPrefix = "game_finished_text_single_";
				args = new Object[] { getPlayerNameWithColor(winners.get(0)), joinPlayerNames(deathFoes) };
			} else {
				args = new Object[] { joinPlayerNames(winners), joinPlayerNames(deathFoes) };
			}
		}

		String winnerText = messageSource.getMessage(gameFinishedTextPrefix + winCondition.name(), args, LocaleContextHolder.getLocale());

		if (additionalText != null) {
			winnerText += " " + additionalText;
		}

		model.addAttribute("winnerText", winnerText);
	}

	protected String createAdditionalText(Player winner) {
		List<Player> lostAllies = dashboardService.getLostAllies(winner);

		if (lostAllies.size() == 1) {
			return messageSource.getMessage("lost_ally", new Object[] { getPlayerNameWithColor(lostAllies.get(0)) }, LocaleContextHolder.getLocale());
		} else if (lostAllies.size() > 1) {
			return messageSource.getMessage("lost_allies", new Object[] { joinPlayerNames(lostAllies) }, LocaleContextHolder.getLocale());
		}

		return null;
	}

	protected String joinPlayerNames(List<Player> players) {
		if (players.size() >= 2) {
			Player last = players.remove(players.size() - 1);
			List<String> names = players.stream().sorted().map(this::getPlayerNameWithColor).collect(Collectors.toList());
			return StringUtils.join(names, ", ") + " " + messageSource.getMessage("and", null, LocaleContextHolder.getLocale()) + " " +
					getPlayerNameWithColor(last);
		}

		return getPlayerNameWithColor(players.get(0));
	}

	protected String getPlayerNameWithColor(Player player) {
		return player.retrieveDisplayNameWithColor(messageSource);
	}

	protected void addPlayerSummaries(long id, Model model) {
		List<PlayerSummaryEntry> playerSummaries = overviewService.getPlayerSummaries(id);
		model.addAttribute("playerSummaries", playerSummaries);
	}
}