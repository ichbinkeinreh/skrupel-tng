package org.skrupeltng.modules.dashboard.controller;

import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.dashboard.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController extends AbstractController {

	@Autowired
	private LoginService loginService;

	@GetMapping("login")
	public String login() {
		if (loginService.noPlayerExist()) {
			return "redirect:init-setup";
		}

		return "dashboard/login";
	}
}