package org.skrupeltng.modules.dashboard.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.ShipAbilityDescriptionMapper;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.controller.NativeSpeciesDescription;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpecies;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetType;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.service.round.combat.space.SpaceCombat;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.database.ShipTemplateRepository;
import org.skrupeltng.modules.masterdata.database.WeaponTemplate;
import org.skrupeltng.modules.masterdata.database.WeaponTemplateRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping({ "/infos" })
public class InfosController extends AbstractController {

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private ShipAbilityDescriptionMapper shipAbilityDescriptionMapper;
	
	@Autowired
	private SpaceCombat spaceCombat;
	
	@Autowired
	private ShipTemplateRepository shipTemplateRepository;
	
	@Autowired
	private WeaponTemplateRepository weaponTemplateRepository;
	
	@Autowired
	protected MessageSource messageSource;

	@GetMapping("")
	public String getInfos(Model model) {
		List<PlanetType> planetTypes = masterDataService.getAllPlanetTypes();
		model.addAttribute("planetTypes", planetTypes);
		return "dashboard/infos/infos";
	}

	@GetMapping("/introduction")
	public String getIntroduction(Model model) {
		return "dashboard/infos/introduction";
	}

	@GetMapping("/planets")
	public String getPlanets(Model model) {
		return "dashboard/infos/planets";
	}

	@GetMapping("/ships")
	public String getShips(Model model) {
		return "dashboard/infos/ships";
	}

	@GetMapping("/starbases")
	public String getStarbases(Model model) {
		model.addAttribute("types", StarbaseType.values());
		return "dashboard/infos/starbases";
	}

	@GetMapping("/factions")
	public String getFaction(Model model) {
		List<Faction> faction = masterDataService.getAllFactions();
		model.addAttribute("faction", faction);
		return "dashboard/infos/factions";
	}

	@GetMapping("/factions/{factionId}")
	public String getFaction(@PathVariable String factionId, Model model) {
		Faction faction = masterDataService.getFactionData(factionId);

		List<ShipTemplateDetails> templateDetails = faction.getShips().stream().map(s -> new ShipTemplateDetails(s, shipAbilityDescriptionMapper)).sorted()
				.collect(Collectors.toList());
		model.addAttribute("faction", faction);
		model.addAttribute("templateDetails", templateDetails);

		String unlockedOrbitalSystems = faction.getUnlockedOrbitalSystems();
		String forbiddenOrbitalSystems = faction.getForbiddenOrbitalSystems();

		if (StringUtils.isNotBlank(unlockedOrbitalSystems)) {
			List<OrbitalSystemType> additionalOrbitalSystems = toEnum(unlockedOrbitalSystems);
			model.addAttribute("additionalOrbitalSystems", additionalOrbitalSystems);
		}

		if (StringUtils.isNotBlank(forbiddenOrbitalSystems)) {
			List<OrbitalSystemType> unavailableOrbitalSystems = toEnum(forbiddenOrbitalSystems);
			model.addAttribute("unavailableOrbitalSystems", unavailableOrbitalSystems);
		}

		return "dashboard/infos/faction-details";
	}

	private List<OrbitalSystemType> toEnum(String unlockedOrbitalSystems) {
		return Stream.of(unlockedOrbitalSystems.split(",")).map(s -> OrbitalSystemType.valueOf(s)).collect(Collectors.toList());
	}

	@GetMapping("/native-species")
	public String getNativeSpecies(Model model) {
		List<NativeSpecies> nativeSpecies = masterDataService.getAllNativeSpecies();
		model.addAttribute("nativeSpecies", nativeSpecies);

		NativeSpeciesDescription description = new NativeSpeciesDescription(messageSource);
		model.addAttribute("description", description);

		return "dashboard/infos/native-species";
	}

	@GetMapping("/planet-types")
	public String getPlanetTypes(Model model) {
		List<PlanetType> planetTypes = masterDataService.getAllPlanetTypes();
		model.addAttribute("planetTypes", planetTypes);
		return "dashboard/infos/planet-types";
	}

	@GetMapping("/orbital-systems")
	public String getOrbitalSystems(Model model) {
		model.addAttribute("types", OrbitalSystemType.values());
		return "dashboard/infos/orbital-systems";
	}

	@GetMapping("/space-combat")
	public String getSpaceCombat(Model model) {
		return "dashboard/infos/space-combat";
	}

	@GetMapping("/orbital-combat")
	public String getOrbitalCombat(Model model) {
		return "dashboard/infos/orbital-combat";
	}

	@GetMapping("/ground-combat")
	public String getGroundCombat(Model model) {
		return "dashboard/infos/ground-combat";
	}

	@GetMapping("/anomalies")
	public String getAnomalies(Model model) {
		return "dashboard/infos/anomalies";
	}
	
	@GetMapping("/combat-calculator/space")
	public String getCombatCalculator(Model model) {
		List<Faction> factions = masterDataService.getAllFactions();
		Map<String, String> hulls = new LinkedHashMap<>();
		for (Faction faction : factions) {
			hulls.put(masterDataService.getI18NValue(faction.getId()), "");
			for (ShipTemplate s : faction.getShips()) {
				hulls.put(masterDataService.getShipTemplateName(s), s.getId());
			}
		}
		model.addAttribute("hulls", hulls.entrySet());
		Map<String, String> energies = new LinkedHashMap<>();
		List<WeaponTemplate> allEnergies = weaponTemplateRepository.findByUsesProjectiles(false);
		for (WeaponTemplate w : allEnergies) {
			energies.put(messageSource.getMessage(w.getName(), null, LocaleContextHolder.getLocale()), w.getName());
		}
		model.addAttribute("energies", energies);
		Map<String, String> projectiles = new LinkedHashMap<>();
		List<WeaponTemplate> allProjectiles = weaponTemplateRepository.findByUsesProjectiles(true);
		for (WeaponTemplate w : allProjectiles) {
			projectiles.put(messageSource.getMessage(w.getName(), null, LocaleContextHolder.getLocale()), w.getName());
		}
		model.addAttribute("projectiles", projectiles);
		
		return "dashboard/infos/combat-calculator";
	}
	
	@PostMapping("/combat-calculator/space")
	public @ResponseBody Map<String, Integer> getCombatCalculatorPost(@RequestBody CombatCalculatorRequest request) {
		Game game = new Game(1111L);
		game.setGalaxySize(1000);
		Faction faction = new Faction("test_faction");
		faction.setShips(new ArrayList<>());
		Player player1 = new Player(1);
		player1.setGame(game);
		player1.setFaction(faction);
		Player player2 = new Player(1);
		player2.setGame(game);
		player2.setFaction(faction);
		
		ShipTemplate hull1 = shipTemplateRepository.getOne(request.getHull1());
		ShipTemplate hull2 = shipTemplateRepository.getOne(request.getHull2());
		WeaponTemplate energy1 = weaponTemplateRepository.getOne(request.getEnergy1());
		WeaponTemplate energy2 = weaponTemplateRepository.getOne(request.getEnergy2());
		WeaponTemplate projectile1 = weaponTemplateRepository.getOne(request.getProjectile1());
		WeaponTemplate projectile2 = weaponTemplateRepository.getOne(request.getProjectile2());
		
		Ship me = new Ship();
		me.setId(1L);
		me.setName("test1");
		me.setPlayer(player1);
		me.setPropulsionSystemTemplate(new PropulsionSystemTemplate());
		me.setShipTemplate(hull1);
		me.setEnergyWeaponTemplate(energy1);
		me.setProjectileWeaponTemplate(projectile1);
		
		Ship enemy = new Ship();
		enemy.setId(2L);
		enemy.setName("test2");
		enemy.setPlayer(player2);
		enemy.setPropulsionSystemTemplate(new PropulsionSystemTemplate());
		enemy.setShipTemplate(hull2);
		enemy.setEnergyWeaponTemplate(energy2);
		enemy.setProjectileWeaponTemplate(projectile2);
		
		//TODO: add tactics to calculator
		//TODO: add assist to calculator
		//TODO: add capture mode to calculator
		//TODO: add ship modules to calculator
		int meSurvived = 0;
		int enemySurvived = 0;
		for (int i = 0; i < 1000; ++i) {
			me.setProjectiles(0);
			me.addProjectiles(request.getProjectile1cnt());
			me.setCrew(hull1.getCrew());
			me.setShield(request.getShield1());
			me.setDamage(request.getDamage1());
			
			enemy.setProjectiles(0);
			enemy.addProjectiles(request.getProjectile2cnt());
			enemy.setCrew(hull2.getCrew());
			enemy.setShield(request.getShield2());
			enemy.setDamage(request.getDamage2());
			
			spaceCombat.processCombat(me, enemy, false, 0, 0);
			
			if (!me.destroyed() && me.getCrew() > 0) {
				meSurvived++;
			}
			if (!enemy.destroyed() && enemy.getCrew() > 0) {
				enemySurvived++;
			}
		}
			
		final Map<String, Integer> ret = new HashMap<>();
		ret.put("me", meSurvived/10);
		ret.put("enemy", enemySurvived/10);
		
		return ret;
	}
}