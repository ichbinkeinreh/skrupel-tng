package org.skrupeltng.modules.dashboard.controller;

import javax.validation.Valid;

import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ChangePasswordController extends AbstractUserSettingsController {

	@Autowired
	private LoginService loginService;

	@Autowired
	private ChangePasswordValidator changePasswordValidator;

	@InitBinder
	protected void initBinder(final WebDataBinder binder) {
		binder.addValidators(changePasswordValidator);
	}

	@GetMapping("/user-settings")
	public String getUserDetails(Model model) {
		Login login = loginService.getCurrentLogin();
		model.addAttribute("request", new PasswordChangeRequest(login));
		addUserDetailsData(model, login, true);
		return "dashboard/user-settings";
	}

	@PostMapping("/user-settings")
	public String changePassword(@Valid @ModelAttribute("request") PasswordChangeRequest request, BindingResult bindingResult, Model model) {
		Login login = loginService.getCurrentLogin();
		addUserDetailsData(model, login, true);

		if (bindingResult.hasErrors()) {
			return "dashboard/user-settings";
		}

		loginService.changePassword(request);
		request.setNewPassword(null);
		request.setNewPasswordRepeated(null);

		return "dashboard/user-settings";
	}
}