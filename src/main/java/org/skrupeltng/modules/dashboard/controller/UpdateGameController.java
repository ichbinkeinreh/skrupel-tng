package org.skrupeltng.modules.dashboard.controller;

import javax.validation.Valid;

import org.skrupeltng.modules.dashboard.GameDetails;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.service.IngameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UpdateGameController extends AbstractGameController {

	@Autowired
	private IngameService gameService;

	@Autowired
	private GameUpdateValidator gameUpdateValidator;

	@Autowired
	private NewGameValidator newGameValidator;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(gameUpdateValidator);
		binder.addValidators(newGameValidator);
	}

	@PostMapping("/game")
	public String updateGame(@Valid @ModelAttribute("game") GameDetails request, BindingResult bindingResult, Model model) {
		Game game = gameService.getGame(request.getId()).get();

		if (hasErrors(game, request, bindingResult)) {
			prepareNewGameModel(model);
			prepareGameDetailsModel(game.getId(), model, game, request);
			return "dashboard/game";
		}

		dashboardService.updateGame(request, game.getId());

		return "redirect:game?id=" + request.getId() + "#options";
	}

	private boolean hasErrors(Game game, GameDetails request, BindingResult bindingResult) {
		if (game.isStarted()) {
			if (request.getMaxConcurrentPlasmaStormCount() < 0 || request.getMaxConcurrentPlasmaStormCount() > 10) {
				return true;
			}
			if (request.getPlasmaStormProbability() < 0 || request.getPlasmaStormProbability() > 100) {
				return true;
			}
			if (request.getPlasmaStormRounds() < 3 || request.getPlasmaStormRounds() > 20) {
				return true;
			}
			if (request.getPirateAttackProbabilityCenter() < 0 || request.getPirateAttackProbabilityCenter() > 100) {
				return true;
			}
			if (request.getPirateAttackProbabilityOutskirts() < 0 || request.getPirateAttackProbabilityOutskirts() > 100) {
				return true;
			}
			if (request.getPirateMaxLoot() < 0 || request.getPirateMaxLoot() > 100) {
				return true;
			}
			if (request.getPirateMinLoot() < 0 || request.getPirateMinLoot() > 100) {
				return true;
			}

			return false;
		}

		return bindingResult.hasErrors();
	}
}