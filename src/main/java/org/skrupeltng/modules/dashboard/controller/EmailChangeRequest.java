package org.skrupeltng.modules.dashboard.controller;

import java.io.Serializable;

import org.skrupeltng.modules.dashboard.database.Login;

public class EmailChangeRequest implements Serializable {

	private static final long serialVersionUID = -7468737030492274304L;

	private String email;
	private boolean roundNotificationsEnabled;
	private boolean joinNotificationsEnabled;
	private boolean gameFullNotificationsEnabled;

	public EmailChangeRequest() {

	}

	public EmailChangeRequest(Login login) {
		email = login.getEmail();
		roundNotificationsEnabled = login.isRoundNotificationsEnabled();
		joinNotificationsEnabled = login.isJoinNotificationsEnabled();
		gameFullNotificationsEnabled = login.isGameFullNotificationsEnabled();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isRoundNotificationsEnabled() {
		return roundNotificationsEnabled;
	}

	public void setRoundNotificationsEnabled(boolean roundNotificationsEnabled) {
		this.roundNotificationsEnabled = roundNotificationsEnabled;
	}

	public boolean isJoinNotificationsEnabled() {
		return joinNotificationsEnabled;
	}

	public void setJoinNotificationsEnabled(boolean joinNotificationsEnabled) {
		this.joinNotificationsEnabled = joinNotificationsEnabled;
	}

	public boolean isGameFullNotificationsEnabled() {
		return gameFullNotificationsEnabled;
	}

	public void setGameFullNotificationsEnabled(boolean gameFullNotificationsEnabled) {
		this.gameFullNotificationsEnabled = gameFullNotificationsEnabled;
	}
}