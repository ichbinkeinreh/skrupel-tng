package org.skrupeltng.modules.dashboard.controller;

import java.io.Serializable;

public class ResetPasswordRequest implements Serializable {

	private static final long serialVersionUID = -40762612259295552L;

	@ValidPassword
	private String password;

	private String passwordRepeated;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRepeated() {
		return passwordRepeated;
	}

	public void setPasswordRepeated(String passwordRepeated) {
		this.passwordRepeated = passwordRepeated;
	}
}