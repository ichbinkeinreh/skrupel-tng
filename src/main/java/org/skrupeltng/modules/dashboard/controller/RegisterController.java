package org.skrupeltng.modules.dashboard.controller;

import javax.validation.Valid;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.dashboard.RegisterRequest;
import org.skrupeltng.modules.dashboard.service.LoginService;
import org.skrupeltng.modules.mail.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RegisterController extends AbstractController {

	@Autowired
	private LoginService loginService;

	@Autowired
	private RegistrationValidator registrationValidator;

	@Autowired
	private MailService mailService;

	@Autowired
	private ConfigProperties configProperties;

	@InitBinder
	protected void initBinder(final WebDataBinder binder) {
		binder.addValidators(registrationValidator);
	}

	@GetMapping("/register")
	public String register(Model model) {
		if (loginService.noPlayerExist()) {
			return "redirect:init-setup";
		}

		model.addAttribute("request", new RegisterRequest());

		return "dashboard/register";
	}

	@PostMapping("/register")
	public String register(@Valid @ModelAttribute("request") RegisterRequest request, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "dashboard/register";
		}

		loginService.registerNewLogin(request);

		if (mailService.mailsEnabled() && configProperties.isEnableAccountValidationByEmail()) {
			return "redirect:/register-successfull";
		}

		UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
		SecurityContext sc = SecurityContextHolder.getContext();
		sc.setAuthentication(authReq);

		return "redirect:/registration-finished";
	}
}