package org.skrupeltng.modules.dashboard.service;

import java.util.List;
import java.util.Set;

import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.TutorialRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoeRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlasmaStormRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFoldRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequestRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplateRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class GameRemoval {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private ShipService shipService;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private StarbaseService starbaseService;

	@Autowired
	private SpaceFoldRepository spaceFoldRepository;

	@Autowired
	private PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Autowired
	private PlayerDeathFoeRepository playerDeathFoeRepository;

	@Autowired
	private WormHoleRepository wormHoleRepository;

	@Autowired
	private PlasmaStormRepository plasmaStormRepository;

	@Autowired
	private AquiredShipTemplateRepository aquiredShipTemplateRepository;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private PlayerRelationRequestRepository playerRelationRequestRepository;

	@Autowired
	private NewsEntryRepository newsEntryRepository;

	@Autowired
	private FleetRepository fleetRepository;

	@Autowired
	private TutorialRepository tutorialRepository;

	@Autowired
	private GameRepository gameRepository;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void delete(Game game) {
		long gameId = game.getId();
		log.info("Deleting game " + gameId + "...");

		List<Ship> ships = shipRepository.findByGameId(gameId);
		shipService.deleteShips(ships);

		wormHoleRepository.deleteByGameId(gameId);
		plasmaStormRepository.deleteByGameId(gameId);
		tutorialRepository.deleteByGameId(gameId);
		aquiredShipTemplateRepository.deleteByGameId(gameId);

		List<Player> players = game.getPlayers();

		for (Player player : players) {
			long playerId = player.getId();
			planetRepository.clearPlayer(playerId);
			planetRepository.clearNewPlayer(playerId);
			playerRelationRepository.deleteByPlayerId(playerId);
			playerRelationRequestRepository.deleteByPlayerId(playerId);
			playerPlanetScanLogRepository.deleteByPlayerId(playerId);
			newsEntryRepository.deleteByPlayerId(playerId);
			playerDeathFoeRepository.deleteByPlayerId(playerId);
			fleetRepository.deleteByPlayerId(playerId);
		}

		playerRepository.deleteInBatch(players);

		Set<Planet> planets = game.getPlanets();

		for (Planet planet : planets) {
			long planetId = planet.getId();
			spaceFoldRepository.deleteByPlanetId(planetId);
			orbitalSystemRepository.deleteByPlanetId(planetId);

			Starbase starbase = planet.getStarbase();

			planetRepository.delete(planet);

			if (starbase != null) {
				starbaseService.delete(starbase);
			}
		}

		gameRepository.delete(game);

		log.info("Game " + gameId + " deleted.");
	}
}