package org.skrupeltng.modules.dashboard.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.GameDetails;
import org.skrupeltng.modules.dashboard.GameDetailsMapper;
import org.skrupeltng.modules.dashboard.GameListResult;
import org.skrupeltng.modules.dashboard.GameSearchParameters;
import org.skrupeltng.modules.dashboard.LoginSearchResultJSON;
import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.dashboard.controller.DeathFoeTeam;
import org.skrupeltng.modules.dashboard.controller.LoginFactionStatsItem;
import org.skrupeltng.modules.dashboard.database.GalaxyConfigRepository;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.database.LoginStatsFactionRepository;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationConstants;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationService;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.LoseCondition;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.service.round.RoundCalculationService;
import org.skrupeltng.modules.mail.service.MailService;
import org.skrupeltng.modules.masterdata.database.FactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

@Service
public class DashboardService {

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private LoginStatsFactionRepository loginStatsFactionRepository;

	@Autowired
	private GalaxyConfigRepository galaxyConfigRepository;

	@Autowired
	private FactionRepository factionRepository;

	@Autowired
	private GameStartingHelper gameStartingHelper;

	@Autowired
	private RoundCalculationService roundCalculationService;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private MailService mailService;

	@Autowired
	private UserDetailServiceImpl userService;

	@Autowired
	private GameRemoval gameRemoval;

	@Autowired
	private GameDetailsMapper gameMapper;

	public Page<GameListResult> searchGames(GameSearchParameters params, Pageable page) {
		return gameRepository.searchGames(params, page);
	}

	public List<String> getGalaxyConfigsIds() {
		return galaxyConfigRepository.getIds();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Game createdNewGame(NewGameRequest request, long loginId) {
		Game game = gameMapper.newGameRequestToGame(request);
		game.setGalaxyConfig(galaxyConfigRepository.getOne(request.getGalaxyConfigId()));

		Login creator = loginRepository.getOne(loginId);
		game.setCreator(creator);

		game = gameRepository.save(game);
		Player player = new Player(game, creator);
		player = playerRepository.save(player);
		game.setPlayers(Lists.newArrayList(player));

		return game;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#gameId, 'game')")
	public void updateGame(GameDetails request, long gameId) {
		Optional<Game> gameOpt = gameRepository.findById(request.getId());

		if (!gameOpt.isPresent()) {
			return;
		}

		Game game = gameOpt.get();

		if (game.getCreator().getId() != userService.getLoginId()) {
			throw new IllegalArgumentException("Only game creators can start games!");
		}

		if (game.isStarted()) {
			game.setLoseCondition(LoseCondition.valueOf(request.getLoseCondition()));
			game.setAutoTickSeconds(request.getAutoTickItem().getSeconds());
			game.setEnableEspionage(request.isEnableEspionage());
			game.setEnableMineFields(request.isEnableMineFields());
			game.setEnableTactialCombat(request.isEnableTactialCombat());
			game.setPlasmaStormProbability(request.getPlasmaStormProbability());
			game.setPlasmaStormRounds(request.getPlasmaStormRounds());
			game.setMaxConcurrentPlasmaStormCount(request.getMaxConcurrentPlasmaStormCount());
			game.setPirateAttackProbabilityCenter(request.getPirateAttackProbabilityCenter());
			game.setPirateAttackProbabilityOutskirts(request.getPirateAttackProbabilityOutskirts());
			game.setPirateMaxLoot(request.getPirateMaxLoot());
			game.setPirateMinLoot(request.getPirateMinLoot());
			gameRepository.save(game);
		} else {
			gameMapper.updateGame(request, game);
			game.setGalaxyConfig(galaxyConfigRepository.getOne(request.getGalaxyConfigId()));
			gameRepository.save(game);
		}
	}

	public Optional<Game> getGame(long id) {
		return gameRepository.findById(id);
	}

	public List<LoginSearchResultJSON> searchLogins(long gameId, String name) {
		List<Login> logins = loginRepository.searchByUsername(name.toLowerCase() + "%");
		Set<Login> existing = playerRepository.findByGameId(gameId).stream().map(Player::getLogin).collect(Collectors.toSet());

		Set<String> aiLevels = Sets.newHashSet(AILevel.values()).stream().map(AILevel::name).collect(Collectors.toSet());

		return logins.stream()
				.filter(l -> !existing.contains(l) || aiLevels.contains(l.getUsername()))
				.map(l -> new LoginSearchResultJSON(l.getId(), l.getUsername()))
				.collect(Collectors.toList());
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void selectFactionForLogin(long gameId, String faction, long loginId) {
		Optional<Player> playerOptional = playerRepository.findByGameIdAndLoginId(gameId, loginId);

		if (playerOptional.isPresent()) {
			Player player = playerOptional.get();

			if (player.getGame().isStarted()) {
				throw new IllegalStateException("The game is already started!");
			}

			player.setFaction(factionRepository.getOne(faction));
			playerRepository.save(player);
		} else {
			throw new RuntimeException("Login " + loginId + " not found for game " + gameId + "!");
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void selectFactionForPlayer(String faction, long playerId) {
		Optional<Player> playerOptional = playerRepository.findById(playerId);

		if (playerOptional.isPresent()) {
			Player player = playerOptional.get();

			if (player.getGame().isStarted()) {
				throw new IllegalStateException("The game is already started!");
			}

			if (player.getGame().getCreator().getId() != userService.getLoginId()) {
				throw new IllegalArgumentException("Only game creators can change AI factions!");
			}

			player.setFaction(factionRepository.getOne(faction));
			playerRepository.save(player);
		} else {
			throw new RuntimeException("Player " + playerId + " not found!");
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Player addPlayer(long gameId, long loginId, Integer teamIndex) throws GameFullException {
		Game game = gameRepository.getOne(gameId);

		if (game.isStarted()) {
			throw new IllegalStateException("The game is already started!");
		}

		boolean isTeamDeathFoe = game.getWinCondition() == WinCondition.TEAM_DEATH_FOE;

		List<Player> players = game.getPlayers();

		if (players != null && players.size() == game.getPlayerCount()) {
			throw new GameFullException(game.getPlayerCount());
		}

		Login login = loginRepository.getOne(loginId);
		Player player = new Player(game, login);

		try {
			AILevel aiLevel = AILevel.valueOf(login.getUsername());
			player.setAiLevel(aiLevel);
		} catch (Exception e) {

		}

		boolean playerSwitchesTeams = false;

		if (player.getAiLevel() == null) {
			Optional<Player> opt = playerRepository.findByGameIdAndLoginId(gameId, loginId);

			if (opt.isPresent() && !isTeamDeathFoe) {
				throw new IllegalArgumentException("Player " + login.getUsername() + " already takes part in game " + gameId + "!");
			} else if (opt.isPresent() && isTeamDeathFoe) {
				player = opt.get();
				playerSwitchesTeams = true;
			}
		}

		player = playerRepository.save(player);

		if (isTeamDeathFoe) {
			List<DeathFoeTeam> teams = getDeathFoeTeams(gameId);

			if (teamIndex != null) {
				DeathFoeTeam team = teams.get(teamIndex);

				for (Player teamPlayer : team.getPlayers()) {
					PlayerRelation relation = new PlayerRelation();
					relation.setType(PlayerRelationType.ALLIANCE);
					relation.setPlayer1(player);
					relation.setPlayer2(teamPlayer);
					playerRelationRepository.save(relation);
				}
			} else if (playerSwitchesTeams) {
				playerRelationRepository.deleteByPlayerId(player.getId());
			}
		}

		return player;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void removeLoginFromGame(long gameId, long loginId) {
		Optional<Player> playerOpt = playerRepository.findByGameIdAndLoginId(gameId, loginId);

		if (playerOpt.isPresent()) {
			long playerId = playerOpt.get().getId();
			removePlayerFromGame(gameId, playerId);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void removePlayerFromGame(long gameId, long playerId) {
		Game game = gameRepository.getOne(gameId);

		if (game.isStarted()) {
			throw new IllegalStateException("The game is already started!");
		}

		playerRelationRepository.deleteByPlayerId(playerId);
		playerRepository.deleteById(playerId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void startGame(long gameId) {
		gameStartingHelper.startGame(gameId);

		roundCalculationService.computeAIRounds(gameId);
		roundCalculationService.setupTurnValues(gameId);

		List<Login> logins = gameRepository.getLoginsByGameId(gameId);
		String gameName = gameRepository.getName(gameId);
		String link = "/ingame/game?id=" + gameId;
		long currentLoginId = userService.getLoginId();

		for (Login login : logins) {
			if (login.getId() != currentLoginId) {
				notificationService.addNotification(login, link, NotificationConstants.notification_game_started, gameName);
			}
		}

		mailService.sendGameStartedNotificationEmails(gameId, currentLoginId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteGame(long id) {
		Game game = gameRepository.getOne(id);
		long loginId = userService.getLoginId();

		if (game.getCreator() != null && game.getCreator().getId() != loginId && !userService.isAdmin()) {
			throw new IllegalArgumentException("Only the creator of a game or an admin is allowed to delete a game!");
		}

		gameRemoval.delete(game);
	}

	public List<DeathFoeTeam> getDeathFoeTeams(long gameId) {
		List<Player> players = playerRepository.findByGameId(gameId);
		Map<Long, DeathFoeTeam> teamsMap = new HashMap<>(players.size());

		for (Player player : players) {
			List<PlayerRelation> relations = playerRelationRepository.findByPlayerId(player.getId());

			DeathFoeTeam team = null;

			for (PlayerRelation relation : relations) {
				team = teamsMap.get(relation.getPlayer1() == player ? relation.getPlayer2().getId() : relation.getPlayer1().getId());

				if (team != null) {
					break;
				}
			}

			if (team == null) {
				team = new DeathFoeTeam();
			}

			teamsMap.put(player.getId(), team);
			team.getPlayers().add(player);
		}

		List<DeathFoeTeam> teams = teamsMap.values().stream().collect(Collectors.toSet()).stream().sorted().collect(Collectors.toList());
		return teams;
	}

	public List<Player> getLostAllies(Player winner) {
		List<PlayerRelation> relations = playerRelationRepository.findByPlayerIdAndType(winner.getId(), PlayerRelationType.ALLIANCE);
		List<Player> lostAllies = new ArrayList<>(relations.size());

		for (PlayerRelation relation : relations) {
			Player player1 = relation.getPlayer1();
			Player player2 = relation.getPlayer2();

			if (player1 != winner && player1.isHasLost()) {
				lostAllies.add(player1);
			}

			if (player2 != winner && player2.isHasLost()) {
				lostAllies.add(player2);
			}
		}

		return lostAllies;
	}

	public List<LoginFactionStatsItem> getLoginFactionStats(long loginId) {
		return loginStatsFactionRepository.getLoginFactionStats(loginId);
	}

	public LoginFactionStatsItem getLoginFactionStatsSums(long loginId) {
		return loginStatsFactionRepository.getLoginFactionStatsSums(loginId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changeUserLanguage(long loginId, String language) {
		Login login = loginRepository.getOne(loginId);
		login.setLanguage(language);
		loginRepository.save(login);
	}
}