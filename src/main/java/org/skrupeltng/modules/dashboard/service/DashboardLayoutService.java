package org.skrupeltng.modules.dashboard.service;

import org.skrupeltng.config.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("dashboardLayoutService")
public class DashboardLayoutService {

	@Autowired
	private UserDetailServiceImpl userService;

	public boolean isLoggedIn() {
		return userService.isLoggedIn();
	}

	public boolean isAdmin() {
		return userService.isAdmin();
	}
}
