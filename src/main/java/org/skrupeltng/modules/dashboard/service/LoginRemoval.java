package org.skrupeltng.modules.dashboard.service;

import java.util.Set;

import org.skrupeltng.modules.dashboard.database.AchievementRepository;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.database.LoginRoleRepository;
import org.skrupeltng.modules.dashboard.database.LoginStatsFactionRepository;
import org.skrupeltng.modules.dashboard.modules.notification.database.NotificationRepository;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.TutorialRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequestRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplateRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class LoginRemoval {

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private LoginRoleRepository loginRoleRepository;

	@Autowired
	private ShipService shipService;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private AquiredShipTemplateRepository aquiredShipTemplateRepository;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private PlayerRelationRequestRepository playerRelationRequestRepository;

	@Autowired
	private PlayerPlanetScanLogRepository planetScanLogRepository;

	@Autowired
	private NewsEntryRepository newsEntryRepository;

	@Autowired
	private FleetRepository fleetRepository;

	@Autowired
	private TutorialRepository tutorialRepository;

	@Autowired
	private LoginStatsFactionRepository loginStatsFactionRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private NotificationRepository notificationRepository;

	@Autowired
	private AchievementRepository achievementRepository;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void delete(Login login) {
		loginRoleRepository.deleteInBatch(login.getRoles());

		Set<Player> players = login.getPlayers();

		for (Player player : players) {
			shipService.deleteShips(player.getShips());
			aquiredShipTemplateRepository.deleteByGameId(player.getGame().getId());

			long playerId = player.getId();
			starbaseRepository.clearLogBooksOfPlayer(playerId);
			planetRepository.clearPlayer(playerId);
			planetRepository.clearNewPlayer(playerId);

			playerRelationRepository.deleteByPlayerId(playerId);
			playerRelationRequestRepository.deleteByPlayerId(playerId);
			planetScanLogRepository.deleteByPlayerId(playerId);
			newsEntryRepository.deleteByPlayerId(playerId);
			fleetRepository.deleteByPlayerId(playerId);

			playerRepository.delete(player);
		}

		long loginId = login.getId();
		notificationRepository.deleteByLoginId(loginId);
		tutorialRepository.deleteByLoginId(loginId);
		loginStatsFactionRepository.deleteByLoginId(loginId);
		achievementRepository.deleteByLoginId(loginId);

		gameRepository.clearGameCreator(loginId);
		loginRepository.delete(login);
	}
}