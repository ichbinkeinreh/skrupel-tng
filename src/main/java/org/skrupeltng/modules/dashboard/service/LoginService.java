package org.skrupeltng.modules.dashboard.service;

import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.InstallationDetailsHelper;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.InitSetupRequest;
import org.skrupeltng.modules.dashboard.RegisterRequest;
import org.skrupeltng.modules.dashboard.Roles;
import org.skrupeltng.modules.dashboard.controller.EmailChangeRequest;
import org.skrupeltng.modules.dashboard.controller.PasswordChangeRequest;
import org.skrupeltng.modules.dashboard.controller.ResetPasswordRequest;
import org.skrupeltng.modules.dashboard.database.DataPrivacyStatement;
import org.skrupeltng.modules.dashboard.database.DataPrivacyStatementRepository;
import org.skrupeltng.modules.dashboard.database.InstallationDetails;
import org.skrupeltng.modules.dashboard.database.InstallationDetailsRepository;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.database.LoginRole;
import org.skrupeltng.modules.dashboard.database.LoginRoleRepository;
import org.skrupeltng.modules.mail.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LoginService {

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private LoginRoleRepository loginRoleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private InstallationDetailsRepository installationDetailsRepository;

	@Autowired
	private DataPrivacyStatementRepository dataPrivacyStatementRepository;

	@Autowired
	private UserDetailServiceImpl userDetailService;

	@Autowired
	private LoginRemoval loginRemoval;

	@Autowired
	private MailService mailService;

	@Autowired
	private InstallationDetailsHelper installationDetailsHelper;

	@Autowired
	private ConfigProperties configProperties;

	public boolean noPlayerExist() {
		return loginRepository.count() == 0L;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public long createAdmin(InitSetupRequest request) {
		Login admin = new Login();
		admin.setUsername(request.getUsername());
		admin.setEmail(request.getEmail());
		admin.setActive(true);
		admin.setPassword(passwordEncoder.encode(request.getPassword()));
		admin.setLanguage(LocaleContextHolder.getLocale().getLanguage());
		admin = loginRepository.save(admin);

		loginRoleRepository.save(new LoginRole(admin, Roles.ADMIN));
		loginRoleRepository.save(new LoginRole(admin, Roles.PLAYER));

		createAILogin(AILevel.AI_EASY);
		createAILogin(AILevel.AI_MEDIUM);
		createAILogin(AILevel.AI_HARD);

		InstallationDetails installationDetails = new InstallationDetails();
		installationDetails.setLegalText(request.getLegalText());
		installationDetails.setDomainUrl(request.getDomainUrl());
		installationDetails.setContactEmail(request.getContactEmail());
		installationDetailsRepository.save(installationDetails);

		dataPrivacyStatementRepository.save(new DataPrivacyStatement(Locale.ENGLISH.getLanguage(), request.getDataPrivacyStatementEnglish()));
		dataPrivacyStatementRepository.save(new DataPrivacyStatement(Locale.GERMAN.getLanguage(), request.getDataPrivacyStatementGerman()));

		installationDetailsHelper.clearDomainUrlCache();
		installationDetailsHelper.clearHasLegalTextCache();
		installationDetailsHelper.clearContactEmailCache();

		return admin.getId();
	}

	private void createAILogin(AILevel level) {
		Login aiLogin = new Login();
		aiLogin.setUsername(level.name());
		aiLogin.setPassword(passwordEncoder.encode(level.name()));
		aiLogin.setLanguage("en");
		aiLogin = loginRepository.save(aiLogin);

		loginRoleRepository.save(new LoginRole(aiLogin, Roles.PLAYER));
		loginRoleRepository.save(new LoginRole(aiLogin, Roles.AI));
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void registerNewLogin(RegisterRequest request) {
		Login login = new Login();
		login.setUsername(request.getUsername());
		login.setPassword(passwordEncoder.encode(request.getPassword()));
		login.setLanguage(LocaleContextHolder.getLocale().getLanguage());
		login.setEmail(request.getEmail());
		login.setRoundNotificationsEnabled(request.isRoundNotificationsEnabled());

		boolean sendEmail = mailService.mailsEnabled() && configProperties.isEnableAccountValidationByEmail();

		if (sendEmail) {
			login.setEmail(request.getEmail());
			login.setActivationCode(UUID.randomUUID().toString());
		} else {
			login.setActive(true);
		}

		login = loginRepository.save(login);

		loginRoleRepository.save(new LoginRole(login, Roles.PLAYER));

		if (sendEmail) {
			mailService.sendAccountActivationMail(login);
		}
	}

	public Login getCurrentLogin() {
		long loginId = userDetailService.getLoginId();
		return loginRepository.getOne(loginId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changePassword(PasswordChangeRequest request) {
		Login login = getCurrentLogin();
		login.setPassword(passwordEncoder.encode(request.getNewPassword()));
		loginRepository.save(login);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteCurrentUser() {
		Login login = getCurrentLogin();

		if (userDetailService.isAdmin()) {
			throw new IllegalArgumentException("The admin cannot be deleted!");
		}

		loginRemoval.delete(login);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Optional<Login> activateAccount(String activationCode) {
		Optional<Login> opt = loginRepository.findByActivationCode(activationCode);

		if (!opt.isPresent()) {
			return opt;
		}

		Login login = opt.get();
		login.setActivationCode(null);
		login.setActive(true);
		login = loginRepository.save(login);

		return Optional.of(login);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changeEmailSettings(long loginId, EmailChangeRequest request) {
		Login login = loginRepository.getOne(loginId);
		login.setEmail(request.getEmail());
		login.setRoundNotificationsEnabled(request.isRoundNotificationsEnabled());
		login.setJoinNotificationsEnabled(request.isJoinNotificationsEnabled());
		login.setGameFullNotificationsEnabled(request.isGameFullNotificationsEnabled());
		loginRepository.save(login);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void sendPasswordRecoveryEmail(String email) {
		Optional<Login> result = loginRepository.findByEmail(email);

		if (result.isPresent()) {
			Login login = result.get();
			login.setPasswordRecoveryToken(UUID.randomUUID().toString());
			login = loginRepository.save(login);
			mailService.sendPasswordRecoveryMail(login);
		} else {
			mailService.sendPasswordRecoveryFailMail(email);
		}
	}

	public Optional<Login> getLoginByPasswordRecoveryToken(String token) {
		return loginRepository.findByPasswordRecoveryToken(token);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Optional<Login> resetPassword(String token, ResetPasswordRequest request) {
		Optional<Login> result = loginRepository.findByPasswordRecoveryToken(token);

		if (result.isPresent()) {
			Login login = result.get();
			login.setPassword(passwordEncoder.encode(request.getPassword()));
			login.setPasswordRecoveryToken(null);
			login = loginRepository.save(login);
			return Optional.of(login);
		}

		return result;
	}
}