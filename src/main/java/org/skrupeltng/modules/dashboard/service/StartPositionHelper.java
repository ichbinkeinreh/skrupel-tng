package org.skrupeltng.modules.dashboard.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.random.MersenneTwister;
import org.skrupeltng.modules.dashboard.ResourceDensityHomePlanet;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.HomePlanetSetup;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetType;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetTypeRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StartPositionHelper {

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private PlanetTypeRepository planetTypeRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private MasterDataService masterDataService;

	protected void assignStartPositionsEqualDistance(Game game, List<Player> players, List<Planet> planets) {
		final int initMinAllowedPlayerDistance;

		if (game.getPlayerCount() > 1) {
			initMinAllowedPlayerDistance = game.getGalaxySize() / (game.getPlayerCount() - 1);
		} else {
			initMinAllowedPlayerDistance = 0;
		}

		List<Planet> playerPlanets = new ArrayList<>(players.size());

		List<Planet> startPositionPlanets = getStartPositionPlanets(planets, players.size(), 60 / game.getPlanetDensity().getFactor());

		for (Player player : players) {
			if (playerPlanets.isEmpty()) {
				Planet planet = startPositionPlanets.get(MasterDataService.RANDOM.nextInt(startPositionPlanets.size()));
				addOwnedPlanet(game, playerPlanets, player, planet);
			} else {
				int tries = 0;
				int minAllowedPlayerDistance = initMinAllowedPlayerDistance;

				while (true) {
					Planet planet = startPositionPlanets.get(MasterDataService.RANDOM.nextInt(startPositionPlanets.size()));

					if (playerPlanets.contains(planet)) {
						continue;
					}

					final int allowedDistance = minAllowedPlayerDistance;
					boolean hasNearPlayerPlanets = playerPlanets.stream().anyMatch(p -> CoordHelper.getDistance(planet, p) < allowedDistance);

					if (!hasNearPlayerPlanets) {
						addOwnedPlanet(game, playerPlanets, player, planet);
						break;
					}

					tries++;

					if (tries > 4) {
						minAllowedPlayerDistance--;
						tries = 0;
					}
				}
			}
		}
	}

	protected List<Planet> getStartPositionPlanets(List<Planet> allPlanets, int playerCount, int minPlanetCount) {
		List<Planet> startingPlanets = new ArrayList<>(allPlanets.size());

		for (Planet planet : allPlanets) {
			long nearPlanets = allPlanets.stream().filter(p -> p != planet && CoordHelper.getDistance(planet, p) <= 250).count();

			if (nearPlanets >= minPlanetCount) {
				startingPlanets.add(planet);
			}
		}

		if (startingPlanets.size() < playerCount) {
			return allPlanets;
		}

		return startingPlanets;
	}

	protected void assignStartPositionsRandom(Game game, List<Player> players, List<Planet> planets) {
		List<Planet> playerPlanets = new ArrayList<>(players.size());

		for (Player player : players) {
			Planet planet = planets.get(MasterDataService.RANDOM.nextInt(planets.size()));
			addOwnedPlanet(game, playerPlanets, player, planet);
		}
	}

	protected void addOwnedPlanet(Game game, List<Planet> playerPlanets, Player player, Planet planet) {
		MersenneTwister random = MasterDataService.RANDOM;

		playerPlanets.add(planet);

		planet.setPlayer(player);
		Faction faction = player.getFaction();
		planet.setName(faction.getHomePlanetName());

		if (faction.getPreferredPlanetType() != null) {
			planet.setType(faction.getPreferredPlanetType());
		}

		planet.setTemperature(faction.getPreferredTemperature());

		PlanetType planetType = planetTypeRepository.getOne(planet.getType());
		String image = planetType.getImagePrefix() + "_" + (1 + random.nextInt(planetType.getImageCount()));
		planet.setImage(image);

		int colonists = random.nextInt(1000) + (random.nextInt(1000) * 5) + 50000;
		planet.setColonists(colonists);
		planet.setMoney(game.getInitMoney());
		planet.setSupplies(5);
		planet.setMines(5);
		planet.setFactories(5);
		planet.setPlanetaryDefense(5);
		planet.setFuel(50 + random.nextInt(20));
		planet.setMineral1(50 + random.nextInt(20));
		planet.setMineral2(50 + random.nextInt(20));
		planet.setMineral3(50 + random.nextInt(20));

		ResourceDensityHomePlanet density = game.getResourceDensityHomePlanet();

		int minResources = density.getMin();
		int resourceDiff = density.getMax() - minResources;
		planet.setUntappedFuel(minResources + random.nextInt(resourceDiff));
		planet.setUntappedMineral1(minResources + random.nextInt(resourceDiff));
		planet.setUntappedMineral2(minResources + random.nextInt(resourceDiff));
		planet.setUntappedMineral3(minResources + random.nextInt(resourceDiff));

		planet.setNecessaryMinesForOneFuel(masterDataService.getNecessaryMinesByIndex(3 + random.nextInt(2)));
		planet.setNecessaryMinesForOneMineral1(masterDataService.getNecessaryMinesByIndex(3 + random.nextInt(2)));
		planet.setNecessaryMinesForOneMineral2(masterDataService.getNecessaryMinesByIndex(3 + random.nextInt(2)));
		planet.setNecessaryMinesForOneMineral3(masterDataService.getNecessaryMinesByIndex(3 + random.nextInt(2)));

		planet.setNativeSpecies(null);
		planet.setNativeSpeciesCount(0);

		HomePlanetSetup homePlanetSetup = game.getHomePlanetSetup();

		if (homePlanetSetup == HomePlanetSetup.STAR_BASE) {
			Starbase starbase = new Starbase();
			starbase.setName("Starbase 1");
			starbase.setType(StarbaseType.STAR_BASE);
			starbase = starbaseRepository.save(starbase);
			planet.setStarbase(starbase);
		} else if (homePlanetSetup == HomePlanetSetup.STAR_BASE) {
			Starbase starbase = new Starbase();
			starbase.setName("Warbase 1");
			starbase.setType(StarbaseType.WAR_BASE);
			starbase = starbaseRepository.save(starbase);
			planet.setStarbase(starbase);
		}

		orbitalSystemRepository.deleteByPlanetId(planet.getId());

		List<OrbitalSystem> orbitalSystems = new ArrayList<>(4);
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystemRepository.saveAll(orbitalSystems);

		planet = planetRepository.save(planet);
		player.setHomePlanet(planet);
		playerRepository.save(player);
	}
}