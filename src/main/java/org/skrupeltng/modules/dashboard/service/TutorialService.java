package org.skrupeltng.modules.dashboard.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.ingame.database.Tutorial;
import org.skrupeltng.modules.ingame.database.TutorialRepository;
import org.skrupeltng.modules.ingame.database.TutorialStage;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.service.round.RoundCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

@Service("tutorialService")
public class TutorialService {

	private static final List<TutorialStage> HOME_PLANET_STAGES = Lists.newArrayList(
			TutorialStage.INITIAL_SELECT_HOME_PLANET, TutorialStage.OPEN_HOME_PLANET_SECOND);

	private static final List<TutorialStage> FIRST_COLONY_STAGES = Lists.newArrayList(
			TutorialStage.SELECT_FIRST_COLONY_FOR_FREIGHTER, TutorialStage.OPEN_NEW_COLONY);

	private static final Set<TutorialStage> HAS_CONTINUE_BUTTON = Sets.newHashSet(
			TutorialStage.COLONISTS, TutorialStage.CANTOX, TutorialStage.SUPPLIES, TutorialStage.MINERALS, TutorialStage.ENEMY_COLONY,
			TutorialStage.SCANNER_DETAILS, TutorialStage.NEWS);

	private static final List<TutorialStage> FREIGHTER_STAGES = Lists.newArrayList(TutorialStage.OPEN_FREIGHTER_FIRST,
			TutorialStage.SELECT_FREIGHTER_COLONIZATION, TutorialStage.OPEN_FREIGHTER_THIRD);

	private static final List<TutorialStage> BOMBER_STAGES = Lists.newArrayList(TutorialStage.OPEN_BOMBER_FIRST);

	@Autowired
	private TutorialRepository tutorialRepository;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private UserDetailServiceImpl userService;

	@Autowired
	private TutorialGenerator tutorialGenerator;

	@Autowired
	private RoundCalculationService roundCalculationService;

	@Autowired
	private AchievementService achievementService;

	@Autowired
	private GameStartingHelper gameStartingHelper;

	@Autowired
	private GameRemoval gameRemoval;

	public boolean hasTutorial() {
		return getTutorial().isPresent();
	}

	public Optional<Tutorial> getTutorial() {
		long loginId = userService.getLoginId();
		return tutorialRepository.findByLoginId(loginId);
	}

	public boolean tutorialFinished() {
		long loginId = userService.getLoginId();
		return loginRepository.tutorialFinished(loginId);
	}

	public long getTutorialGameId() {
		return getTutorial().get().getGame().getId();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long startTutorial() {
		if (hasTutorial()) {
			throw new IllegalStateException("There already is a tutorial!");
		}

		long gameId = tutorialGenerator.generateTutorial();
		gameStartingHelper.initLongRangeSensorPlanetLogs(gameId);

		roundCalculationService.computeAIRounds(gameId);
		roundCalculationService.setupTurnValues(gameId);

		tutorialRepository.resetPlayerOverviewViewed(getTutorial().get().getId());
		return gameId;
	}

	public TutorialStage getCurrentStage() {
		return getTutorial().get().getStage();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public TutorialStage setAndReturnNextStage(String page) {
		Tutorial tutorial = getTutorial().get();

		TutorialStage currentStage = tutorial.getStage();

		if (currentStage != null) {
			TutorialStage max = tutorial.getMaxReachedStage();

			if (max != null && max.ordinal() > currentStage.ordinal()) {
				TutorialStage[] fallbacks = max.getFallbacks();
				String[] pageEntries = page.split(";");
				String mainPage = pageEntries[0];
				String subPage = pageEntries.length == 2 ? pageEntries[1] : null;

				if (fallbacks != null) {
					boolean foundFallback = false;

					for (TutorialStage fallback : fallbacks) {
						if (fallback.ordinal() > currentStage.ordinal()) {
							String fallBackPageValue = fallback.getPage();

							if (fallBackPageValue == null || (fallBackPageValue.startsWith(mainPage) &&
									((subPage == null || !fallBackPageValue.contains(";")) || fallBackPageValue.endsWith(subPage)))) {
								tutorial.setStage(fallback);
								foundFallback = true;
								break;
							}
						}
					}

					if (!foundFallback) {
						tutorial.setStage(max);
					}
				} else {
					tutorial.setStage(max);
				}
			} else {
				int nextStageIndex = currentStage.ordinal() + 1;
				TutorialStage nextStage = TutorialStage.values()[nextStageIndex];
				tutorial.setStage(nextStage);
			}
		} else {
			tutorial.setStage(TutorialStage.INITIAL_SELECT_HOME_PLANET);
		}

		tutorial = tutorialRepository.save(tutorial);

		return tutorial.getStage();
	}

	public boolean isStage(TutorialStage stage) {
		return getCurrentStage() == stage;
	}

	public String getStageClass(TutorialStage stage) {
		if (getCurrentStage() == stage) {
			return "tutorial-element-stage-" + stage;
		}

		return "";
	}

	public String getPlanetStageClass(long planetId) {
		Optional<Tutorial> opt = getTutorial();

		if (!opt.isPresent()) {
			return "";
		}

		Tutorial tutorial = opt.get();

		if (tutorial.getHomePlanet().getId() == planetId) {
			return getStageClasses(HOME_PLANET_STAGES);
		}

		if (tutorial.getFirstColony().getId() == planetId) {
			return getStageClasses(FIRST_COLONY_STAGES);
		}

		return "";
	}

	public String getShipStageClass(String shipTemplateId) {
		if (!hasTutorial()) {
			return "";
		}

		if (shipTemplateId.equals("orion_2")) {
			return getStageClasses(FREIGHTER_STAGES);
		}

		if (shipTemplateId.equals("orion_14")) {
			return getStageClasses(BOMBER_STAGES);
		}

		return "";
	}

	protected String getStageClasses(List<TutorialStage> stages) {
		StringBuilder result = new StringBuilder();
		stages.forEach(stage -> result.append(" tutorial-element-stage-" + stage + " tutorial-click-element-stage-" + stage));
		return result.toString();
	}

	public boolean hasContinuButton(TutorialStage stage) {
		return HAS_CONTINUE_BUTTON.contains(stage);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void finishTutorial(boolean restart) {
		Optional<Tutorial> opt = getTutorial();

		if (opt.isPresent()) {
			Tutorial tutorial = opt.get();

			Login login = tutorial.getLogin();
			login.setTutorialFinished(true);
			long loginId = login.getId();
			loginRepository.save(login);

			if (!restart) {
				Player player = tutorial.getGame().getPlayers().stream().filter(p -> p.getLogin().getId() == loginId).findFirst().get();
				achievementService.unlockAchievement(player, AchievementType.TUTORIAL_FINISHED);
			}

			gameRemoval.delete(tutorial.getGame());
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String checkTutorialStage(String page) {
		TutorialStage tutorialStage = getCurrentStage();

		String[] pageEntries = page.split(";");
		String mainPage = pageEntries[0];
		String subPage = pageEntries.length == 2 ? pageEntries[1] : null;

		if (revertNecessary(page, mainPage, tutorialStage)) {
			TutorialStage[] fallbacks = tutorialStage.getFallbacks();

			if (fallbacks == null) {
				return tutorialStage.name();
			}

			List<TutorialStage> fittingFallbacks = fittingFallbacks(tutorialStage, fallbacks, mainPage);

			if (!fittingFallbacks.isEmpty()) {
				return revertTutorialStage(tutorialStage, fittingFallbacks.get(0));
			}

			for (TutorialStage fallback : fallbacks) {
				String fallBackPageValue = fallback.getPage();
				String targetPage = fallback.getTargetPage();

				if (fallBackPageValue == null && (!mainPage.equals(targetPage))) {
					return revertTutorialStage(tutorialStage, fallback);
				}

				if (fallBackPageValue != null) {
					String[] parts = fallBackPageValue.split(";");

					if (!parts[0].equals(mainPage)) {
						return revertTutorialStage(tutorialStage, fallback);
					}

					if (subPage != null || parts.length == 1) {
						return revertTutorialStage(tutorialStage, fallback);
					}
				}
			}
		}

		return null;
	}

	protected List<TutorialStage> fittingFallbacks(TutorialStage current, TutorialStage[] fallbacks, String mainPage) {
		List<TutorialStage> fittingFallbacks = new ArrayList<>(fallbacks.length);

		for (TutorialStage fallback : fallbacks) {
			String page = fallback.getPage();

			if (page != null && page.startsWith(mainPage) && (!current.getPage().equals(page))) {
				fittingFallbacks.add(fallback);
			}
		}

		Collections.reverse(fittingFallbacks);
		return fittingFallbacks;
	}

	protected boolean revertNecessary(String page, String mainPage, TutorialStage tutorialStage) {
		String pageData = tutorialStage.getPage();
		return (pageData != null && !pageData.equals(page) && ((pageData.contains(";") && page.contains(";")) || !pageData.startsWith(mainPage))) ||
				(tutorialStage.getTargetPage() != null && !page.equals(tutorialStage.getTargetPage()));
	}

	protected String revertTutorialStage(TutorialStage tutorialStage, TutorialStage fallback) {
		revertToStage(tutorialStage, fallback);
		return fallback.name();
	}

	protected void revertToStage(TutorialStage previous, TutorialStage stage) {
		Tutorial tutorial = getTutorial().get();
		tutorial.setStage(stage);

		tutorial.setMaxReachedStage(previous);

		if (previous == TutorialStage.SELECT_SHIP_CONSTRUCTON_FREIGHTER || previous == TutorialStage.BUILD_SHIP_CONSTRUCTON_FREIGHTER) {
			tutorial.setMaxReachedStage(TutorialStage.OPEN_SHIP_CONSTRUCTON_FREIGHTER);
		} else if (previous == TutorialStage.SELECT_SHIP_CONSTRUCTON_BOMBER || previous == TutorialStage.BUILD_SHIP_CONSTRUCTON_BOMBER) {
			tutorial.setMaxReachedStage(TutorialStage.OPEN_SHIP_CONSTRUCTON_BOMBER);
		}

		tutorialRepository.save(tutorial);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void revertToTutorialStage(TutorialStage stage) {
		Tutorial tutorial = getTutorial().get();
		tutorial.setMaxReachedStage(tutorial.getStage());
		tutorial.setStage(stage);

		if (stage == TutorialStage.ACTIVATE_FREIGHTER_COURSE_MODE_FIRST || stage == TutorialStage.SET_COURSE_TO_ENEMY) {
			tutorial.setMaxReachedStage(stage);
		}

		tutorialRepository.save(tutorial);
	}
}