package org.skrupeltng.modules.dashboard.service;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.controller.DeathFoeTeam;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.database.FogOfWarType;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.StableWormholeConfig;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoe;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoeRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLog;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class GameStartingHelper {

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private WormHoleRepository wormHoleRepository;

	@Autowired
	private PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Autowired
	private PlayerDeathFoeRepository playerDeathFoeRepository;

	@Autowired
	private DashboardService dashboardService;

	@Autowired
	private StatsUpdater statsUpdater;

	@Autowired
	private PlanetGenerator planetGenerator;

	@Autowired
	private StartPositionHelper startPositionHelper;

	@Autowired
	private UserDetailServiceImpl userService;

	@Autowired
	private ConfigProperties configProperties;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void startGame(long gameId) {
		Game game = gameRepository.getOne(gameId);

		if (game.getCreator().getId() != userService.getLoginId()) {
			throw new IllegalArgumentException("Only game creators can start games!");
		}

		if (game.getWinCondition() != WinCondition.NONE && game.getPlayers().size() < 2) {
			throw new IllegalStateException("The game mode " + game.getWinCondition() + " requires at least 2 players!");
		}

		if (game.isStarted()) {
			throw new IllegalStateException("The game is already started!");
		}

		game.setStarted(true);
		game.setRoundDate(new Date());

		planetGenerator.generatePlanets(game);

		assignStartPositions(game);
		assignPlayerColors(game);
		processLoginStats(game);

		if (game.getFogOfWarType() == FogOfWarType.LONG_RANGE_SENSORS) {
			initLongRangeSensorPlanetLogs(gameId);
		}

		if (game.getWinCondition() == WinCondition.DEATH_FOE) {
			assignDeathFoes(game);
		} else if (game.getWinCondition() == WinCondition.TEAM_DEATH_FOE) {
			assignTeamDeathFoes(game);
		}

		createUnstableWormholes(game);
		createStableWormholes(game);

		gameRepository.save(game);
	}

	public void initLongRangeSensorPlanetLogs(long gameId) {
		List<Planet> planets = planetRepository.findByGameId(gameId);
		List<Planet> ownedPlanets = planetRepository.findOwnedPlanetsByGameId(gameId);

		for (Planet ownedPlanet : ownedPlanets) {
			planets.stream()
					.filter(p -> CoordHelper.getDistance(p, ownedPlanet) <= 250)
					.forEach(p -> {
						PlayerPlanetScanLog log = new PlayerPlanetScanLog();
						log.setPlanet(p);
						log.setPlayer(ownedPlanet.getPlayer());
						playerPlanetScanLogRepository.save(log);
					});
		}
	}

	protected void assignStartPositions(Game game) {
		List<Player> players = new ArrayList<>(game.getPlayers());
		Collections.sort(players);
		List<Planet> planets = new ArrayList<>(game.getPlanets());
		Collections.sort(planets);

		switch (game.getStartPositionSetup()) {
			case EQUAL_DISTANCE:
				startPositionHelper.assignStartPositionsEqualDistance(game, players, planets);
				break;
			case RANDOM:
				startPositionHelper.assignStartPositionsRandom(game, players, planets);
				break;
		}
	}

	private void assignPlayerColors(Game game) {
		List<Player> players = game.getPlayers();

		float interval = 1f / players.size();
		float hue = 0f;

		for (Player player : players) {
			Color cc = Color.getHSBColor(hue, 1f, 1f);
			String colorString = String.format("%02x%02x%02x", cc.getRed(), cc.getGreen(), cc.getBlue());
			player.setColor(colorString);
			hue += interval;
		}

		playerRepository.saveAll(players);
	}

	private void processLoginStats(Game game) {
		List<Player> players = game.getPlayers();

		for (Player player : players) {
			if (player.getLogin().getId() == game.getCreator().getId()) {
				statsUpdater.incrementStats(player, LoginStatsFaction::getGamesCreated, LoginStatsFaction::setGamesCreated);
			}

			statsUpdater.incrementStats(player, LoginStatsFaction::getGamesPlayed, LoginStatsFaction::setGamesPlayed);
		}
	}

	private void createUnstableWormholes(Game game) {
		int galaxySize = game.getGalaxySize();

		for (int i = 0; i < game.getUnstableWormholeCount(); i++) {
			while (true) {
				int x = createRandomCoordinate(galaxySize);
				int y = createRandomCoordinate(galaxySize);
				Optional<WormHole> resultOpt = checkWormHoleCreation(game, x, y);

				if (resultOpt.isPresent()) {
					WormHole wormhole = resultOpt.get();
					wormhole.setType(WormHoleType.UNSTABLE_WORMHOLE);
					wormhole = wormHoleRepository.save(wormhole);
					break;
				}
			}
		}
	}

	private void createStableWormholes(Game game) {
		StableWormholeConfig stableWormholeConfig = game.getStableWormholeConfig();
		int number = stableWormholeConfig.getNumber();

		for (int i = 0; i < number; i++) {
			WormHole first = createStableWormhole(game, stableWormholeConfig, true, i);
			WormHole second = createStableWormhole(game, stableWormholeConfig, false, i);

			first.setConnection(second);
			wormHoleRepository.save(first);

			second.setConnection(first);
			wormHoleRepository.save(second);
		}
	}

	private WormHole createStableWormhole(Game game, StableWormholeConfig stableWormholeConfig, boolean isFirst, int i) {
		WormHole wormHole = null;
		int galaxySize = game.getGalaxySize();

		while (true) {
			int x = 0;
			int y = 0;

			switch (stableWormholeConfig) {
				case NONE:
				case ONE_RANDOM:
				case TWO_RANDOM:
				case THREE_RANDOM:
				case FOUR_RANDOM:
				case FIVE_RANDOM:
					x = createRandomCoordinate(galaxySize);
					y = createRandomCoordinate(galaxySize);
					break;
				case ONE_NORTH_SOUTH:
				case TWO_NORTH_SOUTH:
					x = createRandomCoordinate(galaxySize);

					if (isFirst) {
						y = getNorthY(galaxySize);
					} else {
						y = getSouthY(galaxySize);
					}
					break;
				case ONE_WEST_EAST:
				case TWO_WEST_EAST:
					y = createRandomCoordinate(galaxySize);

					if (isFirst) {
						x = getWestX(galaxySize);
					} else {
						x = getEastX(galaxySize);
					}
					break;
				case ONE_NORTH_WEST_EAST_SOUTH:
					if (i == 0) {
						x = createRandomCoordinate(galaxySize);

						if (isFirst) {
							y = getNorthY(galaxySize);
						} else {
							y = getSouthY(galaxySize);
						}
					} else {
						y = createRandomCoordinate(galaxySize);

						if (isFirst) {
							x = getWestX(galaxySize);
						} else {
							x = getEastX(galaxySize);
						}
					}
					break;
				case ONE_NORTHWEST_SOUTHEAST:
					if (isFirst) {
						x = getWestX(galaxySize);
						y = getNorthY(galaxySize);
					} else {
						x = getEastX(galaxySize);
						y = getSouthY(galaxySize);
					}
					break;
				case ONE_NORTHEAST_SOUTHWEST:
					if (isFirst) {
						x = getEastX(galaxySize);
						y = getNorthY(galaxySize);
					} else {
						x = getWestX(galaxySize);
						y = getSouthY(galaxySize);
					}
					break;
				case TWO_DIAGONAL:
					if (i == 0) {
						if (isFirst) {
							x = getWestX(galaxySize);
							y = getNorthY(galaxySize);
						} else {
							x = getEastX(galaxySize);
							y = getSouthY(galaxySize);
						}
					} else {
						if (isFirst) {
							x = getEastX(galaxySize);
							y = getNorthY(galaxySize);
						} else {
							x = getWestX(galaxySize);
							y = getSouthY(galaxySize);
						}
					}
					break;
			}

			Optional<WormHole> resultOpt = checkWormHoleCreation(game, x, y);

			if (resultOpt.isPresent()) {
				wormHole = resultOpt.get();
				wormHole.setType(WormHoleType.STABLE_WORMHOLE);
				wormHole = wormHoleRepository.save(wormHole);
				break;
			}
		}

		return wormHole;
	}

	private int max(int galaxySize) {
		return galaxySize - configProperties.getMinGalaxyMapBorder();
	}

	private int half(int galaxySize) {
		return (galaxySize / 2) - configProperties.getMinGalaxyMapBorder();
	}

	private int createRandomCoordinate(int galaxySize) {
		return configProperties.getMinGalaxyMapBorder() + MasterDataService.RANDOM.nextInt(max(galaxySize));
	}

	private int getNorthY(int galaxySize) {
		return configProperties.getMinGalaxyMapBorder() + MasterDataService.RANDOM.nextInt(half(galaxySize));
	}

	private int getSouthY(int galaxySize) {
		int half = half(galaxySize);
		return half + MasterDataService.RANDOM.nextInt(half);
	}

	private int getWestX(int galaxySize) {
		return configProperties.getMinGalaxyMapBorder() + MasterDataService.RANDOM.nextInt(half(galaxySize));
	}

	private int getEastX(int galaxySize) {
		int half = half(galaxySize);
		return half + MasterDataService.RANDOM.nextInt(half);
	}

	private Optional<WormHole> checkWormHoleCreation(Game game, int x, int y) {
		int minAnomalyDistance = configProperties.getMinAnomalyDistance();
		List<Long> planets = planetRepository.getPlanetIdsInRadius(game.getId(), x, y, minAnomalyDistance, false);

		if (planets.isEmpty()) {
			List<Long> wormHoles = wormHoleRepository.getWormHoleIdsInRadius(game.getId(), x, y, minAnomalyDistance);

			if (wormHoles.isEmpty()) {
				WormHole wormhole = new WormHole();
				wormhole.setGame(game);
				wormhole.setX(x);
				wormhole.setY(y);
				return Optional.of(wormhole);
			}
		}

		return Optional.empty();
	}

	private void assignDeathFoes(Game game) {
		List<Player> players = game.getPlayers();

		for (Player player : players) {
			List<Player> foes = new ArrayList<>(players);
			foes.remove(player);

			Player deathFoe = foes.get(MasterDataService.RANDOM.nextInt(foes.size()));
			PlayerDeathFoe playerDeathFoe = new PlayerDeathFoe();
			playerDeathFoe.setPlayer(player);
			playerDeathFoe.setDeathFoe(deathFoe);
			playerDeathFoeRepository.save(playerDeathFoe);
		}
	}

	private void assignTeamDeathFoes(Game game) {
		List<DeathFoeTeam> teams = dashboardService.getDeathFoeTeams(game.getId());

		for (DeathFoeTeam team : teams) {
			List<DeathFoeTeam> foes = new ArrayList<>(teams);
			foes.remove(team);
			DeathFoeTeam enemyTeam = foes.get(MasterDataService.RANDOM.nextInt(foes.size()));

			List<Player> enemyPlayers = enemyTeam.getPlayers();
			List<Player> players = team.getPlayers();

			for (Player player : players) {
				for (Player deathFoe : enemyPlayers) {
					PlayerDeathFoe playerDeathFoe = new PlayerDeathFoe();
					playerDeathFoe.setPlayer(player);
					playerDeathFoe.setDeathFoe(deathFoe);
					playerDeathFoeRepository.save(playerDeathFoe);
				}
			}
		}
	}
}