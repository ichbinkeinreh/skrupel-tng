package org.skrupeltng.modules.dashboard.modules.notification.database;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface NotificationRepository extends JpaRepository<Notification, Long> {

	@Modifying
	@Query("DELETE FROM Notification n WHERE n.login.id = ?1")
	void deleteByLoginId(long loginId);

	@Query("SELECT n FROM Notification n WHERE n.login.id = ?1")
	Page<Notification> findByLoginId(long loginId, Pageable page);

	@Query("SELECT COUNT(n) FROM Notification n WHERE n.login.id = ?1 AND n.read = false")
	long getUnreadCountByLoginId(long loginId);

	@Modifying
	@Query("UPDATE Notification n SET n.read = true WHERE n.login.id = ?1")
	void markAllAsRead(long loginId);

	@Modifying
	@Query("UPDATE Notification n SET n.read = true WHERE n.id IN (?1)")
	void markAsRead(Collection<Long> notificationIds);

	@Query("SELECT n FROM Notification n WHERE n.login.id = ?1 AND n.read = false AND n.desktop = true")
	List<Notification> getUnreadDesktopNotifications(long loginId);
}