package org.skrupeltng.modules.dashboard.modules.notification.controller;

import java.io.Serializable;

public class DesktopNotificationDTO implements Serializable {

	private static final long serialVersionUID = -7299703385560693889L;

	private String message;
	private String link;

	public DesktopNotificationDTO() {

	}

	public DesktopNotificationDTO(String message, String link) {
		this.message = message;
		this.link = link;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
}