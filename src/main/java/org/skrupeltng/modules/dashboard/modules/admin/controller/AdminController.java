package org.skrupeltng.modules.dashboard.modules.admin.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.SortField;
import org.skrupeltng.modules.SortFieldItem;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.modules.admin.service.AdminService;
import org.skrupeltng.modules.dashboard.service.DebugGameCreator;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin")
public class AdminController extends AbstractController {

	@Autowired
	private AdminService adminService;

	@Autowired
	private DebugGameCreator debugGameCreator;

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private ConfigProperties configProperties;

	private final List<SortFieldItem> existingGamesSortItems = new ArrayList<>();

	@PostConstruct
	public void init() {
		addToExistingGamesSortItems(SortField.USERS_USERNAME);
		addToExistingGamesSortItems(SortField.USERS_GAMES_CREATED);
		addToExistingGamesSortItems(SortField.USERS_GAMES_PLAYED);
		addToExistingGamesSortItems(SortField.USERS_GAMES_WON);
		addToExistingGamesSortItems(SortField.USERS_GAMES_LOST);
	}

	private void addToExistingGamesSortItems(SortField sortField) {
		existingGamesSortItems.add(new SortFieldItem(sortField, true));
		existingGamesSortItems.add(new SortFieldItem(sortField, false));
	}

	@GetMapping("/users")
	public String getUsers(
			@RequestParam(required = false) String username,
			@RequestParam(required = false) Boolean tutorial,
			@RequestParam(required = false) Boolean createdGames,
			@RequestParam(required = false) Boolean playedGames,

			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) String sortField,
			@RequestParam(required = false) Boolean sortDirection,
			Model model) {

		UserSearchParameters params = new UserSearchParameters();
		params.setUsername(username);
		params.setTutorial(tutorial);
		params.setCreatedGames(createdGames);
		params.setPlayedGames(playedGames);

		Pageable pageRequest = createPageable(page, 20, sortField, sortDirection);

		Page<UserListResultDTO> results = adminService.searchUsers(params, pageRequest);
		model.addAttribute("results", results);
		model.addAttribute("sortItems", existingGamesSortItems);

		return "dashboard/admin/users";
	}

	@DeleteMapping("/user")
	@ResponseBody
	public void deleteUser(@RequestParam long loginId) {
		adminService.deleteUser(loginId);
	}

	@GetMapping("/data-privacy-statements")
	public String getDataPrivacyStatements(Model model) {
		DataPrivacyStatementsData data = adminService.getDataPrivacyStatementsData();
		model.addAttribute("request", data);
		addLanguages(model);
		return "dashboard/admin/data-privacy-statements";
	}

	@PostMapping("/data-privacy-statements")
	public String changeDataPrivacyStatements(@Valid @ModelAttribute("request") DataPrivacyStatementsData request, BindingResult bindingResult, Model model) {
		addLanguages(model);

		if (bindingResult.hasErrors()) {
			return "dashboard/admin/data-privacy-statements";
		}

		adminService.changeDataPrivacyStatements(request);

		return "redirect:data-privacy-statements";
	}

	private void addLanguages(Model model) {
		model.addAttribute("english", Locale.ENGLISH.getDisplayLanguage());
		model.addAttribute("german", Locale.GERMAN.getDisplayLanguage());
	}

	@GetMapping("/legal")
	public String getLegalText(Model model) {
		model.addAttribute("request", adminService.getLegalText());
		return "dashboard/admin/legal";
	}

	@PostMapping("/legal")
	public String changeLegalText(@Valid @ModelAttribute("request") LegalTextData request, BindingResult bindingResult, Model model) {
		adminService.changeLegalTextData(request);
		return "redirect:legal";
	}

	@GetMapping("/debug-game")
	public String createDebugGame() {
		if (!configProperties.isDebug()) {
			return "redirect:/";
		}

		long adminLoginId = userDetailService.getLoginId();
		Game game = debugGameCreator.createDebugGame(adminLoginId);
		long gameId = game.getId();
		debugGameCreator.setFactionForAdminPlayer(gameId);

		List<String> allFactionIds = masterDataService.getAllFactionIds();

		for (int i = 1; i < game.getPlayerCount(); i++) {
			String factionId = allFactionIds.get(i % (game.getPlayerCount() / 2));
			long playerId = debugGameCreator.addAIPlayerToDebugGame(gameId, AILevel.AI_HARD);
			debugGameCreator.setPlayerFactionForDebugGame(playerId, factionId);
		}

		debugGameCreator.startDebugGame(gameId);

		return "redirect:/ingame/game?id=" + gameId;
	}
}