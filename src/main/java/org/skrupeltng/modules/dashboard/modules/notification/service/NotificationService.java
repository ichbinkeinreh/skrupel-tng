package org.skrupeltng.modules.dashboard.modules.notification.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.modules.notification.controller.DesktopNotificationDTO;
import org.skrupeltng.modules.dashboard.modules.notification.database.Notification;
import org.skrupeltng.modules.dashboard.modules.notification.database.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("notificationService")
public class NotificationService {

	@Autowired
	private NotificationRepository notificationRepository;

	@Autowired
	private UserDetailServiceImpl userDetailService;

	@Autowired
	private MessageSource messageSource;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void addNotification(Login login, String link, String messageKey, String... args) {
		addNotification(login, link, messageKey, true, args);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void addNotification(Login login, String link, String messageKey, boolean sendDesktopNotification, String... args) {
		Notification notification = new Notification();
		notification.setCreated(new Date());
		notification.setLogin(login);
		notification.setLink(link);

		Locale locale = Locale.forLanguageTag(login.getLanguage());
		String message = messageSource.getMessage(messageKey, args, messageKey, locale);
		notification.setMessage(message);
		notification.setDesktop(sendDesktopNotification);

		notificationRepository.save(notification);
	}

	public long getUnreadNotificationCount() {
		long loginId = userDetailService.getLoginId();
		return notificationRepository.getUnreadCountByLoginId(loginId);
	}

	public List<Notification> getAllNotifications() {
		long loginId = userDetailService.getLoginId();
		Pageable pageRequest = PageRequest.of(0, 25, Direction.DESC, "created");
		return notificationRepository.findByLoginId(loginId, pageRequest).getContent();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void markAllAsRead() {
		long loginId = userDetailService.getLoginId();
		notificationRepository.markAllAsRead(loginId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String markAsRead(long notificationId) {
		long loginId = userDetailService.getLoginId();
		Notification notification = notificationRepository.getOne(notificationId);

		if (notification.getLogin().getId() != loginId) {
			throw new IllegalArgumentException("This notification does not belong to you!");
		}

		notification.setRead(true);
		notification = notificationRepository.save(notification);

		return notification.getLink();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public List<DesktopNotificationDTO> getUnreadDesktopNotfications(long loginId) {
		List<Notification> notifications = notificationRepository.getUnreadDesktopNotifications(loginId);

		if (notifications.isEmpty()) {
			return null;
		}

		List<DesktopNotificationDTO> dtos = new ArrayList<>(notifications.size());

		for (Notification notification : notifications) {
			dtos.add(new DesktopNotificationDTO(notification.getMessage(), notification.getLink()));
		}

		return dtos;
	}
}