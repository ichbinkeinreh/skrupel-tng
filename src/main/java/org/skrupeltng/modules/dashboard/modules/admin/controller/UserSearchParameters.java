package org.skrupeltng.modules.dashboard.modules.admin.controller;

public class UserSearchParameters {

	private String username;
	private Boolean tutorial;
	private Boolean createdGames;
	private Boolean playedGames;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Boolean getTutorial() {
		return tutorial;
	}

	public void setTutorial(Boolean tutorial) {
		this.tutorial = tutorial;
	}

	public Boolean getCreatedGames() {
		return createdGames;
	}

	public void setCreatedGames(Boolean createdGames) {
		this.createdGames = createdGames;
	}

	public Boolean getPlayedGames() {
		return playedGames;
	}

	public void setPlayedGames(Boolean playedGames) {
		this.playedGames = playedGames;
	}
}