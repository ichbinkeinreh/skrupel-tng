package org.skrupeltng.modules.dashboard.modules.admin.controller;

import java.io.Serializable;

import org.skrupeltng.modules.PageableResult;

public class UserListResultDTO implements Serializable, PageableResult {

	private static final long serialVersionUID = 4460144974169470550L;

	private long id;
	private String username;

	private int gamesCreated;
	private int gamesPlayed;
	private int gamesWon;
	private int gamesLost;

	private String tutorialStage;
	private boolean tutorialFinished;

	private int totalElements;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getGamesCreated() {
		return gamesCreated;
	}

	public void setGamesCreated(int gamesCreated) {
		this.gamesCreated = gamesCreated;
	}

	public int getGamesPlayed() {
		return gamesPlayed;
	}

	public void setGamesPlayed(int gamesPlayed) {
		this.gamesPlayed = gamesPlayed;
	}

	public int getGamesWon() {
		return gamesWon;
	}

	public void setGamesWon(int gamesWon) {
		this.gamesWon = gamesWon;
	}

	public int getGamesLost() {
		return gamesLost;
	}

	public void setGamesLost(int gamesLost) {
		this.gamesLost = gamesLost;
	}

	public String getTutorialStage() {
		return tutorialStage;
	}

	public void setTutorialStage(String tutorialStage) {
		this.tutorialStage = tutorialStage;
	}

	public boolean isTutorialFinished() {
		return tutorialFinished;
	}

	public void setTutorialFinished(boolean tutorialFinished) {
		this.tutorialFinished = tutorialFinished;
	}

	@Override
	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}
}