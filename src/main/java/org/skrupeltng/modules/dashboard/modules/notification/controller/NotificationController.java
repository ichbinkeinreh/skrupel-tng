package org.skrupeltng.modules.dashboard.modules.notification.controller;

import java.util.List;

import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/notifications")
public class NotificationController extends AbstractController {

	@Autowired
	private NotificationService notificationService;

	@GetMapping("/unread-notification-count")
	@ResponseBody
	public UnreadNotificationResponseDTO getUnreadNotificationCount() {
		long unreadCount = notificationService.getUnreadNotificationCount();

		UnreadNotificationResponseDTO response = new UnreadNotificationResponseDTO();
		response.setUnreadCount(unreadCount);

		if (unreadCount > 0L) {
			List<DesktopNotificationDTO> list = notificationService.getUnreadDesktopNotfications(userDetailService.getLoginId());
			response.setDesktopNotifications(list);
		}

		return response;
	}

	@GetMapping("/dropdown")
	public String getNotificationsDropdown() {
		return "dashboard/layout::notifications";
	}

	@PostMapping("")
	@ResponseBody
	public void markAllAsRead() {
		notificationService.markAllAsRead();
	}

	@PostMapping("/{notificationId}")
	@ResponseBody
	public String markAsRead(@PathVariable(name = "notificationId") long notificationId) {
		return notificationService.markAsRead(notificationId);
	}
}