package org.skrupeltng.modules.dashboard.modules.notification.controller;

import java.io.Serializable;
import java.util.List;

public class UnreadNotificationResponseDTO implements Serializable {

	private static final long serialVersionUID = -6283704921892048533L;

	private long unreadCount;
	private List<DesktopNotificationDTO> desktopNotifications;

	public long getUnreadCount() {
		return unreadCount;
	}

	public void setUnreadCount(long unreadCount) {
		this.unreadCount = unreadCount;
	}

	public List<DesktopNotificationDTO> getDesktopNotifications() {
		return desktopNotifications;
	}

	public void setDesktopNotifications(List<DesktopNotificationDTO> desktopNotifications) {
		this.desktopNotifications = desktopNotifications;
	}
}