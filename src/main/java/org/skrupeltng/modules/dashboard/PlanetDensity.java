package org.skrupeltng.modules.dashboard;

public enum PlanetDensity {

	VERY_HIGH(4),

	HIGH(6),

	MEDIUM(10),

	LOW(15),

	VERY_LOW(20);

	private final int factor;

	private PlanetDensity(int factor) {
		this.factor = factor;
	}

	public int getFactor() {
		return factor;
	}
}