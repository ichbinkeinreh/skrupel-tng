package org.skrupeltng.modules.masterdata.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.random.MersenneTwister;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ShipAbilityDescriptionMapper;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpecies;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetType;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetTypeRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.starbase.controller.StarbaseUpgradeLevel;
import org.skrupeltng.modules.masterdata.ShipFactionItemMapper;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.FactionRepository;
import org.skrupeltng.modules.masterdata.database.I18N;
import org.skrupeltng.modules.masterdata.database.I18NRepository;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityRepository;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.database.ShipTemplateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

@Service("masterDataService")
public class MasterDataService {

	public static final MersenneTwister RANDOM = new MersenneTwister();

	public static final String FACTION_DESCRIPTION_I18N_KEY_PREFIX = "description_";

	private static final String FACTION_DATA_FILE = "/data.yml";
	private static final String FACTION_TEXTS_FILE = "/texts.yml";
	private static final String SHIP_DATA_FILE = "/ships.yml";

	private static final List<String> DEFAULT_FACTIONS = Lists.newArrayList("orion", "kuatoh", "trodan", "nightmare", "silverstarag");

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final Map<Integer, float[]> FUEL_CONSUMPTION = new HashMap<>(10);
	private final Map<StarbaseUpgradeType, List<Integer>> starbaseUpgradeCosts = new HashMap<>(StarbaseUpgradeType.values().length);
	private final int[] NECESSARY_MINES = new int[] { 10, 6, 4, 2, 1 };
	private final Set<Integer> PROPULSION_WITH_TRACTOR_BEAM = Sets.newHashSet(3, 4, 5, 6, 7);

	private final ObjectMapper yamlMapper = new ObjectMapper(new YAMLFactory());

	@Autowired
	private FactionRepository factionRepository;

	@Autowired
	private I18NRepository i18nRepository;

	@Autowired
	private ShipTemplateRepository shipTemplateRepository;

	@Autowired
	private ShipAbilityRepository shipAbilityRepository;

	@Autowired
	private PlanetTypeRepository planetTypeRepository;

	@Autowired
	private NativeSpeciesRepository nativeSpeciesRepository;

	@Autowired
	private ShipFactionItemMapper shipFactionItemMapper;

	@Autowired
	private ShipAbilityDescriptionMapper shipAbilityDescriptionMapper;

	@Autowired
	private ConfigProperties configProperties;

	@PostConstruct
	public void loadFiles() throws Exception {
		long randomnessSeed = configProperties.getRandomnessSeed();

		if (randomnessSeed != 0L) {
			RANDOM.setSeed(randomnessSeed);
		}

		loadDefaultFaction();
		loadFaction();
		initStarbaseCosts();
		initFuelConsumptionData();
	}

	private void loadDefaultFaction() throws Exception {
		for (String factionfolder : DEFAULT_FACTIONS) {
			log.info("Updating " + factionfolder + "...");

			String dir = "/static/factions/";

			InputStream dataStream = getClass().getResourceAsStream(dir + factionfolder + FACTION_DATA_FILE);
			InputStream textsStream = getClass().getResourceAsStream(dir + factionfolder + FACTION_TEXTS_FILE);
			InputStream shipsStream = getClass().getResourceAsStream(dir + factionfolder + SHIP_DATA_FILE);

			addFaction(factionfolder, dataStream, textsStream, shipsStream);
		}

		log.info(DEFAULT_FACTIONS.size() + " factions updated.");
	}

	private void loadFaction() throws Exception {
		String factionFolderName = "factions";
		File factionFolder = new File(factionFolderName);

		if (!factionFolder.exists() || !factionFolder.isDirectory()) {
			return;
		}

		String[] folders = factionFolder.list((f, n) -> !n.startsWith("."));

		for (String factionfolder : folders) {
			log.info("Updating faction in folder " + factionfolder + "...");

			InputStream dataStream = new FileInputStream(factionFolderName + "/" + factionfolder + FACTION_DATA_FILE);
			InputStream textsStream = new FileInputStream(factionFolderName + "/" + factionfolder + FACTION_TEXTS_FILE);
			InputStream shipsStream = new FileInputStream(factionFolderName + "/" + factionfolder + SHIP_DATA_FILE);

			addFaction(factionfolder, dataStream, textsStream, shipsStream);
		}
	}

	@SuppressWarnings("unchecked")
	private void addFaction(String factionfolder, InputStream dataStream, InputStream textsStream, InputStream shipsStream) throws Exception {
		Faction faction = yamlMapper.readValue(dataStream, Faction.class);
		faction.setForbiddenOrbitalSystems(getOrbitalSystems(faction.getForbiddenOrbitalSystems()));
		faction.setUnlockedOrbitalSystems(getOrbitalSystems(faction.getUnlockedOrbitalSystems()));
		faction = factionRepository.save(faction);

		String factionId = faction.getId();

		Map<String, Object> i18nData = yamlMapper.readValue(textsStream, Map.class);
		Map<String, String> factionNameData = (Map<String, String>)i18nData.get("factionName");

		for (Entry<String, String> factionNameEntry : factionNameData.entrySet()) {
			String language = factionNameEntry.getKey();
			String factionName = factionNameEntry.getValue();

			I18N i18n = i18nRepository.findByKeyAndLanguage(factionId, language).orElseGet(() -> new I18N(factionId, language));
			i18n.setValue(factionName);
			i18nRepository.save(i18n);
		}

		Map<String, String> factionDescriptionData = (Map<String, String>)i18nData.get("description");
		String descriptionI18NKey = FACTION_DESCRIPTION_I18N_KEY_PREFIX + factionId;

		for (Entry<String, String> factionDescriptionEntry : factionDescriptionData.entrySet()) {
			String language = factionDescriptionEntry.getKey();
			String description = factionDescriptionEntry.getValue();

			I18N i18n = i18nRepository.findByKeyAndLanguage(descriptionI18NKey, language).orElseGet(() -> new I18N(descriptionI18NKey, language));
			i18n.setValue(description);
			i18nRepository.save(i18n);
		}

		CollectionType shipItemType = yamlMapper.getTypeFactory().constructCollectionType(List.class, ShipFactionItem.class);
		List<ShipFactionItem> shipItems = yamlMapper.readValue(shipsStream, shipItemType);

		String shipTemplateIdPrefix = factionId.replace(" ", "_").replace("(", "").replace(")", "") + "_";

		for (ShipFactionItem shipItem : shipItems) {
			ShipTemplate shipTemplate = shipFactionItemMapper.newShipTemplate(shipItem);

			String id = shipTemplateIdPrefix + shipTemplate.getId();
			shipTemplate.setId(id);
			shipTemplate.setImage(shipItem.getId() + ".jpg");
			shipTemplate.setFaction(faction);
			shipTemplate = shipTemplateRepository.save(shipTemplate);

			for (Map<String, String> shipNameItem : shipItem.getName()) {
				Entry<String, String> entry = shipNameItem.entrySet().iterator().next();
				String language = entry.getKey();
				String shipName = entry.getValue();

				I18N i18n = i18nRepository.findByKeyAndLanguage(id, language).orElseGet(() -> new I18N(id, language));
				i18n.setValue(shipName);
				i18nRepository.save(i18n);
			}

			if (shipItem.getAbilities() != null) {
				for (ShipFactionAbilityItem abilityItem : shipItem.getAbilities()) {
					ShipAbilityType shipAbilityType = ShipAbilityType.valueOf(abilityItem.getName());

					ShipAbility shipAbility = shipAbilityRepository.findByShipTemplateIdAndType(id, shipAbilityType)
							.orElseGet(() -> new ShipAbility(shipAbilityType));

					Map<String, String> values = new HashMap<>();

					if (CollectionUtils.isNotEmpty(abilityItem.getParams())) {
						for (Map<String, String> param : abilityItem.getParams()) {
							values.putAll(param);
						}
					}

					shipAbility.setValues(values);
					shipAbility.setShipTemplate(shipTemplate);
					shipAbilityRepository.save(shipAbility);
				}
			}
		}
	}

	private String getOrbitalSystems(String line) {
		if (StringUtils.isNotBlank(line)) {
			return line.replace(":", ",");
		}

		return null;
	}

	private void initStarbaseCosts() {
		List<Integer> starbaseHullUpgradeCosts = new ArrayList<>(10);
		List<Integer> starbasePropulsionUpgradeCosts = new ArrayList<>(10);
		List<Integer> starbaseWeaponUpgradeCosts = new ArrayList<>(10);

		starbaseUpgradeCosts.put(StarbaseUpgradeType.HULL, starbaseHullUpgradeCosts);
		starbaseUpgradeCosts.put(StarbaseUpgradeType.PROPULSION, starbasePropulsionUpgradeCosts);
		starbaseUpgradeCosts.put(StarbaseUpgradeType.ENERGY, starbaseWeaponUpgradeCosts);
		starbaseUpgradeCosts.put(StarbaseUpgradeType.PROJECTILE, starbaseWeaponUpgradeCosts);

		starbaseHullUpgradeCosts.add(100);
		starbaseHullUpgradeCosts.add(200);
		starbaseHullUpgradeCosts.add(300);
		starbaseHullUpgradeCosts.add(800);
		starbaseHullUpgradeCosts.add(1000);
		starbaseHullUpgradeCosts.add(1200);
		starbaseHullUpgradeCosts.add(2500);
		starbaseHullUpgradeCosts.add(5000);
		starbaseHullUpgradeCosts.add(7500);
		starbaseHullUpgradeCosts.add(10000);

		starbasePropulsionUpgradeCosts.add(100);
		starbasePropulsionUpgradeCosts.add(200);
		starbasePropulsionUpgradeCosts.add(300);
		starbasePropulsionUpgradeCosts.add(400);
		starbasePropulsionUpgradeCosts.add(500);
		starbasePropulsionUpgradeCosts.add(600);
		starbasePropulsionUpgradeCosts.add(700);
		starbasePropulsionUpgradeCosts.add(4000);
		starbasePropulsionUpgradeCosts.add(7000);
		starbasePropulsionUpgradeCosts.add(10000);

		starbaseWeaponUpgradeCosts.add(100);
		starbaseWeaponUpgradeCosts.add(400);
		starbaseWeaponUpgradeCosts.add(900);
		starbaseWeaponUpgradeCosts.add(1600);
		starbaseWeaponUpgradeCosts.add(2500);
		starbaseWeaponUpgradeCosts.add(3600);
		starbaseWeaponUpgradeCosts.add(4900);
		starbaseWeaponUpgradeCosts.add(6400);
		starbaseWeaponUpgradeCosts.add(8100);
		starbaseWeaponUpgradeCosts.add(10000);
	}

	private void initFuelConsumptionData() {
		FUEL_CONSUMPTION.put(1, new float[] { 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f });
		FUEL_CONSUMPTION.put(2, new float[] { 0f, 100f, 107.5f, 300f, 400f, 500f, 600f, 700f, 800f, 900f });
		FUEL_CONSUMPTION.put(3, new float[] { 0f, 100f, 106.25f, 107.78f, 337.5f, 500f, 600f, 700f, 800f, 900f });
		FUEL_CONSUMPTION.put(4, new float[] { 0f, 100f, 103.75f, 104.44f, 106.25f, 300f, 322.22f, 495.92f, 487.5f, 900f });
		FUEL_CONSUMPTION.put(5, new float[] { 0f, 100f, 103.75f, 104.44f, 106.25f, 104f, 291.67f, 291.84f, 366.41f, 900f });
		FUEL_CONSUMPTION.put(6, new float[] { 0f, 100f, 103.75f, 104.44f, 106.25f, 104f, 103.69f, 251.02f, 335.16f, 900f });
		FUEL_CONSUMPTION.put(7, new float[] { 0f, 100f, 103.75f, 104.44f, 106.25f, 104f, 103.69f, 108.16f, 303.91f, 529.63f });
		FUEL_CONSUMPTION.put(9, new float[] { 0f, 100f, 100f, 100f, 100f, 100f, 100f, 102.04f, 109.38f, 529.63f });
		FUEL_CONSUMPTION.put(10, new float[] { 0f, 100f, 100f, 100f, 100f, 100f, 100f, 100f, 100f, 100f });
	}

	public List<String> getAllFactionIds() {
		return factionRepository.getAllFactionIds();
	}

	public Faction getFactionData(String faction) {
		return factionRepository.getOne(faction);
	}

	public List<Faction> getAllFactions() {
		return factionRepository.findAll(Sort.by(Direction.ASC, "id"));
	}

	public List<NativeSpecies> getAllNativeSpecies() {
		return nativeSpeciesRepository.findAll(Sort.by(Direction.ASC, "name"));
	}

	public List<PlanetType> getAllPlanetTypes() {
		return planetTypeRepository.findAll(Sort.by(Direction.ASC, "id"));
	}

	public List<StarbaseUpgradeLevel> getLevelCosts(int startLevel, StarbaseUpgradeType type, int planetMoney) {
		List<StarbaseUpgradeLevel> levelCosts = new ArrayList<>(10);

		for (int i = startLevel + 1; i <= 10; i++) {
			int money = calculateStarbaseUpgradeMoney(type, startLevel, i);

			if (money > 0 && money <= planetMoney) {
				StarbaseUpgradeLevel level = new StarbaseUpgradeLevel(i, money);
				levelCosts.add(level);
			} else {
				break;
			}
		}

		return levelCosts;
	}

	public int calculateStarbaseUpgradeMoney(StarbaseUpgradeType type, int startLevel, int endLevel) {
		List<Integer> list = starbaseUpgradeCosts.get(type);

		int costs = 0;

		for (int i = startLevel + 1; i <= endLevel; i++) {
			costs += list.get(i - 1);
		}

		return costs;
	}

	public float[] getFuelConsumptionData(int propulsionLevel) {
		return FUEL_CONSUMPTION.get(propulsionLevel);
	}

	public int calculateFuelConsumptionPerMonth(Ship ship, int travelSpeed, double distance, double duration, int totalMass) {
		float masterDataFuelConsumption = getFuelConsumptionData(ship.getPropulsionSystemTemplate().getTechLevel())[travelSpeed];
		float fuelConsumption = masterDataFuelConsumption;

		if (fuelConsumption > 0f) {
			if (duration <= 1d) {
				fuelConsumption = (float)Math.floor(distance * fuelConsumption * (totalMass / 100000f));
			} else {
				fuelConsumption = (float)Math.floor(Math.pow(travelSpeed, 2) * fuelConsumption * (totalMass / 100000f));
			}

			if (fuelConsumption == 0f) {
				fuelConsumption = 1f;
			}
		}

		return (int)fuelConsumption;
	}

	public double calculateTravelDuration(float travelSpeed, double distance, double bonus) {
		return distance / (Math.pow(travelSpeed, 2) * bonus);
	}

	public int getNecessaryMinesByIndex(int index) {
		return NECESSARY_MINES[index];
	}

	public ShipTemplate getShipTemplate(String shipTemplateId) {
		return shipTemplateRepository.getOne(shipTemplateId);
	}

	public boolean hasTractorBeam(int propulsionTechLevel) {
		return PROPULSION_WITH_TRACTOR_BEAM.contains(propulsionTechLevel);
	}

	@Cacheable("totalFactionCount")
	public long getFactionCount() {
		return factionRepository.count();
	}

	public String getShipTemplateName(ShipTemplate shipTemplate) {
		return getI18NValue(shipTemplate.getId());
	}

	public String getFactionDescription(String factionId) {
		return getI18NValue(FACTION_DESCRIPTION_I18N_KEY_PREFIX + factionId);
	}

	public String getI18NValue(String key) {
		return i18nRepository.getValue(key, LocaleContextHolder.getLocale().getLanguage());
	}

	@Cacheable("factionLandingpageData")
	public FactionLandingpageData getFactionLandingpageData(String factionId, String shipTemplateId1, ShipAbilityType abilityType1, String shipTemplateId2,
			ShipAbilityType abilityType2, Locale locale) {
		FactionLandingpageData data = new FactionLandingpageData();

		Faction faction = getFactionData(factionId);
		data.setFaction(faction);
		data.setName(getI18NValue(factionId));
		data.setDescription(getFactionDescription(factionId));

		String templateId1 = factionId + "_" + shipTemplateId1;
		String templateId2 = factionId + "_" + shipTemplateId2;
		ShipTemplate shipTemplate1 = shipTemplateRepository.getOne(templateId1);
		ShipTemplate shipTemplate2 = shipTemplateRepository.getOne(templateId2);
		data.setTemplate1(shipTemplate1);
		data.setTemplate2(shipTemplate2);

		data.setTemplateName1(getI18NValue(templateId1));
		data.setTemplateName2(getI18NValue(templateId2));

		ShipAbility ability1 = shipTemplate1.getAbility(abilityType1).get();
		ShipAbility ability2 = shipTemplate2.getAbility(abilityType2).get();
		data.setAbility1(shipAbilityDescriptionMapper.mapShipAbilityDescription(ability1));
		data.setAbility2(shipAbilityDescriptionMapper.mapShipAbilityDescription(ability2));

		return data;
	}
}