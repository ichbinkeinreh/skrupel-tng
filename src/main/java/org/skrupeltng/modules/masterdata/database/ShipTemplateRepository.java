package org.skrupeltng.modules.masterdata.database;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ShipTemplateRepository extends JpaRepository<ShipTemplate, String> {

}