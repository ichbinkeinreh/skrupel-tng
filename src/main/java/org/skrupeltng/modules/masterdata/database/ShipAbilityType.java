package org.skrupeltng.modules.masterdata.database;

import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.CYBERNRITTNIKK_VALUE;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.DESTABILIZER_EFFICIENCY;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.EVADE_DAMAGE_MAX;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.EXTENDED_TRANSPORTER_RANGE;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.GRAVITY_WAVE_GENERATOR_COSTS;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.JUMP_ENGINE_COSTS;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.JUMP_ENGINE_MAX;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.JUMP_ENGINE_MIN;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.JUMP_PORTAL_FUEL;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.JUMP_PORTAL_MINERAL1;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.JUMP_PORTAL_MINERAL2;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.JUMP_PORTAL_MINERAL3;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.KAMIKAZE_DAMAGE;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.KAMIKAZE_PROBABILITY;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.LUCKY_SHOT_PROBABILTY;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.OVERDRIVE_MAX;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.OVERDRIVE_MIN;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.QUARK_REORGANIZER_MINERAL1;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.QUARK_REORGANIZER_MINERAL2;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.QUARK_REORGANIZER_MINERAL3;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.QUARK_REORGANIZER_SUPPLIES;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.REPAIR_PERCENTAGE;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.SHIELD_DAMPER_COSTS;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL1;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL2;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL3;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.SUB_PARTICLE_CLUSTER_SUPPLIES;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.SUB_SPACE_DISTORTION_LEVEL;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.VIRAL_INVASION_MAX;
import static org.skrupeltng.modules.masterdata.database.ShipAbilityConstants.VIRAL_INVASION_MIN;

import java.util.HashMap;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

public enum ShipAbilityType {

	SUB_PARTICLE_CLUSTER(ShipAbilityValueType.BOOLEAN, "auto", new ShipAbilityParam(0, 2, SUB_PARTICLE_CLUSTER_SUPPLIES), new ShipAbilityParam(2, 1, SUB_PARTICLE_CLUSTER_MINERAL1), new ShipAbilityParam(3, 1, SUB_PARTICLE_CLUSTER_MINERAL2), new ShipAbilityParam(4, 1, SUB_PARTICLE_CLUSTER_MINERAL3)),

	TERRA_FORMER_WARM(new ShipAbilityParam(5, 1)),

	TERRA_FORMER_COLD(new ShipAbilityParam(6, 1)),

	QUARK_REORGANIZER(ShipAbilityValueType.BOOLEAN, "auto", new ShipAbilityParam(7, 1, 113f, QUARK_REORGANIZER_SUPPLIES), new ShipAbilityParam(8, 1, 113f, QUARK_REORGANIZER_MINERAL1), new ShipAbilityParam(9, 1, 113f, QUARK_REORGANIZER_MINERAL2), new ShipAbilityParam(10, 1, 113f, QUARK_REORGANIZER_MINERAL3)),

	JUMP_ENGINE(new ShipAbilityParam(11, 3, JUMP_ENGINE_COSTS), new ShipAbilityParam(14, 4, JUMP_ENGINE_MIN), new ShipAbilityParam(18, 4, JUMP_ENGINE_MAX)),

	CLOAKING_RELIABLE(new ShipAbilityParam(22, 1, 1)),

	CLOAKING_PERFECT(new ShipAbilityParam(22, 1, 2)),

	CLOAKING_UNRELIABLE(new ShipAbilityParam(22, 1, 3)),

	SUB_SPACE_DISTORTION(new ShipAbilityParam(23, 1, SUB_SPACE_DISTORTION_LEVEL)),

	EXTENDED_SENSORS(new ShipAbilityParam(24, 1, 1)),

	ASTRO_PHYSICS_LAB(new ShipAbilityParam(24, 1, 2)),

	JUMP_PORTAL(new ShipAbilityParam(25, 3, JUMP_PORTAL_MINERAL1), new ShipAbilityParam(28, 3, JUMP_PORTAL_MINERAL2), new ShipAbilityParam(31, 3, JUMP_PORTAL_MINERAL3), new ShipAbilityParam(34, 3, JUMP_PORTAL_FUEL)),

	EVADE_PERFECT(new ShipAbilityParam(38, 2, 1)),

	EVADE_UNRELIABLE(new ShipAbilityParam(38, 2, 1f, EVADE_DAMAGE_MAX, 0, 2)),

	SIGNATURE_MASK(ShipAbilityValueType.STRING, new ShipAbilityParam(40, 1)),

	VIRAL_INVASION(ShipAbilityValueType.STRING, new ShipAbilityParam(41, 2, VIRAL_INVASION_MIN), new ShipAbilityParam(43, 3, VIRAL_INVASION_MAX)),

	EXTENDED_TRANSPORTER(new ShipAbilityParam(46, 2, EXTENDED_TRANSPORTER_RANGE)),

	CYBERNRITTNIKK(ShipAbilityValueType.BOOLEAN, "auto", new ShipAbilityParam(48, 2, 220f, CYBERNRITTNIKK_VALUE)),

	DESTABILIZER(new ShipAbilityParam(50, 2, DESTABILIZER_EFFICIENCY)),

	OVERDRIVE(ShipAbilityValueType.NUMBER, "efficiency", new ShipAbilityParam(53, 1, 10f, OVERDRIVE_MIN), new ShipAbilityParam(54, 1, 10f, OVERDRIVE_MAX)),

	LUCKY_SHOT(true, new ShipAbilityParam(55, 1, LUCKY_SHOT_PROBABILTY)),

	ORBITAL_SHIELD(true, new ShipAbilityParam(56, 1)),

	INFANTRY_TRANSPORT(true, new ShipAbilityParam(57, 1)),

	HERMETIC_MATRIX(new ShipAbilityParam(58, 1)),

	COMMUNICATION_CENTER(new ShipAbilityParam(59, 1)),

	REPAIR(new ShipAbilityParam(37, 1, REPAIR_PERCENTAGE)),

	STRUCTUR_SCANNER(true, new ShipAbilityParam(52, 1)),

	GRAVITY_WAVE_GENERATOR(new ShipAbilityParam(60, 1, GRAVITY_WAVE_GENERATOR_COSTS)),

	SHIELD_DAMPER(new ShipAbilityParam(61, 1, SHIELD_DAMPER_COSTS)),

	KAMIKAZE(new ShipAbilityParam(62, 1, 10f, KAMIKAZE_PROBABILITY), new ShipAbilityParam(63, 1, 100f, KAMIKAZE_DAMAGE));

	private ShipAbilityParam[] params;
	private ShipAbilityValueType valueType;
	private String valueLabel;
	private boolean passive;

	private ShipAbilityType(ShipAbilityParam... params) {
		this.params = params;
	}

	private ShipAbilityType(boolean passive, ShipAbilityParam... params) {
		this.passive = passive;
		this.params = params;
	}

	private ShipAbilityType(ShipAbilityValueType valueType, ShipAbilityParam... params) {
		this.valueType = valueType;
		this.params = params;
	}

	private ShipAbilityType(ShipAbilityValueType valueType, String valueLabel, ShipAbilityParam... params) {
		this.valueType = valueType;
		this.valueLabel = valueLabel;
		this.params = params;
	}

	public String getValueLabel() {
		return valueLabel;
	}

	public boolean isPassive() {
		return passive;
	}

	public boolean hasStringValueType() {
		return valueType == ShipAbilityValueType.STRING;
	}

	public boolean hasNumberValueType() {
		return valueType == ShipAbilityValueType.NUMBER;
	}

	public boolean hasBooleanValueType() {
		return valueType == ShipAbilityValueType.BOOLEAN;
	}

	public boolean hasValueType() {
		return valueType != null;
	}

	public Optional<ShipAbility> createAbility(String abilityString) {
		ShipAbility ability = null;

		for (ShipAbilityParam param : params) {
			String part = StringUtils.substring(abilityString, param.getStartIndex(), param.getEndIndex());

			if (part.isEmpty()) {
				continue;
			}

			int value = Integer.valueOf(part);

			if (value > 0) {
				if (param.getCheckValue() > 0 && value != param.getCheckValue()) {
					continue;
				}

				if (param.getCheckValueMin() > 0 && value < param.getCheckValueMin()) {
					continue;
				}

				value *= param.getValueMultiplier();

				String name = param.getName();

				if (ability == null) {
					ability = new ShipAbility();

					if (name != null) {
						ability.setValues(new HashMap<>(params.length));
						for (ShipAbilityParam p : params) {
							String n = p.getName();

							if (n != null) {
								ability.getValues().put(n, "0");
							}
						}
					}

					ability.setType(this);
				}

				if (name != null) {
					ability.getValues().put(name, value + "");
				}
			}
		}

		return Optional.ofNullable(ability);
	}
}