package org.skrupeltng.modules.masterdata.database;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "i18n")
public class I18N implements Serializable {

	private static final long serialVersionUID = 3052355343665417288L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String key;

	private String language;

	private String value;

	public I18N() {

	}

	public I18N(String key, String language) {
		this.key = key;
		this.language = language;
	}

	public I18N(String key, String language, String value) {
		this.key = key;
		this.language = language;
		this.value = value;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "I18N [id=" + id + ", key=" + key + ", language=" + language + ", value=" + value + "]";
	}
}