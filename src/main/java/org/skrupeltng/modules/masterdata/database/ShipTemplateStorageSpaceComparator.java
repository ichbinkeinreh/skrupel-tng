package org.skrupeltng.modules.masterdata.database;

import java.util.Comparator;

public class ShipTemplateStorageSpaceComparator implements Comparator<ShipTemplate> {

	@Override
	public int compare(ShipTemplate a, ShipTemplate b) {
		return Integer.compare(a.getStorageSpace(), b.getStorageSpace());
	}
}