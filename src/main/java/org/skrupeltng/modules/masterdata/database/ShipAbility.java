package org.skrupeltng.modules.masterdata.database;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ship_ability")
public class ShipAbility implements Serializable, Comparable<ShipAbility> {

	private static final long serialVersionUID = 210282192562742895L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Enumerated(EnumType.STRING)
	private ShipAbilityType type;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ship_template_id")
	private ShipTemplate shipTemplate;

	@ElementCollection
	@JoinTable(name = "ship_ability_value", joinColumns = @JoinColumn(name = "ship_ability_id"))
	@MapKeyColumn(name = "name")
	@Column(name = "value")
	private Map<String, String> values;

	public ShipAbility() {

	}

	public ShipAbility(ShipAbilityType type) {
		this.type = type;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ShipAbilityType getType() {
		return type;
	}

	public void setType(ShipAbilityType type) {
		this.type = type;
	}

	public ShipTemplate getShipTemplate() {
		return shipTemplate;
	}

	public void setShipTemplate(ShipTemplate shipTemplate) {
		this.shipTemplate = shipTemplate;
	}

	public Map<String, String> getValues() {
		return values;
	}

	public void setValues(Map<String, String> values) {
		this.values = values;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, type);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ShipAbility)) {
			return false;
		}
		ShipAbility other = (ShipAbility)obj;
		return id == other.id && type == other.type;
	}

	@Override
	public int compareTo(ShipAbility o) {
		return type.compareTo(o.getType());
	}
}