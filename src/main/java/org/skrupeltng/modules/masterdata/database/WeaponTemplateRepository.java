package org.skrupeltng.modules.masterdata.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WeaponTemplateRepository extends JpaRepository<WeaponTemplate, String> {

	List<WeaponTemplate> findByUsesProjectiles(boolean usesProjectiles);
}