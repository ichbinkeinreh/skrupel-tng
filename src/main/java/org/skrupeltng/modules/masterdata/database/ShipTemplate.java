package org.skrupeltng.modules.masterdata.database;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.skrupeltng.modules.masterdata.StarbaseProducable;

@Entity
@Table(name = "ship_template")
public class ShipTemplate implements Serializable, StarbaseProducable, Comparable<ShipTemplate> {

	private static final long serialVersionUID = 1381090807145835544L;

	@Id
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "faction_id")
	private Faction faction;

	@Column(name = "tech_level")
	private int techLevel;

	private String image;

	private int crew;

	private int mass;

	@Column(name = "fuel_capacity")
	private int fuelCapacity;

	@Column(name = "storage_space")
	private int storageSpace;

	@Column(name = "propulsion_systems_count")
	private int propulsionSystemsCount;

	@Column(name = "energy_weapons_count")
	private int energyWeaponsCount;

	@Column(name = "projectile_weapons_count")
	private int projectileWeaponsCount;

	@Column(name = "hangar_capacity")
	private int hangarCapacity;

	@Column(name = "cost_money")
	private int costMoney;

	@Column(name = "cost_mineral1")
	private int costMineral1;

	@Column(name = "cost_mineral2")
	private int costMineral2;

	@Column(name = "cost_mineral3")
	private int costMineral3;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shipTemplate")
	private List<ShipAbility> shipAbilities;

	public ShipTemplate() {

	}

	public ShipTemplate(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Faction getFaction() {
		return faction;
	}

	public void setFaction(Faction faction) {
		this.faction = faction;
	}

	@Override
	public int getTechLevel() {
		return techLevel;
	}

	public void setTechLevel(int techLevel) {
		this.techLevel = techLevel;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getCrew() {
		return crew;
	}

	public void setCrew(int crew) {
		this.crew = crew;
	}

	public int getMass() {
		return mass;
	}

	public void setMass(int mass) {
		this.mass = mass;
	}

	public int getFuelCapacity() {
		return fuelCapacity;
	}

	public void setFuelCapacity(int fuelCapacity) {
		this.fuelCapacity = fuelCapacity;
	}

	public int getStorageSpace() {
		return storageSpace;
	}

	public void setStorageSpace(int storageSpace) {
		this.storageSpace = storageSpace;
	}

	public int getPropulsionSystemsCount() {
		return propulsionSystemsCount;
	}

	public void setPropulsionSystemsCount(int propulsionSystemsCount) {
		this.propulsionSystemsCount = propulsionSystemsCount;
	}

	public int getEnergyWeaponsCount() {
		return energyWeaponsCount;
	}

	public void setEnergyWeaponsCount(int energyWeaponsCount) {
		this.energyWeaponsCount = energyWeaponsCount;
	}

	public int getProjectileWeaponsCount() {
		return projectileWeaponsCount;
	}

	public void setProjectileWeaponsCount(int projectileWeaponsCount) {
		this.projectileWeaponsCount = projectileWeaponsCount;
	}

	public int getHangarCapacity() {
		return hangarCapacity;
	}

	public void setHangarCapacity(int hangarCapacity) {
		this.hangarCapacity = hangarCapacity;
	}

	@Override
	public int getCostMoney() {
		return costMoney;
	}

	public void setCostMoney(int costMoney) {
		this.costMoney = costMoney;
	}

	@Override
	public int getCostMineral1() {
		return costMineral1;
	}

	public void setCostMineral1(int costMineral1) {
		this.costMineral1 = costMineral1;
	}

	@Override
	public int getCostMineral2() {
		return costMineral2;
	}

	public void setCostMineral2(int costMineral2) {
		this.costMineral2 = costMineral2;
	}

	@Override
	public int getCostMineral3() {
		return costMineral3;
	}

	public void setCostMineral3(int costMineral3) {
		this.costMineral3 = costMineral3;
	}

	public List<ShipAbility> getShipAbilities() {
		return shipAbilities;
	}

	public void setShipAbilities(List<ShipAbility> shipAbilities) {
		this.shipAbilities = shipAbilities;
	}

	public String createFullImagePath() {
		return "/factions/" + faction.getId() + "/ship_images/" + image;
	}

	public Optional<ShipAbility> getAbility(ShipAbilityType type) {
		return shipAbilities.stream().filter(a -> a.getType() == type).findAny();
	}

	public Optional<Float> getAbilityValue(ShipAbilityType type, String name) {
		Optional<ShipAbility> abilityOpt = shipAbilities.stream().filter(a -> a.getType() == type).findAny();

		if (abilityOpt.isPresent()) {
			String valueString = abilityOpt.get().getValues().get(name);

			if (valueString != null) {
				return Optional.of(Float.valueOf(valueString));
			}
		}

		return Optional.empty();
	}

	@Override
	public int compareTo(ShipTemplate o) {
		return Integer.compare(techLevel, o.getTechLevel());
	}
}