package org.skrupeltng.modules.masterdata.database;

public enum ShipAbilityValueType {

	STRING,

	NUMBER,

	BOOLEAN
}