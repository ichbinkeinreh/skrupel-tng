package org.skrupeltng.modules;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.ingame.controller.AbstractPageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class RepositoryCustomBase {

	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;

	protected String where(List<String> wheres) {
		if (!wheres.isEmpty()) {
			return "WHERE \n	" + StringUtils.join(wheres, " \n	AND ");
		}

		return "";
	}

	protected String having(List<String> havings) {
		if (!havings.isEmpty()) {
			return "HAVING \n	" + StringUtils.join(havings, " \n	AND ");
		}

		return "";
	}

	protected <T extends PageableResult> Page<T> search(String sql, Map<String, Object> params, Pageable pageable, RowMapper<T> rowMapper) {
		sql += " OFFSET " + pageable.getOffset() + " LIMIT " + pageable.getPageSize();
		List<T> page = jdbcTemplate.query(sql, params, rowMapper);

		int totalElements = 0;

		if (page.size() > 0) {
			totalElements = page.get(0).getTotalElements();
		}

		return new PageImpl<>(page, pageable, totalElements);
	}

	protected String createOrderBy(Sort sort, String defaultOrderBy) {
		String orderBy = "";

		if (sort != null && sort.get().count() > 0L) {
			orderBy += " \nORDER BY \n";

			for (Order order : sort) {
				orderBy += "	" + order.getProperty() + " " + order.getDirection().name();
			}
		} else if (StringUtils.isNotBlank(defaultOrderBy)) {
			orderBy += " \nORDER BY \n	" + defaultOrderBy;
		}

		return orderBy;
	}

	protected Pageable createPageable(AbstractPageRequest request) {
		Direction direction = request.isSortAscending() ? Direction.ASC : Direction.DESC;

		String sortFieldString = SortField.valueOf(request.getSortField()).getDatabaseField();
		Sort sort = Sort.by(direction, sortFieldString);

		return PageRequest.of(request.getPage(), request.getPageSize(), sort);
	}
}