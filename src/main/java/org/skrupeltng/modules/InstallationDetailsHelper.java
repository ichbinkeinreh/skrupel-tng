package org.skrupeltng.modules;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.dashboard.database.InstallationDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component("installationDetailsHelper")
public class InstallationDetailsHelper {

	@Autowired
	private InstallationDetailsRepository installationDetailsRepository;

	@Cacheable("hasLegalText")
	public boolean hasLegalText() {
		String legalText = installationDetailsRepository.getLegalText();
		return StringUtils.isNotBlank(legalText);
	}

	@Cacheable("domainUrl")
	public String getDomainUrl() {
		return installationDetailsRepository.getDomainUrl();
	}

	@Cacheable("contactEmail")
	public String getContactEmail() {
		return installationDetailsRepository.getContactEmail();
	}

	@CacheEvict(value = "hasLegalText", allEntries = true)
	public void clearHasLegalTextCache() {

	}

	@CacheEvict(value = "domainUrl", allEntries = true)
	public void clearDomainUrlCache() {

	}

	@CacheEvict(value = "contactEmail", allEntries = true)
	public void clearContactEmailCache() {

	}
}