package org.skrupeltng.modules.ingame;

public interface Coordinate {

	int getX();

	int getY();

	int getScanRadius();
}