package org.skrupeltng.modules.ingame;

import java.util.Comparator;

import org.skrupeltng.modules.ingame.service.CoordHelper;

public class CoordinateDistanceComparator implements Comparator<Coordinate> {

	private final Coordinate distancePoint;

	public CoordinateDistanceComparator(Coordinate distancePoint) {
		this.distancePoint = distancePoint;
	}

	@Override
	public int compare(Coordinate a, Coordinate b) {
		double dist1 = CoordHelper.getDistance(distancePoint, a);
		double dist2 = CoordHelper.getDistance(distancePoint, b);
		return Double.compare(dist1, dist2);
	}
}