package org.skrupeltng.modules.ingame.service.round.combat.space;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ShipEvasion {

	@Autowired
	private NewsService newsService;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private ConfigProperties configProperties;

	public void evade(Ship ship, Ship enemyShip, boolean addNews) {
		Game game = ship.getPlayer().getGame();

		CoordinateImpl next = CoordHelper.getRandomNearCoordinates(ship.getX(), ship.getY(), configProperties.getEvadeDistance(), game.getGalaxySize());
		ship.setX(next.getX());
		ship.setY(next.getY());
		ship.setPlanet(planetRepository.findByGameIdAndXAndY(game.getId(), next.getX(), next.getY()).orElse(null));
		String sector = CoordHelper.getSectorString(next);

		if (addNews) {
			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_evaded_successfull, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName(), enemyShip.getName(), sector);
		}
	}
}