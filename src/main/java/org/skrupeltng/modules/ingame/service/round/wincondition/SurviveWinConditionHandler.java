package org.skrupeltng.modules.ingame.service.round.wincondition;

import java.util.List;

import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("SURVIVE")
public class SurviveWinConditionHandler implements WinConditionHandler {

	private PlayerRepository playerRepository;
	private GameRepository gameRepository;

	@Override
	public void checkWinCondition(long gameId) {
		List<Player> players = playerRepository.getNotLostPlayers(gameId);

		if (players.size() <= 1) {
			Game game = gameRepository.getOne(gameId);
			game.setFinished(true);
			gameRepository.save(game);
		}
	}

	@Autowired
	public void setPlayerRepository(PlayerRepository playerRepository) {
		this.playerRepository = playerRepository;
	}

	@Autowired
	public void setGameRepository(GameRepository gameRepository) {
		this.gameRepository = gameRepository;
	}
}