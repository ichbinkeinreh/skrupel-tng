package org.skrupeltng.modules.ingame.service;

import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.masterdata.service.MasterDataService;

public class CoordHelper {

	public static double getDistance(Coordinate a, Coordinate b) {
		int diffX = a.getX() - b.getX();
		int diffY = a.getY() - b.getY();
		return getDistance(diffX, diffY);
	}

	public static double getDistance(int x1, int y1, int x2, int y2) {
		int diffX = x1 - x2;
		int diffY = y1 - y2;
		return getDistance(diffX, diffY);
	}

	public static double getDistance(int diffX, int diffY) {
		double distance = Math.sqrt((diffX * diffX) + (diffY * diffY));
		return distance;
	}

	public static String getSectorString(Coordinate coordinate) {
		String letter = "" + (char)((coordinate.getX() / 250) + 65);
		int line = (coordinate.getY() / 250) + 1;
		return letter + line;
	}

	public static CoordinateImpl getRandomNearCoordinates(int x, int y, int distance, int galaxySize) {
		double angle = Math.PI * 2 * MasterDataService.RANDOM.nextFloat();
		long deltaX = Math.round(distance * Math.sin(angle));
		long deltaY = Math.round(distance * Math.cos(angle));
		int x2 = (int)Math.max(0, Math.min(galaxySize, x + deltaX));
		int y2 = (int)Math.max(0, Math.min(galaxySize, y + deltaY));
		return new CoordinateImpl(x2, y2, 0);
	}
}