package org.skrupeltng.modules.ingame.service.round.combat.orbit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.ingame.service.round.combat.ShipDamage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrbitalCombat {

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private ShipService shipService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private StatsUpdater statsUpdater;

	public Map<Long, Integer> getTotalPlanetaryDefenses(List<Ship> ships) {
		Map<Long, Integer> totalPlanetaryDefenses = new HashMap<>(ships.size());

		for (Ship ship : ships) {
			Planet planet = ship.getPlanet();
			long planetId = planet.getId();

			if (!totalPlanetaryDefenses.containsKey(planetId)) {
				totalPlanetaryDefenses.put(planetId, planet.retrieveTotalPlanetaryDefense());
			}
		}

		return totalPlanetaryDefenses;
	}

	public void checkStructurScanner(Ship ship) {
		Planet planet = ship.getPlanet();

		if (planet.hasOrbitalSystem(OrbitalSystemType.HOBAN_BUD)) {
			shipService.checkStructurScanner(planet.getPlayer(), ship);
		}
	}

	public int processCombat(Ship ship, int totalPlanetaryDefense) {
		Planet planet = ship.getPlanet();
		Starbase starbase = planet.getStarbase();
		boolean isBattleStation = starbase != null && starbase.getType() == StarbaseType.BATTLE_STATION;

		ShipDamage shipDamage = new ShipDamage(ship, null, 0);

		int planetEnergyWeapons = planet.retrieveEnergyWeaponCount();
		int planetHangarCapacity = planet.retrieveHangarCapacity();

		int planetEnergyDamage = planet.retrieveEnergyWeaponDamage();

		int damageToPlanet = 0;

		while (ship.getDamage() < 100 && damageToPlanet < totalPlanetaryDefense && (planet.getPlanetaryDefense() >= 1 || isBattleStation)) {
			for (int round = 1; round <= 10; round++) {
				if (ship.getDamage() < 100 && damageToPlanet < totalPlanetaryDefense) {
					damageToPlanet += shipDamage.calculateDamageToPlanet(round);

					if (planetEnergyWeapons >= round) {
						ship.receiveDamage(planetEnergyDamage, false, shipDamage.getMass());
					}

					if (planetHangarCapacity >= round) {
						ship.receiveDamage(ShipDamage.HANGAR_DAMAGE, false, shipDamage.getMass());
					}
				}
			}
		}

		return damageToPlanet;
	}

	public void handlePlanetDamage(long planetId, Map<Long, Integer> totalPlanetaryDefenses, int damageToPlanet) {
		int totalPlanetaryDefense = totalPlanetaryDefenses.get(planetId);

		totalPlanetaryDefense -= damageToPlanet;

		if (totalPlanetaryDefense < 0) {
			totalPlanetaryDefense = 0;
		}

		totalPlanetaryDefenses.put(planetId, totalPlanetaryDefense);
	}

	public void handleShipDamage(Set<Ship> destroyedShips, Ship ship) {
		String shipName = ship.getName();
		Planet planet = ship.getPlanet();
		String planetName = planet.getName();

		if (ship.destroyed()) {
			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_destroyed_ship_by_planet, ship.createFullImagePath(), null, null, shipName,
					planetName);
			newsService.add(planet.getPlayer(), NewsEntryConstants.news_entry_planet_defended_enemy_ship, planet.createFullImagePath(), planet.getId(),
					NewsEntryClickTargetType.planet, planetName, shipName);
			destroyedShips.add(ship);

			statsUpdater.incrementStats(ship.getPlayer(), LoginStatsFaction::getShipsLost, LoginStatsFaction::setShipsLost);
			statsUpdater.incrementStats(planet.getPlayer(), LoginStatsFaction::getShipsDestroyed, LoginStatsFaction::setShipsDestroyed,
					AchievementType.SHIPS_DESTROYED_1, AchievementType.SHIPS_DESTROYED_10);
		} else {
			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_entered_enemy_orbit, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, shipName, planetName);
			newsService.add(planet.getPlayer(), NewsEntryConstants.news_entry_enemy_ship_entered_orbit, planet.createFullImagePath(), planet.getId(),
					NewsEntryClickTargetType.planet, shipName, planetName);
			shipRepository.save(ship);
		}
	}
}