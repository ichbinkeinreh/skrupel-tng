package org.skrupeltng.modules.ingame.service.round;

import java.util.List;
import java.util.function.BiConsumer;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFold;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFoldRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class SpaceFoldRoundCalculator {

	@Autowired
	private SpaceFoldRepository spaceFoldRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private NewsService newsService;

	@Autowired
	private ConfigProperties configProperties;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processSpaceFolds(long gameId) {
		List<SpaceFold> spaceFolds = spaceFoldRepository.findByGameId(gameId);

		float travelSpeed = configProperties.getSpaceFoldTravelSpeed();
		double travelSpeedSquared = travelSpeed * travelSpeed;

		for (SpaceFold spaceFold : spaceFolds) {
			int x = spaceFold.getX();
			int y = spaceFold.getY();

			Ship ship = spaceFold.getShip();
			Planet planet = spaceFold.getPlanet();

			int targetX = 0;
			int targetY = 0;

			if (ship != null) {
				targetX = ship.getX();
				targetY = ship.getY();
			} else {
				targetX = planet.getX();
				targetY = planet.getY();
			}

			double distance = CoordHelper.getDistance(x, y, targetX, targetY);
			double duration = distance / travelSpeedSquared;

			if (duration <= 1) {
				if (ship != null) {
					ShipTemplate shipTemplate = ship.getShipTemplate();
					int freeSpace = ship.retrieveStorageSpace() - ship.retrieveTotalGoods();
					int freeTank = shipTemplate.getFuelCapacity() - ship.getFuel();

					ship.setMoney(ship.getMoney() + spaceFold.getMoney());
					freeSpace = checkResource(freeSpace, spaceFold.getMineral1(), ship, ship.getMineral1(), Ship::setMineral1);
					freeSpace = checkResource(freeSpace, spaceFold.getMineral2(), ship, ship.getMineral2(), Ship::setMineral2);
					freeSpace = checkResource(freeSpace, spaceFold.getMineral3(), ship, ship.getMineral3(), Ship::setMineral3);
					checkResource(freeSpace, spaceFold.getSupplies(), ship, ship.getSupplies(), Ship::setSupplies);
					checkResource(freeTank, spaceFold.getFuel(), ship, ship.getFuel(), Ship::setFuel);

					newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_space_fold_reached_ship, "/images/news/raumfalte.jpg", ship.getId(),
							NewsEntryClickTargetType.ship, ship.getName(), spaceFold.getMoney(), spaceFold.getMineral1(), spaceFold.getMineral2(),
							spaceFold.getMineral3(), spaceFold.getSupplies(), spaceFold.getFuel());

					shipRepository.save(ship);
				} else {
					planet.setMoney(planet.getMoney() + spaceFold.getMoney());
					planet.setSupplies(planet.getSupplies() + spaceFold.getSupplies());
					planet.setFuel(planet.getFuel() + spaceFold.getFuel());
					planet.setMineral1(planet.getMineral1() + spaceFold.getMineral1());
					planet.setMineral2(planet.getMineral2() + spaceFold.getMineral2());
					planet.setMineral3(planet.getMineral3() + spaceFold.getMineral3());

					if (planet.getPlayer() != null) {
						newsService.add(planet.getPlayer(), NewsEntryConstants.news_entry_space_fold_reached_planet, "/images/news/raumfalte.jpg",
								planet.getId(), NewsEntryClickTargetType.planet, planet.getName(), spaceFold.getMoney(), spaceFold.getMineral1(),
								spaceFold.getMineral2(), spaceFold.getMineral3(), spaceFold.getSupplies(), spaceFold.getFuel());
					}

					planetRepository.save(planet);
				}

				spaceFoldRepository.delete(spaceFold);
			} else {
				int newX = (int)Math.round(x + ((targetX - x) / duration));
				int newY = (int)Math.round(y + ((targetY - y) / duration));
				spaceFold.setX(newX);
				spaceFold.setY(newY);

				spaceFoldRepository.save(spaceFold);
			}
		}
	}

	private int checkResource(int freeSpace, int spaceFoldResource, Ship ship, int shipResource, BiConsumer<Ship, Integer> setter) {
		if (spaceFoldResource <= freeSpace) {
			freeSpace -= spaceFoldResource;
			setter.accept(ship, shipResource + spaceFoldResource);
		} else {
			setter.accept(ship, shipResource + freeSpace);
			freeSpace = 0;
		}

		return freeSpace;
	}
}