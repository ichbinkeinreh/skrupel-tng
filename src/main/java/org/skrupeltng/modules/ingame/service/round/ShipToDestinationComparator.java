package org.skrupeltng.modules.ingame.service.round;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.service.CoordHelper;

public class ShipToDestinationComparator implements Comparator<Ship> {

	private final Set<Pair<Ship, Ship>> visitedPairs = new HashSet<>();

	@Override
	public int compare(Ship o1, Ship o2) {
		if (o1.getDestinationShip() == null && o2.getDestinationShip() != null) {
			return -1;
		}

		if (o1.getDestinationShip() != null && o2.getDestinationShip() == null) {
			return 1;
		}

		if (visitedPairs.add(Pair.of(o1, o2))) {
			if (o1.getDestinationShip() != null && o2.getDestinationShip() != null) {
				return compare(o1.getDestinationShip(), o2.getDestinationShip());
			}
		}

		double d1 = CoordHelper.getDistance(o1, new CoordinateImpl(o1.getDestinationX(), o1.getDestinationY(), 0));
		double d2 = CoordHelper.getDistance(o2, new CoordinateImpl(o2.getDestinationX(), o2.getDestinationY(), 0));
		return -Double.compare(d1, d2);
	}
}