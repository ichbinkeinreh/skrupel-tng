package org.skrupeltng.modules.ingame.service.round;

import java.util.List;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class PirateRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private ShipRepository shipRepository;
	private GameRepository gameRepository;
	private NewsService newsService;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processPirateAttacks(long gameId) {
		log.debug("Processing pirates attacks...");

		Game game = gameRepository.getOne(gameId);

		if (game.getPirateAttackProbabilityCenter() == 0f && game.getPirateAttackProbabilityOutskirts() == 0f) {
			return;
		}

		List<Ship> ships = shipRepository.getShipsVulnerableToPirates(gameId);

		for (Ship ship : ships) {
			processShip(game, ship);
		}

		log.debug("Processing pirates attacks finished.");
	}

	protected void processShip(Game game, Ship ship) {
		float probability = computeProbability(game, ship);

		int highestTechLevel = shipRepository.getHighestSupportingTechLevel(ship.getId(), ship.getPlayer().getId(), ship.getX(), ship.getY());

		float chance = MasterDataService.RANDOM.nextFloat();

		if (chance <= probability - ((highestTechLevel * highestTechLevel) / 100f)) {
			processPirateLooting(game, ship);
		} else if (chance <= probability && highestTechLevel > 0) {
			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_pirate_attack_averted, ImageConstants.news_pirates, ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName());
		}
	}

	protected void processPirateLooting(Game game, Ship ship) {
		float lootPercentage = game.getPirateMinLoot();

		if (game.getPirateMinLoot() < game.getPirateMaxLoot()) {
			lootPercentage += MasterDataService.RANDOM.nextInt((int)(game.getPirateMaxLoot() - game.getPirateMinLoot()));
		}

		lootPercentage -= (ship.getExperience() * 5);

		if (ship.getShipModule() == ShipModule.ACTIVE_DEFENSE_SYSTEM) {
			lootPercentage = Math.round(lootPercentage * 0.27f);
		}

		if (lootPercentage >= 1f) {
			loot(ship, lootPercentage);
		} else {
			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_pirate_attack_averted, ImageConstants.news_pirates, ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName());
		}
	}

	protected float computeProbability(Game game, Ship ship) {
		float propCenter = game.getPirateAttackProbabilityCenter();
		float propOutskirts = game.getPirateAttackProbabilityOutskirts();

		float prop = 0f;

		if (propCenter == propOutskirts) {
			prop = propCenter;
		} else if (propCenter > propOutskirts) {
			float propDiff = propCenter - propOutskirts;
			float minProp = propOutskirts;
			float dist = getDistanceFromCenter(game, ship);
			float distPercentage = 100 - (dist * 100f / (game.getGalaxySize() / 2f));

			prop = getDiffProbability(propDiff, minProp, distPercentage);
		} else {
			float propDiff = propOutskirts - propCenter;
			float minProp = propCenter;
			float dist = getDistanceFromCenter(game, ship);
			float distPercentage = dist * 100f / (game.getGalaxySize() / 2f);

			prop = getDiffProbability(propDiff, minProp, distPercentage);
		}

		return (prop - (ship.getExperience() * 5)) / 100f;
	}

	protected float getDiffProbability(float propDiff, float minProp, float distPercentage) {
		return Math.round((distPercentage * (propDiff / 100f)) + minProp);
	}

	protected float getDistanceFromCenter(Game game, Ship ship) {
		int half = game.getGalaxySize() / 2;
		Coordinate center = new CoordinateImpl(half, half, 0);
		return (float)CoordHelper.getDistance(ship, center);
	}

	protected void loot(Ship ship, float lootPercentage) {
		List<Float> lootPercentages = getLootPercentages(lootPercentage);

		int lootedMineral1 = (int)Math.ceil(lootPercentages.get(0) * ship.getMineral1());
		int lootedMineral2 = (int)Math.ceil(lootPercentages.get(1) * ship.getMineral2());
		int lootedMineral3 = (int)Math.ceil(lootPercentages.get(2) * ship.getMineral3());
		int lootedMoney = (int)Math.ceil(lootPercentages.get(3) * ship.getMoney());
		int lootedSupplies = (int)Math.ceil(lootPercentages.get(4) * ship.getSupplies());

		if (lootedMineral1 > 0 || lootedMineral2 > 0 || lootedMineral3 > 0 || lootedMoney > 0 || lootedSupplies > 0) {
			ship.setMineral1(ship.getMineral1() - lootedMineral1);
			ship.setMineral2(ship.getMineral2() - lootedMineral2);
			ship.setMineral3(ship.getMineral3() - lootedMineral3);
			ship.setMoney(ship.getMoney() - lootedMoney);
			ship.setSupplies(ship.getSupplies() - lootedSupplies);

			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_pirate_attack, ImageConstants.news_pirates, ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName(), lootedMineral1, lootedMineral2, lootedMineral3, lootedMoney, lootedSupplies);
			shipRepository.save(ship);
		}
	}

	protected List<Float> getLootPercentages(float lootPercentage) {
		List<Integer> lootValues = RandomSplitter.getRandomValues((int)(lootPercentage * 5f), 5);

		for (int i = 0; i < lootValues.size(); i++) {
			if (lootValues.get(i) > lootPercentage) {
				lootValues.set(i, (int)lootPercentage);
			}
		}

		return lootValues.stream().map(v -> v / 100f).collect(Collectors.toList());
	}

	@Autowired
	public void setShipRepository(ShipRepository shipRepository) {
		this.shipRepository = shipRepository;
	}

	@Autowired
	public void setGameRepository(GameRepository gameRepository) {
		this.gameRepository = gameRepository;
	}

	@Autowired
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}
}