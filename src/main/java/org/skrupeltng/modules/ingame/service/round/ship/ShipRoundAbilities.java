package org.skrupeltng.modules.ingame.service.round.ship;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFoldRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.TotalGoodCalculator;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.skrupeltng.modules.ingame.service.round.ShipToDestinationComparator;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

@Component
public class ShipRoundAbilities {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final Set<PlayerRelationType> GRAVITY_WAVE_RELATION_TYPE_EXCLUSIONS = Sets.newHashSet(PlayerRelationType.ALLIANCE,
			PlayerRelationType.NON_AGGRESSION_TREATY);
	private final Set<ShipAbilityType> GRAVITY_WAVE_INTERCEPTING_ABILITIES = Sets.newHashSet(ShipAbilityType.EVADE_PERFECT, ShipAbilityType.EVADE_UNRELIABLE,
			ShipAbilityType.JUMP_ENGINE);

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private StarbaseService starbaseService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private ShipService shipService;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private WormHoleRepository wormHoleRepository;

	@Autowired
	private SpaceFoldRepository spaceFoldRepository;

	@Autowired
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Autowired
	private StatsUpdater statsUpdater;

	@Autowired
	private ConfigProperties configProperties;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processSensors(long gameId) {
		log.debug("Processing sensors...");

		shipRepository.resetShipScanRadius(gameId);
		shipRepository.updateScanRadius(gameId, 116, ShipAbilityType.ASTRO_PHYSICS_LAB);
		shipRepository.updateScanRadius(gameId, 85, ShipAbilityType.EXTENDED_SENSORS);

		log.debug("Processing sensors finished.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processSubParticleCluster(long gameId) {
		log.debug("Processing sub particle cluster...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.SUB_PARTICLE_CLUSTER);

		for (Ship ship : ships) {
			Planet planet = ship.getPlanet();
			String taskValue = ship.getTaskValue();

			if (planet != null && taskValue != null && taskValue.equals("true")) {
				ShipTransportRequest request = new ShipTransportRequest();
				int supplies = Math.min(planet.retrieveTransportableSupplies(ship.getPlayer().getId()), ship.retrieveStorageSpace());
				request.setSupplies(supplies);
				Pair<Ship, Planet> result = shipService.transportWithoutPermissionCheck(request, ship, ship.getPlanet());
				ship = result.getFirst();
			}

			int supplies = ship.getAbilityValue(ShipAbilityType.SUB_PARTICLE_CLUSTER, ShipAbilityConstants.SUB_PARTICLE_CLUSTER_SUPPLIES).get().intValue();
			int freeSpace = ship.getShipTemplate().getStorageSpace() - TotalGoodCalculator.retrieveSum(ship);

			if (ship.getSupplies() >= supplies && freeSpace > 0) {
				int mineral1 = ship.getAbilityValue(ShipAbilityType.SUB_PARTICLE_CLUSTER, ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL1).get().intValue();
				int mineral2 = ship.getAbilityValue(ShipAbilityType.SUB_PARTICLE_CLUSTER, ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL2).get().intValue();
				int mineral3 = ship.getAbilityValue(ShipAbilityType.SUB_PARTICLE_CLUSTER, ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL3).get().intValue();

				int roundsBySupplies = ship.getSupplies() / supplies;
				int freeSpaceRounds = freeSpace / (mineral1 + mineral2 + mineral3);
				int rounds = Math.min(Math.min(roundsBySupplies, freeSpaceRounds), 287);

				if (rounds > 0) {
					int newMineral1 = rounds * mineral1;
					int newMineral2 = rounds * mineral2;
					int newMineral3 = rounds * mineral3;

					ship.setSupplies(ship.getSupplies() - (rounds * supplies));
					ship.setMineral1(ship.getMineral1() + newMineral1);
					ship.setMineral2(ship.getMineral2() + newMineral2);
					ship.setMineral3(ship.getMineral3() + newMineral3);
					shipRepository.save(ship);
				}
			}
		}

		log.debug("Processing sub particle cluster finished.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processTerraformer(long gameId) {
		log.debug("Processing terraformer...");

		processTerraformer(gameId, ShipAbilityType.TERRA_FORMER_COLD);
		processTerraformer(gameId, ShipAbilityType.TERRA_FORMER_WARM);

		log.debug("Processing terraformer finished.");
	}

	protected void processTerraformer(long gameId, ShipAbilityType type) {
		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, type);

		for (Ship ship : ships) {
			Planet planet = ship.getPlanet();

			if (planet != null) {
				int temperature = planet.getTemperature();
				String newsTemplate = NewsEntryConstants.news_entry_terraforming_cold;

				if (type == ShipAbilityType.TERRA_FORMER_COLD) {
					temperature--;
				} else {
					temperature++;
					newsTemplate = NewsEntryConstants.news_entry_terraforming_warm;
				}

				planet.setTemperature(temperature);
				planetRepository.save(planet);

				newsService.add(ship.getPlayer(), newsTemplate, ship.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName(), planet.getName(), temperature);
			}
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processQuarkReorganizer(long gameId) {
		log.debug("Processing quark reorganizer...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.QUARK_REORGANIZER);

		for (Ship ship : ships) {
			Planet planet = ship.getPlanet();
			String taskValue = ship.getTaskValue();

			int supplies = ship.getAbilityValue(ShipAbilityType.QUARK_REORGANIZER, ShipAbilityConstants.QUARK_REORGANIZER_SUPPLIES).get().intValue();
			int mineral1 = ship.getAbilityValue(ShipAbilityType.QUARK_REORGANIZER, ShipAbilityConstants.QUARK_REORGANIZER_MINERAL1).get().intValue();
			int mineral2 = ship.getAbilityValue(ShipAbilityType.QUARK_REORGANIZER, ShipAbilityConstants.QUARK_REORGANIZER_MINERAL2).get().intValue();
			int mineral3 = ship.getAbilityValue(ShipAbilityType.QUARK_REORGANIZER, ShipAbilityConstants.QUARK_REORGANIZER_MINERAL3).get().intValue();

			long playerId = ship.getPlayer().getId();

			if (planet != null && taskValue != null && taskValue.equals("true")) {
				ShipTransportRequest request = new ShipTransportRequest();
				request.setSupplies(Math.min(planet.retrieveTransportableSupplies(playerId), supplies));
				request.setMineral1(Math.min(planet.retrieveTransportableMineral1(playerId), mineral1));
				request.setMineral2(Math.min(planet.retrieveTransportableMineral2(playerId), mineral2));
				request.setMineral3(Math.min(planet.retrieveTransportableMineral3(playerId), mineral3));

				Pair<Ship, Planet> result = shipService.transportWithoutPermissionCheck(request, ship, ship.getPlanet());
				ship = result.getFirst();
			}

			int freeTank = ship.getShipTemplate().getFuelCapacity() - ship.getFuel();

			if (ship.getSupplies() >= supplies && ship.getMineral1() >= mineral1 && ship.getMineral2() >= mineral2 && ship.getMineral3() >= mineral3 &&
					freeTank > 0) {
				List<Integer> roundsList = new ArrayList<>(4);
				if (supplies > 0) {
					roundsList.add(ship.getSupplies());
				}
				if (mineral1 > 0) {
					roundsList.add(ship.getMineral1());
				}
				if (mineral2 > 0) {
					roundsList.add(ship.getMineral2());
				}
				if (mineral3 > 0) {
					roundsList.add(ship.getMineral3());
				}
				Collections.sort(roundsList);
				int rounds = Math.min(Math.min(roundsList.get(0), freeTank), 113);

				if (rounds > 0) {
					ship.setFuel(ship.getFuel() + rounds);

					if (supplies > 0) {
						ship.setSupplies(ship.getSupplies() - rounds);
					}
					if (mineral1 > 0) {
						ship.setMineral1(ship.getMineral1() - rounds);
					}
					if (mineral2 > 0) {
						ship.setMineral2(ship.getMineral2() - rounds);
					}
					if (mineral3 > 0) {
						ship.setMineral3(ship.getMineral3() - rounds);
					}

					shipRepository.save(ship);
				}
			}
		}

		log.debug("Processing quark reorganizer finished.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processCybernrittnikk(long gameId) {
		log.debug("Processing cybernrittnikk...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.CYBERNRITTNIKK);

		for (Ship ship : ships) {
			Planet planet = ship.getPlanet();
			String taskValue = ship.getTaskValue();

			if (planet != null && taskValue != null && taskValue.equals("true")) {
				ShipTransportRequest request = new ShipTransportRequest();
				request.setSupplies(Math.min(planet.retrieveTransportableSupplies(ship.getPlayer().getId()), 220));

				Pair<Ship, Planet> result = shipService.transportWithoutPermissionCheck(request, ship, ship.getPlanet());
				ship = result.getFirst();
			}

			if (ship.getSupplies() >= 220) {
				int colonists = ship.getAbilityValue(ShipAbilityType.CYBERNRITTNIKK, ShipAbilityConstants.CYBERNRITTNIKK_VALUE).get().intValue();

				ship.setSupplies(ship.getSupplies() - 220);
				ship.setColonists(ship.getColonists() + colonists);
				shipRepository.save(ship);
			}
		}

		log.debug("Processing cybernrittnikk finished.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processJumpEngine(long gameId) {
		log.debug("Processing jump engine...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.JUMP_ENGINE);
		Collections.sort(ships, new ShipToDestinationComparator());

		List<Ship> gravityWaveShips = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.GRAVITY_WAVE_GENERATOR);

		for (Ship ship : ships) {
			Player player = ship.getPlayer();
			int fuelCosts = ship.getAbilityValue(ShipAbilityType.JUMP_ENGINE, ShipAbilityConstants.JUMP_ENGINE_COSTS).get().intValue();

			if (ship.getFuel() >= fuelCosts) {
				int minDist = ship.getAbilityValue(ShipAbilityType.JUMP_ENGINE, ShipAbilityConstants.JUMP_ENGINE_MIN).get().intValue();
				int maxDist = ship.getAbilityValue(ShipAbilityType.JUMP_ENGINE, ShipAbilityConstants.JUMP_ENGINE_MAX).get().intValue();
				int dist = minDist + MasterDataService.RANDOM.nextInt(maxDist - minDist);

				updateDestinationBasedOnDistance(ship, dist);
				ship.setFuel(ship.getFuel() - fuelCosts);
				boolean wasStopped = false;

				for (Ship gws : gravityWaveShips) {
					if (gws.getPlayer() == player) {
						continue;
					}

					List<PlayerRelation> relationOpt = playerRelationRepository.findByPlayerIds(player.getId(), gws.getPlayer().getId());
					if (!relationOpt.isEmpty() && GRAVITY_WAVE_RELATION_TYPE_EXCLUSIONS.contains(relationOpt.get(0).getType())) {
						continue;
					}

					double cos = ((gws.getX() - ship.getX()) * (ship.getDestinationX() - ship.getX())) +
							((gws.getY() - ship.getY()) * (ship.getDestinationY() - ship.getY()));
					double c = Math.sqrt(((gws.getX() - ship.getX()) * (gws.getX() - ship.getX())) + ((gws.getY() - ship.getY()) * (gws.getY() - ship.getY())));
					double a = Math.sin(Math.acos(cos / ((c * dist) + 1))) * c;

					if (a <= 65) {
						wasStopped = true;
						dist = (int)Math.sqrt(c * c - a * a);
						updateDestinationBasedOnDistance(ship, dist);

						ship.setX(ship.getDestinationX());
						ship.setY(ship.getDestinationY());
						ship.resetTask();
						ship.resetTravel();
						ship.setPlanet(planetRepository.findByGameIdAndXAndY(gameId, ship.getX(), ship.getY()).orElse(null));
						newsService.add(player, NewsEntryConstants.news_entry_jump_engine_stopped_mid_flight, ship.createFullImagePath(), ship.getId(),
								NewsEntryClickTargetType.ship, ship.getName(), dist);
						newsService.add(gws.getPlayer(), NewsEntryConstants.news_entry_gravity_wave_intercepted_jump_engine, gws.createFullImagePath(),
								gws.getId(), NewsEntryClickTargetType.ship, gws.getName());

						ship = shipRepository.save(ship);
						break;
					}
				}

				if (!wasStopped) {
					int destX = ship.getDestinationX();
					int destY = ship.getDestinationY();
					int galaxySize = player.getGame().getGalaxySize();

					if (destX < 0 || destY < 0 || destX > galaxySize || destY > galaxySize) {
						newsService.add(player, NewsEntryConstants.news_entry_jump_engine_lost, ship.createFullImagePath(), null, null, ship.getName());
						shipService.deleteShip(ship, null);
					} else {
						ship.setX(destX);
						ship.setY(destY);
						ship.resetTask();
						ship.resetTravel();
						ship.setPlanet(planetRepository.findByGameIdAndXAndY(gameId, ship.getX(), ship.getY()).orElse(null));
						shipRepository.save(ship);
						newsService.add(player, NewsEntryConstants.news_entry_jump_engine_success, ship.createFullImagePath(), ship.getId(),
								NewsEntryClickTargetType.ship, ship.getName(), dist);
					}
				}
			} else {
				newsService.add(player, NewsEntryConstants.news_entry_jump_engine_not_enough_fuel, ship.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName());
			}
		}

		log.debug("Processing jump engine finished.");
	}

	private void updateDestinationBasedOnDistance(Ship ship, int dist) {
		if (dist > 0) {
			Vector2D pos = new Vector2D(ship.getX(), ship.getY());

			if (ship.getDestinationShip() != null) {
				ship.setDestinationX(ship.getDestinationShip().getX());
				ship.setDestinationY(ship.getDestinationShip().getY());
			}

			Vector2D target = new Vector2D(ship.getDestinationX(), ship.getDestinationY());

			if (!pos.equals(target)) {
				Vector2D direction = target.subtract(pos).normalize();
				Vector2D newTarget = pos.add(direction.scalarMultiply(dist));

				ship.setDestinationX((int)newTarget.getX());
				ship.setDestinationY((int)newTarget.getY());
			}
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processGravityWaveGenerator(long gameId) {
		log.debug("Processing gravity wave generator...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.GRAVITY_WAVE_GENERATOR);

		for (Ship ship : ships) {
			Player player = ship.getPlayer();
			int mineral3Costs = ship.getAbilityValue(ShipAbilityType.GRAVITY_WAVE_GENERATOR, ShipAbilityConstants.GRAVITY_WAVE_GENERATOR_COSTS).get()
					.intValue();

			if (ship.getMineral3() >= mineral3Costs) {
				ship.setMineral3(ship.getMineral3() - mineral3Costs);
				ship = shipRepository.save(ship);

				List<Long> shipIdsInRadius = shipRepository.getShipIdsInRadius(gameId, ship.getX(), ship.getY(), 65);
				List<Ship> shipsInRadius = shipRepository.findByIds(shipIdsInRadius);

				boolean stoppedJumps = false;

				for (Ship s : shipsInRadius) {
					ShipAbility activeAbility = s.getActiveAbility();
					boolean notJumpIntercepted = activeAbility == null || !GRAVITY_WAVE_INTERCEPTING_ABILITIES.contains(activeAbility.getType());
					Player otherPlayer = s.getPlayer();
					List<PlayerRelation> relationOpt = playerRelationRepository.findByPlayerIds(otherPlayer.getId(), player.getId());
					boolean relationShipClashes = !relationOpt.isEmpty() && GRAVITY_WAVE_RELATION_TYPE_EXCLUSIONS.contains(relationOpt.get(0).getType());

					if (notJumpIntercepted || otherPlayer == player || relationShipClashes) {
						if (s.getTravelSpeed() > 7) {
							s.setTravelSpeed(7);
							newsService.add(otherPlayer, NewsEntryConstants.news_entry_gravity_wave_generator_slowed, s.createFullImagePath(), ship.getId(),
									NewsEntryClickTargetType.ship, s.getName());
							shipRepository.save(s);
						}
					} else {
						stoppedJumps = true;
						s.resetTask();
						s.resetTravel();
						newsService.add(otherPlayer, NewsEntryConstants.news_entry_jump_engine_stopped, s.createFullImagePath(), ship.getId(),
								NewsEntryClickTargetType.ship, s.getName());
						shipRepository.save(s);
					}
				}

				if (stoppedJumps) {
					newsService.add(player, NewsEntryConstants.news_entry_gravity_wave_generator_stopped_jumps, ship.createFullImagePath(), ship.getId(),
							NewsEntryClickTargetType.ship, ship.getName());
				}
			} else {
				newsService.add(player, NewsEntryConstants.news_entry_gravity_wave_generator_not_enough_mineral3, ship.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName());
			}
		}

		log.debug("Processing gravity wave generator finished.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processPerfectCloaking(long gameId) {
		log.debug("Processing perfect cloaking...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.CLOAKING_PERFECT);

		for (Ship ship : ships) {
			updateCloak(ship);
			finishCloakingChecks(ship);
			shipRepository.save(ship);
		}

		shipRepository.cloakAntigravPropulsion(gameId);

		log.debug("Processing perfect cloaking finished.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processReliableCloaking(long gameId) {
		log.debug("Processing reliable cloaking...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.CLOAKING_RELIABLE);

		for (Ship ship : ships) {
			updateCloak(ship);

			int random = MasterDataService.RANDOM.nextInt(10);
			ship.setCloaked(random != 9);

			finishCloakingChecks(ship);
			shipRepository.save(ship);
		}

		log.debug("Processing reliable cloaking finished.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processUnreliableCloaking(long gameId) {
		log.debug("Processing unreliable cloaking...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.CLOAKING_UNRELIABLE);
		int visibilityRadius = configProperties.getVisibilityRadius();

		for (Ship ship : ships) {
			updateCloak(ship);

			List<Long> shipIdsInRadius = shipRepository.getShipIdsInRadius(gameId, ship.getX(), ship.getY(), visibilityRadius);
			List<Long> planetIdsInRadius = planetRepository.getPlanetIdsInRadius(gameId, ship.getX(), ship.getY(), visibilityRadius, false);

			int random = MasterDataService.RANDOM.nextInt(20);
			ship.setCloaked(random > (1 + shipIdsInRadius.size() + planetIdsInRadius.size()));

			finishCloakingChecks(ship);
			shipRepository.save(ship);
		}

		log.debug("Processing unreliable cloaking finished.");
	}

	protected void updateCloak(Ship ship) {
		int mineral2 = ship.getMineral2();
		int needed = (int)Math.ceil(ship.getShipTemplate().getMass() / 100f);
		boolean cloaked = mineral2 >= needed;
		ship.setCloaked(cloaked);

		if (cloaked) {
			ship.setMineral2(mineral2 - needed);
		}
	}

	protected void finishCloakingChecks(Ship ship) {
		if (ship.isCloaked() && ship.getPropulsionSystemTemplate().getTechLevel() == 7) {
			float prop = MasterDataService.RANDOM.nextFloat();
			checkCloakingPlasmaDischarge(ship, prop);
		}

		if (ship.isCloaked()) {
			shipRepository.clearDestinationShip(Lists.newArrayList(ship.getId()));
		}
	}

	protected void checkCloakingPlasmaDischarge(Ship ship, float prop) {
		if (prop <= 0.19f) {
			ship.setCloaked(false);
			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_cloaking_failed_plasma_discharge, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName());
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processSubSpaceDistortion(long gameId) {
		log.debug("Processing sub space distortion...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.SUB_SPACE_DISTORTION);

		for (Ship ship : ships) {
			int level = ship.getAbilityValue(ShipAbilityType.SUB_SPACE_DISTORTION, ShipAbilityConstants.SUB_SPACE_DISTORTION_LEVEL).get().intValue();
			int damage = level * 50;

			damageShipsBySubSpaceDistortion(gameId, ship, damage);
			destroySpaceFoldsBySubSpaceDistortion(gameId, ship, level);

			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_created_subspace_distortion, ship.createFullImagePath(), null, null,
					ship.getName());
			shipService.deleteShip(ship, null, false);
		}

		log.debug("Processing sub space distortion finished.");
	}

	protected void damageShipsBySubSpaceDistortion(long gameId, Ship ship, int damage) {
		int range = configProperties.getSubSpaceDistortionRange();
		List<Long> shipIds = shipRepository.getShipIdsInRadius(gameId, ship.getX(), ship.getY(), range);
		shipIds.remove(ship.getId());
		List<Ship> damagedShips = shipRepository.findByIds(shipIds);

		for (Ship s : damagedShips) {
			if (s.hasActiveAbility(ShipAbilityType.SUB_SPACE_DISTORTION)) {
				continue;
			}

			int mass = s.getShipTemplate().getMass();
			int shipDamage = Math.round(s.getDamage() + (damage * (80f / (mass + 1)) * (80f / (mass + 1)) + 2));

			if (shipDamage < 100) {
				s.setDamage(shipDamage);
				s = shipRepository.save(s);
				newsService.add(s.getPlayer(), NewsEntryConstants.news_entry_damaged_by_subspace_distortion, s.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, s.getName(), shipDamage);
			} else {
				newsService.add(s.getPlayer(), NewsEntryConstants.news_entry_destroyed_by_subspace_distortion, s.createFullImagePath(), null, null,
						s.getName());
				shipService.deleteShip(s, ship.getPlayer());
			}
		}
	}

	protected void destroySpaceFoldsBySubSpaceDistortion(long gameId, Ship ship, int level) {
		int range = configProperties.getSubSpaceDistortionRange();
		List<Long> spaceFoldIds = spaceFoldRepository.getSpaceFoldIdsInRadius(gameId, ship.getX(), ship.getY(), range);

		for (Long spaceFoldId : spaceFoldIds) {
			int chance = 1 + MasterDataService.RANDOM.nextInt(9);

			if (chance <= level) {
				spaceFoldRepository.deleteById(spaceFoldId);
			}
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processJumpPortalConstruction(long gameId) {
		log.debug("Processing jump portal construction...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.JUMP_PORTAL);
		int minAnomalyDistance = configProperties.getMinAnomalyDistance();

		for (Ship ship : ships) {
			if (ship.getTravelSpeed() != 0) {
				continue;
			}

			Player player = ship.getPlayer();
			int x = ship.getX();
			int y = ship.getY();
			List<Long> nearPlanets = planetRepository.getPlanetIdsInRadius(gameId, x, y, minAnomalyDistance, false);

			if (!nearPlanets.isEmpty()) {
				newsService.add(player, NewsEntryConstants.news_entry_jump_portal_planets_too_near, ship.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName());
				continue;
			}

			List<Long> nearWormHoles = wormHoleRepository.getWormHoleIdsInRadius(gameId, x, y, minAnomalyDistance);

			if (!nearWormHoles.isEmpty()) {
				newsService.add(player, NewsEntryConstants.news_entry_jump_portal_anomalies_too_near, ship.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName());
				continue;
			}

			int fuelCosts = ship.getAbilityValue(ShipAbilityType.JUMP_PORTAL, ShipAbilityConstants.JUMP_PORTAL_FUEL).get().intValue();
			int mineral1Costs = ship.getAbilityValue(ShipAbilityType.JUMP_PORTAL, ShipAbilityConstants.JUMP_PORTAL_MINERAL1).get().intValue();
			int mineral2Costs = ship.getAbilityValue(ShipAbilityType.JUMP_PORTAL, ShipAbilityConstants.JUMP_PORTAL_MINERAL2).get().intValue();
			int mineral3Costs = ship.getAbilityValue(ShipAbilityType.JUMP_PORTAL, ShipAbilityConstants.JUMP_PORTAL_MINERAL3).get().intValue();

			if (ship.getFuel() < fuelCosts || ship.getMineral1() < mineral1Costs || ship.getMineral2() < mineral2Costs || ship.getMineral3() < mineral3Costs) {
				newsService.add(player, NewsEntryConstants.news_entry_jump_portal_not_enough_resources, ship.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName());
				continue;
			}

			ship.setFuel(ship.getFuel() - fuelCosts);
			ship.setMineral1(ship.getMineral1() - mineral1Costs);
			ship.setMineral2(ship.getMineral2() - mineral2Costs);
			ship.setMineral3(ship.getMineral3() - mineral3Costs);

			ship = shipRepository.save(ship);

			Optional<WormHole> unfinishedJumpPortalOpt = wormHoleRepository.findUnfinishedJumpPortal(ship.getId());

			buildJumpPortal(ship, unfinishedJumpPortalOpt);
		}

		log.debug("Processing jump portal construction finished.");
	}

	protected void buildJumpPortal(Ship ship, Optional<WormHole> unfinishedJumpPortalOpt) {
		Player player = ship.getPlayer();

		if (unfinishedJumpPortalOpt.isPresent()) {
			WormHole first = unfinishedJumpPortalOpt.get();

			WormHole second = new WormHole();
			second.setConnection(first);
			second.setGame(player.getGame());
			second.setShip(ship);
			second.setType(WormHoleType.JUMP_PORTAL);
			second.setX(ship.getX());
			second.setY(ship.getY());
			second = wormHoleRepository.save(second);

			first.setConnection(second);
			wormHoleRepository.save(first);

			newsService.add(player, NewsEntryConstants.news_entry_jump_portals_completed, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName());
		} else {
			WormHole first = new WormHole();
			first.setGame(player.getGame());
			first.setShip(ship);
			first.setType(WormHoleType.JUMP_PORTAL);
			first.setX(ship.getX());
			first.setY(ship.getY());
			wormHoleRepository.save(first);

			newsService.add(player, NewsEntryConstants.news_entry_jump_portal_build, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName());
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processPerfectEvade(long gameId) {
		log.debug("Processing perfect evade...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.EVADE_PERFECT);

		if (!ships.isEmpty()) {
			for (Ship ship : ships) {
				ship.setX(ship.getCreationX());
				ship.setY(ship.getCreationY());
				ship.resetTask();
				ship.resetTravel();

				Optional<Planet> planetOpt = planetRepository.findByGameIdAndXAndY(gameId, ship.getCreationX(), ship.getCreationY());
				ship.setPlanet(planetOpt.orElse(null));

				newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_evade_perfect, ship.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName());
			}

			shipRepository.saveAll(ships);
			shipRepository.clearDestinationShip(ships.stream().map(Ship::getId).collect(Collectors.toList()));
		}

		log.debug("Processing perfect evade finished.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processUnreliableEvade(long gameId) {
		log.debug("Processing unreliable evade...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.EVADE_UNRELIABLE);

		if (!ships.isEmpty()) {
			for (Ship ship : ships) {
				int maxDamage = ship.getAbilityValue(ShipAbilityType.EVADE_UNRELIABLE, ShipAbilityConstants.EVADE_DAMAGE_MAX).get().intValue();
				int damage = 1 + MasterDataService.RANDOM.nextInt(maxDamage);

				ship.setDamage(ship.getDamage() + damage);

				if (ship.getDamage() >= 100) {
					newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_evade_destroyed, ship.createFullImagePath(), null, null, ship.getName());
					shipService.deleteShip(ship, null);
				} else {
					ship.setX(ship.getCreationX());
					ship.setY(ship.getCreationY());
					ship.resetTask();
					ship.resetTravel();

					Optional<Planet> planetOpt = planetRepository.findByGameIdAndXAndY(gameId, ship.getCreationX(), ship.getCreationY());
					ship.setPlanet(planetOpt.orElse(null));

					shipRepository.clearDestinationShip(Lists.newArrayList(ship.getId()));
					newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_evade_damaged, ship.createFullImagePath(), ship.getId(),
							NewsEntryClickTargetType.ship, ship.getName(), damage);
					shipRepository.save(ship);
				}
			}
		}

		log.debug("Processing unreliable evade finished.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processViralInvasion(long gameId) {
		log.debug("Processing viral invasion...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.VIRAL_INVASION);

		for (Ship ship : ships) {
			Planet planet = ship.getPlanet();
			if (planet == null) {
				continue;
			}

			Player player = planet.getPlayer();
			if (player == ship.getPlayer()) {
				continue;
			}

			if (player != null) {
				List<PlayerRelation> relationOpt = playerRelationRepository.findByPlayerIds(ship.getPlayer().getId(), player.getId());
				if (!relationOpt.isEmpty() && relationOpt.get(0).getType() == PlayerRelationType.ALLIANCE) {
					continue;
				}
			}

			boolean againstColonists = player != null && ShipAbilityConstants.VIRAL_INVASION_COLONISTS.equals(ship.getTaskValue());
			boolean againstNatives = ShipAbilityConstants.VIRAL_INVASION_NATIVES.equals(ship.getTaskValue());

			if (planet.hasOrbitalSystem(OrbitalSystemType.MEDICAL_CENTER)) {
				processMedicalCenter(ship, againstColonists, againstNatives);
			} else {
				performViralInvasion(ship, againstColonists, againstNatives);
			}
		}

		log.debug("Processing viral invasion finished.");
	}

	protected void processMedicalCenter(Ship ship, boolean againstColonists, boolean againstNatives) {
		Planet planet = ship.getPlanet();
		String image = ImageConstants.news_epidemie;

		newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_natives_unsuccessfull, image, ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), planet.getName());

		if (againstColonists) {
			newsService.add(planet.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_natives_prevented_colonists, image, planet.getId(),
					NewsEntryClickTargetType.planet, planet.getName());
		} else if (againstNatives) {
			newsService.add(planet.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_natives_prevented_natives, image, planet.getId(),
					NewsEntryClickTargetType.planet, planet.getName());
		}
	}

	protected void performViralInvasion(Ship ship, boolean againstColonists, boolean againstNatives) {
		Planet planet = ship.getPlanet();
		Player player = planet.getPlayer();
		String image = ImageConstants.news_epidemie;

		int min = ship.getAbilityValue(ShipAbilityType.VIRAL_INVASION, ShipAbilityConstants.VIRAL_INVASION_MIN).get().intValue();
		int max = ship.getAbilityValue(ShipAbilityType.VIRAL_INVASION, ShipAbilityConstants.VIRAL_INVASION_MAX).get().intValue();
		int percentage = min + MasterDataService.RANDOM.nextInt(max - min);

		if (againstColonists) {
			int casualties = (int)(planet.getColonists() / 100f * percentage);
			planet.setColonists(planet.getColonists() - casualties);
			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_killed_colonists, image, ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName(), planet.getName(), casualties, percentage);
			newsService.add(player, NewsEntryConstants.news_entry_viral_invasion_colonists_got_killed, image, planet.getId(),
					NewsEntryClickTargetType.planet, planet.getName(), casualties, percentage);
		} else if (againstNatives) {
			int casualties = (int)(planet.getNativeSpeciesCount() / 100f * percentage);
			planet.setNativeSpeciesCount(planet.getNativeSpeciesCount() - casualties);
			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_killed_natives, image, ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName(), planet.getName(), casualties, percentage);

			if (player != null) {
				newsService.add(player, NewsEntryConstants.news_entry_viral_invasion_natives_got_killed, image, planet.getId(),
						NewsEntryClickTargetType.planet, planet.getName(), casualties, percentage);
			}
		}

		planetRepository.save(planet);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processDestabilizer(long gameId) {
		log.debug("Processing destabilizer...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.DESTABILIZER);

		for (Ship ship : ships) {
			Player player = ship.getPlayer();
			Planet planet = ship.getPlanet();

			if (planet == null || planet.getPlayer() == player) {
				continue;
			}

			if (planet.getPlayer() != null) {
				List<PlayerRelation> relationOpt = playerRelationRepository.findByPlayerIds(player.getId(), planet.getPlayer().getId());

				if (!relationOpt.isEmpty() && relationOpt.get(0).getType() == PlayerRelationType.ALLIANCE) {
					continue;
				}
			}

			if (planet.hasOrbitalSystem(OrbitalSystemType.PLANETARY_SHIELDS)) {
				continue;
			}

			int value = ship.getAbilityValue(ShipAbilityType.DESTABILIZER, ShipAbilityConstants.DESTABILIZER_EFFICIENCY).get().intValue();
			float prop = MasterDataService.RANDOM.nextFloat();

			if (value / 100f >= prop) {
				destroyPlanet(gameId, player, planet);
			}
		}

		log.debug("Processing destabilizer finished.");
	}

	protected void destroyPlanet(long gameId, Player player, Planet planet) {
		String planetName = planet.getName();
		String sector = CoordHelper.getSectorString(planet);

		List<Player> allPlayers = player.getGame().getPlayers();

		for (Player p : allPlayers) {
			String newsTemplate = NewsEntryConstants.news_entry_planet_destroyed;

			if (p == planet.getPlayer()) {
				newsTemplate = NewsEntryConstants.news_entry_our_planet_destroyed;
			}

			newsService.add(p, newsTemplate, ImageConstants.news_explode, null, null, planetName, sector);
		}

		long planetId = planet.getId();
		shipRepository.clearPlanet(planetId);
		List<Ship> shipList = shipRepository.findByDestination(gameId, planet.getX(), planet.getY());

		for (Ship s : shipList) {
			s.resetTravel();
			shipRepository.save(s);
		}

		Player owner = planet.getPlayer();

		if (owner != null && owner.getHomePlanet() == planet) {
			owner.setHomePlanet(null);
			playerRepository.save(owner);
		}

		if (owner != null) {
			statsUpdater.incrementStats(owner, LoginStatsFaction::getPlanetsLost, LoginStatsFaction::setPlanetsLost);

			if (planet.getStarbase() != null) {
				statsUpdater.incrementStats(owner, LoginStatsFaction::getStarbasesLost, LoginStatsFaction::setStarbasesLost);
			}
		}

		spaceFoldRepository.deleteByPlanetId(planetId);
		playerPlanetScanLogRepository.deleteByPlanetId(planetId);
		orbitalSystemRepository.deleteByPlanetId(planetId);
		shipRepository.clearCurrentRouteEntryByRoutePlanetId(planetId);
		shipRouteEntryRepository.deleteByPlanetId(planetId);

		Starbase starbase = planet.getStarbase();
		planetRepository.delete(planet);

		if (starbase != null) {
			starbaseService.delete(starbase);
		}
	}
}