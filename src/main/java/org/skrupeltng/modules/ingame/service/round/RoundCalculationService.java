package org.skrupeltng.modules.ingame.service.round;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.skrupeltng.modules.ai.AIRoundCalculator;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.ingame.service.round.combat.ground.GroundCombatRoundCalculator;
import org.skrupeltng.modules.ingame.service.round.combat.orbit.OrbitalCombatRoundCalculator;
import org.skrupeltng.modules.ingame.service.round.combat.space.SpaceCombatRoundCalculator;
import org.skrupeltng.modules.ingame.service.round.losecondition.LoseConditionCalculator;
import org.skrupeltng.modules.ingame.service.round.ship.ShipRoundAbilities;
import org.skrupeltng.modules.ingame.service.round.ship.ShipRoundMovement;
import org.skrupeltng.modules.ingame.service.round.ship.ShipRoundTasks;
import org.skrupeltng.modules.ingame.service.round.wincondition.WinConditionCalculator;
import org.skrupeltng.modules.mail.service.MailService;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class RoundCalculationService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PoliticsRoundCalculator politicsRoundCalculator;

	@Autowired
	private PlanetRoundCalculator planetRoundCalculator;

	@Autowired
	private RouteRoundCalculator routeRoundCalculator;

	@Autowired
	private StarbaseShipConstructionRoundCalculator starbaseShipConstructionRoundCalculator;

	@Autowired
	private ShipRoundMovement shipRoundMovement;

	@Autowired
	private ShipRoundAbilities shipRoundAbilities;

	@Autowired
	private ShipRoundTasks shipRoundTasks;

	@Autowired
	private OrbitalCombatRoundCalculator orbitalCombatRoundCalculator;

	@Autowired
	private SpaceCombatRoundCalculator spaceCombatRoundCalculator;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private WormHoleRoundCalculator wormHoleRoundCalculator;

	@Autowired
	private SpaceFoldRoundCalculator spaceFoldRoundCalculator;

	@Autowired
	private MeteorRoundCalculator meteorRoundCalculator;

	@Autowired
	private PirateRoundCalculator pirateRoundCalculator;

	@Autowired
	private PlasmaStormRoundCalculator plasmaStormRoundCalculator;

	@Autowired
	private GroundCombatRoundCalculator groundCombatRoundCalculator;

	@Autowired
	private MineFieldRoundCalculator mineFieldRoundCalculator;

	@Autowired
	private FleetRoundCalculator fleetRoundCalculator;

	@Autowired
	private NewsService newsService;

	@Autowired
	private LoseConditionCalculator loseConditionCalculator;

	@Autowired
	private WinConditionCalculator winConditionCalculator;

	@Autowired
	private MailService mailService;

	@Autowired
	private VisibleObjects visibleObjects;

	@Autowired
	private Map<String, AIRoundCalculator> aiRoundCalculators;

	public void calculateRound(long gameId) {
		calculateRound(gameId, null);
	}

	public void calculateRound(long gameId, Long triggeringLoginId) {
		try {
			log.debug("Starting round calculation...");
			execute(gameId, () -> visibleObjects.clearCaches());

			execute(gameId, () -> newsService.cleanNewsEntries(gameId));

			execute(gameId, () -> routeRoundCalculator.processRouteTravel(gameId));

			execute(gameId, () -> politicsRoundCalculator.updateCancelledRelations(gameId));

			execute(gameId, () -> shipRoundTasks.precheckTractorBeam(gameId));

			execute(gameId, () -> plasmaStormRoundCalculator.processShips(gameId));

			execute(gameId, () -> shipRoundAbilities.processGravityWaveGenerator(gameId));
			execute(gameId, () -> shipRoundAbilities.processJumpEngine(gameId));
			execute(gameId, () -> shipRoundAbilities.processSubSpaceDistortion(gameId));
			execute(gameId, () -> shipRoundAbilities.processPerfectEvade(gameId));
			execute(gameId, () -> shipRoundAbilities.processUnreliableEvade(gameId));

			execute(gameId, () -> shipRoundMovement.processMovingShips(gameId));
			execute(gameId, () -> shipRoundTasks.processTractorBeam(gameId));
			execute(gameId, () -> shipRoundMovement.updateDestinations(gameId));

			execute(gameId, () -> wormHoleRoundCalculator.processWormholes(gameId));
			execute(gameId, () -> shipRoundTasks.processAutoDestruction(gameId));
			execute(gameId, () -> spaceFoldRoundCalculator.processSpaceFolds(gameId));

			execute(gameId, () -> shipRoundTasks.processAutomaticProjectileConstruction(gameId));

			execute(gameId, () -> starbaseShipConstructionRoundCalculator.processShipConstructionJobs(gameId));

			execute(gameId, () -> shipRoundMovement.processGravity(gameId));

			execute(gameId, () -> mineFieldRoundCalculator.processClearMineField(gameId));
			execute(gameId, () -> mineFieldRoundCalculator.processMineFieldDepletion(gameId));
			execute(gameId, () -> mineFieldRoundCalculator.processMineFieldHits(gameId));

			execute(gameId, () -> orbitalCombatRoundCalculator.processCombat(gameId));
			execute(gameId, () -> spaceCombatRoundCalculator.processCombat(gameId));

			execute(gameId, () -> shipRoundMovement.processShipEvasionOfAlliedPlanets(gameId));
			execute(gameId, () -> shipRoundMovement.processShipEvasionOfOtherShips(gameId));

			execute(gameId, () -> shipRoundTasks.processCreateMineField(gameId));

			execute(gameId, () -> shipRoundAbilities.processQuarkReorganizer(gameId));
			execute(gameId, () -> shipRoundAbilities.processCybernrittnikk(gameId));
			execute(gameId, () -> shipRoundAbilities.processSubParticleCluster(gameId));

			execute(gameId, () -> shipRoundAbilities.processDestabilizer(gameId));

			execute(gameId, () -> shipRoundTasks.processShipRecycle(gameId));
			execute(gameId, () -> shipRoundTasks.processShipRepair(gameId));
			execute(gameId, () -> shipRoundTasks.processHireCrew(gameId));
			execute(gameId, () -> shipRoundTasks.processAutorefuel(gameId));
			execute(gameId, () -> shipRoundTasks.processPlanetBombardment(gameId));
			execute(gameId, () -> shipRoundAbilities.processViralInvasion(gameId));
			execute(gameId, () -> shipRoundAbilities.processTerraformer(gameId));
			execute(gameId, () -> shipRoundAbilities.processJumpPortalConstruction(gameId));
			execute(gameId, () -> shipRoundTasks.processHunterTraining(gameId));
			execute(gameId, () -> shipRoundAbilities.processPerfectCloaking(gameId));
			execute(gameId, () -> shipRoundAbilities.processReliableCloaking(gameId));
			execute(gameId, () -> shipRoundAbilities.processUnreliableCloaking(gameId));
			execute(gameId, () -> shipRoundMovement.processDrugunConverter(gameId));
			execute(gameId, () -> shipRoundAbilities.processSensors(gameId));

			execute(gameId, () -> planetRoundCalculator.processNewColonies(gameId));
			Map<Long, Float> tradeBonusData = politicsRoundCalculator.getTradeBonusData(gameId);
			execute(gameId, () -> planetRoundCalculator.processOwnedPlanets(gameId, tradeBonusData));

			if (!gameRepository.isTutorial(gameId)) {
				execute(gameId, () -> meteorRoundCalculator.processBigMeteors(gameId, MasterDataService.RANDOM.nextInt(200)));
				int galaxySize = gameRepository.getGalaxySize(gameId);
				execute(gameId, () -> meteorRoundCalculator.processSmallMeteors(gameId, MasterDataService.RANDOM.nextInt(galaxySize / 125)));
			}

			execute(gameId, () -> pirateRoundCalculator.processPirateAttacks(gameId));

			execute(gameId, () -> routeRoundCalculator.processPlanetExchange(gameId));

			execute(gameId, () -> shipRoundTasks.precheckTractorBeam(gameId));

			execute(gameId, () -> plasmaStormRoundCalculator.processPlasmaStormDepletion(gameId));
			execute(gameId, () -> plasmaStormRoundCalculator.processPlasmaStormCreation(gameId));

			execute(gameId, () -> groundCombatRoundCalculator.processGroundCombat(gameId));

			execute(gameId, () -> planetRoundCalculator.updateScanRadius(gameId));
			execute(gameId, () -> planetRoundCalculator.logVisitedPlanets(gameId));

			execute(gameId, () -> fleetRoundCalculator.updateFleetsWithoutLeader(gameId));

			execute(gameId, () -> computeAIRounds(gameId));

			execute(gameId, () -> loseConditionCalculator.checkLoseCondition(gameId));
			execute(gameId, () -> winConditionCalculator.checkWinCondition(gameId));
		} catch (Exception e) {
			log.error("Error while calculating round for game " + gameId + ": ", e);
			mailService.sendRoundErrorEmail(gameId, e);
		} finally {
			setupTurnValues(gameId);

			gameRepository.increaseRound(gameId);
			mailService.sendRoundNotificationEmails(gameId, triggeringLoginId);

			log.debug("Round calculation finished.");
		}
	}

	private void execute(long gameId, Runnable function) {
		try {
			function.run();
		} catch (Exception e) {
			log.error("Error while calculating round for game " + gameId + ": ", e);
			mailService.sendRoundErrorEmail(gameId, e);
		}
	}

	public void computeAIRounds(long gameId) {
		if (!gameRepository.isTutorial(gameId)) {
			log.debug("Computing AI rounds...");

			List<Player> players = playerRepository.findAIPlayers(gameId);

			ExecutorService executor = Executors.newSingleThreadExecutor();

			for (Player player : players) {
				if (!player.isHasLost()) {
					try {
						log.debug("Computing AI round for player " + player.getId() + "...");
						Future<?> future = executor.submit(() -> calculateAIRound(player));
						future.get();
						log.debug("AI round for player " + player.getId() + " done.");
					} catch (Exception e) {
						log.error("Error while calculating ai round for player " + player.getId() + ": ", e);
					}
				}
			}

			executor.shutdown();
			log.debug("AI rounds computed.");
		}
	}

	private void calculateAIRound(Player player) {
		SecurityContext context = SecurityContextHolder.getContext();
		String aiLevel = player.getAiLevel().name();
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(aiLevel, aiLevel);
		context.setAuthentication(authentication);

		AIRoundCalculator roundCalculator = aiRoundCalculators.get(aiLevel);
		roundCalculator.calculateRound(player.getId());
	}

	public void setupTurnValues(long gameId) {
		log.debug("Setting up turn values...");
		playerRepository.setupTurnValues(gameId);
		log.debug("Turn values set up.");
	}
}