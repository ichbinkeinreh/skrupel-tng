package org.skrupeltng.modules.ingame.service.round;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.service.AchievementService;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineField;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineFieldRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.skrupeltng.modules.ingame.service.round.combat.ShipDamage;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class MineFieldRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private MineFieldRepository mineFieldRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private ShipService shipService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private AchievementService achievementService;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processMineFieldDepletion(long gameId) {
		if (!gameRepository.mineFieldsEnabled(gameId)) {
			return;
		}

		log.debug("Processing mine field depletion...");

		List<MineField> mineFields = mineFieldRepository.findByGameId(gameId);

		for (MineField mineField : mineFields) {
			if (MasterDataService.RANDOM.nextFloat() <= 0.8f) {
				depleteMineField(mineField);
			}
		}

		log.debug("Processing mine field depletion finished.");
	}

	protected void depleteMineField(MineField mineField) {
		mineField.setMines(mineField.getMines() - 1);

		if (mineField.getMines() == 0) {
			mineFieldRepository.delete(mineField);
		} else {
			mineFieldRepository.save(mineField);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processMineFieldHits(long gameId) {
		if (!gameRepository.mineFieldsEnabled(gameId)) {
			return;
		}

		log.debug("Processing mine field hits...");

		List<Long> shipIds = shipRepository.findShipsInFreeSpace(gameId);

		for (Long shipId : shipIds) {
			Ship ship = shipRepository.getOne(shipId);
			List<Long> mineFieldIds = mineFieldRepository.getMineFieldIdsInRadius(gameId, ship.getX(), ship.getY(), 85);

			Pair<MineField, Integer> result = getMostDenseMineField(ship.getPlayer().getId(), mineFieldIds);
			MineField mineField = result.getLeft();
			Integer possibleMines = result.getRight();

			if (possibleMines > 0) {
				int mines = 1;

				if (possibleMines > 1) {
					int chance = MasterDataService.RANDOM.nextInt(50);
					mines = Math.round(chance * possibleMines / 100f);
				}

				if (mines > 0) {
					damageShipByMineField(ship, mineField, mines);
				}
			}
		}

		log.debug("Processing mine field hits finished.");
	}

	protected Pair<MineField, Integer> getMostDenseMineField(long shipPlayerId, List<Long> mineFieldIds) {
		int possibleMines = 0;
		MineField densestMineField = null;

		for (Long mineFieldId : mineFieldIds) {
			MineField mineField = mineFieldRepository.getOne(mineFieldId);
			long mineFieldPlayerId = mineField.getPlayer().getId();

			if (shipPlayerId == mineFieldPlayerId) {
				continue;
			}

			List<PlayerRelation> relationOpt = playerRelationRepository.findByPlayerIds(shipPlayerId, mineFieldPlayerId);

			if (!relationOpt.isEmpty()) {
				PlayerRelationType type = relationOpt.get(0).getType();

				if (type == PlayerRelationType.ALLIANCE || type == PlayerRelationType.NON_AGGRESSION_TREATY) {
					continue;
				}
			}

			if (mineField.getMines() > possibleMines) {
				possibleMines = mineField.getMines();
				densestMineField = mineField;
			}
		}

		return Pair.of(densestMineField, possibleMines);
	}

	protected void damageShipByMineField(Ship ship, MineField mineField, int mines) {
		mineField.setMines(mineField.getMines() - mines);

		if (mineField.getMines() == 0) {
			mineFieldRepository.delete(mineField);
		} else {
			mineFieldRepository.save(mineField);
		}

		int damageHull = ShipDamage.PROJECTILE_WEAPON_DAMAGE_DATA[mineField.getLevel()] * mines;
		int damageCrew = ShipDamage.PROJECTILE_WEAPON_DAMAGE_CREW_DATA[mineField.getLevel()] * mines;

		int crew = ship.getCrew();

		ship.receiveDamage(damageHull, damageCrew, true, ship.getShipTemplate().getMass());

		String sector = CoordHelper.getSectorString(ship);
		int damage = ship.getDamage();

		if (damage < 100 && ship.getCrew() > 0) {
			int crewDamage = crew - ship.getCrew();

			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_damaged_ship, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName(), sector, mines, damage, crewDamage);
			shipRepository.save(ship);
		} else {
			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_destroyed_ship, ship.createFullImagePath(), null, null, ship.getName(),
					sector);
			shipService.deleteShip(ship, mineField.getPlayer());

			achievementService.unlockAchievement(mineField.getPlayer(), AchievementType.DESTROY_SHIP_BY_MINE_FIELD);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processClearMineField(long gameId) {
		if (!gameRepository.mineFieldsEnabled(gameId)) {
			return;
		}

		log.debug("Processing clearing mine field...");

		List<Ship> ships = shipRepository.getShipsWithTask(gameId, ShipTaskType.CLEAR_MINE_FIELD);

		for (Ship ship : ships) {
			if (ship.getPlanet() != null) {
				newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_not_cleared, ImageConstants.news_mine_field, ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName());
				continue;
			}

			int hangarCapacity = ship.getShipTemplate().getHangarCapacity();

			if (hangarCapacity == 0) {
				continue;
			}

			int halfHangars = hangarCapacity / 2;
			int minesToBeCleared = hangarCapacity == 1 ? 1 : halfHangars + MasterDataService.RANDOM.nextInt(halfHangars);

			List<Long> mineFieldIds = mineFieldRepository.getMineFieldIdsInRadius(gameId, ship.getX(), ship.getY(), 100);

			Pair<MineField, Integer> result = getMostDenseMineField(ship.getPlayer().getId(), mineFieldIds);

			if (result.getLeft() != null) {
				clearMineField(ship, result.getLeft(), minesToBeCleared);
			}
		}

		log.debug("Processing clearing mine field finished.");
	}

	protected void clearMineField(Ship ship, MineField mineField, int mines) {
		mineField.setMines(mineField.getMines() - mines);

		if (mineField.getMines() > 0) {
			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_cleared, ImageConstants.news_mine_field, ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName(), mines);
			mineFieldRepository.save(mineField);
		} else {
			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_cleared_fully, ImageConstants.news_mine_field, ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName());
			newsService.add(mineField.getPlayer(), NewsEntryConstants.news_entry_mine_field_cleared_by_enemy, ImageConstants.news_mine_field, null, null);
			mineFieldRepository.delete(mineField);
		}
	}
}