package org.skrupeltng.modules.ingame.service.round.wincondition;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoeRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("TEAM_DEATH_FOE")
public class TeamDeathFoeWinConditionHandler implements WinConditionHandler {

	private PlayerRepository playerRepository;
	private PlayerRelationRepository playerRelationRepository;
	private PlayerDeathFoeRepository playerDeathFoeRepository;
	private GameRepository gameRepository;

	@Override
	public void checkWinCondition(long gameId) {
		List<Player> potentialWinners = playerRepository.getNotLostPlayers(gameId);

		for (Player potentialWinner : potentialWinners) {
			List<Player> deathFoes = playerDeathFoeRepository.getDeathFoesByPlayerId(potentialWinner.getId());
			boolean someNotLost = deathFoes.stream().anyMatch(d -> !d.isHasLost());

			if (!someNotLost) {
				List<PlayerRelation> relations = playerRelationRepository.findByPlayerIdAndType(potentialWinner.getId(), PlayerRelationType.ALLIANCE);
				Set<Long> allWinners = new HashSet<>(relations.size() + 1);

				for (PlayerRelation relation : relations) {
					allWinners.add(relation.getPlayer1().getId());
					allWinners.add(relation.getPlayer2().getId());
				}

				Game game = gameRepository.getOne(gameId);
				List<Player> allPlayers = game.getPlayers();

				for (Player player : allPlayers) {
					if (!allWinners.contains(player.getId())) {
						player.setHasLost(true);
						playerRepository.save(player);
					}
				}

				game.setFinished(true);
				gameRepository.save(game);

				break;
			}
		}
	}

	@Autowired
	public void setPlayerRepository(PlayerRepository playerRepository) {
		this.playerRepository = playerRepository;
	}

	@Autowired
	public void setPlayerRelationRepository(PlayerRelationRepository playerRelationRepository) {
		this.playerRelationRepository = playerRelationRepository;
	}

	@Autowired
	public void setPlayerDeathFoeRepository(PlayerDeathFoeRepository playerDeathFoeRepository) {
		this.playerDeathFoeRepository = playerDeathFoeRepository;
	}

	@Autowired
	public void setGameRepository(GameRepository gameRepository) {
		this.gameRepository = gameRepository;
	}
}