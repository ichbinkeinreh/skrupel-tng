package org.skrupeltng.modules.ingame.service.round.ship;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.skrupeltng.modules.ingame.service.round.ShipToDestinationComparator;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ShipRoundMovement {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private ConfigProperties configProperties;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processMovingShips(long gameId) {
		log.debug("Processing moving ships...");

		shipRepository.clearLastCoordinates(gameId);
		List<Ship> ships = shipRepository.getMovingShips(gameId);

		Collections.sort(ships, new ShipToDestinationComparator());

		final int maxExperience = configProperties.getMaxExperience();
		final double distanceTravelledForExperience = configProperties.getDistanceTravelledForExperience();

		for (Ship ship : ships) {
			int travelSpeed = ship.getTravelSpeed();
			int x = ship.getX();
			int y = ship.getY();
			int destinationX = ship.getDestinationX();
			int destinationY = ship.getDestinationY();
			Ship destinationShip = ship.getDestinationShip();

			if (destinationShip != null) {
				destinationShip = shipRepository.getOne(destinationShip.getId());
				destinationX = destinationShip.getX();
				destinationY = destinationShip.getY();
				ship.setDestinationX(destinationX);
				ship.setDestinationY(destinationY);
			}

			if (ship.getX() == ship.getDestinationX() && ship.getY() == ship.getDestinationY()) {
				continue;
			}

			double bonus = 1d;

			if (ship.hasActiveAbility(ShipAbilityType.OVERDRIVE)) {
				double overDrivePercentage = Float.valueOf(ship.getTaskValue()) * 0.01d;
				double prop = MasterDataService.RANDOM.nextDouble();

				if (prop <= overDrivePercentage) {
					newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_overdrive_failed, ship.createFullImagePath(), ship.getId(),
							NewsEntryClickTargetType.ship, ship.getName());
					continue;
				}

				bonus += overDrivePercentage;
			}

			double distance = CoordHelper.getDistance(x, y, destinationX, destinationY);
			double duration = masterDataService.calculateTravelDuration(travelSpeed, distance, bonus);

			duration = checkGravityCatapult(ship, distance, duration);
			duration = checkSunSailDuration(ship, distance, duration);

			int totalMass = getTotalMass(ship);
			int fuelConsumption = masterDataService.calculateFuelConsumptionPerMonth(ship, travelSpeed, distance, duration, totalMass);

			fuelConsumption = checkFuelConsumptionReduction(ship, fuelConsumption);
			fuelConsumption = checkLinearEngineConsumption(ship, fuelConsumption);
			fuelConsumption = checkMicroGravityConsumption(ship, fuelConsumption);

			if (fuelConsumption > ship.getFuel()) {
				newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_not_enough_fuel, ship.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName());
				ship.resetTravel();
				shipRepository.save(ship);
				continue;
			}

			ship.setLastX(ship.getX());
			ship.setLastY(ship.getY());

			double additionalDistanceTravelled = 0.0;

			if (duration <= 1d) {
				ship.setX(destinationX);
				ship.setY(destinationY);
				additionalDistanceTravelled = distance;

				if (destinationShip == null) {
					ship.resetTravel();
				}
			} else {
				additionalDistanceTravelled = Math.pow(travelSpeed, 2);

				ship.setX(x + (int)Math.floor((destinationX - x) / duration));
				ship.setY(y + (int)Math.floor((destinationY - y) / duration));

				if (destinationShip != null && destinationShip.getTravelSpeed() == travelSpeed) {
					double dist = CoordHelper.getDistance(ship, destinationShip);

					if (dist <= 1d) {
						ship.setX(destinationX);
						ship.setY(destinationY);
					}
				}
			}

			ship.setPlanet(null);
			ship.setFuel(ship.getFuel() - fuelConsumption);
			ship.setDistanceTravelled(ship.getDistanceTravelled() + additionalDistanceTravelled);

			if (ship.getExperience() < maxExperience && ship.getDistanceTravelled() >= distanceTravelledForExperience) {
				ship.setDistanceTravelled(ship.getDistanceTravelled() - distanceTravelledForExperience);
				ship.setExperience(ship.getExperience() + 1);
			}

			Optional<Planet> newPlanet = planetRepository.findByGameIdAndXAndY(gameId, ship.getX(), ship.getY());

			if (newPlanet.isPresent()) {
				Planet planet = newPlanet.get();
				ship.setPlanet(planet);

				if (ship.getTravelSpeed() == 0) {
					addPlanetArrivalNewsEntry(ship, planet);
				}
			} else if (ship.getTravelSpeed() == 0) {
				newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_arrived_at_destination_coordinates, ship.createFullImagePath(),
						ship.getId(), NewsEntryClickTargetType.ship, ship.getName());
			}

			shipRepository.save(ship);
		}

		log.debug("Moving ships processed.");
	}

	protected int getTotalMass(Ship ship) {
		int totalMass = ship.getShipTemplate().getMass();

		if (ship.getTaskType() == ShipTaskType.TRACTOR_BEAM) {
			int mass = shipRepository.getMass(Long.valueOf(ship.getTaskValue()));
			totalMass += mass / 2;
		}

		return totalMass;
	}

	protected double checkGravityCatapult(Ship ship, double distance, double duration) {
		if (ship.getPropulsionSystemTemplate().getTechLevel() <= 3 && ship.getPlanet() != null && ship.getTravelSpeed() <= 3) {
			duration = distance / 16;
		}

		return duration;
	}

	protected double checkSunSailDuration(Ship ship, double distance, double duration) {
		if (ship.getPropulsionSystemTemplate().getTechLevel() == 1) {
			float val = MasterDataService.RANDOM.nextFloat();

			if (val <= 0.11f) {
				duration = distance / 81;
				newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_ion_wind, ship.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName());
			}
		}

		return duration;
	}

	protected int checkFuelConsumptionReduction(Ship ship, int fuelConsumption) {
		int reduction = 0;

		ShipTemplate template = ship.getShipTemplate();

		if (template.getEnergyWeaponsCount() == 0 && template.getProjectileWeaponsCount() == 0 && template.getHangarCapacity() == 0) {
			reduction = ship.getExperience() * 8;
		}

		if (ship.getShipModule() == ShipModule.PROPULSON_ENHANCEMENT) {
			reduction += 11;
		}

		fuelConsumption -= Math.round(fuelConsumption / 100f * reduction);

		return fuelConsumption;
	}

	protected int checkLinearEngineConsumption(Ship ship, int fuelConsumption) {
		if (ship.getPropulsionSystemTemplate().getTechLevel() == 4 && fuelConsumption > 0) {
			float prop = MasterDataService.RANDOM.nextFloat();

			if (prop <= 0.17f) {
				int newConsumption = (int)Math.floor(fuelConsumption * 0.63f);
				newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_linear_engine_fuel_reduction, ship.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName(), fuelConsumption, newConsumption);
				fuelConsumption = newConsumption;
			}
		}

		return fuelConsumption;
	}

	protected int checkMicroGravityConsumption(Ship ship, int fuelConsumption) {
		if (ship.getPropulsionSystemTemplate().getTechLevel() == 6 && fuelConsumption > ship.getFuel() &&
				ship.getMineral2() + ship.getFuel() > fuelConsumption) {
			int diff = fuelConsumption - ship.getFuel();
			ship.setMineral2(ship.getMineral2() - diff);
			fuelConsumption = ship.getFuel();
		}

		return fuelConsumption;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void updateDestinations(long gameId) {
		log.debug("Updating ship destinations...");

		List<Ship> ships = shipRepository.getMovingShips(gameId);

		for (Ship ship : ships) {
			Ship destinationShip = ship.getDestinationShip();

			if (destinationShip != null) {
				ship.setDestinationX(destinationShip.getX());
				ship.setDestinationY(destinationShip.getY());
			}
		}

		log.debug("Ship destinations updated.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processGravity(long gameId) {
		log.debug("Processing gravities pull on ships...");

		List<Ship> ships = shipRepository.findByGameId(gameId);

		for (Ship ship : ships) {
			if (ship.getPlanet() != null) {
				continue;
			}

			List<Long> planetIds = planetRepository.getPlanetIdsInRadius(gameId, ship.getX(), ship.getY(), 13, false);

			if (planetIds.size() == 1) {
				Planet planet = planetRepository.getOne(planetIds.get(0));

				if (planet != ship.getPlanet()) {
					ship.setPlanet(planet);
					ship.setX(planet.getX());
					ship.setY(planet.getY());

					if (ship.getX() == ship.getDestinationX() && ship.getY() == ship.getDestinationY()) {
						ship.resetTravel();

						addPlanetArrivalNewsEntry(ship, planet);
					}

					shipRepository.save(ship);
				}
			}
		}

		log.debug("Finished processing gravity.");
	}

	protected void addPlanetArrivalNewsEntry(Ship ship, Planet planet) {
		if (planet.getPlayer() != null && planet.getPlayer().getId() != ship.getPlayer().getId()) {
			return;
		}

		if (ship.getCurrentRouteEntry() != null && ship.getCurrentRouteEntry().getPlanet() == planet) {
			return;
		}

		newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_arrived_at_destination_planet, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processDrugunConverter(long gameId) {
		log.debug("Processing drugun converter...");

		List<Ship> ships = shipRepository.findByPropulsionAndGameId(gameId, "drugun_converter");

		for (Ship ship : ships) {
			int count = ship.getShipTemplate().getPropulsionSystemsCount();
			int propability = (int)(MasterDataService.RANDOM.nextFloat() * 100);

			if (propability <= count * 2) {
				List<Long> shipIds = shipRepository.getShipIdsInRadius(gameId, ship.getX(), ship.getY(), 117);
				List<Ship> list = shipRepository.findByIds(shipIds);

				for (Ship s : list) {
					if (s.isCloaked()) {
						s.setCloaked(false);
						shipRepository.save(s);
					}
				}
			}
		}

		log.debug("Finished processing drugun converter.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processShipEvasionOfAlliedPlanets(long gameId) {
		List<Long> shipsIds = shipRepository.getShipsOnAlliedPlanets(gameId, PlayerRelationType.NON_AGGRESSION_TREATY);

		if (shipsIds.isEmpty()) {
			return;
		}

		List<Ship> ships = shipRepository.findByIds(shipsIds);

		for (Ship ship : ships) {
			int galaxySize = ship.getPlayer().getGame().getGalaxySize();
			CoordinateImpl nearCoordinates = CoordHelper.getRandomNearCoordinates(ship.getX(), ship.getY(), 20, galaxySize);

			ship.setX(nearCoordinates.getX());
			ship.setY(nearCoordinates.getY());
			ship.setLastX(ship.getX());
			ship.setLastY(ship.getY());
			Planet planet = ship.getPlanet();
			ship.setPlanet(null);
			ship.resetTravel();

			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_evaded_allied_planet, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName(), planet.getName());

			shipRepository.save(ship);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processShipEvasionOfOtherShips(long gameId) {
		Set<Ship> ships = shipRepository.getShipsWithOtherForeignShips(gameId);

		for (Ship ship : ships) {
			int galaxySize = ship.getPlayer().getGame().getGalaxySize();
			CoordinateImpl nearCoordinates = CoordHelper.getRandomNearCoordinates(ship.getX(), ship.getY(), 20, galaxySize);

			ship.setX(nearCoordinates.getX());
			ship.setY(nearCoordinates.getY());
			ship.setLastX(ship.getX());
			ship.setLastY(ship.getY());
			ship.setPlanet(null);
			ship.resetTravel();

			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_evaded_allied_ship, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName());

			shipRepository.save(ship);
		}
	}
}