package org.skrupeltng.modules.ingame.service.round.combat;

import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTactics;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.database.WeaponTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataService;

public class ShipDamage {

	public static final float CAPTURE_DAMAGE_FACTOR = 0.5f;
	public static final float CAPTURE_DAMAGE_CREW_FACTOR = 1.25f;

	protected static final float TACTICS_BONUS = 1.1f;
	protected static final float TACTICS_PENALTY = 0.85f;

	public static final int HANGAR_DAMAGE = 4;
	public static final float HANGAR_DAMAGE_CREW = 1.5F;
	public static final int[] ENERGY_WEAPON_DAMAGE_DATA = new int[] { 0, 3, 7, 10, 15, 12, 29, 35, 37, 18, 45 };
	public static final int[] ENERGY_WEAPON_DAMAGE_CREW_DATA = new int[] { 0, 1, 2, 2, 4, 16, 7, 8, 9, 33, 11 };
	public static final int[] PROJECTILE_WEAPON_DAMAGE_DATA = new int[] { 0, 5, 8, 10, 6, 15, 30, 35, 12, 48, 55 };
	public static final int[] PROJECTILE_WEAPON_DAMAGE_CREW_DATA = new int[] { 0, 1, 2, 2, 13, 6, 7, 8, 36, 12, 14 };

	protected final Ship ship;
	protected int mass;

	protected int energyWeaponCount;
	protected int projectileWeaponCount;
	protected int hangarCount;

	protected int energyWeaponDamage;
	protected int energyWeaponDamageCrew;
	protected int projectileWeaponDamage;
	protected int projectileWeaponDamageCrew;

	protected final ShipTactics enemyTactics;
	protected int advantage;

	public ShipDamage(Ship ship, ShipTactics enemyTactics, int advantage) {
		this.ship = ship;
		this.enemyTactics = enemyTactics;
		this.advantage = advantage;

		mass = ship.getShipTemplate().getMass();

		initEnergyWeaponDamage();
		initProjectileWeaponDamage();

		energyWeaponCount = ship.getShipTemplate().getEnergyWeaponsCount();
		projectileWeaponCount = ship.getShipTemplate().getProjectileWeaponsCount();
		hangarCount = ship.getShipTemplate().getHangarCapacity();

		applyDamageToWeapons();

		if (ship.getShipModule() == ShipModule.IMPROVED_ENERGY_SYSTEMS && energyWeaponCount < 10) {
			energyWeaponCount++;
		}

		applyAdvantage();
	}

	protected void applyDamageToWeapons() {
		if (ship.getDamage() > 50) {
			float percentage = (ship.getDamage() - 50) * 2f;
			int random = MasterDataService.RANDOM.nextInt(3);

			switch (random) {
				case 0:
					if (energyWeaponCount > 0) {
						energyWeaponCount -= (int)(energyWeaponCount * percentage);
						energyWeaponCount = Math.max(0, energyWeaponCount);
					}
					break;
				case 1:
					if (projectileWeaponCount > 0) {
						projectileWeaponCount -= (int)(projectileWeaponCount * percentage);
						projectileWeaponCount = Math.max(0, projectileWeaponCount);
					}
					break;
				case 2:
					if (hangarCount > 0) {
						hangarCount -= (int)(hangarCount * percentage);
						hangarCount = Math.max(0, hangarCount);
					}
					break;
			}
		}
	}

	protected void initEnergyWeaponDamage() {
		WeaponTemplate template = ship.getEnergyWeaponTemplate();

		int index = 0;

		if (template != null) {
			index = template.getDamageIndex();
		} else if (ship.getShipModule() == ShipModule.IMPROVED_ENERGY_SYSTEMS) {
			index = 1;
		}

		if (index > 0) {
			energyWeaponDamage = ENERGY_WEAPON_DAMAGE_DATA[index];
			energyWeaponDamageCrew = ENERGY_WEAPON_DAMAGE_CREW_DATA[index];

			if (ship.getTaskType() == ShipTaskType.CAPTURE_SHIP) {
				energyWeaponDamage = Math.round(energyWeaponDamage * CAPTURE_DAMAGE_FACTOR);
				energyWeaponDamageCrew = Math.round(energyWeaponDamageCrew * CAPTURE_DAMAGE_CREW_FACTOR);
			}

			energyWeaponDamage = checkTactics(energyWeaponDamage);
			energyWeaponDamageCrew = checkTactics(energyWeaponDamageCrew);
		}
	}

	protected void initProjectileWeaponDamage() {
		WeaponTemplate template = ship.getProjectileWeaponTemplate();

		if (template != null) {
			int index = template.getDamageIndex();
			projectileWeaponDamage = PROJECTILE_WEAPON_DAMAGE_DATA[index];
			projectileWeaponDamageCrew = PROJECTILE_WEAPON_DAMAGE_CREW_DATA[index];

			if (ship.getTaskType() == ShipTaskType.CAPTURE_SHIP) {
				projectileWeaponDamage = Math.round(projectileWeaponDamage * CAPTURE_DAMAGE_FACTOR);
				projectileWeaponDamageCrew = Math.round(projectileWeaponDamageCrew * CAPTURE_DAMAGE_CREW_FACTOR);
			}

			projectileWeaponDamage = checkTactics(projectileWeaponDamage);
			projectileWeaponDamageCrew = checkTactics(projectileWeaponDamageCrew);
		}
	}

	protected int checkTactics(int damageValue) {
		if (enemyTactics != null && ship.getPlayer().getGame().isEnableTactialCombat()) {
			ShipTactics shipTactics = ship.getTactics();

			if (shipTactics == ShipTactics.STANDARD && enemyTactics == ShipTactics.DEFENSIVE) {
				return (int)(damageValue * TACTICS_PENALTY);
			}

			if (shipTactics == ShipTactics.STANDARD && enemyTactics == ShipTactics.OFFENSIVE) {
				return (int)(damageValue * TACTICS_BONUS);
			}
		}

		return damageValue;
	}

	protected void applyAdvantage() {
		if (advantage > 0) {
			energyWeaponCount += advantage;

			if (energyWeaponCount == advantage) {
				energyWeaponDamage = ENERGY_WEAPON_DAMAGE_DATA[1];
			}

			if (energyWeaponCount > 10) {
				advantage = energyWeaponCount - 10;
				energyWeaponCount = 10;
				hangarCount += advantage;

				if (hangarCount > 10) {
					advantage = hangarCount - 10;
					hangarCount = 10;
					mass += advantage * 100;

					if (mass > 1000) {
						mass = 1000;
					}
				}
			}
		}
	}

	public void damageEnemyShip(Ship enemyShip, int round, boolean ignoreShields, int mass) {
		enemyShip.receiveDamage(getEnergyWeaponDamage(round), getEnergyWeaponDamageCrew(round), ignoreShields, mass);
		enemyShip.receiveDamage(getProjectileWeaponDamage(round), getProjectileWeaponDamageCrew(round), ignoreShields, mass);
		enemyShip.receiveDamage(getHangarDamage(round), getHangarDamageToCrew(round), ignoreShields, mass);
	}

	protected int getEnergyWeaponDamage(int round) {
		if (energyWeaponCount >= round) {
			return energyWeaponDamage;
		}

		return 0;
	}

	protected int getEnergyWeaponDamageCrew(int round) {
		if (energyWeaponCount >= round) {
			return energyWeaponDamageCrew;
		}

		return 0;
	}

	protected int getProjectileWeaponDamage(int round) {
		return getProjectileDamage(round, projectileWeaponDamage);
	}

	protected int getProjectileWeaponDamageCrew(int round) {
		return getProjectileDamage(round, projectileWeaponDamageCrew);
	}

	protected int getProjectileDamage(int round, int damage) {
		if (projectileWeaponCount >= round && ship.getProjectiles() > 0) {
			ship.spendProjectile();
			int chance = MasterDataService.RANDOM.nextInt(100);

			if (chance < (66 + (6 * ship.getExperienceForCombat()))) {
				return damage;
			}
		}

		return 0;
	}

	protected int getHangarDamage(int round) {
		if (hangarCount >= round) {
			return HANGAR_DAMAGE;
		}

		return 0;
	}

	protected float getHangarDamageToCrew(int round) {
		if (hangarCount >= round) {
			return HANGAR_DAMAGE_CREW;
		}

		return 0;
	}

	public int calculateDamageToPlanet(int round) {
		int damage = getEnergyWeaponDamage(round);
		damage += getProjectileWeaponDamage(round);
		damage += getHangarDamage(round);
		return damage;
	}

	public int getMass() {
		return mass;
	}

	public boolean cannotDealDamage() {
		if (energyWeaponCount > 0) {
			return false;
		}

		if (hangarCount > 0) {
			return false;
		}

		if (projectileWeaponCount > 0 && ship.getProjectiles() > 0) {
			return false;
		}

		return true;
	}
}