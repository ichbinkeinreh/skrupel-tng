package org.skrupeltng.modules.ingame.service.round.combat.space;

import java.util.Set;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTactics;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.ingame.service.round.combat.ShipDamage;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SpaceCombat {

	@Autowired
	private ShipEvasion shipEvasion;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private NewsService newsService;

	@Autowired
	private ShipService shipService;

	@Autowired
	private FleetService fleetService;

	@Autowired
	private StatsUpdater statsUpdater;

	@Autowired
	private ConfigProperties configProperties;

	public boolean bothEvade(Ship ship, Ship enemyShip) {
		Player player = ship.getPlayer();
		Game game = player.getGame();

		if (game.isEnableTactialCombat() && ship.getTactics() == ShipTactics.DEFENSIVE && enemyShip.getTactics() == ShipTactics.DEFENSIVE) {
			shipEvasion.evade(ship, enemyShip, true);
			shipEvasion.evade(enemyShip, ship, true);

			shipRepository.save(ship);
			shipRepository.save(enemyShip);

			return true;
		}

		return false;
	}

	public void checkStructurScanner(Ship ship, Ship enemyShip) {
		if (ship.getAbility(ShipAbilityType.STRUCTUR_SCANNER).isPresent()) {
			shipService.checkStructurScanner(ship.getPlayer(), enemyShip);
		}
	}

	public boolean evadeAfterFirstCombatRound(Ship ship, Ship enemyShip) {
		if (ship.getPlayer().getGame().isEnableTactialCombat()) {
			ShipTactics t1 = ship.getTactics();
			ShipTactics t2 = enemyShip.getTactics();

			return (t1 == ShipTactics.DEFENSIVE && t2 == ShipTactics.STANDARD) || (t2 == ShipTactics.DEFENSIVE && t1 == ShipTactics.STANDARD);
		}

		return false;
	}

	public boolean checkLuckyShot(Ship ship, Ship enemyShip, Set<Ship> destroyedShips) {
		if (ship.getAbility(ShipAbilityType.LUCKY_SHOT).isPresent()) {
			float chance = ship.getAbilityValue(ShipAbilityType.LUCKY_SHOT, ShipAbilityConstants.LUCKY_SHOT_PROBABILTY).get().intValue() / 100f;
			chance += ship.getExperienceForCombat();
			float value = MasterDataService.RANDOM.nextFloat();

			if (chance > value) {
				String sector = CoordHelper.getSectorString(ship);
				newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_lucky_shot_success, ship.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName(), sector, enemyShip.getName());
				newsService.add(enemyShip.getPlayer(), NewsEntryConstants.news_entry_lucky_shot_victim, enemyShip.createFullImagePath(), null, null,
						enemyShip.getName(), sector, ship.getName());
				destroyedShips.add(enemyShip);

				statsUpdater.incrementStats(ship.getPlayer(), LoginStatsFaction::getShipsDestroyed, LoginStatsFaction::setShipsDestroyed,
						AchievementType.SHIPS_DESTROYED_1, AchievementType.SHIPS_DESTROYED_10);
				statsUpdater.incrementStats(enemyShip.getPlayer(), LoginStatsFaction::getShipsLost, LoginStatsFaction::setShipsLost);

				return true;
			}
		}

		return false;
	}

	public boolean checkCombatWithoutWeapons(Ship ship1, Ship ship2) {
		ShipTemplate template1 = ship1.getShipTemplate();
		ShipTemplate template2 = ship2.getShipTemplate();

		if (template1.getEnergyWeaponsCount() == 0 && template1.getProjectileWeaponsCount() == 0 && template1.getHangarCapacity() == 0 &&
				template2.getEnergyWeaponsCount() == 0 && template2.getProjectileWeaponsCount() == 0 && template2.getHangarCapacity() == 0) {
			performCombatWithoutWeapons(ship1, ship2);
			return true;
		}

		return false;
	}

	protected void performCombatWithoutWeapons(Ship ship1, Ship ship2) {
		ShipTemplate template1 = ship1.getShipTemplate();
		ShipTemplate template2 = ship2.getShipTemplate();

		int mass1 = template1.getMass();
		int mass2 = template2.getMass();

		if (mass1 == mass2) {
			ship1.setDamage(100);
			ship2.setDamage(100);
		} else if (mass1 > mass2) {
			ship1.setDamage(ship1.getDamage() + Math.round(mass2 / (mass1 / 100f)));
			ship1.setShield(0);
			ship2.setDamage(100);
		} else {
			ship2.setDamage(ship2.getDamage() + Math.round(mass1 / (mass2 / 100f)));
			ship2.setShield(0);
			ship1.setDamage(100);
		}
	}

	public void checkShieldDamper(Ship ship) {
		ShipAbility activeAbility = ship.getActiveAbility();

		if (activeAbility != null && activeAbility.getType() == ShipAbilityType.SHIELD_DAMPER) {
			int costs = ship.getAbilityValue(ShipAbilityType.SHIELD_DAMPER, ShipAbilityConstants.SHIELD_DAMPER_COSTS).get().intValue();

			if (ship.getMineral2() >= costs) {
				ship.setMineral2(ship.getMineral2() - costs);
			} else {
				ship.setActiveAbility(null);
			}
		}
	}

	public int getSupport(Ship ship) {
		long playerId = ship.getPlayer().getId();
		int x = ship.getX();
		int y = ship.getY();

		ShipTemplate template = ship.getShipTemplate();
		int minWeaponCount = Math.max(Math.max(template.getEnergyWeaponsCount(), template.getProjectileWeaponsCount()), template.getHangarCapacity());

		int minTechLevel = template.getTechLevel() - 2;

		if (minTechLevel < 1) {
			minTechLevel = 1;
		}

		long advantage = shipRepository.getSupportingShipCount(playerId, x, y, minWeaponCount / 2, minTechLevel);

		if (advantage > minWeaponCount) {
			advantage = minWeaponCount;
		}

		advantage += ship.getExperienceForCombat();

		long comCenterCount = shipRepository.getCommunicationCenterShipCount(playerId, x, y, ship.getId());

		if (comCenterCount > 0) {
			advantage++;
		}

		return (int)advantage;
	}

	public void processCombat(Ship ship, Ship enemyShip, boolean evadeAfterFirstCombatRound, int support, int enemySupport) {
		ShipDamage shipDamage = new ShipDamage(ship, enemyShip.getTactics(), support - enemySupport);
		ShipDamage enemyShipDamage = new ShipDamage(enemyShip, ship.getTactics(), enemySupport - support);

		boolean ignoreShields = ship.hasActiveAbility(ShipAbilityType.SHIELD_DAMPER);
		boolean ignoreShieldsEnemy = enemyShip.hasActiveAbility(ShipAbilityType.SHIELD_DAMPER);

		while (ship.getDamage() < 100 && enemyShip.getDamage() < 100 && ship.getCrew() > 0 && enemyShip.getCrew() > 0) {
			if (shipDamage.cannotDealDamage() && enemyShipDamage.cannotDealDamage()) {
				performCombatWithoutWeapons(ship, enemyShip);
				break;
			}

			for (int round = 1; round <= 10; round++) {
				if (ship.getDamage() < 100 && enemyShip.getDamage() < 100 && ship.getCrew() > 0 && enemyShip.getCrew() > 0) {
					shipDamage.damageEnemyShip(enemyShip, round, ignoreShields, enemyShipDamage.getMass());
					enemyShipDamage.damageEnemyShip(ship, round, ignoreShieldsEnemy, shipDamage.getMass());
				}
			}

			if (evadeAfterFirstCombatRound) {
				break;
			}
		}
	}

	public void handleShipDamage(Set<Ship> destroyedShips, Ship ship, Ship enemyShip, boolean evadeAfterFirstCombatRound, Set<Ship> capturedShips) {
		String sector = CoordHelper.getSectorString(ship);
		processExperience(ship, enemyShip);

		if (ship.getCrew() <= 0 && enemyShip.getCrew() <= 0) {
			destroyedShips.add(ship);

			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_mutually_destroyed, ship.createFullImagePath(), null, null, ship.getName(),
					enemyShip.getName(), sector);

			statsUpdater.incrementStats(enemyShip.getPlayer(), LoginStatsFaction::getShipsDestroyed, LoginStatsFaction::setShipsDestroyed,
					AchievementType.SHIPS_DESTROYED_1, AchievementType.SHIPS_DESTROYED_10);
			statsUpdater.incrementStats(ship.getPlayer(), LoginStatsFaction::getShipsLost, LoginStatsFaction::setShipsLost);
		} else if (ship.destroyed()) {
			destroyedShips.add(ship);

			statsUpdater.incrementStats(enemyShip.getPlayer(), LoginStatsFaction::getShipsDestroyed, LoginStatsFaction::setShipsDestroyed);
			statsUpdater.incrementStats(ship.getPlayer(), LoginStatsFaction::getShipsLost, LoginStatsFaction::setShipsLost);

			if (enemyShip.destroyed()) {
				newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_mutually_destroyed, ship.createFullImagePath(), null, null, ship.getName(),
						enemyShip.getName(), sector);
			} else {
				newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_destroyed_ship_by_ship, ship.createFullImagePath(), null, null, ship.getName(),
						enemyShip.getName(), sector);
				newsService.add(enemyShip.getPlayer(), NewsEntryConstants.news_entry_ship_destroyed_enemy_ship, enemyShip.createFullImagePath(),
						enemyShip.getId(), NewsEntryClickTargetType.ship, enemyShip.getName(), ship.getName(), sector);
			}
		} else {
			if (!enemyShip.destroyed() && ship.getCrew() <= 0) {
				newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_captured_by_enemy, ship.createFullImagePath(), null, null,
						ship.getName(), enemyShip.getName(), sector);
				newsService.add(enemyShip.getPlayer(), NewsEntryConstants.news_entry_ship_captured, ship.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, enemyShip.getName(), ship.getName(), sector);

				statsUpdater.incrementStats(enemyShip.getPlayer(), LoginStatsFaction::getShipsCaptured, LoginStatsFaction::setShipsCaptured,
						AchievementType.SHIPS_CAPTURED);
				statsUpdater.incrementStats(ship.getPlayer(), LoginStatsFaction::getShipsLost, LoginStatsFaction::setShipsLost);

				fleetService.clearFleetOfShipWithoutPermissionCheck(ship.getId());

				ship.setPlayer(enemyShip.getPlayer());
				ship.setExperience(0);
				ship.resetTask();
				ship.resetTravel();

				capturedShips.add(ship);

				shipEvasion.evade(ship, enemyShip, false);
			} else if (ship.getCrew() > 0 && evadeAfterFirstCombatRound) {
				shipEvasion.evade(ship, enemyShip, true);
			}

			shipRepository.save(ship);
		}
	}

	protected void processExperience(Ship ship, Ship enemyShip) {
		if (ship.getExperienceForCombat() < configProperties.getMaxExperience()) {
			ship.increaseExperienceBySpaceCombat(enemyShip);
		}
	}
}