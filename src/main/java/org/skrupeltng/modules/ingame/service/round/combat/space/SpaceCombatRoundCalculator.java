package org.skrupeltng.modules.ingame.service.round.combat.space;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;

@Component
public class SpaceCombatRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private SpaceCombat spaceCombat;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Autowired
	private ShipService shipService;

	@Autowired
	private PoliticsService politicsService;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processCombat(long gameId) {
		log.debug("Processing space combat...");

		List<Ship> ships = shipRepository.findForSpaceCombat(gameId);

		Set<Ship> destroyedShips = new HashSet<>(ships.size());
		Set<Ship> capturedShips = new HashSet<>(ships.size());
		Set<Set<Long>> processedShipPairs = new HashSet<>();

		for (Ship ship : ships) {
			long playerId = ship.getPlayer().getId();
			List<Ship> enemyShips = shipRepository.findEnemyShipsOnSamePosition(gameId, playerId, ship.getX(), ship.getY());

			processCombat(enemyShips, destroyedShips, ship, processedShipPairs, capturedShips);
		}

		capturedShips.removeAll(destroyedShips);

		shipService.deleteShips(destroyedShips);

		for (Ship ship : capturedShips) {
			shipRepository.clearCurrentRouteEntry(ship.getId());
			shipRouteEntryRepository.deleteByShipId(ship.getId());
		}

		shipRepository.restoreShields(gameId);

		log.debug("Space combat processed.");
	}

	protected void processCombat(List<Ship> enemyShips, Set<Ship> destroyedShips, Ship ship, Set<Set<Long>> processedShipPairs, Set<Ship> capturedShips) {
		for (Ship enemyShip : enemyShips) {
			if (destroyedShips.contains(enemyShip)) {
				continue;
			}

			if (destroyedShips.contains(ship)) {
				break;
			}

			Set<Long> pair = Sets.newHashSet(ship.getId(), enemyShip.getId());

			if (processedShipPairs.add(pair)) {
				if (politicsService.isAlly(ship.getPlayer().getId(), enemyShip.getPlayer().getId())) {
					continue;
				}

				if (spaceCombat.bothEvade(ship, enemyShip)) {
					break;
				}

				spaceCombat.checkStructurScanner(ship, enemyShip);
				spaceCombat.checkStructurScanner(enemyShip, ship);

				if (spaceCombat.checkLuckyShot(ship, enemyShip, destroyedShips) || spaceCombat.checkLuckyShot(enemyShip, ship, destroyedShips)) {
					continue;
				}

				boolean evadeAfterFirstCombatRound = false;

				if (!spaceCombat.checkCombatWithoutWeapons(ship, enemyShip)) {
					evadeAfterFirstCombatRound = spaceCombat.evadeAfterFirstCombatRound(ship, enemyShip);
					spaceCombat.checkShieldDamper(ship);
					spaceCombat.checkShieldDamper(enemyShip);
					spaceCombat.processCombat(ship, enemyShip, evadeAfterFirstCombatRound, spaceCombat.getSupport(ship), spaceCombat.getSupport(enemyShip));
				}

				spaceCombat.handleShipDamage(destroyedShips, ship, enemyShip, evadeAfterFirstCombatRound, capturedShips);
				spaceCombat.handleShipDamage(destroyedShips, enemyShip, ship, evadeAfterFirstCombatRound, capturedShips);
			}
		}
	}
}