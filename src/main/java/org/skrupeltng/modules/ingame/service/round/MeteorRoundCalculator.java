package org.skrupeltng.modules.ingame.service.round;

import java.util.List;

import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class MeteorRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private NewsService newsService;

	@Autowired
	private StatsUpdater statsUpdater;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processBigMeteors(long gameId, int randomValue) {
		log.debug("Processing big meteors...");

		if (randomValue == 0) {
			long randomPlanetId = planetRepository.getRandomPlanetId(gameId);
			Planet planet = planetRepository.getOne(randomPlanetId);

			List<Integer> minerals = RandomSplitter.getRandomValues(7500 + MasterDataService.RANDOM.nextInt(2500), 4);
			planet.setUntappedFuel(planet.getUntappedFuel() + minerals.get(0));
			planet.setUntappedMineral1(planet.getUntappedMineral1() + minerals.get(1));
			planet.setUntappedMineral2(planet.getUntappedMineral2() + minerals.get(2));
			planet.setUntappedMineral3(planet.getUntappedMineral3() + minerals.get(3));

			Player owner = planet.getPlayer();

			if (owner != null) {
				statsUpdater.incrementStats(owner, LoginStatsFaction::getPlanetsLost, LoginStatsFaction::setPlanetsLost);
			}

			planet.setPlayer(null);
			planet.setColonists(0);
			planet.setMines(0);
			planet.setFactories(0);
			planet.setPlanetaryDefense(0);
			planet.setNativeSpecies(null);
			planet.setNativeSpeciesCount(0);

			planet = planetRepository.save(planet);

			List<Player> players = playerRepository.findByGameId(gameId);
			Object[] args = { planet.getName(), planet.getX(), planet.getY(), minerals.get(0), minerals.get(1), minerals.get(2), minerals.get(3) };

			for (Player player : players) {
				String newsText = player == owner ? NewsEntryConstants.news_entry_big_meteor_hit_colony : NewsEntryConstants.news_entry_big_meteor_hit_planet;
				newsService.add(player, newsText, "/images/news/meteor_gross.jpg", null, null, args);
			}
		}

		log.debug("Processing big meteors finished.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processSmallMeteors(long gameId, int count) {
		log.debug("Processing small meteors...");

		for (int i = 0; i < count; i++) {
			long randomPlanetId = planetRepository.getRandomPlanetId(gameId);
			Planet planet = planetRepository.getOne(randomPlanetId);

			List<Integer> minerals = RandomSplitter.getRandomValues(50 + MasterDataService.RANDOM.nextInt(150), 4);
			planet.spendFuel(-minerals.get(0));
			planet.spendMineral1(-minerals.get(1));
			planet.spendMineral2(-minerals.get(2));
			planet.spendMineral3(-minerals.get(3));

			planet = planetRepository.save(planet);

			Player player = planet.getPlayer();

			if (player != null) {
				newsService.add(player, NewsEntryConstants.news_entry_small_meteor_hit_colony, "/images/news/meteor_klein.jpg", planet.getId(),
						NewsEntryClickTargetType.planet, planet.getName(), minerals.get(0), minerals.get(1), minerals.get(2), minerals.get(3));
			}
		}

		log.debug("Processing small meteors finished.");
	}
}