package org.skrupeltng.modules.ingame.modules.overview.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.RoundSummary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

import com.google.common.collect.Lists;

public class NewsEntryRepositoryImpl extends RepositoryCustomBase implements NewsEntryRepositoryCustom {

	private static final List<String> DESTROYED_SHIPS = Lists.newArrayList(
			NewsEntryConstants.news_entry_destroyed_ship_by_ship,
			NewsEntryConstants.news_entry_destroyed_ship_by_planet,
			NewsEntryConstants.news_entry_destroyed_by_subspace_distortion,
			NewsEntryConstants.news_entry_evade_destroyed,
			NewsEntryConstants.news_entry_mine_field_destroyed_ship,
			NewsEntryConstants.news_entry_ship_mutually_destroyed,
			NewsEntryConstants.news_entry_ship_destroyed_by_auto_destruction,
			NewsEntryConstants.news_entry_ship_auto_destructed);

	@Override
	public void deleteByGameId(long gameId) {
		String sql = "" +
				"DELETE FROM \n" +
				"	news_entry \n" +
				"WHERE \n" +
				"	game_id = :gameId \n" +
				"	AND delete = true";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		jdbcTemplate.update(sql, params);
	}

	@Override
	public RoundSummary getRoundSummary(long gameId) {
		String sql = "" +
				"SELECT \n" +
				"	g.round, \n" +
				"	g.round_date as \"date\", \n" +
				"	g.win_condition as \"winCondition\", \n" +
				"	(SELECT COUNT(id) FROM news_entry WHERE game_id = :gameId AND template IN (:newColonies)) as \"newColonies\", \n" +
				"	(SELECT COUNT(id) FROM news_entry WHERE game_id = :gameId AND template IN (:newShips)) as \"newShips\", \n" +
				"	(SELECT COUNT(id) FROM news_entry WHERE game_id = :gameId AND template IN (:newStarbases)) as \"newStarbases\", \n" +
				"	(SELECT COUNT(id) FROM news_entry WHERE game_id = :gameId AND template IN (:destroyedShips)) as \"destroyedShips\", \n" +
				"	(SELECT COUNT(id) FROM news_entry WHERE game_id = :gameId AND template IN (:lostShips)) as \"lostShips\" \n" +
				"FROM \n" +
				"	game g \n" +
				"WHERE \n" +
				"	g.id = :gameId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);
		params.put("newColonies", NewsEntryConstants.news_entry_new_colony);
		params.put("newShips", NewsEntryConstants.news_entry_ship_constructed);
		params.put("newStarbases", NewsEntryConstants.news_entry_starbase_constructed);
		params.put("destroyedShips", DESTROYED_SHIPS);
		params.put("lostShips", NewsEntryConstants.news_entry_jump_engine_lost);

		RowMapper<RoundSummary> rowMapper = new BeanPropertyRowMapper<>(RoundSummary.class);
		return jdbcTemplate.queryForObject(sql, params, rowMapper);
	}
}