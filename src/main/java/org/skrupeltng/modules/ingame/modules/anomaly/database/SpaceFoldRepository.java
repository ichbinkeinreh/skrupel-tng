package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface SpaceFoldRepository extends JpaRepository<SpaceFold, Long>, SpaceFoldRepositoryCustom {

	List<SpaceFold> findByGameId(long gameId);

	List<SpaceFold> findByShipId(long shipId);

	@Modifying
	@Query("DELETE FROM SpaceFold s WHERE s.ship.id = ?1")
	void deleteByShipId(long shipId);

	@Modifying
	@Query("DELETE FROM SpaceFold s WHERE s.ship.id IN (?1)")
	void deleteByShipIds(List<Long> shipIds);

	@Modifying
	@Query("DELETE FROM SpaceFold s WHERE s.planet.id = ?1")
	void deleteByPlanetId(long planetId);
}