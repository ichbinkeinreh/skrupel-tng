package org.skrupeltng.modules.ingame.modules.planet.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ingame.controller.ColonyOverviewRequest;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.controller.PlanetListResultDTO;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

import com.google.common.collect.Lists;

public class PlanetRepositoryImpl extends RepositoryCustomBase implements PlanetRepositoryCustom {

	@Override
	public List<PlanetEntry> getPlanetsForGalaxy(long gameId) {
		String sql = "" +
				"SELECT \n" +
				"	pl.id, \n" +
				"	pl.name, \n" +
				"	pl.x, \n" +
				"	pl.y, \n" +
				"	pl.type, \n" +
				"	pl.image, \n" +
				"	p.color as \"playerColor\", \n" +
				"	pl.player_id as \"playerId\", \n" +
				"	p.ai_level as \"playerAiLevel\", \n" +
				"	l.username as \"playerName\", \n" +
				"	sb.id as \"starbaseId\", \n" +
				"	sb.name as \"starbaseName\", \n" +
				"	sb.log as \"starbaseLog\", \n" +
				"	(COALESCE(COUNT(s.id), 0) > 0) as \"hasShips\", \n" +
				"	pl.colonists, \n" +
				"	pl.money, \n" +
				"	pl.supplies, \n" +
				"	pl.fuel, \n" +
				"	pl.mineral1, \n" +
				"	pl.mineral2, \n" +
				"	pl.mineral3, \n" +
				"	pl.untapped_fuel as \"untappedFuel\", \n" +
				"	pl.untapped_mineral1 as \"untappedMineral1\", \n" +
				"	pl.untapped_mineral2 as \"untappedMineral2\", \n" +
				"	pl.untapped_mineral3 as \"untappedMineral3\", \n" +
				"	pl.mines, \n" +
				"	pl.factories, \n" +
				"	pl.planetary_defense as \"planetaryDefense\", \n" +
				"	pl.auto_build_mines as \"autoBuildMines\", \n" +
				"	pl.auto_build_factories as \"autoBuildFactories\", \n" +
				"	pl.auto_build_planetary_defense as \"autoBuildPlanetaryDefense\", \n" +
				"	pl.log, \n" +
				"	ns.id IS NOT NULL as \"hasRevealableNativeSpecies\", \n" +
				"	pl.scan_radius as \"scanRadius\", \n" +
				"	true as \"visibleByLongRangeScanners\", \n" +
				"	(cfg.id IS NOT NULL) as \"hasCloakingFieldGenerator\", \n" +
				"	COALESCE(COUNT(DISTINCT osb.id), 0) as \"builtOrbitalSystemCount\", \n" +
				"	COUNT(DISTINCT os.id) as \"orbitalSystemCount\" \n" +
				"FROM \n" +
				"	planet pl \n" +
				"	LEFT OUTER JOIN player p \n" +
				"		ON p.id = pl.player_id \n" +
				"	LEFT OUTER JOIN login l \n" +
				"		ON l.id = p.login_id \n" +
				"	LEFT OUTER JOIN ship s \n" +
				"		ON s.planet_id = pl.id \n" +
				"	LEFT OUTER JOIN starbase sb \n" +
				"		ON sb.id = pl.starbase_id \n" +
				"	LEFT OUTER JOIN native_species ns \n" +
				"		ON ns.id = pl.native_species_id AND ns.effect = 'ALL_PLANETS_WITH_SPECIES_VISIBLE' AND pl.native_species_count > 0 \n" +
				"	LEFT OUTER JOIN orbital_system cfg \n" +
				"		ON cfg.planet_id = pl.id AND cfg.type = :cloakingFieldGeneratorType \n" +
				"	LEFT OUTER JOIN orbital_system osb \n" +
				"		ON osb.planet_id = pl.id AND osb.type IS NOT NULL \n" +
				"	LEFT OUTER JOIN orbital_system os \n" +
				"		ON os.planet_id = pl.id \n" +
				"WHERE \n" +
				"	pl.game_id = :gameId \n" +
				"GROUP BY \n" +
				"	pl.id, \n" +
				"	pl.name, \n" +
				"	pl.x, \n" +
				"	pl.y, \n" +
				"	pl.type, \n" +
				"	pl.image, \n" +
				"	p.color, \n" +
				"	pl.player_id, \n" +
				"	p.ai_level, \n" +
				"	l.username, \n" +
				"	sb.id, \n" +
				"	sb.name, \n" +
				"	sb.log, \n" +
				"	pl.colonists, \n" +
				"	pl.money, \n" +
				"	pl.supplies, \n" +
				"	pl.fuel, \n" +
				"	pl.mineral1, \n" +
				"	pl.mineral2, \n" +
				"	pl.mineral3, \n" +
				"	pl.untapped_fuel, \n" +
				"	pl.untapped_mineral1, \n" +
				"	pl.untapped_mineral2, \n" +
				"	pl.untapped_mineral3, \n" +
				"	pl.mines, \n" +
				"	pl.factories, \n" +
				"	pl.planetary_defense, \n" +
				"	pl.auto_build_mines, \n" +
				"	pl.auto_build_factories, \n" +
				"	pl.auto_build_planetary_defense, \n" +
				"	pl.log, \n" +
				"	ns.id, \n" +
				"	cfg.id";

		Map<String, Object> params = new HashMap<>(2);
		params.put("gameId", gameId);
		params.put("cloakingFieldGeneratorType", OrbitalSystemType.CLOAKING_FIELD_GENERATOR.name());

		RowMapper<PlanetEntry> rowMapper = new BeanPropertyRowMapper<>(PlanetEntry.class);
		return jdbcTemplate.query(sql, params, rowMapper);
	}

	@Override
	public boolean loginOwnsPlanet(long planetId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	p.login_id \n" +
				"FROM \n" +
				"	planet pl \n" +
				"	INNER JOIN player p \n" +
				"		ON p.id = pl.player_id \n" +
				"WHERE \n" +
				"	pl.id = :planetId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("planetId", planetId);

		List<Long> results = jdbcTemplate.queryForList(sql, params, Long.class);
		return results.size() == 1 && results.get(0).longValue() == loginId;
	}

	@Override
	public boolean hasRevealingNativeSpeciesPlanets(long playerId) {
		String sql = "" +
				"SELECT \n" +
				"	COUNT(p.id) \n" +
				"FROM \n" +
				"	planet p \n" +
				"	INNER JOIN native_species ns \n" +
				"		ON ns.id = p.native_species_id \n" +
				"WHERE \n" +
				"	p.player_id = :playerId \n" +
				"	AND p.native_species_count > 0 \n" +
				"	AND ns.effect = 'ALL_PLANETS_WITH_SPECIES_VISIBLE'";

		Map<String, Object> params = new HashMap<>(1);
		params.put("playerId", playerId);

		Long count = jdbcTemplate.queryForObject(sql, params, Long.class);

		return count > 0L;
	}

	@Override
	public List<Long> getPlanetIdsInRadius(long gameId, int x, int y, int distance, boolean colonizedOnly) {
		String sql = "" +
				"SELECT \n" +
				"	p.id \n" +
				"FROM \n" +
				"	planet p \n" +
				"WHERE \n" +
				"	p.game_id = :gameId \n" +
				"	AND sqrt(((p.x - :x) * (p.x - :x)) + ((p.y - :y) * (p.y - :y))) <= :distance";

		if (colonizedOnly) {
			sql += " AND p.player_id IS NOT NULL";
		}

		Map<String, Object> params = new HashMap<>(4);
		params.put("gameId", gameId);
		params.put("x", x);
		params.put("y", y);
		params.put("distance", distance);

		return jdbcTemplate.queryForList(sql, params, Long.class);
	}

	@Override
	public void resetScanRadius(long gameId) {
		String sql = "" +
				"UPDATE \n" +
				"	planet \n" +
				"SET \n" +
				"	scan_radius = 53 \n" +
				"WHERE \n" +
				"	game_id = :gameId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		jdbcTemplate.update(sql, params);
	}

	@Override
	public void updateExtendedScanRadius(long gameId) {
		String sql = "" +
				"UPDATE \n" +
				"	planet \n" +
				"SET \n" +
				"	scan_radius = 116 \n" +
				"WHERE \n" +
				"	game_id = :gameId \n" +
				"	AND id IN (\n" +
				"		SELECT \n" +
				"			p.id \n" +
				"		FROM \n" +
				"			planet p \n" +
				"			INNER JOIN starbase sb \n" +
				"				ON sb.id = p.starbase_id AND sb.type IN (:extendedRangeTypes) \n" +
				"	)";

		Map<String, Object> params = new HashMap<>(2);
		params.put("gameId", gameId);
		params.put("extendedRangeTypes", Lists.newArrayList(StarbaseType.BATTLE_STATION.name(), StarbaseType.WAR_BASE.name()));

		jdbcTemplate.update(sql, params);
	}

	@Override
	public void updatePsyCorpsScanRadius(long gameId) {
		String sql = "" +
				"UPDATE \n" +
				"	planet \n" +
				"SET \n" +
				"	scan_radius = 90 \n" +
				"WHERE \n" +
				"	game_id = :gameId \n" +
				"	AND id IN (\n" +
				"		SELECT \n" +
				"			p.id \n" +
				"		FROM \n" +
				"			planet p \n" +
				"			INNER JOIN orbital_system os \n" +
				"				ON os.planet_id = p.id AND os.type = :type \n" +
				"	)";

		Map<String, Object> params = new HashMap<>(2);
		params.put("gameId", gameId);
		params.put("type", OrbitalSystemType.PSY_CORPS.name());

		jdbcTemplate.update(sql, params);
	}

	@Override
	public List<Long> findOwnedPlanetIdsWithoutRoute(long playerId) {
		String sql = "" +
				"SELECT \n" +
				"	p.id \n" +
				"FROM \n" +
				"	planet p \n" +
				"WHERE \n" +
				"	p.player_id = :playerId \n" +
				"	AND p.id NOT IN ( \n" +
				"		SELECT \n" +
				"			e.planet_id \n" +
				"		FROM \n" +
				"			ship_route_entry e \n" +
				"			INNER JOIN ship s \n" +
				"				ON s.id = e.ship_id AND s.player_id = :playerId \n" +
				"	)";

		Map<String, Object> params = new HashMap<>(1);
		params.put("playerId", playerId);

		return jdbcTemplate.queryForList(sql, params, Long.class);
	}

	@Override
	public long getRandomPlanetId(long gameId) {
		String sql = "SELECT p.id FROM planet p WHERE p.game_id = :gameId AND p.starbase_id IS NULL ORDER BY random() LIMIT 1";
		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);
		return jdbcTemplate.queryForObject(sql, params, Long.class);
	}

	@Override
	public Page<PlanetListResultDTO> searchColonies(ColonyOverviewRequest request, long loginId) {
		long gameId = request.getGameId();
		String name = request.getName();
		Boolean starbase = request.getStarbase();
		Boolean hasRoute = request.getHasRoute();

		Pageable page = createPageable(request);

		String sql = "" +
				"SELECT \n" +
				"	p.id, \n" +
				"	p.name, \n" +
				"	p.colonists, \n" +
				"	p.supplies, \n" +
				"	p.money, \n" +
				"	p.fuel, \n" +
				"	p.mineral1, \n" +
				"	p.mineral2, \n" +
				"	p.mineral3, \n" +
				"	p.mines, \n" +
				"	p.factories, \n" +
				"	p.planetary_defense as \"planetaryDefense\", \n " +
				"	COALESCE(COUNT(DISTINCT osb.id), 0) as \"builtOrbitalSystemCount\", \n" +
				"	COUNT(DISTINCT os.id) as \"orbitalSystemCount\", \n" +
				"	count(*) OVER() AS \"totalElements\" \n" +
				"FROM \n" +
				"	planet p " +
				"	INNER JOIN player pl \n" +
				"		ON pl.id = p.player_id AND pl.login_id = :loginId AND pl.game_id = :gameId \n" +
				"	INNER JOIN orbital_system os \n" +
				"		ON os.planet_id = p.id \n" +
				"	LEFT OUTER JOIN orbital_system osb \n" +
				"		ON osb.planet_id = p.id AND osb.type IS NOT NULL \n";

		Map<String, Object> params = new HashMap<>();
		params.put("gameId", gameId);
		params.put("loginId", loginId);

		List<String> wheres = new ArrayList<>();

		if (StringUtils.isNotBlank(name)) {
			wheres.add("p.name ILIKE :name");
			params.put("name", "%" + name + "%");
		}

		if (starbase != null) {
			wheres.add("p.starbase_id IS NOT NULL = :starbase");
			params.put("starbase", starbase);
		}

		if (hasRoute != null) {
			String join = hasRoute ? "INNER" : "LEFT OUTER";

			sql += "" +
					"	" + join + " JOIN ship_route_entry sr \n" +
					"		ON sr.planet_id = p.id \n" +
					"	" + join + " JOIN ship s \n" +
					"		ON s.id = sr.ship_id AND s.player_id = pl.id \n";

			if (!hasRoute) {
				wheres.add("sr.id IS NULL");
			}
		}

		sql += where(wheres);

		sql += " " +
				"GROUP BY \n" +
				"	p.id, \n" +
				"	p.name, \n" +
				"	p.colonists, \n" +
				"	p.supplies, \n" +
				"	p.money, \n" +
				"	p.fuel, \n" +
				"	p.mineral1, \n" +
				"	p.mineral2, \n" +
				"	p.mineral3, \n" +
				"	p.mines, \n" +
				"	p.factories, \n" +
				"	p.planetary_defense \n";

		sql += createOrderBy(page.getSort(), "p.colonists DESC");

		RowMapper<PlanetListResultDTO> rowMapper = new BeanPropertyRowMapper<>(PlanetListResultDTO.class);

		return search(sql, params, page, rowMapper);
	}
}