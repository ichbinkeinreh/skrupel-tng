package org.skrupeltng.modules.ingame.modules.politics.database;

import java.util.List;

public interface PlayerRelationRepositoryCustom {

	List<PlayerRelationContainer> getContainers(long currentPlayerId, long gameId);
}