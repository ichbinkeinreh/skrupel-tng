package org.skrupeltng.modules.ingame.modules.starbase.controller;

import java.io.Serializable;

public class StarbaseShipConstructionRequest implements Serializable {

	private static final long serialVersionUID = 5059411943616371765L;

	private String name;
	private long hullStockId;
	private long propulsionSystemStockId;
	private Long energyStockId;
	private Long projectileStockId;
	private String shipModule;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getHullStockId() {
		return hullStockId;
	}

	public void setHullStockId(long hullStockId) {
		this.hullStockId = hullStockId;
	}

	public long getPropulsionSystemStockId() {
		return propulsionSystemStockId;
	}

	public void setPropulsionSystemStockId(long propulsionSystemStockId) {
		this.propulsionSystemStockId = propulsionSystemStockId;
	}

	public Long getEnergyStockId() {
		return energyStockId;
	}

	public void setEnergyStockId(Long energyStockId) {
		this.energyStockId = energyStockId;
	}

	public Long getProjectileStockId() {
		return projectileStockId;
	}

	public void setProjectileStockId(Long projectileStockId) {
		this.projectileStockId = projectileStockId;
	}

	public String getShipModule() {
		return shipModule;
	}

	public void setShipModule(String shipModule) {
		this.shipModule = shipModule;
	}
}