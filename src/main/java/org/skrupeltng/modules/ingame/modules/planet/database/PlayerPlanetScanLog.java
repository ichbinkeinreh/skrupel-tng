package org.skrupeltng.modules.ingame.modules.planet.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.skrupeltng.modules.ingame.database.player.Player;

@Entity
@Table(name = "player_planet_scan_log")
public class PlayerPlanetScanLog implements Serializable {

	private static final long serialVersionUID = 3965983809933870765L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	@ManyToOne(fetch = FetchType.LAZY)
	private Planet planet;

	@Column(name = "last_player_color")
	private String lastPlayerColor;

	@Column(name = "had_starbase")
	private boolean hadStarbase;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public String getLastPlayerColor() {
		return lastPlayerColor;
	}

	public void setLastPlayerColor(String lastPlayerColor) {
		this.lastPlayerColor = lastPlayerColor;
	}

	public boolean isHadStarbase() {
		return hadStarbase;
	}

	public void setHadStarbase(boolean hadStarbase) {
		this.hadStarbase = hadStarbase;
	}
}