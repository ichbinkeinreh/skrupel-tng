package org.skrupeltng.modules.ingame.modules.starbase.database;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.masterdata.StarbaseProducable;
import org.skrupeltng.modules.masterdata.service.StarbaseUpgradeType;

@Entity
@Table(name = "starbase")
public class Starbase implements Serializable, Coordinate {

	private static final long serialVersionUID = -7737821269504592068L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "starbase")
	private Planet planet;

	private String name;

	private String log;

	@Enumerated(EnumType.STRING)
	private StarbaseType type;

	private int defense;

	@Column(name = "hull_level")
	private int hullLevel;

	@Column(name = "propulsion_level")
	private int propulsionLevel;

	@Column(name = "energy_level")
	private int energyLevel;

	@Column(name = "projectile_level")
	private int projectileLevel;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "starbase")
	private List<StarbaseHullStock> hullStocks;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "starbase")
	private List<StarbasePropulsionStock> propulsionStocks;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "starbase")
	private List<StarbaseWeaponStock> weaponStocks;

	@Override
	public String toString() {
		return "Starbase [id=" + id + ", planet=" + planet + ", name=" + name + "]";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public StarbaseType getType() {
		return type;
	}

	public void setType(StarbaseType type) {
		this.type = type;
	}

	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		if (defense > retrieveMaxDefense()) {
			throw new IllegalArgumentException("Cannot build " + defense + " defensive systems in starbase " + id + "!");
		}

		this.defense = defense;
	}

	public int getHullLevel() {
		return hullLevel;
	}

	public void setHullLevel(int hullLevel) {
		this.hullLevel = hullLevel;
	}

	public int getPropulsionLevel() {
		return propulsionLevel;
	}

	public void setPropulsionLevel(int propulsionLevel) {
		this.propulsionLevel = propulsionLevel;
	}

	public int getEnergyLevel() {
		return energyLevel;
	}

	public void setEnergyLevel(int energyLevel) {
		this.energyLevel = energyLevel;
	}

	public int getProjectileLevel() {
		return projectileLevel;
	}

	public void setProjectileLevel(int projectileLevel) {
		this.projectileLevel = projectileLevel;
	}

	public List<StarbaseHullStock> getHullStocks() {
		return hullStocks;
	}

	public void setHullStocks(List<StarbaseHullStock> hullStocks) {
		this.hullStocks = hullStocks;
	}

	public List<StarbasePropulsionStock> getPropulsionStocks() {
		return propulsionStocks;
	}

	public void setPropulsionStocks(List<StarbasePropulsionStock> propulsionStocks) {
		this.propulsionStocks = propulsionStocks;
	}

	public List<StarbaseWeaponStock> getWeaponStocks() {
		return weaponStocks;
	}

	public void setWeaponStocks(List<StarbaseWeaponStock> weaponStocks) {
		this.weaponStocks = weaponStocks;
	}

	public int retrieveTechLevel(StarbaseUpgradeType type) {
		switch (type) {
			case HULL:
				return hullLevel;
			case PROPULSION:
				return propulsionLevel;
			case ENERGY:
				return energyLevel;
			case PROJECTILE:
				return projectileLevel;
		}

		return 0;
	}

	public void upgrade(StarbaseUpgradeType type) {
		switch (type) {
			case HULL:
				hullLevel++;
				break;
			case PROPULSION:
				propulsionLevel++;
				break;
			case ENERGY:
				energyLevel++;
				break;
			case PROJECTILE:
				projectileLevel++;
				break;
		}
	}

	public int retrieveMaxDefense() {
		int moneyCosts = planet.getMoney() / 10;
		int mineral2 = planet.getMineral2();

		return Math.min(Math.min(type.getMaxDefense(), defense + moneyCosts), defense + mineral2);
	}

	public boolean canSendSpaceFolds() {
		return (type == StarbaseType.STAR_BASE || type == StarbaseType.WAR_BASE) && hullLevel >= 7 && propulsionLevel >= 8;
	}

	public boolean canBeUpgraded() {
		return type == StarbaseType.STAR_BASE || type == StarbaseType.WAR_BASE;
	}

	@Override
	public int getX() {
		return planet.getX();
	}

	@Override
	public int getY() {
		return planet.getY();
	}

	@Override
	public int getScanRadius() {
		return planet.getScanRadius();
	}

	public boolean canBeProduced(StarbaseProducable producable, int quantity) {
		return (type == StarbaseType.STAR_BASE || type == StarbaseType.WAR_BASE) && planet.canBeProduced(producable, quantity);
	}

	public boolean fullyUpgraded() {
		return hullLevel == 10 && propulsionLevel == 10 && energyLevel == 10 && projectileLevel == 10;
	}
}