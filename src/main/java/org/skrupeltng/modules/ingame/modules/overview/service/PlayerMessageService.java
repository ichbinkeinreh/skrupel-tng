package org.skrupeltng.modules.ingame.modules.overview.service;

import java.util.Optional;

import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.controller.SendPlayerMessageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlayerMessageService {

	@Autowired
	private NewsService newsService;

	@Autowired
	private UserDetailServiceImpl userService;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private MessageSource messageSource;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void sendMessage(SendPlayerMessageRequest request) {
		Optional<Player> recipientOpt = playerRepository.findById(request.getRecipientId());

		if (!recipientOpt.isPresent()) {
			throw new IllegalArgumentException("Recipient " + request.getRecipientId() + " not found!");
		}

		Player recipient = recipientOpt.get();

		if (recipient.getGame().getId() != request.getGameId()) {
			throw new IllegalArgumentException("Recipient " + request.getRecipientId() + " is not in game " + request.getGameId() + "!");
		}

		Player sender = playerRepository.findByGameIdAndLoginId(request.getGameId(), userService.getLoginId()).get();

		newsService.add(recipient, NewsEntryConstants.news_entry_player_message, ImageConstants.news_message, null, null,
				sender.retrieveDisplayNameWithColor(messageSource), request.getMessage(), sender.getId(),
				"'" + sender.retrieveDisplayName(messageSource) + "'");
	}
}