package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

@Entity
@Table(name = "space_fold")
public class SpaceFold implements Serializable, Coordinate {

	private static final long serialVersionUID = 6411670819616788848L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private int x;

	private int y;

	@ManyToOne(fetch = FetchType.LAZY)
	private Game game;

	private int money;

	private int supplies;

	private int fuel;

	private int mineral1;

	private int mineral2;

	private int mineral3;

	@ManyToOne(fetch = FetchType.LAZY)
	private Ship ship;

	@ManyToOne(fetch = FetchType.LAZY)
	private Planet planet;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public int getSupplies() {
		return supplies;
	}

	public void setSupplies(int supplies) {
		this.supplies = supplies;
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

	public int getMineral1() {
		return mineral1;
	}

	public void setMineral1(int mineral1) {
		this.mineral1 = mineral1;
	}

	public int getMineral2() {
		return mineral2;
	}

	public void setMineral2(int mineral2) {
		this.mineral2 = mineral2;
	}

	public int getMineral3() {
		return mineral3;
	}

	public void setMineral3(int mineral3) {
		this.mineral3 = mineral3;
	}

	public Ship getShip() {
		return ship;
	}

	public void setShip(Ship ship) {
		this.ship = ship;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	@Override
	public int getScanRadius() {
		return 0;
	}

	@Override
	public String toString() {
		return "SpaceFold [id=" + id + ", x=" + x + ", y=" + y + "]";
	}

	public String retrieveGalaxyMapStyle() {
		int xy = 7;

		return "position: absolute; right: -" + xy + "px; bottom: -" + xy + "px;";
	}
}