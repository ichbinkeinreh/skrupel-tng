package org.skrupeltng.modules.ingame.modules.overview.service;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NewsService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private NewsEntryRepository newsEntryRepository;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void add(Player player, String template, String image, Long clickTargetId, NewsEntryClickTargetType clickTargetType, Object... arguments) {
		NewsEntry e = new NewsEntry();
		e.setPlayer(player);
		e.setGame(player.getGame());
		e.setImage(image);
		e.setTemplate(template);
		e.setClickTargetId(clickTargetId);
		e.setClickTargetType(clickTargetType);
		e.setArguments(StringUtils.join(arguments, ","));

		newsEntryRepository.save(e);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void cleanNewsEntries(long gameId) {
		log.debug("Clearing news entries...");
		newsEntryRepository.deleteByGameId(gameId);
		log.debug("News entries cleared.");
	}

	public RoundSummary getRoundSummary(long gameId) {
		return newsEntryRepository.getRoundSummary(gameId);
	}

	public List<NewsEntry> findNews(long gameId, long loginId) {
		return newsEntryRepository.findByGameIdAndLoginId(gameId, loginId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public boolean toggleDeleteNews(long newsEntryId) {
		Optional<NewsEntry> result = newsEntryRepository.findById(newsEntryId);

		if (result.isPresent()) {
			NewsEntry e = result.get();
			e.setDelete(!e.isDelete());
			e = newsEntryRepository.save(e);

			return e.isDelete();
		}

		return false;
	}
}