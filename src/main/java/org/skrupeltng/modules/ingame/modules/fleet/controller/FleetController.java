package org.skrupeltng.modules.ingame.modules.fleet.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.ship.controller.AbstractShipController;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTravelData;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ingame/fleet")
public class FleetController extends AbstractShipController {

	@Autowired
	private FleetService fleetService;

	@GetMapping("")
	public String getFleetDetails(@RequestParam(required = false) Long fleetId, Model model) {
		if (fleetId == null) {
			Fleet fleet = new Fleet();
			model.addAttribute("fleet", fleet);
			return "ingame/fleet/fleet::creation";
		}

		model.addAttribute("turnDone", turnDoneForFleet(fleetId) && !configProperties.isDisablePermissionChecks());
		addFleetDetailsToModel(fleetId, model);
		return "ingame/fleet/fleet::content";
	}

	@GetMapping("/stats-details")
	public String getFleetDetailStats(@RequestParam long fleetId, Model model) {
		addFleetDetailsToModel(fleetId, model);
		return "ingame/fleet/fleet::stats-details";
	}

	private void addFleetDetailsToModel(Long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);
		model.addAttribute("fleet", fleet);
		model.addAttribute("resourceData", fleetService.getFleetResourceData(fleetId));
		model.addAttribute("taskItem", fleetService.getTaskOfAllShipsInFleet(fleetId));

		String shipIds = fleet.getShips().stream().map(s -> s.getId() + "").collect(Collectors.joining(","));
		model.addAttribute("shipIds", shipIds);
	}

	@GetMapping("/navigation")
	public String getNavigation(@RequestParam long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);
		Ship ship = fleet.getLeader();

		if (ship != null) {
			return getNavigation(ship, model);
		}

		return "ingame/fleet/fleet::fleet-empty";
	}

	@GetMapping("/task")
	public String getTask(@RequestParam long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);
		List<Ship> ships = fleet.getShips();

		if (ships.isEmpty()) {
			return "ingame/fleet/fleet::fleet-empty";
		}

		List<FleetShipTaskItemDTO> taskItems = fleetService.getTaskItems(fleetId);
		FleetShipTaskItemDTO taskItem = fleetService.getTaskOfAllShipsInFleet(fleetId);

		model.addAttribute("taskItems", taskItems);
		model.addAttribute("taskItem", taskItem);

		if (taskItems.stream().anyMatch(i -> ShipAbilityType.SIGNATURE_MASK.name().equals(i.getActiveAbilityType()))) {
			model.addAttribute("players", fleet.getPlayer().getGame().getPlayers());
		}

		return "ingame/fleet/task::content";
	}

	@GetMapping("/ships")
	public String getShips(@RequestParam long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);
		List<Ship> ships = fleet.retrieveSortedShips();

		if (ships.isEmpty()) {
			return "ingame/fleet/fleet::fleet-empty";
		}

		model.addAttribute("fleet", fleet);
		model.addAttribute("ships", ships);

		return "ingame/fleet/ships::content";
	}

	@GetMapping("/fuel")
	public String getFuelPage(@RequestParam long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);

		if (fleet.getShips().isEmpty()) {
			return "ingame/fleet/fleet::fleet-empty";
		}

		model.addAttribute("fleet", fleet);
		model.addAttribute("resourceData", fleetService.getFleetResourceData(fleetId));

		return "ingame/fleet/fuel::content";
	}

	@GetMapping("/projectiles")
	public String getProjectilePage(@RequestParam long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);

		if (fleet.getShips().isEmpty()) {
			return "ingame/fleet/fleet::fleet-empty";
		}

		model.addAttribute("fleet", fleet);
		model.addAttribute("resourceData", fleetService.getFleetResourceData(fleetId));

		return "ingame/fleet/projectiles::content";
	}

	@GetMapping("/tactics")
	public String getTacticsPage(@RequestParam long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);

		if (fleet.getShips().isEmpty()) {
			return "ingame/fleet/fleet::fleet-empty";
		}

		model.addAttribute("fleet", fleet);
		model.addAttribute("resourceData", fleetService.getFleetResourceData(fleetId));

		return "ingame/fleet/tactics::content";
	}

	@GetMapping("/options")
	public String getOptionsPage(@RequestParam long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);
		model.addAttribute("fleet", fleet);
		return "ingame/fleet/options::content";
	}

	@GetMapping("/merge")
	public String getMergePage(@RequestParam long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);
		model.addAttribute("fleet", fleet);

		List<Fleet> fleets = fleetService.getFleets(fleetId);
		model.addAttribute("fleets", fleets);

		return "ingame/fleet/merge::content";
	}

	@PutMapping("")
	@ResponseBody
	public long createFleet(@RequestParam long gameId, @RequestParam String name, @RequestParam(required = false) Long shipId) {
		long loginId = userDetailService.getLoginId();
		long fleetId = fleetService.createFleet(gameId, name, loginId);

		if (shipId != null) {
			fleetService.addShipToFleet(shipId, fleetId);
		}

		return fleetId;
	}

	@DeleteMapping("")
	@ResponseBody
	public void deleteFleet(@RequestParam long fleetId) {
		fleetService.deleteFleet(fleetId);
	}

	@PostMapping("/ship")
	@ResponseBody
	public void addShipToFleet(@RequestParam long shipId, @RequestParam long fleetId) {
		fleetService.addShipToFleet(shipId, fleetId);
	}

	@DeleteMapping("/ship")
	@ResponseBody
	public void clearShipOfFleet(@RequestParam long shipId) {
		fleetService.clearFleetOfShip(shipId);
	}

	@PostMapping("/leader")
	@ResponseBody
	public List<ShipTravelData> setLeader(@RequestParam long fleetId, @RequestParam long shipId) {
		return fleetService.setLeader(fleetId, shipId);
	}

	@PostMapping("/fill-tanks")
	@ResponseBody
	public List<Long> fillTanks(@RequestParam long fleetId, @RequestParam float percentage) {
		return fleetService.fillTanks(fleetId, percentage);
	}

	@PostMapping("/build-projectiles")
	@ResponseBody
	public List<Long> buildProjectiles(@RequestParam long fleetId) {
		return fleetService.buildProjectiles(fleetId);
	}

	@PostMapping("/tactics")
	public String changeTactics(@RequestParam long fleetId, @RequestParam int aggressiveness) {
		fleetService.changeTactics(fleetId, aggressiveness);
		return "ingame/fleet/tactics::success";
	}

	@PostMapping("/task")
	public String setTask(@RequestParam long fleetId, @RequestParam String taskType, @RequestParam(required = false) String activeAbilityType,
			@RequestParam(required = false) String taskValue) {
		fleetService.setTask(fleetId, taskType, activeAbilityType, taskValue);
		return "ingame/ship/task::success";
	}

	@PostMapping("/name")
	public String changeName(@RequestParam long fleetId, @RequestParam String name) {
		fleetService.changeName(fleetId, name);
		return "ingame/fleet/options::name-changed";
	}

	@PostMapping("/merge")
	public String mergeFleets(@RequestParam long fleetId, @RequestParam long otherFleetId) {
		fleetService.mergeFleets(fleetId, otherFleetId);
		return "ingame/fleet/merge::success";
	}
}