package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skrupeltng.modules.RepositoryCustomBase;

public class WormHoleRepositoryImpl extends RepositoryCustomBase implements WormHoleRepositoryCustom {

	@Override
	public List<Long> getWormHoleIdsInRadius(long gameId, int x, int y, int distance) {
		String sql = "" +
				"SELECT \n" +
				"	a.id \n" +
				"FROM \n" +
				"	worm_hole a \n" +
				"WHERE \n" +
				"	a.game_id = :gameId \n" +
				"	AND sqrt(((a.x - :x) * (a.x - :x)) + ((a.y - :y) * (a.y - :y))) <= :distance";

		Map<String, Object> params = new HashMap<>(4);
		params.put("gameId", gameId);
		params.put("x", x);
		params.put("y", y);
		params.put("distance", distance);

		return jdbcTemplate.queryForList(sql, params, Long.class);
	}
}