package org.skrupeltng.modules.ingame.modules.orbitalsystem.service;

import java.util.List;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.orbitalsystem.controller.GroundUnitProductionData;
import org.skrupeltng.modules.ingame.modules.orbitalsystem.controller.GroundUnitProductionRequest;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrbitalSystemService {

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private ShipRepository shipRepository;

	@PreAuthorize("hasPermission(#orbitalSystemId, 'orbitalSystem') and hasPermission(#orbitalSystemId, 'turnNotDoneOrbitalSystem')")
	public OrbitalSystem get(long orbitalSystemId) {
		return orbitalSystemRepository.getOne(orbitalSystemId);
	}

	@PreAuthorize("hasPermission(#orbitalSystemId, 'orbitalSystem') and hasPermission(#orbitalSystemId, 'turnNotDoneOrbitalSystem')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Planet construct(long orbitalSystemId, OrbitalSystemType type) {
		OrbitalSystem orbitalSystem = orbitalSystemRepository.getOne(orbitalSystemId);

		return constructWithoutPermissionsCheck(type, orbitalSystem);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Planet constructWithoutPermissionsCheck(OrbitalSystemType type, OrbitalSystem orbitalSystem) {
		if (orbitalSystem.getType() != null) {
			throw new IllegalArgumentException("OrbitalSystem " + orbitalSystem.getId() + " already has a type!");
		}

		Planet planet = orbitalSystem.getPlanet();

		if (!type.canBeBuild(planet)) {
			throw new IllegalArgumentException("Planet " + planet.getId() + " does not have enough resources to build orbital system " + type + "!");
		}

		if (planet.getOrbitalSystems().stream().anyMatch(o -> o.getType() == type)) {
			throw new IllegalArgumentException("Planet " + planet.getId() + " already has an OrbitalSystem with type '" + type + "'!");
		}

		if (!planet.getPlayer().getFaction().orbitalSystemAllowed(type)) {
			throw new IllegalArgumentException("OrbitalSystem not allowed for this faction!");
		}

		orbitalSystem.setType(type);
		orbitalSystem = orbitalSystemRepository.save(orbitalSystem);

		planet = orbitalSystem.getPlanet();
		planet.spendMoney(type.getMoney());
		planet.spendSupplies(type.getSupplies());
		planet.spendFuel(type.getFuel());
		planet.spendMineral1(type.getMineral1());
		planet.spendMineral2(type.getMineral2());
		planet.spendMineral3(type.getMineral3());

		planet = planetRepository.save(planet);

		if (type == OrbitalSystemType.ORBITAL_EXTENSION) {
			int size = planet.getOrbitalSystems().size();

			if (size < 6) {
				orbitalSystem = new OrbitalSystem(planet);
				orbitalSystemRepository.save(orbitalSystem);
			}

			if (size < 5) {
				orbitalSystem = new OrbitalSystem(planet);
				orbitalSystemRepository.save(orbitalSystem);
			}
		}

		return planet;
	}

	@PreAuthorize("hasPermission(#orbitalSystemId, 'orbitalSystem') and hasPermission(#orbitalSystemId, 'turnNotDoneOrbitalSystem')")
	public List<Ship> getScannedShips(long orbitalSystemId) {
		OrbitalSystem orbitalSystem = orbitalSystemRepository.getOne(orbitalSystemId);
		Planet planet = orbitalSystem.getPlanet();
		Player player = planet.getPlayer();
		long playerId = player.getId();
		long gameId = player.getGame().getId();
		int scannerDist = planet.getScanRadius();

		if (planet.hasOrbitalSystem(OrbitalSystemType.PSY_CORPS)) {
			scannerDist = 90;
		}

		if (planet.getStarbase() != null && planet.getStarbase().getType().getTypeId() > 1) {
			scannerDist = 116;
		}

		int dist = scannerDist;

		List<Ship> ships = shipRepository.findByGameId(gameId);
		return ships.stream().filter(s -> s.getPlayer().getId() != playerId && CoordHelper.getDistance(s, planet) <= dist).collect(Collectors.toList());
	}

	@PreAuthorize("hasPermission(#orbitalSystemId, 'orbitalSystem') and hasPermission(#orbitalSystemId, 'turnNotDoneOrbitalSystem')")
	public Ship getScannedShip(long orbitalSystemId, long scannedShipId) {
		Ship ship = shipRepository.getOne(scannedShipId);

		if (getScannedShips(orbitalSystemId).contains(ship)) {
			return ship;
		}

		throw new IllegalArgumentException("Ship " + scannedShipId + " is not in scanner reach of OrbitalSystem " + orbitalSystemId + "!");
	}

	@PreAuthorize("hasPermission(#orbitalSystemId, 'orbitalSystem') and hasPermission(#orbitalSystemId, 'turnNotDoneOrbitalSystem')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void produceGroundUnits(long orbitalSystemId, GroundUnitProductionRequest request) {
		OrbitalSystem orbitalSystem = orbitalSystemRepository.getOne(orbitalSystemId);
		Planet planet = orbitalSystem.getPlanet();

		GroundUnitProductionData data = new GroundUnitProductionData(orbitalSystem.getType(), planet);
		int quantity = request.getQuantity();

		if (quantity <= 0) {
			throw new IllegalArgumentException("Quantity must be higher than zero!");
		}

		if (quantity > data.retrieveNewlyProducable()) {
			throw new IllegalArgumentException("OrbitalSystem " + orbitalSystemId + " cannot produce ground units: " + request);
		}

		planet.setColonists(planet.getColonists() - (data.getColonistCosts() * quantity));
		planet.spendMoney(data.getMoneyCosts() * quantity);
		planet.spendSupplies(data.getSupplyCosts() * quantity);
		planet.spendFuel(data.getFuelCosts() * quantity);
		planet.spendMineral1(data.getMineral1Costs() * quantity);
		planet.spendMineral2(data.getMineral2Costs() * quantity);
		planet.spendMineral3(data.getMineral3Costs() * quantity);

		if (request.isLight()) {
			planet.setLightGroundUnitsProduction(planet.getLightGroundUnitsProduction() + quantity);
		} else {
			planet.setHeavyGroundUnitsProduction(planet.getHeavyGroundUnitsProduction() + quantity);
		}

		planetRepository.save(planet);
	}
}