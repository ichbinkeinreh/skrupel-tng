package org.skrupeltng.modules.ingame.modules.orbitalsystem.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.orbitalsystem.service.OrbitalSystemService;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ingame/orbital-system")
public class OrbitalSystemController extends AbstractController {

	@Autowired
	private OrbitalSystemService orbitalSystemService;

	@GetMapping("")
	public String getOrbitalSystem(@RequestParam long orbitalSystemId, Model model) {
		OrbitalSystem orbitalSystem = orbitalSystemService.get(orbitalSystemId);
		model.addAttribute("orbitalSystem", orbitalSystem);
		Planet planet = orbitalSystem.getPlanet();

		OrbitalSystemType type = orbitalSystem.getType();

		if (type == null) {
			Player player = planet.getPlayer();
			Faction faction = player.getFaction();
			boolean enableEspionage = player.getGame().isEnableEspionage();
			OrbitalSystemType[] types = OrbitalSystemType.values();

			List<OrbitalSystemType> typeList = new ArrayList<>(types.length);
			Set<OrbitalSystemType> existingTypes = planet.getOrbitalSystems().stream().map(OrbitalSystem::getType).collect(Collectors.toSet());

			for (OrbitalSystemType t : types) {
				if (!existingTypes.contains(t) && faction.orbitalSystemAllowed(t) && (enableEspionage || t != OrbitalSystemType.SPY_CENTER)) {
					typeList.add(t);
				}
			}

			model.addAttribute("types", typeList);

			return "ingame/orbitalsystem/orbitalsystem-construction::content";
		}

		if (type == OrbitalSystemType.ECHO_CENTER) {
			List<Ship> ships = orbitalSystemService.getScannedShips(orbitalSystemId);
			model.addAttribute("ships", ships);
			model.addAttribute("orbitalSystemId", orbitalSystemId);
			model.addAttribute("planets", Collections.emptyList());
			return "ingame/ship/scanner::content";
		}

		if (type == OrbitalSystemType.MILITARY_BASE || type == OrbitalSystemType.ARMS_COMPLEX) {
			boolean light = type == OrbitalSystemType.MILITARY_BASE;
			model.addAttribute("light", light);

			GroundUnitProductionData data = new GroundUnitProductionData(type, planet);
			model.addAttribute("data", data);

			return "ingame/orbitalsystem/orbitalsystem::groundunits";
		}

		return "ingame/orbitalsystem/orbitalsystem::simple";
	}

	@PostMapping("")
	@ResponseBody
	public void construct(@RequestParam long orbitalSystemId, @RequestParam String type) {
		orbitalSystemService.construct(orbitalSystemId, OrbitalSystemType.valueOf(type));
	}

	@GetMapping("/scanner/ship")
	public String getScannedShipDetails(@RequestParam long orbitalSystemId, @RequestParam long scannedShipId, Model model) {
		Ship ship = orbitalSystemService.getScannedShip(orbitalSystemId, scannedShipId);
		model.addAttribute("ship", ship);
		return "ingame/ship/scanner::ship";
	}

	@PostMapping("/ground-units")
	@ResponseBody
	public void produceGroundUnits(@RequestBody GroundUnitProductionRequest request) {
		orbitalSystemService.produceGroundUnits(request.getOrbitalSystemId(), request);
	}
}