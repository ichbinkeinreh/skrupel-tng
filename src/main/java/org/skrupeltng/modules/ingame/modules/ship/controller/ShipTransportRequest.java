package org.skrupeltng.modules.ingame.modules.ship.controller;

import java.io.Serializable;
import java.util.Objects;

import org.skrupeltng.modules.ingame.modules.ship.GoodsContainer;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

public class ShipTransportRequest implements Serializable, GoodsContainer {

	private static final long serialVersionUID = 1745259421219000897L;

	private int fuel;
	private int colonists;
	private int money;
	private int mineral1;
	private int mineral2;
	private int mineral3;
	private int supplies;
	private int lightGroundUnits;
	private int heavyGroundUnits;

	public ShipTransportRequest() {

	}

	public ShipTransportRequest(Ship ship) {
		fuel = ship.getFuel();
		colonists = ship.getColonists();
		money = ship.getMoney();
		mineral1 = ship.getMineral1();
		mineral2 = ship.getMineral2();
		mineral3 = ship.getMineral3();
		supplies = ship.getSupplies();
		lightGroundUnits = ship.getLightGroundUnits();
		heavyGroundUnits = ship.getHeavyGroundUnits();
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

	@Override
	public int getColonists() {
		return colonists;
	}

	public void setColonists(int colonists) {
		this.colonists = colonists;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	@Override
	public int getMineral1() {
		return mineral1;
	}

	public void setMineral1(int mineral1) {
		this.mineral1 = mineral1;
	}

	@Override
	public int getMineral2() {
		return mineral2;
	}

	public void setMineral2(int mineral2) {
		this.mineral2 = mineral2;
	}

	@Override
	public int getMineral3() {
		return mineral3;
	}

	public void setMineral3(int mineral3) {
		this.mineral3 = mineral3;
	}

	@Override
	public int getSupplies() {
		return supplies;
	}

	public void setSupplies(int supplies) {
		this.supplies = supplies;
	}

	@Override
	public int getLightGroundUnits() {
		return lightGroundUnits;
	}

	public void setLightGroundUnits(int lightGroundUnits) {
		this.lightGroundUnits = lightGroundUnits;
	}

	@Override
	public int getHeavyGroundUnits() {
		return heavyGroundUnits;
	}

	public void setHeavyGroundUnits(int heavyGroundUnits) {
		this.heavyGroundUnits = heavyGroundUnits;
	}

	@Override
	public String toString() {
		return "ShipTransportRequest [fuel=" + fuel + ", colonists=" + colonists + ", money=" + money + ", mineral1=" + mineral1 + ", mineral2=" + mineral2 +
				", mineral3=" + mineral3 + ", supplies=" + supplies + ", lightGroundUnits=" + lightGroundUnits + ", heavyGroundUnits=" + heavyGroundUnits + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(colonists, fuel, heavyGroundUnits, lightGroundUnits, mineral1, mineral2, mineral3, money, supplies);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ShipTransportRequest)) {
			return false;
		}
		ShipTransportRequest other = (ShipTransportRequest)obj;
		return colonists == other.colonists && fuel == other.fuel && heavyGroundUnits == other.heavyGroundUnits && lightGroundUnits == other.lightGroundUnits &&
				mineral1 == other.mineral1 && mineral2 == other.mineral2 && mineral3 == other.mineral3 && money == other.money && supplies == other.supplies;
	}
}