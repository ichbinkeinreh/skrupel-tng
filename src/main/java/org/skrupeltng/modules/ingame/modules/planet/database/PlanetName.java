package org.skrupeltng.modules.ingame.modules.planet.database;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "planet_name")
public class PlanetName implements Serializable {

	private static final long serialVersionUID = 6161776666838748963L;

	@Id
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}