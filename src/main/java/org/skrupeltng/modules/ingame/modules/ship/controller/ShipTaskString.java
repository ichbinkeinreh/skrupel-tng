package org.skrupeltng.modules.ingame.modules.ship.controller;

import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;

public class ShipTaskString {

	public static String retrieveTaskString(ShipTaskType taskType, boolean hasRoute, ShipAbilityType activeAbilityType) {
		if (taskType == ShipTaskType.ACTIVE_ABILITY) {
			return "ship_ability_" + activeAbilityType.name();
		}

		if (hasRoute) {
			return "routing";
		}

		return "task_" + taskType.name().toLowerCase();
	}

	public static String retrieveTravelString(int x, int y, String targetName) {
		if (x >= 0 || y >= 0) {
			String target = targetName != null ? " (" + targetName + ")" : "";

			return "-> X:" + x + ",Y:" + y + target;
		}

		return "";
	}
}
