package org.skrupeltng.modules.ingame.modules.overview.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface NewsEntryRepository extends JpaRepository<NewsEntry, Long>, NewsEntryRepositoryCustom {

	@Query("SELECT " +
			"	n " +
			"FROM " +
			"	NewsEntry n " +
			"	INNER JOIN n.player p " +
			"WHERE " +
			"	n.game.id = ?1 " +
			"	AND p.login.id = ?2")
	List<NewsEntry> findByGameIdAndLoginId(long gameId, long loginId);

	@Modifying
	@Query("DELETE FROM NewsEntry n WHERE n.player.id = ?1")
	void deleteByPlayerId(long playerId);
}