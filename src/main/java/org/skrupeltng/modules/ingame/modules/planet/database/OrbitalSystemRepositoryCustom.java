package org.skrupeltng.modules.ingame.modules.planet.database;

public interface OrbitalSystemRepositoryCustom {

	boolean loginOwnsOrbitalSystem(long orbitalSystemId, long loginId);
}