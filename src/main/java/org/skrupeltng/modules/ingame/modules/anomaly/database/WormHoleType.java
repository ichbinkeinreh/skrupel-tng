package org.skrupeltng.modules.ingame.modules.anomaly.database;

public enum WormHoleType {

	STABLE_WORMHOLE,

	UNSTABLE_WORMHOLE,

	JUMP_PORTAL
}