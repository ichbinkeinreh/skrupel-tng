package org.skrupeltng.modules.ingame.modules.planet.database;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PlanetNameRepository extends JpaRepository<PlanetName, String>, PlanetNameRepositoryCustom {

}