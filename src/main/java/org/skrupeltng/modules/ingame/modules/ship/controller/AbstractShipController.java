package org.skrupeltng.modules.ingame.modules.ship.controller;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.ShipAbilityDescriptionMapper;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

public class AbstractShipController extends AbstractController {

	@Autowired
	protected ShipAbilityDescriptionMapper shipAbilityDescriptionMapper;

	@Autowired
	protected ShipService shipService;

	@Autowired
	protected MasterDataService masterDataService;

	protected String getNavigation(Ship ship, Model model) {
		if (ship.getTravelSpeed() <= 0) {
			ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
		}

		model.addAttribute("ship", ship);

		int propulsionLevel = ship.getPropulsionSystemTemplate().getTechLevel();
		float[] fuelConsumptionData = masterDataService.getFuelConsumptionData(propulsionLevel);
		String fuel = StringUtils.join(fuelConsumptionData, ',');
		model.addAttribute("fuelConsumptionData", fuel);

		String destinationName = shipService.getDestinationName(ship.getId());
		model.addAttribute("destinationName", destinationName);

		return "ingame/ship/navigation::content";
	}
}