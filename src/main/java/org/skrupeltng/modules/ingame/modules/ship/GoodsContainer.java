package org.skrupeltng.modules.ingame.modules.ship;

public interface GoodsContainer {

	int getMineral1();

	int getMineral2();

	int getMineral3();

	int getColonists();

	int getSupplies();

	int getLightGroundUnits();

	int getHeavyGroundUnits();
}