package org.skrupeltng.modules.ingame.modules.ship;

import org.skrupeltng.modules.masterdata.service.MasterDataConstants;

public class TotalGoodCalculator {

	public static int retrieveSum(GoodsContainer container) {
		int mineral1 = container.getMineral1();
		int mineral2 = container.getMineral2();
		int mineral3 = container.getMineral3();
		int supplies = container.getSupplies();
		int colonists = container.getColonists();
		int lightGroundUnits = container.getLightGroundUnits();
		int heavyGroundUnits = container.getHeavyGroundUnits();

		return Math.round(mineral1 + mineral2 + mineral3 + supplies + (colonists / MasterDataConstants.COLONIST_STORAGE_FACTOR) + (lightGroundUnits * MasterDataConstants.LIGHT_GROUND_UNITS_FACTOR) + (heavyGroundUnits * MasterDataConstants.HEAVY_GROUND_UNITS_FACTOR));
	}
}