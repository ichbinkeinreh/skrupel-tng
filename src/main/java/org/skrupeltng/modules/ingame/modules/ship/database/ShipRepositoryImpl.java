package org.skrupeltng.modules.ingame.modules.ship.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ingame.controller.ShipListResultDTO;
import org.skrupeltng.modules.ingame.controller.ShipOverviewRequest;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.service.MasterDataConstants;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

import com.google.common.collect.Lists;

public class ShipRepositoryImpl extends RepositoryCustomBase implements ShipRepositoryCustom {

	private final List<String> PLASMA_STORM_ABILITIES = Lists.newArrayList(
			ShipAbilityType.JUMP_PORTAL.name(),
			ShipAbilityType.CLOAKING_PERFECT.name(),
			ShipAbilityType.CLOAKING_RELIABLE.name(),
			ShipAbilityType.CLOAKING_UNRELIABLE.name(),
			ShipAbilityType.ASTRO_PHYSICS_LAB.name(),
			ShipAbilityType.EXTENDED_SENSORS.name(),
			ShipAbilityType.SUB_SPACE_DISTORTION.name(),
			ShipAbilityType.JUMP_ENGINE.name());

	@Override
	public boolean loginOwnsShip(long shipId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	p.login_id \n" +
				"FROM \n" +
				"	ship s \n" +
				"	INNER JOIN player p \n" +
				"		ON p.id = s.player_id \n" +
				"WHERE \n" +
				"	s.id = :shipId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("shipId", shipId);

		List<Long> results = jdbcTemplate.queryForList(sql, params, Long.class);
		return results.size() == 1 && results.get(0).longValue() == loginId;
	}

	@Override
	public long getEnemyShipCountByPlanetDestination(int x, int y, long gameId, long playerId) {
		String sql = "" +
				"SELECT \n" +
				"	COUNT(s.id) \n" +
				"FROM \n" +
				"	ship s \n" +
				"	INNER JOIN player pl \n" +
				"		ON pl.id = s.player_id AND pl.game_id = :gameId AND pl.id != :playerId \n" +
				"	LEFT OUTER JOIN player_relation pr \n" +
				"		ON (pr.player1_id = :playerId OR pr.player2_id = :playerId) AND (pr.player1_id = pl.id OR pr.player2_id = pl.id) " +
				"			AND pr.type IN (:excludedRelationTypes) \n" +
				"WHERE \n" +
				"	pr IS NULL \n" +
				"	AND s.destination_x = :x \n" +
				"	AND s.destination_y = :y \n" +
				"	AND s.destination_ship_id IS NULL";

		Map<String, Object> params = new HashMap<>(5);
		params.put("x", x);
		params.put("y", y);
		params.put("gameId", gameId);
		params.put("playerId", playerId);
		params.put("excludedRelationTypes", Lists.newArrayList(PlayerRelationType.TRADE_AGREEMENT.name(), PlayerRelationType.NON_AGGRESSION_TREATY.name(),
				PlayerRelationType.ALLIANCE.name()));

		return jdbcTemplate.queryForObject(sql, params, Long.class);
	}

	@Override
	public List<Long> getShipIdsInRadius(long gameId, int x, int y, int distance) {
		String sql = "" +
				"SELECT \n" +
				"	s.id \n" +
				"FROM \n" +
				"	ship s \n" +
				"	INNER JOIN player pl \n" +
				"		ON pl.id = s.player_id AND pl.game_id = :gameId \n" +
				"WHERE \n" +
				"	sqrt(((s.x - :x) * (s.x - :x)) + ((s.y - :y) * (s.y - :y))) <= :distance";

		Map<String, Object> params = new HashMap<>(4);
		params.put("gameId", gameId);
		params.put("x", x);
		params.put("y", y);
		params.put("distance", distance);

		return jdbcTemplate.queryForList(sql, params, Long.class);
	}

	@Override
	public void resetShipScanRadius(long gameId) {
		String sql = "" +
				"UPDATE \n" +
				"	ship \n" +
				"SET \n" +
				"	scan_radius = 47 \n" +
				"WHERE \n" +
				"	id IN (\n" +
				"		SELECT \n" +
				"			s.id \n" +
				"		FROM \n" +
				"			ship s \n" +
				"			INNER JOIN player p \n" +
				"				ON p.id = s.player_id AND p.game_id = :gameId \n" +
				"	)";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		jdbcTemplate.update(sql, params);
	}

	@Override
	public void updateScanRadius(long gameId, int scanRadius, ShipAbilityType abilityType) {
		String sql = "" +
				"UPDATE \n" +
				"	ship \n" +
				"SET \n" +
				"	scan_radius = :scanRadius \n" +
				"WHERE \n" +
				"	id IN (\n" +
				"		SELECT \n" +
				"			s.id \n" +
				"		FROM \n" +
				"			ship s \n" +
				"			INNER JOIN player p \n" +
				"				ON p.id = s.player_id AND p.game_id = :gameId \n" +
				"			INNER JOIN ship_ability sa \n" +
				"				ON sa.id = s.active_ability_id AND sa.type = :abilityType \n" +
				"	)";

		Map<String, Object> params = new HashMap<>(3);
		params.put("gameId", gameId);
		params.put("scanRadius", scanRadius);
		params.put("abilityType", abilityType.name());

		jdbcTemplate.update(sql, params);
	}

	@Override
	public void cloakAntigravPropulsion(long gameId) {
		String sql = "" +
				"UPDATE \n" +
				"	ship \n" +
				"SET \n" +
				"	cloaked = true \n" +
				"WHERE \n" +
				"	id IN (\n" +
				"		SELECT \n" +
				"			s.id \n" +
				"		FROM \n" +
				"			ship s \n" +
				"			INNER JOIN player p \n" +
				"				ON p.id = s.player_id AND p.game_id = :gameId \n" +
				"		WHERE \n" +
				"			s.propulsion_system_template_name = 'antigrav_engine' \n" +
				"	)";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		jdbcTemplate.update(sql, params);
	}

	@Override
	public long getSupportingShipCount(long playerId, int x, int y, int minWeaponCount, int minTechLevel) {
		String sql = "" +
				"SELECT \n" +
				"	COUNT(s.id) \n" +
				"FROM \n" +
				"	ship s \n" +
				"	INNER JOIN ship_template st \n" +
				"		ON st.id = s.ship_template_id \n" +
				"WHERE \n" +
				"	s.player_id = :playerId \n" +
				"	AND s.x = :x \n" +
				"	AND s.y = :y \n" +
				"	AND st.tech_level >= :minTechLevel \n" +
				"	AND (st.energy_weapons_count >= :minWeaponCount OR st.projectile_weapons_count >= :minWeaponCount OR st.hangar_capacity >= :minWeaponCount)";

		Map<String, Object> params = new HashMap<>(5);
		params.put("playerId", playerId);
		params.put("x", x);
		params.put("y", y);
		params.put("minWeaponCount", minWeaponCount);
		params.put("minTechLevel", minTechLevel);

		return jdbcTemplate.queryForObject(sql, params, Long.class);
	}

	@Override
	public long getCommunicationCenterShipCount(long playerId, int x, int y, long excludedShipId) {
		String sql = "" +
				"SELECT \n" +
				"	COUNT(s.id) \n" +
				"FROM \n" +
				"	ship s \n" +
				"	INNER JOIN ship_template st \n" +
				"		ON st.id = s.ship_template_id \n" +
				"	INNER JOIN ship_ability sa \n" +
				"		ON sa.ship_template_id = st.id \n" +
				"WHERE \n" +
				"	s.player_id = :playerId \n" +
				"	AND s.x = :x \n" +
				"	AND s.y = :y \n" +
				"	AND s.id != :excludedShipId \n" +
				"	AND sa.type = '" + ShipAbilityType.COMMUNICATION_CENTER + "' ";

		Map<String, Object> params = new HashMap<>(4);
		params.put("playerId", playerId);
		params.put("x", x);
		params.put("y", y);
		params.put("excludedShipId", excludedShipId);

		return jdbcTemplate.queryForObject(sql, params, Long.class);
	}

	@Override
	public int getHighestSupportingTechLevel(long shipId, long playerId, int x, int y) {
		String sql = "" +
				"SELECT \n" +
				"	COALESCE(MAX(st.tech_level), 0) \n" +
				"FROM \n" +
				"	ship s \n" +
				"	INNER JOIN ship_template st \n" +
				"		ON st.id = s.ship_template_id \n" +
				"WHERE \n" +
				"	s.player_id = :playerId \n" +
				"	AND s.destination_ship_id = :shipId \n" +
				"	AND s.x = :x \n" +
				"	AND s.y = :y \n" +
				"	AND (st.energy_weapons_count >= 1 OR st.projectile_weapons_count >= 1 OR st.hangar_capacity >= 1)";

		Map<String, Object> params = new HashMap<>(4);
		params.put("shipId", shipId);
		params.put("playerId", playerId);
		params.put("x", x);
		params.put("y", y);

		List<Integer> results = jdbcTemplate.queryForList(sql, params, Integer.class);

		if (results.size() == 1) {
			return results.get(0);
		}

		return 0;
	}

	@Override
	public void processShipsInPlasmaStorms(long gameId) {
		Map<String, Object> params = new HashMap<>(3);
		params.put("gameId", gameId);
		params.put("mimoticShields", ShipModule.MIMOTIC_SHIELDS.name());

		String subSelect = "" +
				"		SELECT \n" +
				"			s.id \n" +
				"		FROM \n" +
				"			ship s \n" +
				"			INNER JOIN player p \n" +
				"				ON p.id = s.player_id AND p.game_id = :gameId AND s.ship_module != :mimoticShields \n" +
				"			INNER JOIN plasma_storm ps \n" +
				"				ON ps.game_id = :gameId AND s.x >= ps.x AND s.x <= ps.x + 10 AND s.y >= ps.y AND s.y <= ps.y + 10 \n";

		String limitTravelSpeedSql = "" +
				"UPDATE \n" +
				"	ship \n" +
				"SET \n" +
				"	travel_speed = 5, \n" +
				"	pre_plasma_storm_travel_speed = travel_speed \n" +
				"WHERE \n" +
				"	travel_speed > 5 \n" +
				"	AND id IN (\n" + subSelect +
				"	)";
		jdbcTemplate.update(limitTravelSpeedSql, params);

		String restoreTravelSpeedSql = "" +
				"UPDATE \n" +
				"	ship \n" +
				"SET \n" +
				"	travel_speed = pre_plasma_storm_travel_speed, \n" +
				"	pre_plasma_storm_travel_speed = 0 \n" +
				"WHERE \n" +
				"	pre_plasma_storm_travel_speed > 0 " +
				"	AND id NOT IN (\n" + subSelect +
				"	)";
		jdbcTemplate.update(restoreTravelSpeedSql, params);

		params.put("abilities", PLASMA_STORM_ABILITIES);
		String resetTasksSql = "" +
				"UPDATE \n" +
				"	ship \n" +
				"SET \n" +
				"	task_type = null, \n" +
				"	active_ability_id = null \n" +
				"WHERE \n" +
				"	id IN (\n" + subSelect +
				"			INNER JOIN ship_ability sa \n" +
				"				ON sa.id = s.active_ability_id AND sa.type IN (:abilities) \n" +
				"	)";
		jdbcTemplate.update(resetTasksSql, params);
	}

	@Override
	public Page<ShipListResultDTO> searchShips(ShipOverviewRequest request, long loginId) {
		long gameId = request.getGameId();
		String name = request.getName();
		Boolean idle = request.getIdle();
		String templateId = request.getTemplateId();
		String fleetName = request.getFleetName();

		Pageable page = createPageable(request);

		String sql = "" +
				"SELECT \n" +
				"	s.id, \n" +
				"	s.name, \n" +
				"	((cast (s.fuel as double precision)) / st.fuel_capacity) * 100 as \"fuelPercentage\", \n" +
				"	(((s.colonists / :colFactor) + s.supplies + s.mineral1 + s.mineral2 + s.mineral3 + (s.light_ground_units * :lightFactor) + (s.heavy_ground_units * :heavyFactor)) / st.storage_space) * 100 as \"cargoPercentage\", \n" +
				"	((cast (s.crew as double precision)) / st.crew) * 100 as \"crewPercentage\", \n" +
				"	s.damage, \n" +
				"	i.value as \"templateName\", \n" +
				"	s.task_type as \"taskType\", \n" +
				"	(s.current_route_entry_id IS NOT NULL AND s.route_disabled = false) as \"hasRoute\", \n" +
				"	sa.type as \"activeAbilityType\", \n" +
				"	s.destination_x as \"destinationX\", \n" +
				"	s.destination_y as \"destinationY\", \n" +
				"	f.name as \"fleet\", \n" +
				"	count(*) OVER() AS \"totalElements\" \n" +
				"FROM \n" +
				"	ship s " +
				"	INNER JOIN player pl \n" +
				"		ON pl.id = s.player_id AND pl.login_id = :loginId AND pl.game_id = :gameId \n" +
				"	INNER JOIN ship_template st \n" +
				"		ON st.id = s.ship_template_id \n" +
				"	INNER JOIN i18n i \n" +
				"		ON i.key = st.id AND i.language = :language \n" +
				"	LEFT OUTER JOIN fleet f \n" +
				"		ON f.id = s.fleet_id \n" +
				"	LEFT OUTER JOIN ship_ability sa \n" +
				"		ON sa.id = s.active_ability_id \n";

		Map<String, Object> params = new HashMap<>();
		params.put("gameId", gameId);
		params.put("loginId", loginId);
		params.put("colFactor", MasterDataConstants.COLONIST_STORAGE_FACTOR);
		params.put("lightFactor", MasterDataConstants.LIGHT_GROUND_UNITS_FACTOR);
		params.put("heavyFactor", MasterDataConstants.HEAVY_GROUND_UNITS_FACTOR);
		params.put("language", LocaleContextHolder.getLocale().getLanguage());

		List<String> wheres = new ArrayList<>();

		if (StringUtils.isNotBlank(name)) {
			wheres.add("s.name ILIKE :name");
			params.put("name", "%" + name + "%");
		}

		if (StringUtils.isNotBlank(templateId)) {
			wheres.add("st.id = :templateId");
			params.put("templateId", templateId);
		}

		if (StringUtils.isNotBlank(fleetName)) {
			wheres.add("f.name ILIKE :fleetName");
			params.put("fleetName", "%" + fleetName + "%");
		}

		if (idle != null) {
			String condition = "s.destination_x < 0 AND s.destination_y < 0 AND s.task_type = '" + ShipTaskType.NONE + "' AND s.current_route_entry_id IS NULL";

			if (idle) {
				wheres.add(condition);
			} else {
				wheres.add("NOT (" + condition + ")");
			}
		}

		sql += where(wheres);

		sql += createOrderBy(page.getSort(), "s.name ASC");

		RowMapper<ShipListResultDTO> rowMapper = new BeanPropertyRowMapper<>(ShipListResultDTO.class);

		return search(sql, params, page, rowMapper);
	}

	@Override
	public List<Long> getShipsOnAlliedPlanets(long gameId, PlayerRelationType type) {
		String sql = "" +
				"SELECT \n" +
				"	s.id \n" +
				"FROM \n" +
				"	ship s \n" +
				"	INNER JOIN player p \n" +
				"		ON p.id = s.player_id AND p.game_id = :gameId \n" +
				"	INNER JOIN planet pl \n" +
				"		ON pl.id = s.planet_id AND pl.player_id IS NOT NULL AND pl.player_id != p.id \n" +
				"	INNER JOIN player_relation r \n" +
				"		ON (r.player1_id = p.id OR r.player2_id = p.id) AND (r.player1_id = pl.player_id OR r.player2_id = pl.player_id) AND r.type = :type";

		Map<String, Object> params = new HashMap<>(2);
		params.put("gameId", gameId);
		params.put("type", type.name());

		return jdbcTemplate.queryForList(sql, params, Long.class);
	}
}