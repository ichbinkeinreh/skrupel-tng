package org.skrupeltng.modules.ingame.modules.starbase.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseHullStock;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbasePropulsionStock;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseWeaponStock;
import org.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.skrupeltng.modules.ingame.service.IngameService;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.skrupeltng.modules.masterdata.service.StarbaseProductionEntry;
import org.skrupeltng.modules.masterdata.service.StarbaseUpgradeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ingame/starbase")
public class StarbaseController extends AbstractController {

	@Autowired
	private StarbaseService starbaseService;

	@Autowired
	private IngameService ingameService;

	@Autowired
	private ShipService shipService;

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private ConfigProperties configProperties;

	@GetMapping("")
	public String getStarbase(@RequestParam long starbaseId, Model model) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute("starbase", starbase);
		model.addAttribute("turnDone", turnDoneForStarbase(starbaseId) && !configProperties.isDisablePermissionChecks());
		return "ingame/starbase/starbase::content";
	}

	@GetMapping("/upgrade")
	public String getUpgrades(@RequestParam long starbaseId, Model model) {
		prepareUpgradeModel(model, starbaseId);
		return "ingame/starbase/upgrade::content";
	}

	private void prepareUpgradeModel(Model model, long starbaseId) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute("starbase", starbase);

		int money = starbase.getPlanet().getMoney();
		model.addAttribute("hullCosts", masterDataService.getLevelCosts(starbase.getHullLevel(), StarbaseUpgradeType.HULL, money));
		model.addAttribute("propulsionCosts", masterDataService.getLevelCosts(starbase.getPropulsionLevel(), StarbaseUpgradeType.PROPULSION, money));
		model.addAttribute("energyCosts", masterDataService.getLevelCosts(starbase.getEnergyLevel(), StarbaseUpgradeType.ENERGY, money));
		model.addAttribute("projectileCosts", masterDataService.getLevelCosts(starbase.getProjectileLevel(), StarbaseUpgradeType.PROJECTILE, money));
	}

	@PostMapping("/upgrade")
	@ResponseBody
	public void upgrade(@RequestBody StarbaseUpgradeRequest request, @RequestParam long starbaseId) {
		starbaseService.upgradeStarbase(starbaseId, request);
	}

	@GetMapping("/production-hull")
	public String getHullProduction(@RequestParam long starbaseId, Model model) {
		return getProduction(starbaseId, model, StarbaseUpgradeType.HULL);
	}

	@GetMapping("/production-propulsion")
	public String getPropulsionProduction(@RequestParam long starbaseId, Model model) {
		return getProduction(starbaseId, model, StarbaseUpgradeType.PROPULSION);
	}

	@GetMapping("/production-energyweapon")
	public String getEnergyProduction(@RequestParam long starbaseId, Model model) {
		return getProduction(starbaseId, model, StarbaseUpgradeType.ENERGY);
	}

	@GetMapping("/production-projectileweapon")
	public String getProjectileProduction(@RequestParam long starbaseId, Model model) {
		return getProduction(starbaseId, model, StarbaseUpgradeType.PROJECTILE);
	}

	private String getProduction(long starbaseId, Model model, StarbaseUpgradeType type) {
		List<StarbaseProductionEntry> data = starbaseService.getStarbaseProductionData(starbaseId, type);

		Collections.sort(data);
		model.addAttribute("data", data);
		model.addAttribute("type", type.name());
		model.addAttribute("typeEnum", type);
		model.addAttribute("starbase", starbaseService.getStarbase(starbaseId));
		return "ingame/starbase/production::content";
	}

	@PostMapping("/produce")
	@ResponseBody
	public void produce(@RequestBody StarbaseProductionRequest request, @RequestParam long starbaseId) {
		starbaseService.produce(request, starbaseId);
	}

	@GetMapping("/shipconstruction")
	public String getShipPConstruction(@RequestParam long starbaseId, Model model) {
		if (starbaseService.shipConstructionJobExists(starbaseId)) {
			return "ingame/starbase/shipconstruction::ship-job-exists";
		}

		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute("starbase", starbase);

		return "ingame/starbase/shipconstruction::hull-selection";
	}

	@GetMapping("/shipconstruction-details")
	public String getShipConstructionDetails(@RequestParam long starbaseId, @RequestParam long hullStockId, Model model) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute("starbase", starbase);

		Optional<StarbaseHullStock> stockResult = starbase.getHullStocks().stream().filter(stock -> stock.getId() == hullStockId).findFirst();

		if (stockResult.isPresent()) {
			StarbaseHullStock hullStock = stockResult.get();
			model.addAttribute("hullStock", hullStock);

			boolean shipCannotBeBuild = false;

			ShipTemplate shipTemplate = hullStock.getShipTemplate();

			int propulsionSystemsCount = shipTemplate.getPropulsionSystemsCount();
			List<StarbasePropulsionStock> propulsionSystems = starbase.getPropulsionStocks().stream().filter(s -> s.getStock() >= propulsionSystemsCount)
					.collect(Collectors.toList());
			model.addAttribute("propulsionSystems", propulsionSystems);

			if (propulsionSystems.isEmpty()) {
				shipCannotBeBuild = true;
			}

			int energyWeaponsCount = shipTemplate.getEnergyWeaponsCount();

			if (energyWeaponsCount > 0) {
				List<StarbaseWeaponStock> energyWeapons = starbase.getWeaponStocks().stream()
						.filter(s -> !s.getWeaponTemplate().isUsesProjectiles() && s.getStock() >= energyWeaponsCount).collect(Collectors.toList());
				model.addAttribute("energyWeapons", energyWeapons);

				if (energyWeapons.isEmpty()) {
					shipCannotBeBuild = true;
				}
			}

			int projectileWeaponsCount = shipTemplate.getProjectileWeaponsCount();

			if (projectileWeaponsCount > 0) {
				List<StarbaseWeaponStock> projectileWeapons = starbase.getWeaponStocks().stream()
						.filter(s -> s.getWeaponTemplate().isUsesProjectiles() && s.getStock() >= projectileWeaponsCount).collect(Collectors.toList());
				model.addAttribute("projectileWeapons", projectileWeapons);

				if (projectileWeapons.isEmpty()) {
					shipCannotBeBuild = true;
				}
			}

			model.addAttribute("shipCannotBeBuild", shipCannotBeBuild);

			return "ingame/starbase/shipconstruction::content";
		}

		throw new IllegalArgumentException("hullStockId " + hullStockId + " not found!");
	}

	@PostMapping("/ship-module-selection")
	public String getShipModuleSelection(@RequestParam long starbaseId, @RequestBody StarbaseShipConstructionRequest request, Model model) {
		model.addAttribute("request", request);
		model.addAttribute("modules", ShipModule.values());
		return "ingame/starbase/shipconstruction::module-selection";
	}

	@PostMapping("/shipconstruction")
	public String addShipConstructionJob(@RequestParam long starbaseId, @RequestBody StarbaseShipConstructionRequest request) {
		starbaseService.addShipConstructionJob(starbaseId, request);
		return "ingame/starbase/shipconstruction::success";
	}

	@GetMapping("/defense")
	public String getDefense(@RequestParam long starbaseId, Model model) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute("starbase", starbase);
		return "ingame/starbase/defense::content";
	}

	@PostMapping("/defense")
	@ResponseBody
	public void buildDefense(@RequestParam long starbaseId, @RequestParam int quantity) {
		starbaseService.buildDefense(starbaseId, quantity);
	}

	@GetMapping("/logbook")
	public String getLogbook(@RequestParam long starbaseId, Model model) {
		model.addAttribute("logbook", starbaseService.getLogbook(starbaseId));
		return "ingame/logbook::content";
	}

	@PostMapping("/logbook")
	public String changeLogbook(@RequestParam long starbaseId, @RequestParam String logbook) {
		starbaseService.changeLogbook(starbaseId, logbook);
		return "ingame/logbook::success";
	}

	@GetMapping("/space-folds")
	public String getSpaceFolds(@RequestParam long starbaseId, Model model) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);

		if (starbase.getHullLevel() < 7 || starbase.getPropulsionLevel() < 8) {
			return "ingame/starbase/space-folds::not-available";
		}

		model.addAttribute("starbase", starbase);
		Planet planet = starbase.getPlanet();

		long loginId = userDetailService.getLoginId();
		Player player = planet.getPlayer();
		long gameId = player.getGame().getId();

		Set<CoordinateImpl> visibilityCoordinates = ingameService.getVisibilityCoordinates(gameId, loginId);

		List<PlanetEntry> planets = ingameService.getPlanets(gameId, loginId, visibilityCoordinates, false);
		planets = planets.stream().filter(p -> p.getId() != planet.getId()).sorted(Comparator.comparing(PlanetEntry::getName)).collect(Collectors.toList());

		List<Ship> ships = shipService.getShipsOfLogin(gameId, loginId, null, null);
		ships = ships.stream().filter(s -> s.getX() != planet.getX() || s.getY() != planet.getY()).collect(Collectors.toList());

		Map<Long, Integer> shipTravelDurations = getSpaceFoldTravelDurationData(ships, Ship::getId, planet);
		Map<Long, Integer> planetTravelDurations = getSpaceFoldTravelDurationData(planets, PlanetEntry::getId, planet);

		model.addAttribute("playerId", player.getId());
		model.addAttribute("planets", planets);
		model.addAttribute("ships", ships);
		model.addAttribute("shipTravelDurations", shipTravelDurations);
		model.addAttribute("planetTravelDurations", planetTravelDurations);

		return "ingame/starbase/space-folds::content";
	}

	private <T extends Coordinate> Map<Long, Integer> getSpaceFoldTravelDurationData(List<T> coordinates, Function<T, Long> idProvider,
			Coordinate starbaseLocation) {
		return coordinates.stream().collect(Collectors.toMap(p -> idProvider.apply(p), p -> {
			double distance = CoordHelper.getDistance(p, starbaseLocation);
			return (int)Math.ceil(masterDataService.calculateTravelDuration(configProperties.getSpaceFoldTravelSpeed(), distance, 1f));
		}));
	}

	@PostMapping("/space-folds")
	public String initSpaceFold(@RequestParam long starbaseId, @RequestBody SpaceFoldRequest request) {
		starbaseService.initSpaceFold(starbaseId, request);
		return "ingame/starbase/space-folds::success";
	}

	@GetMapping("/stats-details")
	public String getStatsDetails(@RequestParam long starbaseId, Model model) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute("starbase", starbase);
		return "ingame/starbase/starbase::stats-details";
	}
}