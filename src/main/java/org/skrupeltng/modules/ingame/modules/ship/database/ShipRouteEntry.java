package org.skrupeltng.modules.ingame.modules.ship.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.skrupeltng.modules.ingame.modules.planet.database.Planet;

@Entity
@Table(name = "ship_route_entry")
public class ShipRouteEntry implements Serializable {

	private static final long serialVersionUID = 8202306637651551521L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Ship ship;

	@ManyToOne(fetch = FetchType.LAZY)
	private Planet planet;

	@Column(name = "order_id")
	private int orderId;

	@Column(name = "wait_for_full_storage")
	private boolean waitForFullStorage;

	@Enumerated(EnumType.STRING)
	@Column(name = "money_action")
	private ShipRouteResourceAction moneyAction = ShipRouteResourceAction.IGNORE;

	@Enumerated(EnumType.STRING)
	@Column(name = "supply_action")
	private ShipRouteResourceAction supplyAction = ShipRouteResourceAction.IGNORE;

	@Enumerated(EnumType.STRING)
	@Column(name = "fuel_action")
	private ShipRouteResourceAction fuelAction = ShipRouteResourceAction.IGNORE;

	@Enumerated(EnumType.STRING)
	@Column(name = "mineral1_action")
	private ShipRouteResourceAction mineral1Action = ShipRouteResourceAction.IGNORE;

	@Enumerated(EnumType.STRING)
	@Column(name = "mineral2_action")
	private ShipRouteResourceAction mineral2Action = ShipRouteResourceAction.IGNORE;

	@Enumerated(EnumType.STRING)
	@Column(name = "mineral3_action")
	private ShipRouteResourceAction mineral3Action = ShipRouteResourceAction.IGNORE;

	public ShipRouteEntry() {

	}

	public ShipRouteEntry(long id, Ship ship, Planet planet, int orderId) {
		this.id = id;
		this.ship = ship;
		this.planet = planet;
		this.orderId = orderId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Ship getShip() {
		return ship;
	}

	public void setShip(Ship ship) {
		this.ship = ship;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public boolean isWaitForFullStorage() {
		return waitForFullStorage;
	}

	public void setWaitForFullStorage(boolean waitForFullStorage) {
		this.waitForFullStorage = waitForFullStorage;
	}

	public ShipRouteResourceAction getMoneyAction() {
		return moneyAction;
	}

	public void setMoneyAction(ShipRouteResourceAction moneyAction) {
		this.moneyAction = moneyAction;
	}

	public ShipRouteResourceAction getSupplyAction() {
		return supplyAction;
	}

	public void setSupplyAction(ShipRouteResourceAction supplyAction) {
		this.supplyAction = supplyAction;
	}

	public ShipRouteResourceAction getFuelAction() {
		return fuelAction;
	}

	public void setFuelAction(ShipRouteResourceAction fuelAction) {
		this.fuelAction = fuelAction;
	}

	public ShipRouteResourceAction getMineral1Action() {
		return mineral1Action;
	}

	public void setMineral1Action(ShipRouteResourceAction mineral1Action) {
		this.mineral1Action = mineral1Action;
	}

	public ShipRouteResourceAction getMineral2Action() {
		return mineral2Action;
	}

	public void setMineral2Action(ShipRouteResourceAction mineral2Action) {
		this.mineral2Action = mineral2Action;
	}

	public ShipRouteResourceAction getMineral3Action() {
		return mineral3Action;
	}

	public void setMineral3Action(ShipRouteResourceAction mineral3Action) {
		this.mineral3Action = mineral3Action;
	}

	@Override
	public String toString() {
		return "ShipRouteEntry [id=" + id + ", planet=" + planet.getName() + ", orderId=" + orderId + "]";
	}
}