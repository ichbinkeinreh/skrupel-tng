package org.skrupeltng.modules.ingame.modules.politics.database;

import java.util.List;
import java.util.Set;

import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface PlayerRelationRepository extends JpaRepository<PlayerRelation, Long>, PlayerRelationRepositoryCustom {

	@Query("SELECT " +
			"	pr " +
			"FROM " +
			"	PlayerRelation pr " +
			"	INNER JOIN pr.player1 p1 " +
			"	INNER JOIN pr.player2 p2 " +
			"WHERE " +
			"	p1.game.id = ?2" +
			"	AND p1.id != ?1 " +
			"	AND p2.id != ?1")
	List<PlayerRelation> findOtherRelations(long currentPlayerId, long gameId);

	@Query("SELECT " +
			"	pr " +
			"FROM " +
			"	PlayerRelation pr " +
			"	INNER JOIN pr.player1 p1 " +
			"	INNER JOIN pr.player2 p2 " +
			"WHERE " +
			"	(p1.id = ?1 OR p2.id = ?1) " +
			"	AND (p1.id = ?2 OR p2.id = ?2)")
	List<PlayerRelation> findByPlayerIds(long playerId1, long playerId2);

	@Modifying
	@Query("DELETE FROM PlayerRelation r WHERE (r.player1.id = ?1 OR r.player2.id = ?1)")
	void deleteByPlayerId(long playerId);

	@Modifying
	@Query("DELETE FROM " +
			"	PlayerRelation r " +
			"WHERE " +
			"	r.roundsLeft = 1 " +
			"	AND r.player1.id IN (SELECT p.id FROM Player p WHERE p.game.id = ?1)")
	void deleteFinishedRelations(long gameId);

	@Modifying
	@Query("UPDATE " +
			"	PlayerRelation r " +
			"SET " +
			"	r.roundsLeft = r.roundsLeft -1 " +
			"WHERE " +
			"	r.roundsLeft > 0 " +
			"	AND r.player1.id IN (SELECT p.id FROM Player p WHERE p.game.id = ?1)")
	void countDownCancelledRelations(long gameId);

	@Query("SELECT r FROM PlayerRelation r WHERE r.player1.id = ?1 OR r.player2.id = ?1")
	List<PlayerRelation> findByPlayerId(long playerId);

	@Query("SELECT r FROM PlayerRelation r WHERE (r.player1.id = ?1 OR r.player2.id = ?1) AND r.type = ?2")
	List<PlayerRelation> findByPlayerIdAndType(long playerId, PlayerRelationType type);

	@Query("SELECT r FROM PlayerRelation r WHERE (r.player1.id = ?1 OR r.player2.id = ?1) AND r.type IN (?2)")
	List<PlayerRelation> findByPlayerIdAndTypes(long playerId, List<PlayerRelationType> types);

	@Query("SELECT " +
			"	r " +
			"FROM " +
			"	PlayerRelation r " +
			"	INNER JOIN r.player1 p " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND r.type = ?2")
	Set<PlayerRelation> findByGameIdAndType(long gameId, PlayerRelationType tradeAgreement);
}