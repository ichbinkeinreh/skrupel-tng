package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ingame.controller.MineFieldEntry;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

import com.google.common.collect.Lists;

public class MineFieldRepositoryImpl extends RepositoryCustomBase implements MineFieldRepositoryCustom {

	@Override
	public List<MineFieldEntry> getMineFieldEntries(long gameId, long currentPlayerId) {
		String sql = "" +
				"SELECT \n" +
				"	m.id, \n" +
				"	m.x, \n" +
				"	m.y, \n" +
				"	(p.id = :currentPlayerId) as \"own\", \n" +
				"	BOOL_OR(p.id = :currentPlayerId OR (pr.id IS NOT NULL AND pr.type IN (:relationTypes))) as \"friendly\" \n" +
				"FROM \n" +
				"	mine_field m \n" +
				"	INNER JOIN player p \n" +
				"		ON p.id = m.player_id \n" +
				"	LEFT OUTER JOIN player_relation pr \n" +
				"		ON (pr.player1_id = :currentPlayerId OR pr.player2_id = :currentPlayerId) AND (pr.player1_id = p.id OR pr.player2_id = p.id) \n" +
				"WHERE \n" +
				"	m.game_id = :gameId \n" +
				"GROUP BY \n" +
				"	m.id, \n" +
				"	m.x, \n" +
				"	m.y, \n" +
				"	p.id \n" +
				"ORDER BY \n" +
				"	m.id ASC";

		Map<String, Object> params = new HashMap<>(3);
		params.put("gameId", gameId);
		params.put("currentPlayerId", currentPlayerId);
		params.put("relationTypes", Lists.newArrayList(PlayerRelationType.ALLIANCE.name(), PlayerRelationType.NON_AGGRESSION_TREATY.name()));

		RowMapper<MineFieldEntry> rowMapper = new BeanPropertyRowMapper<>(MineFieldEntry.class);
		return jdbcTemplate.query(sql, params, rowMapper);
	}

	@Override
	public List<Long> getMineFieldIdsInRadius(long gameId, int x, int y, int distance) {
		String sql = "" +
				"SELECT \n" +
				"	m.id \n" +
				"FROM \n" +
				"	mine_field m \n" +
				"WHERE \n" +
				"	m.game_id = :gameId \n" +
				"	AND sqrt(((m.x - :x) * (m.x - :x)) + ((m.y - :y) * (m.y - :y))) <= :distance";

		Map<String, Object> params = new HashMap<>(4);
		params.put("gameId", gameId);
		params.put("x", x);
		params.put("y", y);
		params.put("distance", distance);

		return jdbcTemplate.queryForList(sql, params, Long.class);
	}
}