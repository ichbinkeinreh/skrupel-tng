package org.skrupeltng.modules.ingame.modules.fleet.service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.HashMultimap;

@Component
public class FleetFuel {

	@Autowired
	private FleetRepository fleetRepository;

	@Autowired
	private ShipService shipService;

	protected HashMultimap<Planet, Ship> createMap(long fleetId) {
		Fleet fleet = fleetRepository.getOne(fleetId);
		List<Ship> allShipsInFleet = fleet.getShips();

		HashMultimap<Planet, Ship> planetsToShipsMap = HashMultimap.create();

		for (Ship ship : allShipsInFleet) {
			if (ship.getPlanet() != null) {
				planetsToShipsMap.put(ship.getPlanet(), ship);
			}
		}

		return planetsToShipsMap;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void transportFuelToPlanets(long fleetId) {
		HashMultimap<Planet, Ship> planetsToShipsMap = createMap(fleetId);

		for (Planet planet : planetsToShipsMap.keySet()) {
			Set<Ship> ships = planetsToShipsMap.get(planet);

			for (Ship ship : ships) {
				ShipTransportRequest request = new ShipTransportRequest(ship);
				request.setFuel(0);
				shipService.transport(ship.getId(), request);
			}
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public List<Long> distributeFuel(long fleetId, float percentage) {
		HashMultimap<Planet, Ship> planetsToShipsMap = createMap(fleetId);

		for (Planet planet : planetsToShipsMap.keySet()) {
			if (planet.getFuel() == 0) {
				continue;
			}

			Set<Ship> ships = planetsToShipsMap.get(planet);
			float totalFuelCapacity = 0f;

			for (Ship ship : ships) {
				totalFuelCapacity += ship.getShipTemplate().getFuelCapacity();
			}

			float maxFillPercentage = planet.getFuel() / totalFuelCapacity;

			if (maxFillPercentage > 1f) {
				maxFillPercentage = 1f;
			}

			if (percentage > maxFillPercentage) {
				percentage = maxFillPercentage;
			}

			for (Ship ship : ships) {
				ShipTransportRequest request = new ShipTransportRequest(ship);

				int fuel = Math.round(ship.getShipTemplate().getFuelCapacity() * percentage);
				request.setFuel(fuel);

				shipService.transport(ship.getId(), request);
			}
		}

		return planetsToShipsMap.keySet().stream().map(Planet::getId).collect(Collectors.toList());
	}
}