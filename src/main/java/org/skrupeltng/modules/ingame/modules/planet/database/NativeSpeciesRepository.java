package org.skrupeltng.modules.ingame.modules.planet.database;

import org.springframework.data.jpa.repository.JpaRepository;

public interface NativeSpeciesRepository extends JpaRepository<NativeSpecies, Long> {

}