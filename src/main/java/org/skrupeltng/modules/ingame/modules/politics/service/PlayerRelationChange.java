package org.skrupeltng.modules.ingame.modules.politics.service;

import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequest;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PlayerRelationChange {

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private PlayerRelationRequestRepository playerRelationRequestRepository;

	@Autowired
	private PlayerRepository playerRepository;

	public void checkExistingRelation(long currentPlayerId, long otherPlayerId, PlayerRelationAction action, PlayerRelation relation, Game game) {
		PlayerRelationType type = relation.getType();

		switch (action) {
			case CANCEL_ALLIANCE:
				if (game.getWinCondition() == WinCondition.TEAM_DEATH_FOE) {
					throw new IllegalArgumentException("Cannot cancel alliance in Team Death Foe!");
				}

				if (type != PlayerRelationType.ALLIANCE) {
					throw new IllegalArgumentException("Cannot cancel non-existing alliance!");
				}

				relation.setRoundsLeft(12);
				playerRelationRepository.save(relation);
				break;
			case CANCEL_NON_AGGRESSION_TREATY:
				if (type != PlayerRelationType.NON_AGGRESSION_TREATY) {
					throw new IllegalArgumentException("Cannot cancel non-existing non-aggression treaty!");
				}

				relation.setRoundsLeft(6);
				playerRelationRepository.save(relation);
				break;
			case CANCEL_TRADE_AGREEMENT:
				if (type != PlayerRelationType.TRADE_AGREEMENT) {
					throw new IllegalArgumentException("Cannot cancel non-existing trade agreement!");
				}

				relation.setRoundsLeft(3);
				playerRelationRepository.save(relation);
				break;
			case DELCARE_WAR:
				if (type == PlayerRelationType.WAR) {
					throw new IllegalArgumentException("War already has been declared!");
				}

				relation.setType(PlayerRelationType.WAR);
				playerRelationRepository.save(relation);
				break;
			case OFFER_ALLIANCE:
				if (game.getWinCondition() == WinCondition.TEAM_DEATH_FOE) {
					throw new IllegalArgumentException("Cannot offer alliance in Team Death Foe!");
				}

				if (type != null) {
					throw new IllegalArgumentException("There already exists a relation!");
				}

				addRelationRequest(currentPlayerId, otherPlayerId, PlayerRelationType.ALLIANCE);
				break;
			case OFFER_NON_AGGRESSION_TREATY:
				if (type != null) {
					throw new IllegalArgumentException("There already exists a relation!");
				}

				addRelationRequest(currentPlayerId, otherPlayerId, PlayerRelationType.NON_AGGRESSION_TREATY);
				break;
			case OFFER_PEACE:
				if (type != PlayerRelationType.WAR) {
					throw new IllegalArgumentException("Can only request peace in war!");
				}

				addRelationRequest(currentPlayerId, otherPlayerId, null);
				break;
			case OFFER_TRADE_AGREEMENT:
				if (type != null) {
					throw new IllegalArgumentException("There already exists a relation!");
				}

				addRelationRequest(currentPlayerId, otherPlayerId, PlayerRelationType.TRADE_AGREEMENT);
				break;
		}
	}

	public void createNewRelation(long currentPlayerId, PlayerRelationAction action, long otherPlayerId, Game game) {
		switch (action) {
			case DELCARE_WAR:
				PlayerRelation relation = new PlayerRelation();
				relation.setPlayer1(playerRepository.getOne(currentPlayerId));
				relation.setPlayer2(playerRepository.getOne(otherPlayerId));
				relation.setType(PlayerRelationType.WAR);
				playerRelationRepository.save(relation);
				break;
			case OFFER_ALLIANCE:
				if (game.getWinCondition() == WinCondition.TEAM_DEATH_FOE) {
					throw new IllegalArgumentException("Cannot offer alliance in Team Death Foe!");
				}

				addRelationRequest(currentPlayerId, otherPlayerId, PlayerRelationType.ALLIANCE);
				break;
			case OFFER_NON_AGGRESSION_TREATY:
				addRelationRequest(currentPlayerId, otherPlayerId, PlayerRelationType.NON_AGGRESSION_TREATY);
				break;
			case OFFER_PEACE:
				throw new IllegalArgumentException("Can only offer peace in war!");
			case OFFER_TRADE_AGREEMENT:
				addRelationRequest(currentPlayerId, otherPlayerId, PlayerRelationType.TRADE_AGREEMENT);
				break;
			case CANCEL_ALLIANCE:
				throw new IllegalArgumentException("Cannot cancel non-existing alliance!");
			case CANCEL_NON_AGGRESSION_TREATY:
				throw new IllegalArgumentException("Cannot cancel non-existing non-aggression treaty!");
			case CANCEL_TRADE_AGREEMENT:
				throw new IllegalArgumentException("Cannot cancel non-existing trade agreement!");
		}
	}

	protected void addRelationRequest(long requestingPlayer, long otherPlayerId, PlayerRelationType type) {
		PlayerRelationRequest newRequest = new PlayerRelationRequest();
		newRequest.setRequestingPlayer(playerRepository.getOne(requestingPlayer));
		newRequest.setOtherPlayer(playerRepository.getOne(otherPlayerId));
		newRequest.setType(type);
		playerRelationRequestRepository.save(newRequest);
	}
}