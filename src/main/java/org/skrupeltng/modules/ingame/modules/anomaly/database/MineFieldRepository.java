package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MineFieldRepository extends JpaRepository<MineField, Long>, MineFieldRepositoryCustom {

	List<MineField> findByGameId(long gameId);
}