package org.skrupeltng.modules.ingame.modules.planet.database;

import java.util.List;

import org.skrupeltng.modules.ingame.controller.ColonyOverviewRequest;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.controller.PlanetListResultDTO;
import org.springframework.data.domain.Page;

public interface PlanetRepositoryCustom {

	List<PlanetEntry> getPlanetsForGalaxy(long gameId);

	boolean loginOwnsPlanet(long planetId, long loginId);

	boolean hasRevealingNativeSpeciesPlanets(long playerId);

	List<Long> getPlanetIdsInRadius(long gameId, int x, int y, int distance, boolean colonizedOnly);

	void resetScanRadius(long gameId);

	void updatePsyCorpsScanRadius(long gameId);

	void updateExtendedScanRadius(long gameId);

	List<Long> findOwnedPlanetIdsWithoutRoute(long playerId);

	long getRandomPlanetId(long gameId);

	Page<PlanetListResultDTO> searchColonies(ColonyOverviewRequest request, long loginId);
}