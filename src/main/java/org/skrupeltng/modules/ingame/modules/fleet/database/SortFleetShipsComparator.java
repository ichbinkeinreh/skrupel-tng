package org.skrupeltng.modules.ingame.modules.fleet.database;

import java.util.Comparator;

import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

public class SortFleetShipsComparator implements Comparator<Ship> {

	@Override
	public int compare(Ship a, Ship b) {
		if (a.getFleet().getLeader() == a) {
			return -1;
		}

		if (b.getFleet().getLeader() == b) {
			return 1;
		}

		return Long.compare(b.getId(), a.getId());
	}
}