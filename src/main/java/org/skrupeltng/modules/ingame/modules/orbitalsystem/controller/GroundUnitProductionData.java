package org.skrupeltng.modules.ingame.modules.orbitalsystem.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;

public class GroundUnitProductionData {

	private int colonistCosts;
	private int moneyCosts;
	private int supplyCosts;
	private int fuelCosts;
	private int mineral1Costs;
	private int mineral2Costs;
	private int mineral3Costs;

	private int inProduction;
	private int existing;

	private final Planet planet;
	private final boolean light;

	public GroundUnitProductionData(OrbitalSystemType type, Planet planet) {
		this.planet = planet;

		light = type == OrbitalSystemType.MILITARY_BASE;

		colonistCosts = light ? 1 : 3;
		moneyCosts = light ? 2 : 13;
		supplyCosts = light ? 1 : 0;
		fuelCosts = light ? 0 : 2;
		mineral1Costs = light ? 1 : 4;
		mineral2Costs = light ? 1 : 5;
		mineral3Costs = light ? 0 : 1;

		inProduction = light ? planet.getLightGroundUnitsProduction() : planet.getHeavyGroundUnitsProduction();
		existing = light ? planet.getLightGroundUnits() : planet.getHeavyGroundUnits();
	}

	public int getColonistCosts() {
		return colonistCosts;
	}

	public void setColonistCosts(int colonistCosts) {
		this.colonistCosts = colonistCosts;
	}

	public int getMoneyCosts() {
		return moneyCosts;
	}

	public void setMoneyCosts(int moneyCosts) {
		this.moneyCosts = moneyCosts;
	}

	public int getSupplyCosts() {
		return supplyCosts;
	}

	public void setSupplyCosts(int supplyCosts) {
		this.supplyCosts = supplyCosts;
	}

	public int getFuelCosts() {
		return fuelCosts;
	}

	public void setFuelCosts(int fuelCosts) {
		this.fuelCosts = fuelCosts;
	}

	public int getMineral1Costs() {
		return mineral1Costs;
	}

	public void setMineral1Costs(int mineral1Costs) {
		this.mineral1Costs = mineral1Costs;
	}

	public int getMineral2Costs() {
		return mineral2Costs;
	}

	public void setMineral2Costs(int mineral2Costs) {
		this.mineral2Costs = mineral2Costs;
	}

	public int getMineral3Costs() {
		return mineral3Costs;
	}

	public void setMineral3Costs(int mineral3Costs) {
		this.mineral3Costs = mineral3Costs;
	}

	public int getInProduction() {
		return inProduction;
	}

	public void setInProduction(int inProduction) {
		this.inProduction = inProduction;
	}

	public int getExisting() {
		return existing;
	}

	public void setExisting(int existing) {
		this.existing = existing;
	}

	public int retrieveMaxInProduction() {
		return light ? 100 : 50;
	}

	public int retrieveNewlyProducable() {
		List<Integer> costs = new ArrayList<>();

		if (!planet.hasOrbitalSystem(OrbitalSystemType.BATTLE_SQUARE)) {
			addCosts(costs, planet.getMoney(), moneyCosts);
			addCosts(costs, planet.getSupplies(), supplyCosts);
			addCosts(costs, planet.getFuel(), fuelCosts);
			addCosts(costs, planet.getMineral1(), mineral1Costs);
			addCosts(costs, planet.getMineral2(), mineral2Costs);
			addCosts(costs, planet.getMineral3(), mineral3Costs);
		}

		if (!planet.hasOrbitalSystem(OrbitalSystemType.CLONE_FACTORY)) {
			addCosts(costs, planet.getColonists() - 1000, colonistCosts);
		}

		addCosts(costs, retrieveMaxInProduction() - inProduction, 1);

		int max = light ? 3000 : 1500;
		addCosts(costs, max - existing, 1);

		Collections.sort(costs);
		return costs.get(0);
	}

	private void addCosts(List<Integer> costs, int planetResources, int unitCosts) {
		if (unitCosts > 0) {
			costs.add(planetResources / unitCosts);
		}
	}
}