package org.skrupeltng.modules.ingame.modules.overview.database;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;

@Entity
@Table(name = "news_entry")
public class NewsEntry implements Serializable {

	private static final long serialVersionUID = 6445263230441442975L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	@ManyToOne(fetch = FetchType.LAZY)
	private Game game;

	private String image;

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	private boolean delete;

	private String template;

	@Column(name = "click_target_id")
	private Long clickTargetId;

	@Enumerated(EnumType.STRING)
	@Column(name = "click_target_type")
	private NewsEntryClickTargetType clickTargetType;

	private String arguments;

	public NewsEntry() {
		delete = true;
		date = new Date();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public Long getClickTargetId() {
		return clickTargetId;
	}

	public void setClickTargetId(Long clickTargetId) {
		this.clickTargetId = clickTargetId;
	}

	public NewsEntryClickTargetType getClickTargetType() {
		return clickTargetType;
	}

	public void setClickTargetType(NewsEntryClickTargetType clickTargetType) {
		this.clickTargetType = clickTargetType;
	}

	public String getArguments() {
		return arguments;
	}

	public void setArguments(String arguments) {
		this.arguments = arguments;
	}
}