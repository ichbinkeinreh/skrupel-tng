package org.skrupeltng.modules.ingame.modules.overview;

public enum NewsEntryClickTargetType {

	ship, planet, starbase
}