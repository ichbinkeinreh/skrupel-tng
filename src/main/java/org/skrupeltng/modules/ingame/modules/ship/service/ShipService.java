package org.skrupeltng.modules.ingame.modules.ship.service;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.controller.ShipListResultDTO;
import org.skrupeltng.modules.ingame.controller.ShipOverviewRequest;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFoldRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.TotalGoodCalculator;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipCourseSelectionRequest;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipCourseSelectionResponse;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipOptionsChangeRequest;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTacticsChangeRequest;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTaskChangeRequest;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteResourceAction;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTactics;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplate;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplateRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.service.CoordHelper;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.database.Resource;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataConstants;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.util.Pair;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

@Service
public class ShipService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private WormHoleRepository wormHoleRepository;

	@Autowired
	private SpaceFoldRepository spaceFoldRepository;

	@Autowired
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Autowired
	private AquiredShipTemplateRepository aquiredShipTemplateRepository;

	@Autowired
	private FleetRepository fleetRepository;

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private FleetService fleetService;

	@Autowired
	private StatsUpdater statsUpdater;

	@Autowired
	private VisibleObjects visibleObjects;

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public Ship getShip(long shipId) {
		return shipRepository.getOne(shipId);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public Ship getShipWithRoutesFetched(long shipId) {
		Ship ship = getShip(shipId);

		List<ShipRouteEntry> route = ship.getRoute();

		if (route != null) {
			for (ShipRouteEntry entry : route) {
				entry.getPlanet();
			}

			Collections.sort(route, Comparator.comparing(ShipRouteEntry::getOrderId));
		}

		return ship;
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public List<Ship> getShipsOfLogin(long gameId, long loginId, Integer x, Integer y) {
		List<Ship> ships = null;

		if (x == null || y == null) {
			ships = shipRepository.findShipsByGameIdAndLoginId(gameId, loginId);
		} else {
			ships = shipRepository.findShipsByGameIdAndLoginIdAndCoordinates(gameId, loginId, x, y);
		}

		return ships;
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public Page<ShipListResultDTO> getShipsOfLogin(long gameId, ShipOverviewRequest request, long loginId) {
		return shipRepository.searchShips(request, loginId);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public String getDestinationName(long shipId) {
		Ship ship = shipRepository.getOne(shipId);

		Ship destinationShip = ship.getDestinationShip();

		if (destinationShip != null) {
			return destinationShip.getName();
		}

		int x = ship.getDestinationX();
		int y = ship.getDestinationY();
		long gameId = ship.getPlayer().getGame().getId();

		Optional<Planet> planetOpt = planetRepository.findByGameIdAndXAndY(gameId, x, y);

		if (planetOpt.isPresent()) {
			return planetOpt.get().getName();
		}

		return null;
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public ShipCourseSelectionResponse setCourse(long shipId, ShipCourseSelectionRequest request) {
		Ship ship = shipRepository.getOne(shipId);
		Player player = ship.getPlayer();
		Game game = player.getGame();

		int x = request.getX();
		int y = request.getY();
		int speed = request.getSpeed();
		String name = request.getTargetName();

		if (x < 0 || y < 0 || x > game.getGalaxySize() || y > game.getGalaxySize() || speed < 0 || speed > 9 ||
				(ship.getPropulsionSystemTemplate().getTechLevel() == 1 && speed > 1)) {
			throw new IllegalArgumentException("Invalid ship course!");
		}

		ShipCourseSelectionResponse response = new ShipCourseSelectionResponse();

		ship.setDestinationX(x);
		ship.setDestinationY(y);
		ship.setTravelSpeed(speed);

		if (ship.getRoute().size() >= 2) {
			ship.setRouteDisabled(true);
		}

		List<Ship> destinationShips = shipRepository.findByGameIdAndXAndYAndName(game.getId(), x, y, name);

		if (!destinationShips.isEmpty()) {
			ship.setDestinationShip(destinationShips.get(0));
		} else {
			ship.setDestinationShip(null);
		}

		ship = shipRepository.save(ship);

		Fleet fleet = ship.getFleet();

		if (fleet != null && fleet.getLeader() != null && fleet.getLeader().getId() == ship.getId()) {
			fleetService.setLeader(fleet.getId(), shipId);
		}

		return response;
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void transport(long shipId, ShipTransportRequest request) {
		Ship ship = getShip(shipId);
		Planet planet = getNearestPlanetInExtendedTransporterRange(ship);
		transportWithoutPermissionCheck(request, ship, planet);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Pair<Ship, Planet> transportWithoutPermissionCheck(ShipTransportRequest request, Ship ship, Planet planet) {
		long shipId = ship.getId();

		if (planet == null) {
			throw new IllegalArgumentException("Ship " + shipId + " is not in a planet's orbit!");
		}

		int fuel = request.getFuel();
		int mineral1 = request.getMineral1();
		int mineral2 = request.getMineral2();
		int mineral3 = request.getMineral3();
		int money = request.getMoney();
		int supplies = request.getSupplies();
		int colonists = request.getColonists();
		int lightGroundUnits = request.getLightGroundUnits();
		int heavyGroundUnits = request.getHeavyGroundUnits();

		boolean infantryTransport = ship.getAbility(ShipAbilityType.INFANTRY_TRANSPORT).isPresent();
		if (infantryTransport && (mineral1 > 0 || mineral2 > 0 || mineral3 > 0 || money > 0 || colonists > 0 || heavyGroundUnits > 0)) {
			throw new IllegalArgumentException("Infantry transport can only carry fuel, supplies and light ground units!");
		}

		int fuelCapacity = ship.getShipTemplate().getFuelCapacity();
		int storageSpace = ship.retrieveStorageSpace();

		// Reset everything to the planet first so later checks are easier
		planet.setFuel(planet.getFuel() + ship.getFuel());
		planet.setMineral1(planet.getMineral1() + ship.getMineral1());
		planet.setMineral2(planet.getMineral2() + ship.getMineral2());
		planet.setMineral3(planet.getMineral3() + ship.getMineral3());
		planet.setMoney(planet.getMoney() + ship.getMoney());
		planet.setSupplies(planet.getSupplies() + ship.getSupplies());

		int requestSum = TotalGoodCalculator.retrieveSum(request);

		if (fuel < 0 || fuelCapacity < fuel || requestSum < 0 || storageSpace < requestSum) {
			throwError(request, shipId);
		}
		if (mineral1 < 0) {
			throwError(request, shipId);
		}
		if (mineral2 < 0) {
			throwError(request, shipId);
		}
		if (mineral3 < 0) {
			throwError(request, shipId);
		}
		if (money < 0) {
			throwError(request, shipId);
		}
		if (supplies < 0) {
			throwError(request, shipId);
		}
		if (colonists < 0) {
			throwError(request, shipId);
		}
		if (lightGroundUnits < 0) {
			throwError(request, shipId);
		}
		if (heavyGroundUnits < 0) {
			throwError(request, shipId);
		}

		long playerId = ship.getPlayer().getId();
		if (fuel > planet.retrieveTransportableFuel(playerId)) {
			fuel = planet.retrieveTransportableFuel(playerId);
		}
		if (money > planet.retrieveTransportableMoney(playerId)) {
			money = planet.retrieveTransportableMoney(playerId);
		}
		if (mineral1 > planet.retrieveTransportableMineral1(playerId)) {
			mineral1 = planet.retrieveTransportableMineral1(playerId);
		}
		if (mineral2 > planet.retrieveTransportableMineral2(playerId)) {
			mineral2 = planet.retrieveTransportableMineral2(playerId);
		}
		if (mineral3 > planet.retrieveTransportableMineral3(playerId)) {
			mineral3 = planet.retrieveTransportableMineral3(playerId);
		}
		if (supplies > planet.retrieveTransportableSupplies(playerId)) {
			supplies = planet.retrieveTransportableSupplies(playerId);
		}

		ship.setFuel(fuel);
		planet.setFuel(planet.getFuel() - fuel);

		ship.setMineral1(mineral1);
		planet.setMineral1(planet.getMineral1() - mineral1);

		ship.setMineral2(mineral2);
		planet.setMineral2(planet.getMineral2() - mineral2);

		ship.setMineral3(mineral3);
		planet.setMineral3(planet.getMineral3() - mineral3);

		ship.setMoney(money);
		planet.setMoney(planet.getMoney() - money);

		ship.setSupplies(supplies);
		planet.setSupplies(planet.getSupplies() - supplies);

		if (planet.getPlayer() != null && planet.getPlayer() == ship.getPlayer()) {
			planet.setColonists(planet.getColonists() + ship.getColonists() - colonists);
			ship.setColonists(colonists);

			planet.setLightGroundUnits(planet.getLightGroundUnits() + ship.getLightGroundUnits() - lightGroundUnits);
			ship.setLightGroundUnits(lightGroundUnits);

			planet.setHeavyGroundUnits(planet.getHeavyGroundUnits() + ship.getHeavyGroundUnits() - heavyGroundUnits);
			ship.setHeavyGroundUnits(heavyGroundUnits);
		} else {
			planet.setNewPlayer(ship.getPlayer());

			planet.setNewColonists(planet.getNewColonists() + ship.getColonists() - colonists);
			ship.setColonists(colonists);

			planet.setNewLightGroundUnits(planet.getNewLightGroundUnits() + ship.getLightGroundUnits() - lightGroundUnits);
			ship.setLightGroundUnits(lightGroundUnits);

			planet.setNewHeavyGroundUnits(planet.getNewHeavyGroundUnits() + ship.getHeavyGroundUnits() - heavyGroundUnits);
			ship.setHeavyGroundUnits(heavyGroundUnits);
		}

		// It is only necessary to do these checks for colonists and ground units because the other resources are checked with the retrieveTransportable... methods
		if (ship.getColonists() < 0 || planet.getColonists() < 0 || ship.getLightGroundUnits() < 0 || planet.getLightGroundUnits() < 0 ||
				ship.getHeavyGroundUnits() < 0 || planet.getHeavyGroundUnits() < 0 || planet.getNewColonists() < 0 || planet.getNewHeavyGroundUnits() < 0 ||
				planet.getNewLightGroundUnits() < 0 || TotalGoodCalculator.retrieveSum(ship) > storageSpace) {
			throw new IllegalArgumentException("Invalid transport request!");
		}

		ship = shipRepository.save(ship);
		planet = planetRepository.save(planet);

		return Pair.of(ship, planet);
	}

	private void throwError(ShipTransportRequest request, long shipId) {
		throw new IllegalArgumentException("Invalid change ship storage request for ship " + shipId + ": " + request);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteCourse(long shipId) {
		Ship ship = shipRepository.getOne(shipId);
		ship.resetTravel();
		shipRepository.save(ship);

		visibleObjects.clearCaches();
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public List<Ship> getScannedShips(long shipId) {
		Ship ship = shipRepository.getOne(shipId);
		Player player = ship.getPlayer();
		long playerId = player.getId();
		long gameId = player.getGame().getId();
		int scannerDist = ship.getScanRadius();

		List<Ship> ships = shipRepository.findByGameId(gameId);
		return ships.stream().filter(s -> s.getPlayer().getId() != playerId && CoordHelper.getDistance(s, ship) <= scannerDist).collect(Collectors.toList());
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public List<Planet> getScannedPlanets(long shipId) {
		Ship ship = shipRepository.getOne(shipId);
		Player player = ship.getPlayer();
		long playerId = player.getId();
		long gameId = player.getGame().getId();
		int scannerDist = ship.getScanRadius();

		List<Planet> planets = planetRepository.findByGameId(gameId);
		return planets.stream().filter(p -> (p.getPlayer() == null || p.getPlayer().getId() != playerId) && CoordHelper.getDistance(p, ship) <= scannerDist)
				.collect(Collectors.toList());
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public Ship getScannedShip(long shipId, long scannedShipId) {
		Ship ship = shipRepository.getOne(scannedShipId);

		if (getScannedShips(shipId).contains(ship)) {
			return ship;
		}

		throw new IllegalArgumentException("Ship " + scannedShipId + " is not in scanner reach of ship " + shipId + "!");
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public Planet getScannedPlanet(long shipId, long scannedPlanetId) {
		Planet planet = planetRepository.getOne(scannedPlanetId);

		if (getScannedPlanets(shipId).stream().anyMatch(p -> p.getId() == planet.getId())) {
			return planet;
		}

		throw new IllegalArgumentException("Planet " + scannedPlanetId + " is not in scanner reach of ship " + shipId + "!");
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public String getLogbook(long shipId) {
		Ship ship = shipRepository.getOne(shipId);
		return ship.getLog();
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changeLogbook(long shipId, String logbook) {
		Ship ship = shipRepository.getOne(shipId);
		ship.setLog(logbook);
		shipRepository.save(ship);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Ship changeTask(long shipId, ShipTaskChangeRequest request) {
		Ship ship = shipRepository.getOne(shipId);

		String taskTypeString = request.getTaskType();
		Long escortTargetId = request.getEscortTargetId();

		ShipTaskType taskType = ShipTaskType.valueOf(taskTypeString);
		ShipTemplate shipTemplate = ship.getShipTemplate();

		switch (taskType) {
			case AUTO_DESTRUCTION:
				if (ship.getShipModule() != ShipModule.AUTO_DESTRUCTION) {
					throw new IllegalArgumentException("Missing ship module!");
				}
				break;
			case CAPTURE_SHIP:
				if (!ship.hasWeapons()) {
					throw new IllegalArgumentException("Ship has no weapons!");
				}
				break;
			case CLEAR_MINE_FIELD:
				if (shipTemplate.getHangarCapacity() == 0) {
					throw new IllegalArgumentException("Ship has no hangars!");
				}
				break;
			case CREATE_MINE_FIELD:
				if (ship.getPropulsionSystemTemplate() == null) {
					throw new IllegalArgumentException("Ship has no projectile weapons!");
				}

				if (ship.getProjectiles() < Integer.valueOf(request.getTaskValue())) {
					throw new IllegalArgumentException("Not enough projectiles!");
				}
				break;
			case HIRE_CREW:
				int crewToBeHired = Integer.valueOf(request.getTaskValue());

				if (shipTemplate.getCrew() - ship.getCrew() < crewToBeHired) {
					throw new IllegalArgumentException("Crew hire mismatch!");
				}

				if (crewToBeHired > ship.getPlanet().getColonists()) {
					throw new IllegalArgumentException("Not enough colonists to hire crew!");
				}
				break;
			case HUNTER_TRAINING:
				Planet planet = ship.getPlanet();

				if (planet == null || !planet.hasOrbitalSystem(OrbitalSystemType.HUNTER_ACADEMY) || shipTemplate.getMass() >= 100) {
					throw new IllegalArgumentException("Cannot train hunter!");
				}

				Starbase starbase = planet.getStarbase();

				if (starbase == null || starbase.getType() != StarbaseType.BATTLE_STATION || ship.getExperience() == 5 || ship.getTravelSpeed() != 0) {
					throw new IllegalArgumentException("Cannot train hunter!");
				}
				break;
			case PLANET_BOMBARDMENT:
				if (ship.getPropulsionSystemTemplate() == null) {
					throw new IllegalArgumentException("Ship has no projectile weapons!");
				}
				break;
			case REPAIR:
				if (ship.getDamage() == 0) {
					throw new IllegalArgumentException("Ship is not damaged!");
				}
				break;
			case TRACTOR_BEAM:
				if (!masterDataService.hasTractorBeam(ship.getPropulsionSystemTemplate().getTechLevel())) {
					throw new IllegalArgumentException("Ship has no tractor beam!");
				}

				long tractorShipId = Long.valueOf(request.getTaskValue());
				Ship towee = shipRepository.getOne(tractorShipId);

				if (!ship.canTowShip(towee)) {
					throw new IllegalArgumentException("This ship cannot tow that!");
				}

				break;
			default:
				break;
		}

		if (ship.getDestinationShip() != null && escortTargetId == null) {
			ship.resetTravel();
		}

		ship.setActiveAbility(null);
		ship.setTaskType(taskType);
		ship.setTaskValue(request.getTaskValue());

		if (taskType == ShipTaskType.ACTIVE_ABILITY) {
			String activeAbilityTypeString = request.getActiveAbilityType();

			ShipAbilityType abilityType = ShipAbilityType.valueOf(activeAbilityTypeString);
			Optional<ShipAbility> abilityOpt = ship.getAbility(abilityType);

			if (abilityOpt.isPresent()) {
				ShipAbility activeAbility = abilityOpt.get();
				ship.setActiveAbility(activeAbility);
			} else {
				throw new IllegalArgumentException("Ship " + shipId + " does not have ShipAbility " + abilityType + "!");
			}
		}

		int escortTargetSpeed = request.getEscortTargetSpeed();

		if (escortTargetId != null && escortTargetSpeed > 0 && escortTargetSpeed <= 10) {
			Optional<Ship> targetOpt = shipRepository.findById(escortTargetId);

			if (targetOpt.isPresent()) {
				Ship target = targetOpt.get();
				ship.setDestinationX(target.getX());
				ship.setDestinationY(target.getY());
				ship.setDestinationShip(target);
				ship.setTravelSpeed(escortTargetSpeed);
			} else {
				throw new IllegalArgumentException("Escort target ship with id " + escortTargetId + " not found!");
			}
		}

		return shipRepository.save(ship);
	}

	public List<String> getPlayerColors(long gameId) {
		return gameRepository.getPlayerColors(gameId);
	}

	public Planet getNearestPlanetInExtendedTransporterRange(Ship ship) {
		Planet planet = ship.getPlanet();

		if (planet == null && ship.getAbility(ShipAbilityType.EXTENDED_TRANSPORTER).isPresent()) {
			long gameId = ship.getPlayer().getGame().getId();
			int x = ship.getX();
			int y = ship.getY();
			int distance = ship.getAbilityValue(ShipAbilityType.EXTENDED_TRANSPORTER, ShipAbilityConstants.EXTENDED_TRANSPORTER_RANGE).get().intValue();
			List<Long> planetIds = planetRepository.getPlanetIdsInRadius(gameId, x, y, distance, false);

			if (!planetIds.isEmpty()) {
				List<Planet> planets = planetRepository.findByIds(planetIds);
				return planets.stream().min(new CoordinateDistanceComparator(ship)).get();
			}
		}

		return planet;
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changeTactics(long shipId, ShipTacticsChangeRequest request) {
		if (request.getAggressiveness() < 0 || request.getAggressiveness() > 100) {
			throw new IllegalArgumentException("Invalid aggressiveness value!");
		}

		Ship ship = shipRepository.getOne(shipId);
		ship.setAggressiveness(request.getAggressiveness());

		ShipTactics tactics = ShipTactics.valueOf(request.getTactics());
		ship.setTactics(tactics);

		shipRepository.save(ship);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteShip(Ship ship, Player attackingPlayer) {
		deleteShip(ship, attackingPlayer, true);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteShip(Ship ship, Player attackingPlayer, boolean shipLost) {
		log.debug("Deleting ship " + ship + "; attacked by " + attackingPlayer + "; was lost: " + shipLost);

		if (shipLost) {
			statsUpdater.incrementStats(ship.getPlayer(), LoginStatsFaction::getShipsLost, LoginStatsFaction::setShipsLost);
		}

		long shipId = ship.getId();
		shipRepository.clearDestinationShip(Lists.newArrayList(shipId));
		wormHoleRepository.deleteByShipId(shipId);
		spaceFoldRepository.deleteByShipId(shipId);
		shipRepository.clearCurrentRouteEntry(shipId);
		shipRouteEntryRepository.deleteByShipId(shipId);
		fleetRepository.clearLeaderShipFromFleets(shipId);
		shipRepository.delete(ship);

		if (attackingPlayer != null) {
			statsUpdater.incrementStats(attackingPlayer, LoginStatsFaction::getShipsDestroyed, LoginStatsFaction::setShipsDestroyed,
					AchievementType.SHIPS_DESTROYED_1, AchievementType.SHIPS_DESTROYED_10);
		}

		log.debug("Ship " + ship + " deleted.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteShips(Collection<Ship> ships) {
		if (!ships.isEmpty()) {
			List<Long> shipIds = ships.stream().map(Ship::getId).collect(Collectors.toList());
			shipRepository.clearDestinationShip(shipIds);
			wormHoleRepository.deleteByShipIds(shipIds);
			spaceFoldRepository.deleteByShipIds(shipIds);
			shipRepository.clearCurrentRouteEntry(shipIds);
			shipRouteEntryRepository.deleteByShipIds(shipIds);
			fleetRepository.clearLeaderShipsFromFleets(shipIds);
			shipRepository.deleteInBatch(ships);
		}
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void buildProjectiles(long shipId, int quantity) {
		buildProjectilesWithoutPermissionCheck(shipId, quantity);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void buildProjectilesWithoutPermissionCheck(long shipId, int quantity) {
		if (quantity <= 0) {
			throw new IllegalArgumentException("Quantity must be higher than zero!");
		}

		Ship ship = shipRepository.getOne(shipId);
		ship.setProjectiles(ship.getProjectiles() + quantity);
		ship.setMoney(ship.getMoney() - (35 * quantity));
		ship.setMineral1(ship.getMineral1() - (2 * quantity));
		ship.setMineral2(ship.getMineral2() - quantity);
		shipRepository.save(ship);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	public int getBuildableProjectiles(long shipId) {
		Ship ship = shipRepository.getOne(shipId);

		int currentProjectiles = ship.getProjectiles();
		int maxProjectiles = ship.getShipTemplate().getProjectileWeaponsCount() * MasterDataConstants.PROJECTILES_PER_WEAPON;

		if (currentProjectiles == maxProjectiles) {
			return 0;
		}

		Planet planet = ship.getPlanet();

		if (planet == null) {
			return 0;
		}

		long playerId = ship.getPlayer().getId();

		int totalMoney = planet.retrieveTransportableMoney(playerId) + ship.getMoney();
		int totalMineral1 = planet.retrieveTransportableMineral1(playerId) + ship.getMineral1();
		int totalMineral2 = planet.retrieveTransportableMineral2(playerId) + ship.getMineral2();

		int newProjectiles = maxProjectiles - currentProjectiles;
		newProjectiles = Math.min(newProjectiles, Math.min(totalMoney / 35, Math.min(totalMineral1 / 2, totalMineral2)));
		return newProjectiles;
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Planet buildAllPossibleProjectilesAutomatically(long shipId) {
		Ship ship = shipRepository.getOne(shipId);
		Planet planet = ship.getPlanet();

		if (planet == null) {
			return null;
		}

		int newProjectiles = getBuildableProjectiles(shipId);
		ship.addProjectiles(newProjectiles);

		int moneyCosts = newProjectiles * 35;
		int mineral1Costs = newProjectiles * 2;
		int mineral2Costs = newProjectiles;

		if (ship.getMoney() >= moneyCosts) {
			ship.setMoney(ship.getMoney() - moneyCosts);
		} else {
			moneyCosts -= ship.getMoney();
			ship.setMoney(0);
			planet.spendMoney(moneyCosts);
		}

		if (ship.getMineral1() >= mineral1Costs) {
			ship.setMineral1(ship.getMineral1() - mineral1Costs);
		} else {
			mineral1Costs -= ship.getMineral1();
			ship.setMineral1(0);
			planet.spendMineral1(mineral1Costs);
		}

		if (ship.getMineral2() >= mineral2Costs) {
			ship.setMineral2(ship.getMineral2() - mineral2Costs);
		} else {
			mineral2Costs -= ship.getMineral2();
			ship.setMineral2(0);
			planet.spendMineral2(mineral2Costs);
		}

		shipRepository.save(ship);
		return planetRepository.save(planet);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void toggleAutoBuildProjectiles(long shipId) {
		Ship ship = shipRepository.getOne(shipId);
		ship.setAutoBuildProjectiles(!ship.isAutoBuildProjectiles());
		shipRepository.save(ship);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	public List<ShipRouteEntry> getRoute(long shipId) {
		return shipRouteEntryRepository.findByShipId(shipId);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addRouteEntry(long shipId, long planetId, ShipRouteResourceAction action) {
		Ship ship = shipRepository.getOne(shipId);
		Planet planet = planetRepository.getOne(planetId);

		if (planet.getPlayer() != ship.getPlayer()) {
			throw new IllegalArgumentException("Planet " + planetId + " does not belong to player of ship " + shipId + "!");
		}

		ShipRouteEntry routeEntry = new ShipRouteEntry();
		routeEntry.setShip(ship);
		routeEntry.setPlanet(planet);
		routeEntry.setOrderId(ship.getRoute().size());
		routeEntry.setFuelAction(action);
		routeEntry.setMoneyAction(action);
		routeEntry.setSupplyAction(action);
		routeEntry.setMineral1Action(action);
		routeEntry.setMineral2Action(action);
		routeEntry.setMineral3Action(action);

		routeEntry = shipRouteEntryRepository.save(routeEntry);

		ship = routeEntry.getShip();

		if (ship.getCurrentRouteEntry() == null) {
			List<ShipRouteEntry> routeEntries = shipRouteEntryRepository.findByShipId(shipId);

			if (routeEntries.size() > 1) {
				ship.setRouteTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
				ship.setRoutePrimaryResource(Resource.SUPPLIES);
				ship.updateRoute(routeEntries.get(0));
				shipRepository.save(ship);
			}
		}

		visibleObjects.clearCaches();
	}

	@PreAuthorize("hasPermission(#entryId, 'route') and hasPermission(#entryId, 'turnNotDoneRoute')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changeRouteEntryPlanet(long entryId, long planetId) {
		ShipRouteEntry entry = shipRouteEntryRepository.getOne(entryId);

		Planet planet = planetRepository.getOne(planetId);

		if (entry.getShip().getPlayer() != planet.getPlayer()) {
			throw new IllegalArgumentException("Planet " + planetId + " does not belong to player of ship " + entry.getShip().getId() + "!");
		}

		entry.setPlanet(planet);

		entry = shipRouteEntryRepository.save(entry);

		Ship ship = entry.getShip();

		if (ship.getCurrentRouteEntry() == entry && !ship.isRouteDisabled()) {
			ship.setDestinationX(planet.getX());
			ship.setDestinationY(planet.getY());
			ship.setTravelSpeed(ship.getRouteTravelSpeed());
			shipRepository.save(ship);
		}
	}

	@PreAuthorize("hasPermission(#entryId, 'route') and hasPermission(#entryId, 'turnNotDoneRoute')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changeRouteEntryAction(long entryId, Resource resource, ShipRouteResourceAction action) {
		ShipRouteEntry entry = shipRouteEntryRepository.getOne(entryId);

		switch (resource) {
			case FUEL:
				entry.setFuelAction(action);
				break;
			case MINERAL1:
				entry.setMineral1Action(action);
				break;
			case MINERAL2:
				entry.setMineral2Action(action);
				break;
			case MINERAL3:
				entry.setMineral3Action(action);
				break;
			case SUPPLIES:
				entry.setSupplyAction(action);
				break;
			case MONEY:
				entry.setMoneyAction(action);
				break;
		}

		shipRouteEntryRepository.save(entry);
	}

	@PreAuthorize("hasPermission(#entryId, 'route') and hasPermission(#entryId, 'turnNotDoneRoute')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void toggleWaitForFullStorage(long entryId) {
		ShipRouteEntry entry = shipRouteEntryRepository.getOne(entryId);
		entry.setWaitForFullStorage(!entry.isWaitForFullStorage());
		shipRouteEntryRepository.save(entry);
	}

	@PreAuthorize("hasPermission(#entryId, 'route') and hasPermission(#entryId, 'turnNotDoneRoute')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long moveRouteEntryUp(long entryId) {
		ShipRouteEntry entry = shipRouteEntryRepository.getOne(entryId);
		Ship ship = entry.getShip();
		List<ShipRouteEntry> routeEntries = shipRouteEntryRepository.findByShipId(ship.getId());

		int index = routeEntries.indexOf(entry);

		if (index > 0) {
			swapRouteEntries(index, index - 1, routeEntries);
		}

		return ship.getId();
	}

	@PreAuthorize("hasPermission(#entryId, 'route') and hasPermission(#entryId, 'turnNotDoneRoute')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long moveRouteEntryDown(long entryId) {
		ShipRouteEntry entry = shipRouteEntryRepository.getOne(entryId);
		Ship ship = entry.getShip();
		List<ShipRouteEntry> routeEntries = shipRouteEntryRepository.findByShipId(ship.getId());

		int index = routeEntries.indexOf(entry);

		if (index < routeEntries.size() - 1) {
			swapRouteEntries(index, index + 1, routeEntries);
		}

		return ship.getId();
	}

	private void swapRouteEntries(int index1, int index2, List<ShipRouteEntry> routeEntries) {
		ShipRouteEntry a = routeEntries.get(index1);
		ShipRouteEntry b = routeEntries.get(index2);
		routeEntries.set(index1, b);
		routeEntries.set(index2, a);

		for (int i = 0; i < routeEntries.size(); i++) {
			ShipRouteEntry entry = routeEntries.get(i);
			entry.setOrderId(i);
			shipRouteEntryRepository.save(entry);
		}

		visibleObjects.clearCaches();
	}

	@PreAuthorize("hasPermission(#entryId, 'route') and hasPermission(#entryId, 'turnNotDoneRoute')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long deleteRouteEntry(long entryId) {
		ShipRouteEntry entry = shipRouteEntryRepository.getOne(entryId);
		Ship ship = entry.getShip();
		List<ShipRouteEntry> routeEntries = shipRouteEntryRepository.findByShipId(ship.getId());

		if (routeEntries.size() > 2) {
			if (ship.getCurrentRouteEntry() == entry) {
				int index = routeEntries.indexOf(entry) + 1;

				if (index >= routeEntries.size() - 1) {
					index = 0;
				}

				ShipRouteEntry next = routeEntries.get(index);
				ship.setCurrentRouteEntry(next);

				if (!ship.isRouteDisabled()) {
					ship.setDestinationX(next.getPlanet().getX());
					ship.setDestinationY(next.getPlanet().getY());
					ship.setTravelSpeed(ship.getRouteTravelSpeed());
				}

				shipRepository.save(ship);
			}
		} else {
			ship.resetTravel();
			ship.setCurrentRouteEntry(null);

			shipRepository.save(ship);
		}

		shipRouteEntryRepository.delete(entry);

		visibleObjects.clearCaches();

		return ship.getId();
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void setRouteTravelSpeed(long shipId, int travelSpeed) {
		if (travelSpeed < 0 || travelSpeed > 9) {
			throw new IllegalArgumentException("Invalid travel speed value: " + travelSpeed);
		}

		Ship ship = shipRepository.getOne(shipId);
		ship.setTravelSpeed(travelSpeed);
		ship.setRouteTravelSpeed(travelSpeed);
		shipRepository.save(ship);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void setRouteDisabled(long shipId, boolean disabled) {
		Ship ship = shipRepository.getOne(shipId);
		ship.setRouteDisabled(disabled);
		ship.resetTravel();

		if (!disabled && ship.getCurrentRouteEntry() != null) {
			Planet planet = ship.getCurrentRouteEntry().getPlanet();
			ship.setDestinationX(planet.getX());
			ship.setDestinationY(planet.getY());
			ship.setTravelSpeed(ship.getRouteTravelSpeed());
		}

		shipRepository.save(ship);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void setRouteMinFuel(long shipId, int minFuel) {
		Ship ship = shipRepository.getOne(shipId);

		if (minFuel < 0 || minFuel > ship.getShipTemplate().getFuelCapacity()) {
			throw new IllegalArgumentException("Invalid minFuel value: " + minFuel);
		}

		ship.setRouteMinFuel(minFuel);
		shipRepository.save(ship);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void setRouteTravelSpeed(long shipId, Resource primaryResource) {
		Ship ship = shipRepository.getOne(shipId);
		ship.setRoutePrimaryResource(primaryResource);
		shipRepository.save(ship);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void checkStructurScanner(Player player, Ship otherShip) {
		ShipTemplate shipTemplate = otherShip.getShipTemplate();
		Game game = player.getGame();

		Optional<AquiredShipTemplate> resultOpt = aquiredShipTemplateRepository.findByGameIdAndShipTemplateId(game.getId(), shipTemplate.getId());

		if (!resultOpt.isPresent() && shipTemplate.getFaction() != player.getFaction()) {
			AquiredShipTemplate aquiredShipTemplate = new AquiredShipTemplate();
			aquiredShipTemplate.setGame(game);
			aquiredShipTemplate.setShipTemplate(shipTemplate);
			aquiredShipTemplateRepository.save(aquiredShipTemplate);
		}
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changeOptions(long shipId, ShipOptionsChangeRequest request) {
		Ship ship = shipRepository.getOne(shipId);

		PolicyFactory policy = new HtmlPolicyBuilder().toFactory();
		String name = policy.sanitize(request.getName());

		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("Ship name cannot be empty!");
		}

		ship.setName(name);
		ship = shipRepository.save(ship);

		Long newOwnerId = request.getNewOwnerId();

		if (newOwnerId != null) {
			giveShipToAlly(ship, newOwnerId);
		}
	}

	protected void giveShipToAlly(Ship ship, long playerId) {
		Game game = ship.getPlayer().getGame();
		Player player = playerRepository.getOne(playerId);

		if (player.getGame().getId() != game.getId()) {
			throw new IllegalArgumentException("Player " + playerId + " is not part of game " + game.getId() + "!");
		}

		List<PlayerRelation> relations = playerRelationRepository.findByPlayerIds(ship.getPlayer().getId(), playerId);

		if (relations.isEmpty() || relations.get(0).getType() != PlayerRelationType.ALLIANCE) {
			throw new IllegalArgumentException("Player " + playerId + " is not in an Alliance with player " + ship.getPlayer().getId() + "!");
		}

		ship.setPlayer(player);
		ship.resetTask();
		ship.resetTravel();

		ship = shipRepository.save(ship);

		shipRepository.clearCurrentRouteEntry(ship.getId());
		shipRouteEntryRepository.deleteByShipId(ship.getId());

		visibleObjects.clearCaches();
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public List<ShipTemplate> getBuiltShipTemplates(long gameId, long loginId) {
		ShipTemplateNameI18NComparator comparator = new ShipTemplateNameI18NComparator(masterDataService);
		return shipRepository.getBuiltShipTemplates(gameId, loginId).stream().sorted(comparator).collect(Collectors.toList());
	}
}