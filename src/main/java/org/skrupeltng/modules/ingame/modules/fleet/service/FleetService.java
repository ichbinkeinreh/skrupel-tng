package org.skrupeltng.modules.ingame.modules.fleet.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.ingame.controller.FleetListResultDTO;
import org.skrupeltng.modules.ingame.controller.FleetOverviewRequest;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.fleet.controller.FleetResourceData;
import org.skrupeltng.modules.ingame.modules.fleet.controller.FleetShipTaskItemDTO;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTaskChangeRequest;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTravelData;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Sets;

@Service
public class FleetService {

	@Autowired
	private FleetRepository fleetRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private ShipService shipService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private FleetFuel fleetFuel;

	@Autowired
	private VisibleObjects visibleObjects;

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public Page<FleetListResultDTO> getFleetsOfLogin(long gameId, FleetOverviewRequest request, long loginId) {
		Page<FleetListResultDTO> fleets = fleetRepository.searchFleets(request, loginId);

		for (FleetListResultDTO dto : fleets) {
			String[] tasks = dto.getTaskData().split(";");
			Set<String> set = Sets.newHashSet(tasks);

			if (set.size() == 1) {
				String[] parts = set.iterator().next().split(",");

				if (parts.length > 0) {
					dto.setTaskType(parts[0]);

					if (parts.length == 2) {
						dto.setActiveAbilityType(parts[1]);
					}
				}
			} else {
				dto.setTaskData(null);
			}
		}

		return fleets;
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet')")
	public Fleet getById(long fleetId) {
		return fleetRepository.getOne(fleetId);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long createFleet(long gameId, String name, long loginId) {
		Optional<Player> playerOpt = playerRepository.findByGameIdAndLoginId(gameId, loginId);

		if (!playerOpt.isPresent()) {
			throw new IllegalArgumentException("Login " + loginId + " is not part of game " + gameId + "!");
		}

		Fleet fleet = new Fleet();
		fleet.setName(name);
		fleet.setPlayer(playerOpt.get());

		fleet = fleetRepository.save(fleet);

		return fleet.getId();
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteFleet(long fleetId) {
		fleetRepository.clearFleetsFromShipsByFleetId(fleetId);
		fleetRepository.deleteById(fleetId);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addShipToFleet(long shipId, long fleetId) {
		Ship ship = shipRepository.getOne(shipId);
		Fleet fleet = fleetRepository.getOne(fleetId);
		ship.setFleet(fleet);

		ship = shipRepository.save(ship);

		if (fleet.getShips() == null) {
			fleet.setShips(new ArrayList<>());
		}

		fleet.getShips().add(ship);

		if (fleet.getLeader() == null) {
			changeLeader(fleet, ship);
		} else {
			updateFleetDestinations(fleet, fleet.getLeader());
		}
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void clearFleetOfShip(long shipId) {
		clearFleetOfShipWithoutPermissionCheck(shipId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void clearFleetOfShipWithoutPermissionCheck(long shipId) {
		Ship ship = shipRepository.getOne(shipId);
		Fleet fleet = ship.getFleet();

		if (fleet != null && fleet.getLeader() == ship) {
			if (fleet.getShips().size() > 1) {
				Optional<Ship> result = fleet.getShips().stream().filter(s -> s != ship).findFirst();

				if (result.isPresent()) {
					changeLeader(fleet, result.get());
					ship.resetTravel();
				} else {
					changeLeader(fleet, null);
				}
			} else {
				changeLeader(fleet, null);
			}
		} else {
			ship.resetTravel();
		}

		ship.setFleet(null);

		shipRepository.save(ship);

		visibleObjects.clearCaches();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public List<ShipTravelData> setLeaderWithoutPermissionCheck(long fleetId, long shipId) {
		Fleet fleet = fleetRepository.getOne(fleetId);
		Ship ship = shipRepository.getOne(shipId);

		if (!fleet.getShips().contains(ship)) {
			throw new IllegalArgumentException("Ship " + shipId + " cannot be made leader of fleet " + fleetId + " because it is not part of that fleet!");
		}

		changeLeader(fleet, ship);

		fleet = fleetRepository.getOne(fleetId);
		List<Ship> ships = fleet.getShips();

		return ships.stream().map(s -> new ShipTravelData(s)).collect(Collectors.toList());
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public List<ShipTravelData> setLeader(long fleetId, long shipId) {
		return setLeaderWithoutPermissionCheck(fleetId, shipId);
	}

	protected void changeLeader(Fleet fleet, Ship newLeader) {
		fleet.setLeader(newLeader);
		fleet = fleetRepository.save(fleet);

		updateFleetDestinations(fleet, newLeader);
	}

	protected void updateFleetDestinations(Fleet fleet, Ship newLeader) {
		if (newLeader != null && fleet.getShips() != null && fleet.getShips().size() > 1) {
			if (fleet.getShips().contains(newLeader.getDestinationShip())) {
				newLeader.resetTravel();
				shipRepository.save(newLeader);
			}

			for (Ship ship : fleet.getShips()) {
				if (ship != newLeader) {
					ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
					ship.setDestinationShip(newLeader);
					ship.setDestinationX(newLeader.getX());
					ship.setDestinationY(newLeader.getY());
					shipRepository.save(ship);
				}
			}
		}

		visibleObjects.clearCaches();
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public List<Fleet> getPossibleFleetsForShip(long shipId) {
		return fleetRepository.getPossibleFleetsForShip(shipId);
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet')")
	public FleetResourceData getFleetResourceData(long fleetId) {
		FleetResourceData data = new FleetResourceData();

		Fleet fleet = fleetRepository.getOne(fleetId);
		List<Ship> ships = fleet.getShips();

		data.setShipCount(ships.size());

		float totalShipTemplateFuel = 0;
		float totalShipFuel = 0;
		float totalProjectiles = 0;
		float totalProjectilesOnShips = 0;
		float availableFuel = 0;
		float buildableProjectiles = 0;
		float totalAggressiveness = 0;
		int lastAggressiveness = -1;
		boolean hasDifferentAggressivenesses = false;

		HashMultimap<Planet, Ship> planetsMap = HashMultimap.create();
		List<Ship> shipsWithoutPlanet = new ArrayList<>();

		int weaponCount = 0;

		Set<String> availableSpecialAbilities = new HashSet<>();

		for (Ship ship : ships) {
			ShipTemplate shipTemplate = ship.getShipTemplate();
			weaponCount += shipTemplate.getEnergyWeaponsCount();
			weaponCount += shipTemplate.getProjectileWeaponsCount();
			weaponCount += shipTemplate.getHangarCapacity();

			totalAggressiveness += ship.getAggressiveness();

			if (lastAggressiveness == -1) {
				lastAggressiveness = ship.getAggressiveness();
			} else if (lastAggressiveness != ship.getAggressiveness()) {
				hasDifferentAggressivenesses = true;
			}

			availableSpecialAbilities.addAll(shipTemplate.getShipAbilities().stream().map(sa -> sa.getType().name()).collect(Collectors.toSet()));

			if (ship.getPlanet() != null) {
				planetsMap.put(ship.getPlanet(), ship);
			} else {
				shipsWithoutPlanet.add(ship);
			}
		}

		data.setWeaponCount(weaponCount);

		Locale locale = LocaleContextHolder.getLocale();
		String abilities = availableSpecialAbilities.stream().map(a -> messageSource.getMessage("ship_ability_" + a, null, a, locale)).sorted()
				.collect(Collectors.joining(", "));
		data.setAvailableSpecialAbilities(abilities);

		long playerId = fleet.getPlayer().getId();

		for (Planet planet : planetsMap.keySet()) {
			Set<Ship> shipsList = planetsMap.get(planet);

			int planetFuel = planet.retrieveTransportableFuel(playerId);
			int planetMoney = planet.retrieveTransportableMoney(playerId);
			int planetMineral1 = planet.retrieveTransportableMineral1(playerId);
			int planetMineral2 = planet.retrieveTransportableMineral2(playerId);

			for (Ship ship : shipsList) {
				ShipTemplate shipTemplate = ship.getShipTemplate();

				totalShipTemplateFuel += shipTemplate.getFuelCapacity();
				totalShipFuel += ship.getFuel();

				totalProjectiles += shipTemplate.getProjectileWeaponsCount() * MasterDataConstants.PROJECTILES_PER_WEAPON;
				totalProjectilesOnShips += ship.getProjectiles();

				int newFuel = Math.min(shipTemplate.getFuelCapacity() - ship.getFuel(), planetFuel);
				availableFuel += newFuel;
				planetFuel -= newFuel;

				int currentProjectiles = ship.getProjectiles();
				int maxProjectiles = ship.getShipTemplate().getProjectileWeaponsCount() * MasterDataConstants.PROJECTILES_PER_WEAPON;

				int money = planetMoney + ship.getMoney();
				int mineral1 = planetMineral1 + ship.getMineral1();
				int mineral2 = planetMineral2 + ship.getMineral2();

				int newProjectiles = maxProjectiles - currentProjectiles;
				newProjectiles = Math.min(newProjectiles, Math.min(money / 35, Math.min(mineral1 / 2, mineral2)));

				buildableProjectiles += newProjectiles;
				planetMoney -= newProjectiles * 35;
				planetMineral1 -= newProjectiles * 2;
				planetMineral2 -= newProjectiles;
			}
		}

		for (Ship ship : shipsWithoutPlanet) {
			ShipTemplate shipTemplate = ship.getShipTemplate();

			totalShipTemplateFuel += shipTemplate.getFuelCapacity();
			totalShipFuel += ship.getFuel();

			totalProjectiles += shipTemplate.getProjectileWeaponsCount() * MasterDataConstants.PROJECTILES_PER_WEAPON;
			totalProjectilesOnShips += ship.getProjectiles();
		}

		if (totalShipTemplateFuel > 0) {
			data.setCurrentTankPercentage(roundPercentage(totalShipFuel / totalShipTemplateFuel));
			data.setPossibleTankPercentage(roundPercentage((totalShipFuel + availableFuel) / totalShipTemplateFuel));
		}

		if (totalProjectiles > 0) {
			data.setCurrentProjectilePercentage(roundPercentage(totalProjectilesOnShips / totalProjectiles));
			data.setPossibleProjectilePercentage(roundPercentage((totalProjectilesOnShips + buildableProjectiles) / totalProjectiles));
		}

		data.setAverageAggressiveness((int)(totalAggressiveness / ships.size()));
		data.setHasDifferentAggressivenesses(hasDifferentAggressivenesses);

		return data;
	}

	protected int roundPercentage(float percentage) {
		return Math.round(percentage * 100f);
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public List<Long> fillTanks(long fleetId, float percentage) {
		if (percentage < 0f || percentage > 1f) {
			throw new IllegalArgumentException("Invalid fuel percentage value for fleet!");
		}

		fleetFuel.transportFuelToPlanets(fleetId);
		List<Long> planetIds = fleetFuel.distributeFuel(fleetId, percentage);

		return planetIds;
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public List<Long> buildProjectiles(long fleetId) {
		Fleet fleet = fleetRepository.getOne(fleetId);
		List<Ship> ships = fleet.getShips();

		List<Long> planetIds = new ArrayList<>();

		for (Ship ship : ships) {
			Planet planet = shipService.buildAllPossibleProjectilesAutomatically(ship.getId());

			if (planet != null) {
				planetIds.add(planet.getId());
			}
		}

		return planetIds;
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public List<FleetShipTaskItemDTO> getTaskItems(long fleetId) {
		Fleet fleet = fleetRepository.getOne(fleetId);
		List<Ship> ships = fleet.getShips();

		Set<FleetShipTaskItemDTO> abilityItems = new HashSet<>();

		boolean shipWithNoWeaponsFound = false;
		boolean shipWithNoHangarsFound = false;
		boolean shipWithoutAutoDesctructFound = false;

		for (Ship ship : ships) {
			if (ship.getShipTemplate().getHangarCapacity() == 0) {
				shipWithNoHangarsFound = true;
			}

			if (!ship.hasWeapons()) {
				shipWithNoWeaponsFound = true;
			}

			if (ship.getShipModule() != ShipModule.AUTO_DESTRUCTION) {
				shipWithoutAutoDesctructFound = true;
			}

			List<ShipAbility> shipAbilities = ship.getShipTemplate().getShipAbilities();

			for (ShipAbility ability : shipAbilities) {
				ShipAbilityType abilityType = ability.getType();

				if (!abilityType.isPassive() && abilityType.getValueLabel() == null) {
					FleetShipTaskItemDTO dto = new FleetShipTaskItemDTO();
					dto.setActiveAbilityType(abilityType.name());
					dto.setTaskType(ShipTaskType.ACTIVE_ABILITY.name());
					abilityItems.add(dto);
				}
			}
		}

		List<FleetShipTaskItemDTO> items = new ArrayList<>();
		items.add(new FleetShipTaskItemDTO(ShipTaskType.NONE.name()));
		items.add(new FleetShipTaskItemDTO(ShipTaskType.AUTOREFUEL.name()));
		items.add(new FleetShipTaskItemDTO(ShipTaskType.SHIP_RECYCLE.name()));

		if (!shipWithNoWeaponsFound) {
			items.add(new FleetShipTaskItemDTO(ShipTaskType.CAPTURE_SHIP.name()));
			items.add(new FleetShipTaskItemDTO(ShipTaskType.PLANET_BOMBARDMENT.name()));
		}

		if (!shipWithNoHangarsFound) {
			items.add(new FleetShipTaskItemDTO(ShipTaskType.CLEAR_MINE_FIELD.name()));
		}

		if (!shipWithoutAutoDesctructFound) {
			items.add(new FleetShipTaskItemDTO(ShipTaskType.AUTO_DESTRUCTION.name()));
		}

		for (FleetShipTaskItemDTO item : abilityItems) {
			if (!ships.stream().anyMatch(s -> !shipHasAbility(item, s))) {
				items.add(item);
			}
		}

		return items;
	}

	protected boolean shipHasAbility(FleetShipTaskItemDTO item, Ship ship) {
		List<ShipAbility> shipAbilities = ship.getShipTemplate().getShipAbilities();

		for (ShipAbility ability : shipAbilities) {
			ShipAbilityType abilityType = ability.getType();

			if (abilityType.name().equals(item.getActiveAbilityType())) {
				return true;
			}
		}

		return false;
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet')")
	public FleetShipTaskItemDTO getTaskOfAllShipsInFleet(long fleetId) {
		Fleet fleet = fleetRepository.getOne(fleetId);
		List<Ship> ships = fleet.getShips();

		FleetShipTaskItemDTO item = null;

		for (Ship ship : ships) {
			FleetShipTaskItemDTO shipItem = new FleetShipTaskItemDTO(ship.getTaskType().name());
			if (ship.getActiveAbility() != null) {
				shipItem.setActiveAbilityType(ship.getActiveAbility().getType().name());
			}

			if (item == null) {
				item = shipItem;
				item.setTaskValue(ship.getTaskValue());
			} else if (!item.equals(shipItem)) {
				return null;
			}
		}

		return item;
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void setTask(long fleetId, String taskType, String activeAbilityType, String taskValue) {
		Fleet fleet = fleetRepository.getOne(fleetId);
		List<Ship> ships = fleet.getShips();

		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		request.setTaskType(taskType);
		request.setActiveAbilityType(activeAbilityType);
		request.setTaskValue(taskValue);

		Ship leader = fleet.getLeader();

		for (Ship ship : ships) {
			if (leader != null && ship != leader) {
				request.setEscortTargetSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
				request.setEscortTargetId(leader.getId());
			}

			request.setShipId(ship.getId());
			shipService.changeTask(ship.getId(), request);
		}
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changeTactics(long fleetId, int aggressiveness) {
		if (aggressiveness < 0 || aggressiveness > 100) {
			throw new IllegalArgumentException("Invalid aggressiveness value!");
		}

		List<Ship> ships = fleetRepository.getOne(fleetId).getShips();

		for (Ship ship : ships) {
			ship.setAggressiveness(aggressiveness);
		}

		shipRepository.saveAll(ships);
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changeName(long fleetId, String name) {
		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("Fleet name cannot be blank!");
		}

		Fleet fleet = fleetRepository.getOne(fleetId);
		fleet.setName(name);
		fleetRepository.save(fleet);
	}

	public List<Fleet> getFleets(long fleetId) {
		return fleetRepository.findOtherFleets(fleetId);
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#otherFleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void mergeFleets(long fleetId, long otherFleetId) {
		Fleet otherFleet = fleetRepository.getOne(otherFleetId);

		List<Ship> ships = new ArrayList<>(otherFleet.getShips());

		for (Ship ship : ships) {
			clearFleetOfShip(ship.getId());
			addShipToFleet(ship.getId(), fleetId);
		}

		deleteFleet(otherFleetId);
	}
}
