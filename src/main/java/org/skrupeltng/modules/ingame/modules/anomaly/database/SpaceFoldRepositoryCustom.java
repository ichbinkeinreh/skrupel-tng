package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.util.List;

public interface SpaceFoldRepositoryCustom {

	List<Long> getSpaceFoldIdsInRadius(long gameId, int x, int y, int distance);
}
