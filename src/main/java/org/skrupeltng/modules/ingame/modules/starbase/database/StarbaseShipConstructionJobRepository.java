package org.skrupeltng.modules.ingame.modules.starbase.database;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface StarbaseShipConstructionJobRepository extends JpaRepository<StarbaseShipConstructionJob, Long> {

	Optional<StarbaseShipConstructionJob> findByStarbaseId(long starbaseId);

	@Query("SELECT " +
			"	j " +
			"FROM " +
			"	StarbaseShipConstructionJob j " +
			"	INNER JOIN FETCH j.starbase s " +
			"	INNER JOIN FETCH s.planet p " +
			"	INNER JOIN FETCH p.player o " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"ORDER BY " +
			"	j.id ASC")
	List<StarbaseShipConstructionJob> findByGameId(long gameId);

	@Modifying
	@Query("DELETE FROM StarbaseShipConstructionJob s WHERE s.starbase.id = ?1")
	void deleteByStarbaseId(long starbaseId);
}