package org.skrupeltng.modules.ingame.modules.fleet.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ingame.controller.FleetListResultDTO;
import org.skrupeltng.modules.ingame.controller.FleetOverviewRequest;
import org.skrupeltng.modules.masterdata.service.MasterDataConstants;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

public class FleetRepositoryImpl extends RepositoryCustomBase implements FleetRepositoryCustom {

	@Override
	public boolean loginOwnsFleet(long fleetId, long loginId) {

		String sql = "" +
				"SELECT \n" +
				"	p.login_id \n" +
				"FROM \n" +
				"	fleet f \n" +
				"	INNER JOIN player p \n" +
				"		ON p.id = f.player_id \n" +
				"WHERE \n" +
				"	f.id = :fleetId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("fleetId", fleetId);

		List<Long> results = jdbcTemplate.queryForList(sql, params, Long.class);
		return results.size() == 1 && results.get(0).longValue() == loginId;
	}

	@Override
	public Page<FleetListResultDTO> searchFleets(FleetOverviewRequest request, long loginId) {
		long gameId = request.getGameId();
		String name = request.getName();

		Pageable page = createPageable(request);

		String sql = "" +
				"SELECT \n" +
				"	f.id, \n" +
				"	f.name, \n" +
				"	COUNT(s.id) as \"shipCount\", \n" +
				"	STRING_AGG(CONCAT(s.task_type, ',', sa.type), ';') as \"taskData\", \n" +
				"	COALESCE(SUM(s.fuel) / SUM(CAST(st.fuel_capacity as double precision)), 0) as \"fuelPercentage\", \n" +
				"	COALESCE(CASE WHEN (SUM(st.projectile_weapons_count) > 0) THEN SUM(s.projectiles) / SUM(st.projectile_weapons_count * :ppw) ELSE 0 END, 0) as \"projectilePercentage\", \n" +
				"	count(*) OVER() AS \"totalElements\" \n" +
				"FROM \n" +
				"	fleet f \n" +
				"	INNER JOIN player p \n" +
				"		ON p.id = f.player_id AND p.game_id = :gameId AND p.login_id = :loginId \n" +
				"	LEFT OUTER JOIN ship s \n" +
				"		ON s.fleet_id = f.id \n" +
				"	LEFT OUTER JOIN ship_template st \n" +
				"		ON st.id = s.ship_template_id \n" +
				"	LEFT OUTER JOIN ship_ability sa \n" +
				"		ON sa.id = s.active_ability_id \n";

		Map<String, Object> params = new HashMap<>();
		params.put("gameId", gameId);
		params.put("loginId", loginId);
		params.put("ppw", MasterDataConstants.PROJECTILES_PER_WEAPON * 1.0f);

		List<String> wheres = new ArrayList<>();

		if (StringUtils.isNotBlank(name)) {
			wheres.add("f.name ILIKE :name");
			params.put("name", "%" + name + "%");
		}

		sql += where(wheres);

		sql += "" +
				" GROUP BY \n" +
				"	f.id, \n" +
				"	f.name \n";

		sql += createOrderBy(page.getSort(), "f.name ASC");

		RowMapper<FleetListResultDTO> rowMapper = new BeanPropertyRowMapper<>(FleetListResultDTO.class);

		return search(sql, params, page, rowMapper);
	}
}