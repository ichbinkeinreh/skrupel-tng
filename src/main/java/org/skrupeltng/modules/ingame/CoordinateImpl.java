package org.skrupeltng.modules.ingame;

import java.util.Objects;

public class CoordinateImpl implements Coordinate {

	private int x;
	private int y;
	private int scanRadius;

	public CoordinateImpl() {

	}

	public CoordinateImpl(int x, int y, int scanRadius) {
		this.x = x;
		this.y = y;
		this.scanRadius = scanRadius;
	}

	@Override
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public int getScanRadius() {
		return scanRadius;
	}

	public void setScanRadius(int scanRadius) {
		this.scanRadius = scanRadius;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CoordinateImpl other = (CoordinateImpl)obj;
		return x == other.x && y == other.y;
	}

	@Override
	public String toString() {
		return "CoordinateImpl [x=" + x + ", y=" + y + ", scanRadius=" + scanRadius + "]";
	}
}