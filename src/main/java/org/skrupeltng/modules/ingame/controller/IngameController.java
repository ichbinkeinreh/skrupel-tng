package org.skrupeltng.modules.ingame.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.skrupeltng.exceptions.ResourceNotFoundException;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.SortField;
import org.skrupeltng.modules.dashboard.service.TutorialService;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.PlayerGameTurnInfoItem;
import org.skrupeltng.modules.ingame.database.TutorialStage;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlasmaStorm;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFold;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.service.PlanetService;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.skrupeltng.modules.ingame.service.IngameService;
import org.skrupeltng.modules.ingame.service.VisiblePlanetsAndShips;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ingame")
public class IngameController extends AbstractController {

	@Autowired
	private IngameService ingameService;

	@Autowired
	private PlanetService planetService;

	@Autowired
	private StarbaseService starbaseService;

	@Autowired
	private ShipService shipService;

	@Autowired
	private FleetService fleetService;

	@Autowired
	private PoliticsService politicsService;

	@Autowired
	private TutorialService tutorialService;

	@GetMapping("/game")
	public String game(@RequestParam(required = true) long id, Model model) {
		Optional<Game> optional = ingameService.getGame(id);

		if (optional.isPresent()) {
			Game game = optional.get();
			long loginId = userDetailService.getLoginId();
			Player player = ingameService.getPlayer(loginId, id);

			if (player.isHasLost() || game.isFinished()) {
				return "redirect:/game?id=" + id;
			}

			Planet homePlanet = player.getHomePlanet();
			int initX = homePlanet.getX();
			int initY = homePlanet.getY();

			if (homePlanet.getPlayer() != player) {
				Optional<Planet> planetOpt = player.getPlanets().stream().max(Comparator.comparing(Planet::getColonists));

				if (planetOpt.isPresent()) {
					Planet planet = planetOpt.get();
					initX = planet.getX();
					initY = planet.getY();
				}
			}

			model.addAttribute("initX", initX);
			model.addAttribute("initY", initY);

			long playerId = player.getId();

			model.addAttribute("game", game);
			model.addAttribute("playerId", playerId);
			model.addAttribute("turnFinished", player.isTurnFinished());
			model.addAttribute("random", MasterDataService.RANDOM);
			model.addAttribute("playerColor", player.getColor());
			model.addAttribute("overviewViewed", player.isOverviewViewed());

			if (game.isTutorial()) {
				model.addAttribute("tutorial", tutorialService.getTutorial().get());
			}

			List<PlayerGameTurnInfoItem> turnInfos = ingameService.getTurnInfosByLogin(loginId, id);
			model.addAttribute("turnInfos", turnInfos);

			long unfinishedTurns = turnInfos.stream().filter(g -> !g.isTurnFinished()).count();
			model.addAttribute("unfinishedTurns", unfinishedTurns);

			Set<CoordinateImpl> visibilityCoordinates = ingameService.getVisibilityCoordinates(id, loginId);
			model.addAttribute("visibilityCoordinates", visibilityCoordinates);

			List<WormHole> wormHoles = ingameService.getWormHoles(id, visibilityCoordinates);
			model.addAttribute("wormHoles", wormHoles);

			List<SpaceFold> spaceFolds = ingameService.getSpaceFolds(id, visibilityCoordinates);
			model.addAttribute("spaceFolds", spaceFolds);

			List<PlasmaStorm> plasmaStorms = ingameService.getPlasmaStorms(id);
			model.addAttribute("plasmaStorms", plasmaStorms);

			List<MineFieldEntry> mineFields = ingameService.getMineFields(id, visibilityCoordinates, playerId);
			model.addAttribute("mineFields", mineFields);

			model.addAttribute("disablePermissionChecks", configProperties.isDisablePermissionChecks());

			Map<Long, PlayerRelationType> relationsMap = getRelationsMap(playerId);
			model.addAttribute("relations", relationsMap);

			Map<String, Player> colorToPlayerMap = game.getPlayers().stream().collect(Collectors.toMap(Player::getColor, p -> p));
			model.addAttribute("colorToPlayerMap", colorToPlayerMap);

			int galaxySize = game.getGalaxySize();
			int sectorCount = (int)Math.ceil(galaxySize / 250f);
			List<Sector> sectors = new ArrayList<>(sectorCount * sectorCount);

			for (int lineNumber = 1; lineNumber <= sectorCount; lineNumber++) {
				for (int letterIndex = 1; letterIndex <= sectorCount; letterIndex++) {
					char letter = (char)(letterIndex + 64);

					String label = letter + "" + lineNumber;
					int x = letterIndex * 125 + ((letterIndex - 1) * 125) - 125;
					int y = lineNumber * 125 + (lineNumber - 1) * 125 - 125;

					sectors.add(new Sector(label, x, y, galaxySize));
				}
			}

			model.addAttribute("sectors", sectors);

			VisiblePlanetsAndShips visiblePlanetsAndShips = ingameService.getVisiblePlanetsAndShips(id, loginId, visibilityCoordinates, false);
			model.addAttribute("planets", visiblePlanetsAndShips.getPlanets());
			model.addAttribute("ships", visiblePlanetsAndShips.getAllShips());
			model.addAttribute("singleShips", visiblePlanetsAndShips.getSingleShips());
			model.addAttribute("shipClusters", visiblePlanetsAndShips.getShipClusters());

			return "ingame/ingame";
		}

		throw new ResourceNotFoundException();
	}

	private Map<Long, PlayerRelationType> getRelationsMap(long playerId) {
		Map<Long, PlayerRelationType> relationsMap = new HashMap<>();

		for (PlayerRelationType type : PlayerRelationType.values()) {
			List<Player> players = politicsService.getPlayersWithRelation(playerId, type);

			for (Player p : players) {
				relationsMap.put(p.getId(), type);
			}
		}

		return relationsMap;
	}

	@PostMapping("/overview-viewed")
	@ResponseBody
	public void overviewViewed(@RequestParam(required = true) long gameId) {
		ingameService.overviewViewed(gameId, userDetailService.getLoginId());
	}

	@PostMapping("/news-viewed")
	@ResponseBody
	public void newsViewed(@RequestParam(required = true) long gameId) {
		ingameService.newsViewed(gameId, userDetailService.getLoginId());
	}

	@PostMapping("/turn")
	@ResponseBody
	public boolean finishTurn(@RequestParam(required = true) long gameId) {
		return ingameService.finishTurn(gameId, userDetailService.getLoginId());
	}

	@GetMapping("/turn-finished")
	public String turnFinished() {
		return "ingame/turn-finished::content";
	}

	@PostMapping("/round")
	@ResponseBody
	public void finishRound(@RequestParam(required = true) long gameId) {
		ingameService.calculateRound(gameId, userDetailService.getLoginId());
	}

	@GetMapping("/ship-selection")
	public String showShips(@RequestParam long gameId, @RequestParam int x, @RequestParam int y, Model model) {
		List<Ship> ships = shipService.getShipsOfLogin(gameId, userDetailService.getLoginId(), x, y);
		model.addAttribute("ships", ships);
		model.addAttribute("x", x);
		model.addAttribute("y", y);
		return "ingame/ship-selection";
	}

	@GetMapping("/colony-overview")
	public String showColonies(@RequestParam long gameId, Model model) {
		ColonyOverviewRequest request = new ColonyOverviewRequest();
		request.setGameId(gameId);
		request.setPageSize(15);
		request.setSortField(SortField.COLONY_OVERVIEW_COLONISTS.name());
		request.setSortAscending(false);

		Page<PlanetListResultDTO> planets = planetService.getPlanetsOfLogin(gameId, request, userDetailService.getLoginId());
		model.addAttribute("planets", planets);
		return "ingame/colony-overview::content";
	}

	@PostMapping("/colony-overview")
	public String updateColoniesOverview(@RequestBody ColonyOverviewRequest request, Model model) {
		Page<PlanetListResultDTO> planets = planetService.getPlanetsOfLogin(request.getGameId(), request, userDetailService.getLoginId());
		model.addAttribute("planets", planets);
		return "ingame/colony-overview::table-content";
	}

	@GetMapping("/starbase-overview")
	public String showStarbases(@RequestParam long gameId, Model model) {
		StarbaseOverviewRequest request = new StarbaseOverviewRequest();
		request.setGameId(gameId);
		request.setPageSize(15);
		request.setSortField(SortField.SHIP_OVERVIEW_NAME.name());
		request.setSortAscending(true);

		Page<StarbaseListResultDTO> starbases = starbaseService.getStarbasesOfLogin(request.getGameId(), request, userDetailService.getLoginId());
		model.addAttribute("starbases", starbases);
		return "ingame/starbase-overview::content";
	}

	@PostMapping("/starbase-overview")
	public String updateStarbasesOverview(@RequestBody StarbaseOverviewRequest request, Model model) {
		Page<StarbaseListResultDTO> starbases = starbaseService.getStarbasesOfLogin(request.getGameId(), request, userDetailService.getLoginId());
		model.addAttribute("starbases", starbases);
		return "ingame/starbase-overview::table-content";
	}

	@GetMapping("/ship-overview")
	public String showShips(@RequestParam long gameId, Model model) {
		ShipOverviewRequest request = new ShipOverviewRequest();
		request.setGameId(gameId);
		request.setPageSize(15);
		request.setSortField(SortField.SHIP_OVERVIEW_NAME.name());
		request.setSortAscending(true);

		Page<ShipListResultDTO> ships = shipService.getShipsOfLogin(gameId, request, userDetailService.getLoginId());
		model.addAttribute("ships", ships);
		addBuiltShipTemplates(model, gameId);
		return "ingame/ship-overview::content";
	}

	@PostMapping("/ship-overview")
	public String updateShipsOverview(@RequestBody ShipOverviewRequest request, Model model) {
		Page<ShipListResultDTO> ships = shipService.getShipsOfLogin(request.getGameId(), request, userDetailService.getLoginId());
		model.addAttribute("ships", ships);
		addBuiltShipTemplates(model, request.getGameId());
		return "ingame/ship-overview::table-content";
	}

	private void addBuiltShipTemplates(Model model, long gameId) {
		long loginId = userDetailService.getLoginId();
		List<ShipTemplate> shipTemplates = shipService.getBuiltShipTemplates(gameId, loginId);
		model.addAttribute("shipTemplates", shipTemplates);
	}

	@GetMapping("/fleet-overview")
	public String showFleets(@RequestParam long gameId, Model model) {
		FleetOverviewRequest request = new FleetOverviewRequest();
		request.setGameId(gameId);
		request.setPageSize(15);
		request.setSortField(SortField.FLEET_OVERVIEW_NAME.name());
		request.setSortAscending(true);

		Page<FleetListResultDTO> fleets = fleetService.getFleetsOfLogin(request.getGameId(), request, userDetailService.getLoginId());
		model.addAttribute("fleets", fleets);
		return "ingame/fleet-overview::content";
	}

	@PostMapping("/fleet-overview")
	public String updateFleetsOverview(@RequestBody FleetOverviewRequest request, Model model) {
		Page<FleetListResultDTO> fleets = fleetService.getFleetsOfLogin(request.getGameId(), request, userDetailService.getLoginId());
		model.addAttribute("fleets", fleets);
		return "ingame/fleet-overview::table-content";
	}

	@GetMapping("/planet-mouse-over")
	public String getPlanetMouseOver(@RequestParam long planetId, Model model) {
		Planet planet = planetService.getPlanet(planetId);
		model.addAttribute("planet", planet);

		boolean disablePermissionChecks = configProperties.isDisablePermissionChecks();
		model.addAttribute("disablePermissionChecks", disablePermissionChecks);

		if (disablePermissionChecks) {
			model.addAttribute("playerId", 0L);
		} else {
			long playerId = planet.getPlayer().getId();
			model.addAttribute("playerId", playerId);
			Map<Long, PlayerRelationType> relationsMap = getRelationsMap(playerId);
			model.addAttribute("relations", relationsMap);
		}

		return "ingame/galaxymap/planet-mouseover::content";
	}

	@GetMapping("/ship-mouse-over")
	public String getShipMouseOver(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);

		boolean disablePermissionChecks = configProperties.isDisablePermissionChecks();
		model.addAttribute("disablePermissionChecks", disablePermissionChecks);

		if (disablePermissionChecks) {
			model.addAttribute("playerId", 0L);
		} else {
			model.addAttribute("playerId", ship.getPlayer().getId());
		}

		return "ingame/galaxymap/ship-mouseover::content";
	}

	@GetMapping("/ships-mouse-over-item")
	public String getShipsMouseOverItem(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);

		boolean disablePermissionChecks = configProperties.isDisablePermissionChecks();
		model.addAttribute("disablePermissionChecks", disablePermissionChecks);

		if (disablePermissionChecks) {
			model.addAttribute("playerId", 0L);
		} else {
			model.addAttribute("playerId", ship.getPlayer().getId());
		}

		return "ingame/galaxymap/ships-mouseover::ship-item";
	}

	@GetMapping("/starbase-mouse-over")
	public String getStarbaseMouseOver(@RequestParam long starbaseId, Model model) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute("starbaseName", starbase.getName());
		model.addAttribute("starbaseLog", starbase.getLog());
		model.addAttribute("disablePermissionChecks", configProperties.isDisablePermissionChecks());
		return "ingame/galaxymap/starbase-mouseover::content";
	}

	@GetMapping("/tutorial-stage-value")
	@ResponseBody
	public String getCurrentTutorialStageValue(Model model) {
		TutorialStage stage = tutorialService.getCurrentStage();
		return stage.name();
	}

	@GetMapping("/tutorial-stage")
	public String getTutorialStage(Model model) {
		TutorialStage stage = tutorialService.getCurrentStage();
		setupTutorialStage(model, stage);
		return "ingame/tutorial::content";
	}

	@PostMapping("/tutorial-stage")
	public String setAndGetNextTutorialStage(@RequestParam String page, Model model) {
		TutorialStage stage = tutorialService.setAndReturnNextStage(page);
		setupTutorialStage(model, stage);
		return "ingame/tutorial::content";
	}

	@GetMapping("/check-tutorial-stage")
	@ResponseBody
	public String checkTutorialStage(@RequestParam String page) {
		return tutorialService.checkTutorialStage(page);
	}

	@PostMapping("/revert-to-tutorial-stage")
	public String revertToTutorialStage(@RequestParam String stage, Model model) {
		TutorialStage tutorialStage = TutorialStage.valueOf(stage);
		tutorialService.revertToTutorialStage(tutorialStage);
		setupTutorialStage(model, tutorialStage);
		return "ingame/tutorial::content";
	}

	private void setupTutorialStage(Model model, TutorialStage stage) {
		model.addAttribute("stage", stage);
		model.addAttribute("hasContinueButton", tutorialService.hasContinuButton(stage));
	}

	@DeleteMapping("/tutorial")
	@ResponseBody
	public void finishTutorial() {
		tutorialService.finishTutorial(false);
	}

	@PostMapping("/tutorial")
	@ResponseBody
	public long restartTutorial() {
		tutorialService.finishTutorial(true);
		return tutorialService.startTutorial();
	}
}