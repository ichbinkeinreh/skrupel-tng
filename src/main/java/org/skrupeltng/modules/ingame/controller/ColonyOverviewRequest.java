package org.skrupeltng.modules.ingame.controller;

public class ColonyOverviewRequest extends AbstractPageRequest {

	private static final long serialVersionUID = 1071094212625152494L;

	private long gameId;
	private String name;
	private Boolean starbase;
	private Boolean hasRoute;

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getStarbase() {
		return starbase;
	}

	public void setStarbase(Boolean starbase) {
		this.starbase = starbase;
	}

	public Boolean getHasRoute() {
		return hasRoute;
	}

	public void setHasRoute(Boolean hasRoute) {
		this.hasRoute = hasRoute;
	}
}