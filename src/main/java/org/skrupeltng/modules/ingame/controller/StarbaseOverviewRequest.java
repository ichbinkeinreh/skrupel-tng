package org.skrupeltng.modules.ingame.controller;

public class StarbaseOverviewRequest extends AbstractPageRequest {

	private static final long serialVersionUID = 9172697705704310936L;

	private long gameId;
	private String name;
	private Boolean fullyUpgraded;
	private Boolean hasShipInProduction;

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getFullyUpgraded() {
		return fullyUpgraded;
	}

	public void setFullyUpgraded(Boolean fullyUpgraded) {
		this.fullyUpgraded = fullyUpgraded;
	}

	public Boolean getHasShipInProduction() {
		return hasShipInProduction;
	}

	public void setHasShipInProduction(Boolean hasShipInProduction) {
		this.hasShipInProduction = hasShipInProduction;
	}
}