package org.skrupeltng.modules.ingame.controller;

import java.io.Serializable;

import org.skrupeltng.modules.PageableResult;

public class FleetListResultDTO implements Serializable, PageableResult {

	private static final long serialVersionUID = 1105036215916046847L;

	private long id;
	private String name;
	private String taskData;
	private String taskType;
	private String activeAbilityType;
	private int shipCount;
	private float fuelPercentage;
	private float projectilePercentage;

	private int totalElements;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTaskData() {
		return taskData;
	}

	public void setTaskData(String taskData) {
		this.taskData = taskData;
	}

	public int getShipCount() {
		return shipCount;
	}

	public void setShipCount(int shipCount) {
		this.shipCount = shipCount;
	}

	public float getFuelPercentage() {
		return fuelPercentage;
	}

	public void setFuelPercentage(float fuelPercentage) {
		this.fuelPercentage = fuelPercentage;
	}

	public float getProjectilePercentage() {
		return projectilePercentage;
	}

	public void setProjectilePercentage(float projectilePercentage) {
		this.projectilePercentage = projectilePercentage;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getActiveAbilityType() {
		return activeAbilityType;
	}

	public void setActiveAbilityType(String activeAbilityType) {
		this.activeAbilityType = activeAbilityType;
	}

	@Override
	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}
}