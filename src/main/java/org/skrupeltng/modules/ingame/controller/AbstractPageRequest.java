package org.skrupeltng.modules.ingame.controller;

import java.io.Serializable;

public class AbstractPageRequest implements Serializable {

	private static final long serialVersionUID = -3532555403298945090L;

	private int page;
	private int pageSize;
	private String sortField;
	private boolean sortAscending;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public boolean isSortAscending() {
		return sortAscending;
	}

	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}
}