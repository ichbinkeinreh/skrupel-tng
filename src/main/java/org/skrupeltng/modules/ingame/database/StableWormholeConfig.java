package org.skrupeltng.modules.ingame.database;

public enum StableWormholeConfig {

	//@formatter:off
	NONE(0),
	ONE_RANDOM(1),
	TWO_RANDOM(2),
	THREE_RANDOM(3),
	FOUR_RANDOM(4),
	FIVE_RANDOM(5),
	ONE_NORTH_SOUTH(1),
	TWO_NORTH_SOUTH(2),
	ONE_WEST_EAST(1),
	TWO_WEST_EAST(2),
	ONE_NORTH_WEST_EAST_SOUTH(2),
	ONE_NORTHWEST_SOUTHEAST(1),
	ONE_NORTHEAST_SOUTHWEST(1),
	TWO_DIAGONAL(2);
	//@formatter:on

	private int number;

	private StableWormholeConfig(int number) {
		this.number = number;
	}

	public int getNumber() {
		return number;
	}
}