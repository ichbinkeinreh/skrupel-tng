package org.skrupeltng.modules.ingame.database;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.skrupeltng.modules.dashboard.PlanetDensity;
import org.skrupeltng.modules.dashboard.ResourceDensity;
import org.skrupeltng.modules.dashboard.ResourceDensityHomePlanet;
import org.skrupeltng.modules.dashboard.database.GalaxyConfig;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;

@Entity
@Table(name = "game")
public class Game implements Serializable {

	private static final long serialVersionUID = 7630810870074432707L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Temporal(TemporalType.TIMESTAMP)
	private final Date created;

	@ManyToOne(fetch = FetchType.LAZY)
	private Login creator;

	private String name;

	@Enumerated(EnumType.STRING)
	@Column(name = "win_condition")
	private WinCondition winCondition;

	private boolean started;

	private boolean finished;

	private int round;

	@Column(name = "is_private")
	private boolean isPrivate;

	@Column(name = "round_date")
	private Date roundDate;

	@Column(name = "auto_tick_seconds")
	private int autoTickSeconds;

	@Column(name = "enable_espionage")
	private boolean enableEspionage;

	@Column(name = "enable_mine_fields")
	private boolean enableMineFields;

	@Column(name = "enable_tactial_combat")
	private boolean enableTactialCombat;

	@Enumerated(EnumType.STRING)
	@Column(name = "start_position_setup")
	private StartPositionSetup startPositionSetup;

	@Enumerated(EnumType.STRING)
	@Column(name = "home_planet_setup")
	private HomePlanetSetup homePlanetSetup;

	@Enumerated(EnumType.STRING)
	@Column(name = "lose_condition")
	private LoseCondition loseCondition;

	@Column(name = "player_count")
	private int playerCount;

	@Column(name = "galaxy_size")
	private int galaxySize;

	@Column(name = "init_money")
	private int initMoney;

	@Enumerated(EnumType.STRING)
	@Column(name = "planet_density")
	private PlanetDensity planetDensity;

	@Enumerated(EnumType.STRING)
	@Column(name = "resource_density")
	private ResourceDensity resourceDensity;

	@Enumerated(EnumType.STRING)
	@Column(name = "resource_density_home_planet")
	private ResourceDensityHomePlanet resourceDensityHomePlanet;

	@Column(name = "inhabited_planets_percentage")
	private float inhabitedPlanetsPercentage;

	@Column(name = "unstable_wormhole_count")
	private int unstableWormholeCount;

	@Enumerated(EnumType.STRING)
	@Column(name = "stable_wormhole_config")
	private StableWormholeConfig stableWormholeConfig = StableWormholeConfig.NONE;

	@Column(name = "max_concurrent_plasma_storm_count")
	private int maxConcurrentPlasmaStormCount;

	@Column(name = "plasma_storm_probability")
	private float plasmaStormProbability;

	@Column(name = "plasma_storm_rounds")
	private int plasmaStormRounds;

	@Enumerated(EnumType.STRING)
	@Column(name = "fog_of_war_type")
	private FogOfWarType fogOfWarType;

	@Column(name = "pirate_attack_probability_center")
	private float pirateAttackProbabilityCenter;

	@Column(name = "pirate_attack_probability_outskirts")
	private float pirateAttackProbabilityOutskirts;

	@Column(name = "pirate_min_loot")
	private float pirateMinLoot;

	@Column(name = "pirate_max_loot")
	private float pirateMaxLoot;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "galaxy_config_id")
	private GalaxyConfig galaxyConfig;

	private boolean tutorial;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "game")
	private List<Player> players;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "game")
	private Set<Planet> planets;

	public Game() {
		created = new Date();
		roundDate = created;
		round = 1;
	}

	public Game(long id) {
		this();
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Login getCreator() {
		return creator;
	}

	public void setCreator(Login creator) {
		this.creator = creator;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public WinCondition getWinCondition() {
		return winCondition;
	}

	public void setWinCondition(WinCondition winCondition) {
		this.winCondition = winCondition;
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public boolean isPrivate() {
		return isPrivate;
	}

	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	public Date getRoundDate() {
		return roundDate;
	}

	public void setRoundDate(Date roundDate) {
		this.roundDate = roundDate;
	}

	public int getAutoTickSeconds() {
		return autoTickSeconds;
	}

	public void setAutoTickSeconds(int autoTickSeconds) {
		this.autoTickSeconds = autoTickSeconds;
	}

	public boolean isEnableEspionage() {
		return enableEspionage;
	}

	public void setEnableEspionage(boolean enableEspionage) {
		this.enableEspionage = enableEspionage;
	}

	public boolean isEnableMineFields() {
		return enableMineFields;
	}

	public void setEnableMineFields(boolean enableMineFields) {
		this.enableMineFields = enableMineFields;
	}

	public boolean isEnableTactialCombat() {
		return enableTactialCombat;
	}

	public void setEnableTactialCombat(boolean enableTactialCombat) {
		this.enableTactialCombat = enableTactialCombat;
	}

	public StartPositionSetup getStartPositionSetup() {
		return startPositionSetup;
	}

	public void setStartPositionSetup(StartPositionSetup startPositionSetup) {
		this.startPositionSetup = startPositionSetup;
	}

	public HomePlanetSetup getHomePlanetSetup() {
		return homePlanetSetup;
	}

	public void setHomePlanetSetup(HomePlanetSetup homePlanetSetup) {
		this.homePlanetSetup = homePlanetSetup;
	}

	public LoseCondition getLoseCondition() {
		return loseCondition;
	}

	public void setLoseCondition(LoseCondition loseCondition) {
		this.loseCondition = loseCondition;
	}

	public int getPlayerCount() {
		return playerCount;
	}

	public void setPlayerCount(int playerCount) {
		this.playerCount = playerCount;
	}

	public int getGalaxySize() {
		return galaxySize;
	}

	public void setGalaxySize(int galaxySize) {
		this.galaxySize = galaxySize;
	}

	public int getInitMoney() {
		return initMoney;
	}

	public void setInitMoney(int initMoney) {
		this.initMoney = initMoney;
	}

	public PlanetDensity getPlanetDensity() {
		return planetDensity;
	}

	public void setPlanetDensity(PlanetDensity planetDensity) {
		this.planetDensity = planetDensity;
	}

	public ResourceDensity getResourceDensity() {
		return resourceDensity;
	}

	public void setResourceDensity(ResourceDensity resourceDensity) {
		this.resourceDensity = resourceDensity;
	}

	public ResourceDensityHomePlanet getResourceDensityHomePlanet() {
		return resourceDensityHomePlanet;
	}

	public void setResourceDensityHomePlanet(ResourceDensityHomePlanet resourceDensityHomePlanet) {
		this.resourceDensityHomePlanet = resourceDensityHomePlanet;
	}

	public float getInhabitedPlanetsPercentage() {
		return inhabitedPlanetsPercentage;
	}

	public void setInhabitedPlanetsPercentage(float inhabitedPlanetsPercentage) {
		this.inhabitedPlanetsPercentage = inhabitedPlanetsPercentage;
	}

	public int getUnstableWormholeCount() {
		return unstableWormholeCount;
	}

	public void setUnstableWormholeCount(int unstableWormholeCount) {
		this.unstableWormholeCount = unstableWormholeCount;
	}

	public StableWormholeConfig getStableWormholeConfig() {
		return stableWormholeConfig;
	}

	public void setStableWormholeConfig(StableWormholeConfig stableWormholeConfig) {
		this.stableWormholeConfig = stableWormholeConfig;
	}

	public int getMaxConcurrentPlasmaStormCount() {
		return maxConcurrentPlasmaStormCount;
	}

	public void setMaxConcurrentPlasmaStormCount(int maxConcurrentPlasmaStormCount) {
		this.maxConcurrentPlasmaStormCount = maxConcurrentPlasmaStormCount;
	}

	public float getPlasmaStormProbability() {
		return plasmaStormProbability;
	}

	public void setPlasmaStormProbability(float plasmaStormProbability) {
		this.plasmaStormProbability = plasmaStormProbability;
	}

	public int getPlasmaStormRounds() {
		return plasmaStormRounds;
	}

	public void setPlasmaStormRounds(int plasmaStormRounds) {
		this.plasmaStormRounds = plasmaStormRounds;
	}

	public FogOfWarType getFogOfWarType() {
		return fogOfWarType;
	}

	public void setFogOfWarType(FogOfWarType fogOfWarType) {
		this.fogOfWarType = fogOfWarType;
	}

	public float getPirateAttackProbabilityCenter() {
		return pirateAttackProbabilityCenter;
	}

	public void setPirateAttackProbabilityCenter(float pirateAttackProbabilityCenter) {
		this.pirateAttackProbabilityCenter = pirateAttackProbabilityCenter;
	}

	public float getPirateAttackProbabilityOutskirts() {
		return pirateAttackProbabilityOutskirts;
	}

	public void setPirateAttackProbabilityOutskirts(float pirateAttackProbabilityOutskirts) {
		this.pirateAttackProbabilityOutskirts = pirateAttackProbabilityOutskirts;
	}

	public float getPirateMinLoot() {
		return pirateMinLoot;
	}

	public void setPirateMinLoot(float pirateMinLoot) {
		this.pirateMinLoot = pirateMinLoot;
	}

	public float getPirateMaxLoot() {
		return pirateMaxLoot;
	}

	public void setPirateMaxLoot(float pirateMaxLoot) {
		this.pirateMaxLoot = pirateMaxLoot;
	}

	public GalaxyConfig getGalaxyConfig() {
		return galaxyConfig;
	}

	public void setGalaxyConfig(GalaxyConfig galaxyConfig) {
		this.galaxyConfig = galaxyConfig;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public Set<Planet> getPlanets() {
		return planets;
	}

	public void setPlanets(Set<Planet> planets) {
		this.planets = planets;
	}

	public Date getCreated() {
		return created;
	}

	public boolean isTutorial() {
		return tutorial;
	}

	public void setTutorial(boolean tutorial) {
		this.tutorial = tutorial;
	}
}