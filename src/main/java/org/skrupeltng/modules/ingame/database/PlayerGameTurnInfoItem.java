package org.skrupeltng.modules.ingame.database;

import java.io.Serializable;

public class PlayerGameTurnInfoItem implements Serializable {

	private static final long serialVersionUID = -4423678793628652052L;

	private long gameId;
	private String gameName;
	private boolean turnFinished;

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public boolean isTurnFinished() {
		return turnFinished;
	}

	public void setTurnFinished(boolean turnFinished) {
		this.turnFinished = turnFinished;
	}
}