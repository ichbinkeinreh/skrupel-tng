package org.skrupeltng.modules.ingame.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;

@Entity
@Table(name = "tutorial")
public class Tutorial implements Serializable {

	private static final long serialVersionUID = 66866151364474134L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Game game;

	@ManyToOne(fetch = FetchType.LAZY)
	private Login login;

	@Enumerated(EnumType.STRING)
	private TutorialStage stage;

	@Enumerated(EnumType.STRING)
	@Column(name = "max_reached_stage")
	private TutorialStage maxReachedStage;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "home_planet_id")
	private Planet homePlanet;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "first_colony_id")
	private Planet firstColony;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "enemy_colony_id")
	private Planet enemyColony;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public TutorialStage getStage() {
		return stage;
	}

	public void setStage(TutorialStage stage) {
		this.stage = stage;
	}

	public TutorialStage getMaxReachedStage() {
		return maxReachedStage;
	}

	public void setMaxReachedStage(TutorialStage maxReachedStage) {
		this.maxReachedStage = maxReachedStage;
	}

	public Planet getHomePlanet() {
		return homePlanet;
	}

	public void setHomePlanet(Planet homePlanet) {
		this.homePlanet = homePlanet;
	}

	public Planet getFirstColony() {
		return firstColony;
	}

	public void setFirstColony(Planet firstColony) {
		this.firstColony = firstColony;
	}

	public Planet getEnemyColony() {
		return enemyColony;
	}

	public void setEnemyColony(Planet enemyColony) {
		this.enemyColony = enemyColony;
	}
}