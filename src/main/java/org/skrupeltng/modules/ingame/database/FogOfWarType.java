package org.skrupeltng.modules.ingame.database;

public enum FogOfWarType {

	NONE, FULL, VISITED, LONG_RANGE_SENSORS
}