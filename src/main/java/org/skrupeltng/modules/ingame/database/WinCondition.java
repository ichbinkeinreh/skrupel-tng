package org.skrupeltng.modules.ingame.database;

public enum WinCondition {

	NONE, SURVIVE, DEATH_FOE, TEAM_DEATH_FOE
}