package org.skrupeltng.modules.ingame.database.player;

import java.util.List;

import org.skrupeltng.modules.ingame.modules.overview.service.PlayerSummaryEntry;

public interface PlayerRepositoryCustom {

	void setupTurnValues(long gameId);

	List<PlayerSummaryEntry> getSummaries(long gameId);

	boolean playersTurnNotDoneForPlanet(long planetId, long loginId);

	boolean playersTurnNotDoneForShip(long shipId, long loginId);

	boolean playersTurnNotDoneForStarbase(long starbaseId, long loginId);

	boolean playersTurnNotDoneForRoute(long shipRouteEntryId, long loginId);

	boolean playersTurnNotDoneForOrbitalSystem(long orbitalSystemId, long loginId);

	boolean playersTurnNotDoneForFleet(long fleetId, long loginId);
}