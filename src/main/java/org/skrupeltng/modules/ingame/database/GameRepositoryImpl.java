package org.skrupeltng.modules.ingame.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.dashboard.GameListResult;
import org.skrupeltng.modules.dashboard.GameSearchParameters;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

public class GameRepositoryImpl extends RepositoryCustomBase implements GameRepositoryCustom {

	@Override
	public Page<GameListResult> searchGames(GameSearchParameters params, Pageable page) {
		String name = params.getName();
		WinCondition winCondition = params.getWinCondition();
		long loginId = params.getCurrentLoginId();
		Boolean started = params.getStarted();
		Boolean onlyOwnGames = params.getOnlyOwnGames();
		Boolean turnNotDone = params.getTurnNotDone();

		String join = "LEFT OUTER";

		if (onlyOwnGames != null && onlyOwnGames) {
			join = "INNER";
		}

		String sql = "" +
				"SELECT \n" +
				"	g.id, \n" +
				"	g.name, \n" +
				"	g.win_condition as \"winCondition\", \n" +
				"	g.created, \n" +
				"	COUNT(gp.id) as \"currentPlayerCount\", \n" +
				"	g.player_count as \"playerCount\", \n" +
				"	g.started, \n" +
				"	g.finished, \n" +
				"	g.round, \n" +
				"	COALESCE(p.turn_finished, false) as \"turnFinished\", \n" +
				"	(p.id IS NOT NULL) as \"playerOfGame\", \n" +
				"	COALESCE(p.faction_id IS NOT NULL, false) as \"factionSelected\", \n" +
				"	COALESCE(p.has_lost, false) as \"lost\", \n" +
				"	count(*) OVER() AS \"totalElements\" \n" +
				"FROM \n" +
				"	game g \n" +
				"	INNER JOIN player gp \n" +
				"		ON gp.game_id = g.id \n" +
				"	" + join + " JOIN player p \n" +
				"		ON p.game_id = g.id AND p.login_id = :loginId \n";

		Map<String, Object> parameters = new HashMap<>();
		parameters.put("loginId", loginId);

		List<String> wheres = new ArrayList<>();

		wheres.add("g.tutorial = false");

		if (!params.isAdmin()) {
			wheres.add("(g.is_private = false OR p.login_id = :loginId)");
		}

		if (onlyOwnGames != null && !onlyOwnGames) {
			wheres.add("p.id IS NULL");
		}

		if (started != null) {
			wheres.add("g.started = :started");
			parameters.put("started", started);
		}

		if (StringUtils.isNotBlank(name)) {
			wheres.add("g.name ILIKE :name");
			parameters.put("name", "%" + name + "%");
		}

		if (winCondition != null) {
			wheres.add("g.win_condition = :winCondition");
			parameters.put("winCondition", winCondition.name());
		}

		if (turnNotDone != null) {
			wheres.add("p.turn_finished = :turnNotDone");

			if (turnNotDone) {
				wheres.add("g.started = true AND g.finished = false");
			}

			parameters.put("turnNotDone", !turnNotDone);
		}

		if (!wheres.isEmpty()) {
			sql += "WHERE \n	" + StringUtils.join(wheres, " \n	AND ");
		}

		sql += "\n" +
				"GROUP BY \n" +
				"	g.id, \n" +
				"	g.name, \n" +
				"	g.win_condition, \n" +
				"	g.created, \n" +
				"	g.player_count, \n" +
				"	g.started, \n" +
				"	g.finished, \n" +
				"	g.round, \n" +
				"	p.turn_finished, \n" +
				"	p.faction_id, \n" +
				"	p.id";

		Sort sort = page.getSort();
		String orderBy = createOrderBy(sort, "g.created DESC");

		sql += orderBy;

		RowMapper<GameListResult> rowMapper = new BeanPropertyRowMapper<>(GameListResult.class);
		return search(sql, parameters, page, rowMapper);
	}

	@Override
	public Set<CoordinateImpl> getVisibilityCoordinates(long playerId) {
		String sql = "" +
				"SELECT \n" +
				"	p.x, \n" +
				"	p.y, \n" +
				"	p.scan_radius as \"scanRadius\" \n" +
				"FROM \n" +
				"	planet p \n" +
				"WHERE \n" +
				"	p.player_id = :playerId \n" +
				"" +
				"UNION \n" +
				"" +
				"SELECT \n" +
				"	s.x, \n" +
				"	s.y, \n" +
				"	s.scan_radius \"scanRadius\" \n" +
				"FROM \n" +
				"	ship s \n" +
				"WHERE \n" +
				"	s.player_id = :playerId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("playerId", playerId);

		RowMapper<CoordinateImpl> rowMapper = new BeanPropertyRowMapper<>(CoordinateImpl.class);
		return new HashSet<>(jdbcTemplate.query(sql, params, rowMapper));
	}

	@Override
	public List<PlayerGameTurnInfoItem> getTurnInfos(long loginId, long excludedGameId) {
		String sql = "" +
				"SELECT \n" +
				"	g.id as \"gameId\", \n" +
				"	g.name as \"gameName\", \n" +
				"	p.turn_finished as \"turnFinished\" \n" +
				"FROM \n" +
				"	player p \n" +
				"	INNER JOIN game g \n" +
				"		ON g.id = p.game_id \n" +
				"WHERE \n" +
				"	p.login_id = :loginId \n" +
				"	AND g.id != :excludedGameId \n" +
				"	AND g.started = true \n" +
				"	AND g.finished = false \n" +
				"	AND g.tutorial = false \n" +
				"ORDER BY \n" +
				"	g.name ASC";

		Map<String, Object> params = new HashMap<>(2);
		params.put("loginId", loginId);
		params.put("excludedGameId", excludedGameId);

		RowMapper<PlayerGameTurnInfoItem> rowMapper = new BeanPropertyRowMapper<>(PlayerGameTurnInfoItem.class);
		return jdbcTemplate.query(sql, params, rowMapper);
	}
}