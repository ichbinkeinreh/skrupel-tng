package org.skrupeltng.config;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class LoginDetails extends User {

	private static final long serialVersionUID = 3695448487866890929L;

	private final long id;
	private final boolean isAdmin;

	public LoginDetails(String username, String password, Collection<GrantedAuthority> authorities, long id, boolean isAdmin, boolean enabled) {
		super(username, password, enabled, true, true, true, authorities);
		this.id = id;
		this.isAdmin = isAdmin;
	}

	public long getId() {
		return id;
	}

	public boolean isAdmin() {
		return isAdmin;
	}
}