package org.skrupeltng.config;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.skrupeltng.modules.dashboard.Roles;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userDetailsService")
public class UserDetailServiceImpl implements UserDetailsService {

	@Autowired
	private LoginRepository loginRepository;

	@Override
	public LoginDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Login> loginOpt = loginRepository.findByUsername(username);

		if (loginOpt.isPresent()) {
			Login login = loginOpt.get();

			List<GrantedAuthority> authorities = login.getRoles().stream().map(r -> new SimpleGrantedAuthority(r.getRoleName()))
					.collect(Collectors.toList());
			boolean isAdmin = login.getRoles().stream().filter(r -> r.getRoleName().equals(Roles.ADMIN)).count() == 1;

			LoginDetails user = new LoginDetails(username, login.getPassword(), authorities, login.getId(), isAdmin, login.isActive());
			return user;
		}

		throw new UsernameNotFoundException(username);
	}

	public boolean isLoggedIn() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication != null && authentication.getPrincipal() instanceof LoginDetails;
	}

	private LoginDetails getLoginDetails() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		LoginDetails user = (LoginDetails)auth.getPrincipal();
		return user;
	}

	public long getLoginId() {
		LoginDetails user = getLoginDetails();
		return user.getId();
	}

	public boolean isAdmin() {
		LoginDetails user = getLoginDetails();
		return user.isAdmin();
	}
}