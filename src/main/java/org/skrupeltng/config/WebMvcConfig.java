package org.skrupeltng.config;

import java.nio.charset.StandardCharsets;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import nz.net.ultraq.thymeleaf.LayoutDialect;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	@Autowired
	private UserDetailServiceImpl userDetailService;

	@Bean
	public CookieLocaleResolver localeResolver() {
		CookieLocaleResolver localeResolver = new CookieLocaleResolver();
		localeResolver.setDefaultLocale(Locale.ENGLISH);
		return localeResolver;
	}

	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
		lci.setParamName("lang");
		return lci;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(localeChangeInterceptor());
	}

	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}

	@Bean
	public RememberMeServices rememberMeServices() {
		return new TokenBasedRememberMeServices("skrupel-tng-remember-me", userDetailService);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/factions/**").addResourceLocations("file:factions/", "classpath:static/factions/");
	}

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
		messageSource.setBasenames("classpath:/landingpage_messages", "classpath:/messages", "classpath:native_species_messages",
				"classpath:ship_ability_messages", "classpath:news_entry_messages", "classpath:orbital_system_messages", "classpath:help_planet_messages",
				"classpath:help_starbase_messages", "classpath:help_ship_messages", "classpath:help_propulsion_messages", "classpath:help_fleet_messages",
				"classpath:help_politics_messages", "classpath:help_game_options_messages", "classpath:help_native_species_messages",
				"classpath:help_infos_messages", "classpath:tutorial_messages", "classpath:email_messages", "classpath:password_messages",
				"classpath:achievement_messages");
		return messageSource;
	}
}