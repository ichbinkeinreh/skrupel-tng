import { ingame } from './ingame.js';

class OverviewTable {
	name;
	params;
	defaultSortField;
	defaultSortAscending;
	detailsPageName;

	constructor(name, defaultSortField, defaultSortAscending, detailsPageName) {
		this.name = name;
		this.defaultSortField = defaultSortField;
		this.defaultSortAscending = defaultSortAscending;
		this.detailsPageName = detailsPageName;

		this.initParams();

		const modalId = '#skr-ingame-' + this.name + '-modal';

		$(document).on('click', modalId + ' .skr-table-header a', event => {
			const key = $(event.target)
				.parent()
				.prop('id');

			if (this.params.sortField === key) {
				this.params.sortAscending = !this.params.sortAscending;
			} else {
				this.params.sortAscending = true;
			}

			this.params.sortField = key;

			this.updateTableContent();
		});

		$(document).on('click', modalId + ' .fa-search', () => {
			this.updateTableContent();
		});

		$(document).on('click', modalId + ' .fa-minus-circle', () => {
			this.initParams();

			$(modalId)
				.find('input')
				.each((index, elem) => {
					$(elem).val('');
				});

			$(modalId)
				.find('select')
				.each((index, elem) => {
					$(elem).val('');
				});

			this.updateTableContent();
		});

		$(document).on('change', modalId + ' input, ' + modalId + ' select', event => {
			this.updateParam(event.target);
		});

		$(document).on('keyup', modalId + ' input', event => {
			if (event.keyCode === 13) {
				this.updateTableContent();
			}
		});

		$(document).on('click', modalId + ' .skr-table-row', event => {
			let target = $(event.target);

			if (!target.hasClass('row')) {
				target = target.parent();
			}

			const id = target.prop('id');
			ingame.select(this.detailsPageName, id);
			$(modalId).modal('hide');
		});

		$(document).on('click', modalId + ' .pagination-number', event => {
			const page = parseInt($(event.target).text()) - 1;
			this.params.page = page;
			this.updateTableContent();
		});

		$(document).on('click', modalId + ' .pagination-prev', () => {
			const page = this.params.page - 1;

			if (page >= 0) {
				this.params.page = page;
				this.updateTableContent();
			}
		});

		$(document).on('click', modalId + ' .pagination-next', () => {
			const maxPage = parseInt($('#' + this.name + '-pagination-max-page').val());

			const page = this.params.page + 1;

			if (page < maxPage) {
				this.params.page = page;
				this.updateTableContent();
			}
		});
	}

	initParams() {
		const urlParams = new URLSearchParams(window.location.search);

		this.params = {
			gameId: parseInt(urlParams.get('id')),
			page: 0,
			pageSize: 15,
			sortField: this.defaultSortField,
			sortAscending: this.defaultSortAscending
		};
	}

	updateParam(target) {
		const elem = $(target);
		const key = elem.prop('id');
		const value = elem.val();

		this.params[key] = value;
	}

	updateTableContent() {
		const name = this.name;

		$.ajax({
			url: name,
			type: 'POST',
			data: JSON.stringify(this.params),
			contentType: 'application/json',
			success: function(response) {
				$('#skr-ingame-' + name + '-table-content').replaceWith(response);
			}
		});
	}
}

export const coloniesTable = new OverviewTable('colony-overview', 'COLONY_OVERVIEW_COLONISTS', false, 'planet');
export const shipsTable = new OverviewTable('ship-overview', 'SHIP_OVERVIEW_NAME', true, 'ship');
export const starbasesTable = new OverviewTable('starbase-overview', 'STARBASE_OVERVIEW_NAME', true, 'starbase');
export const fleetsTable = new OverviewTable('fleet-overview', 'FLEET_OVERVIEW_NAME', true, 'fleet');
