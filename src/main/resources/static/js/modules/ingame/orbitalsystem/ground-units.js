class GroundUnits {
	buildUnits(light) {
		const quantity = parseInt($('#skr-ingame-ground-units-quantity-input').val());
		const orbitalSystemId = parseInt($('#skr-ingame-orbitalsystem-id').val());

		const request = {
			orbitalSystemId: orbitalSystemId,
			light: light,
			quantity: quantity,
		};

		$.ajax({
			url: 'orbital-system/ground-units',
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(request),
			success: function() {
				ingame.refreshSelection(planet);
				ingame.updatePlanetMouseOver(ingame.getSelectedId());
				$('#skr-planet-selection').load('orbital-system?orbitalSystemId=' + orbitalSystemId);
			},
		});
	}
}

const groundUnits = new GroundUnits();
window.groundUnits = groundUnits;
