class ShipScanner {
	constructor() {
		$(document).on('click', '.skr-ingame-native-species-details-button', function() {
			const planetId = $(this)
				.prop('id')
				.replace('skr-ingame-native-species-details-button-', '');
			showCopyOfModal('#skr-ingame-native-species-details-' + planetId);
		});
	}

	showPlanetDetails(planetId) {
		$.ajax({
			url: 'ship/scanner/planet?shipId=' + ingame.getSelectedId() + '&scannedPlanetId=' + planetId,
			type: 'GET',
			success: response => {
				$('#skr-game-ship-scanner').replaceWith(response);
			},
		});
	}

	showShipDetails(shipId) {
		$.ajax({
			url: 'ship/scanner/ship?shipId=' + ingame.getSelectedId() + '&scannedShipId=' + shipId,
			type: 'GET',
			success: response => {
				$('#skr-game-ship-scanner').replaceWith(response);
			},
		});
	}

	showShipDetailsByOrbitalSystem(shipId, orbitalSystemId) {
		$.ajax({
			url: 'orbital-system/scanner/ship?orbitalSystemId=' + orbitalSystemId + '&scannedShipId=' + shipId,
			type: 'GET',
			success: response => {
				$('#skr-game-ship-scanner').replaceWith(response);
			},
		});
	}
}

const shipScanner = new ShipScanner();
window.shipScanner = shipScanner;
