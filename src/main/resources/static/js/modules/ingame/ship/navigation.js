import { ingame } from '../ingame.js';
import { tutorial } from '../tutorial.js';

class ShipNavigation {
	courseModeActive = false;
	courseSaved = false;

	constructor() {
		$(document).on('click', '#skr-ship-courseselect-checkbox', event => {
			this.courseModeActive = $(event.target).prop('checked');

			if (this.courseModeActive) {
				ingame.hideDetailsOfSmallDisplay();
				this.courseSaved = false;
			} else if (!this.courseSaved) {
				$('#skr-game-ship-destinationx-' + this.shipId).val(this.originDestinationX);
				$('#skr-game-ship-destinationy-' + this.shipId).val(this.originDestinationY);
				ingame.drawShipTravelLine(this.shipId, false);
				this.refreshSelection();
			}

			const textSource = this.courseModeActive ? 'skr-ship-courseselect-active-text' : 'skr-ship-courseselect-inactive-text';
			const text = $('#' + textSource).val();
			$('#skr-ship-courseselect-checkbox-label').text(text);
		});

		$(document).on('click', '.skr-game-planet', event => {
			if (this.courseModeActive) {
				event.stopPropagation();

				const target = $(event.target).parent();
				const x = this.getCoord(target.css('left'));
				const y = this.getCoord(target.css('top'));
				const name = target.attr('name');

				this.selectDestination(x, y, name);
			}
		});

		$(document).on('click', '.ship-in-space', event => {
			if (this.courseModeActive) {
				event.stopPropagation();

				const target = $(event.target);
				const x = this.getCoord(target.parent().css('left'));
				const y = this.getCoord(target.parent().css('top'));
				const name = target.attr('name');

				this.selectDestination(x, y, name);
			}
		});

		$(document).on('click', '.ship', event => {
			if (this.courseModeActive) {
				event.stopPropagation();

				const target = $(event.target);
				const x = this.getCoord(target.parent().css('left'));
				const y = this.getCoord(target.parent().css('top'));
				let name = target.attr('name');

				const names = name.split(';');

				if (names.length > 1) {
					for (let i in names) {
						const n = names[i];

						if (n != this.shipName) {
							name = n;
							break;
						}
					}
				}

				this.selectDestination(x, y, name);
			}
		});

		$(document).on('change', '#skr-ingame-navigation-content #skr-ship-courseselect-speed-select', () => {
			this.updateFuelConsumption();
		});

		$(document).on('click', '.skr-ingame-scan-circle-inner', event => {
			if (this.courseModeActive) {
				event.stopPropagation();
				const elem = $(event.target);
				const elemParent = elem.parent();

				const x = this.getCoord(elemParent.css('left')) + event.offsetX - elem.width() / 2;
				const y = this.getCoord(elemParent.css('top')) + event.offsetY - elem.height() / 2;
				const name = $('#skr-ship-courseselect-emptyspace-text').val();

				this.selectDestination(x, y, name);
			}
		});

		$(document).on('click', '.skr-game-wormhole', event => {
			if (this.courseModeActive) {
				event.stopPropagation();
				const elem = $(event.target);

				const x = this.getCoord(elem.css('left')) + event.offsetX - elem.width() / 2;
				const y = this.getCoord(elem.css('top')) + event.offsetY - elem.height() / 2;
				const name = $('#skr-ship-courseselect-emptyspace-text').val();

				this.selectDestination(x, y, name);
			}
		});

		$(document).on('click', '.skr-ingame-ship-scan-circle, .skr-ingame-planet-scan-circle, .skr-ingame-section-line', event => {
			if (this.courseModeActive) {
				event.stopPropagation();
				const elem = $(event.target);

				const x = this.getCoord(elem.css('left')) + event.offsetX;
				const y = this.getCoord(elem.css('top')) + event.offsetY;
				const name = $('#skr-ship-courseselect-emptyspace-text').val();

				this.selectDestination(x, y, name);
			}
		});

		$(document).on('click', '#skr-game-galaxy', event => {
			if (this.courseModeActive) {
				const elem = $(event.target);
				const x = event.offsetX;
				const y = event.offsetY;

				if (y <= elem.height() && x <= elem.width()) {
					const name = $('#skr-ship-courseselect-emptyspace-text').val();

					this.selectDestination(x, y, name);
				}
			}
		});
	}

	initData() {
		this.shipId = parseInt($('#skr-ship-id').val());

		this.mass = parseInt($('#skr-ship-courseselect-mass').val());
		this.shipName = $('#skr-ship-courseselect-shipname').val();
		this.shipX = parseInt($('#skr-ship-courseselect-shipx').val());
		this.shipY = parseInt($('#skr-ship-courseselect-shipy').val());

		this.originDestinationX = $('#skr-game-ship-destinationx-' + this.shipId).val();
		this.originDestinationY = $('#skr-game-ship-destinationy-' + this.shipId).val();

		this.destinationX = -1;
		this.destinationY = -1;

		this.fuelConsumptionData = $('#skr-ship-courseselect-fuelconsumptiondata')
			.val()
			.split(',');

		if (this.getSelectedSpeed() > 0) {
			const x = parseInt($('#skr-ship-courseselect-destinationx').val());
			const y = parseInt($('#skr-ship-courseselect-destinationy').val());

			if (x >= 0 && y >= 0) {
				this.destinationX = x;
				this.destinationY = y;

				const name = $('#skr-ship-courseselect-destination-name').val();

				$('#skr-ship-courseselect-coordinates').text(x + ' / ' + y);
				$('#skr-ship-courseselect-distance').text(this.calculateDistance());
				$('#skr-ship-courseselect-name').text(name);
				this.updateFuelConsumption();
				$('#skr-ship-courseselect-delete-course').prop('disabled', false);
			}
		}
	}

	getCoord(v) {
		return parseInt(v.replace('px', ''));
	}

	selectDestination(x, y, name) {
		if (this.courseModeActive) {
			this.destinationX = x;
			this.destinationY = y;

			$('#skr-ship-courseselect-coordinates').text(x + ' / ' + y);
			$('#skr-ship-courseselect-distance').text(this.calculateDistance());
			$('#skr-ship-courseselect-name').text(name);

			this.updateFuelConsumption();

			$('#skr-game-ship-destinationx-' + this.shipId).val(x);
			$('#skr-game-ship-destinationy-' + this.shipId).val(y);
			ingame.drawShipTravelLine(this.shipId, this.fuelTooLow());
		}
	}

	calculateDistance() {
		return Math.round(Math.sqrt(Math.pow(this.destinationX - this.shipX, 2) + Math.pow(this.destinationY - this.shipY, 2)));
	}

	getSelectedSpeed() {
		return parseInt($('#skr-ship-courseselect-speed-select').val());
	}

	getTravelMonths() {
		const distance = this.calculateDistance();
		const speed = this.getSelectedSpeed();
		return Math.ceil(distance / (speed * speed));
	}

	updateFuelConsumption() {
		const months = this.getTravelMonths();
		$('#skr-ship-courseselect-duration').text(months);

		const speed = this.getSelectedSpeed();
		const consumptionPerMonth = parseFloat(this.fuelConsumptionData[speed]);
		let consumption = 0;

		if (consumptionPerMonth > 0) {
			const distance = this.calculateDistance();
			consumption = consumptionPerMonth * (this.mass / 100000);

			if (months <= 1) {
				consumption = Math.floor(distance * consumption);
			} else {
				consumption = Math.floor(speed * speed * consumption);
			}

			if (consumption == 0) {
				consumption = 1;
			}

			consumption *= months;
		}

		const textElem = $('#skr-ship-courseselect-fuelconsumption');
		textElem.text(consumption);

		if (this.fuelTooLow()) {
			textElem.addClass('text-danger');
			textElem.addClass('font-weight-bold');
		} else {
			textElem.removeClass('text-danger');
			textElem.removeClass('font-weight-bold');
		}
	}

	getFuel() {
		return parseInt($('#skr-ship-courseselect-fuel').val());
	}

	getFuelConsumption(fuel) {
		let fuelConsumption = parseInt($('#skr-ship-courseselect-fuelconsumption').text());
		const propulsionLevel = parseInt($('#skr-ship-courseselect-propulsion-techlevel').val());

		if (propulsionLevel == 6 && fuelConsumption > fuel) {
			const mineral2 = parseInt($('#skr-ship-courseselect-mineral2').val());

			if (mineral2 + fuel >= fuelConsumption) {
				fuelConsumption = fuel;
			}
		}

		return fuelConsumption;
	}

	fuelTooLow() {
		const fuel = this.getFuel();
		const fuelConsumption = this.getFuelConsumption(fuel);

		return fuelConsumption > fuel;
	}

	setCourse() {
		if (this.fuelTooLow()) {
			window.alert($('#skr-ship-courseselect-notenoughfuel-text').val());
		} else {
			const targetName = $('#skr-ship-courseselect-name').text();
			const speed = this.getSelectedSpeed();

			const request = {
				x: this.destinationX,
				y: this.destinationY,
				speed: speed,
				targetName: targetName
			};

			$.ajax({
				url: 'ship/navigation?shipId=' + this.shipId,
				type: 'POST',
				contentType: 'application/json',
				data: JSON.stringify(request),
				success: response => {
					if (response.cycleDetected) {
						window.alert($('#skr-ship-courseselect-cycle-detected-text').val());
					} else {
						this.originDestinationX = this.destinationX;
						this.originDestinationY = this.destinationY;
						this.courseSaved = true;
						this.courseModeActive = false;
						this.refreshSelection();

						if (tutorial) {
							const shipTemplateId = $('#skr-ingame-ship-template-id').val();

							if (shipTemplateId === 'orion_2') {
								if (targetName !== 'Tau Ceti' || speed !== 7) {
									tutorial.revert('ACTIVATE_FREIGHTER_COURSE_MODE_FIRST');
								} else {
									tutorial.next('SET_COURSE_FREIGHTER_FIRST');
								}
							} else if (shipTemplateId === 'orion_14') {
								if (targetName !== 'Rigel' || speed !== 7) {
									tutorial.revert('SET_COURSE_TO_ENEMY');
								} else {
									tutorial.next('SET_COURSE_TO_ENEMY');
								}
							}
						}
					}
				}
			});
		}
	}

	refreshSelection() {
		if (window.location.href.indexOf('#fleet=') >= 0) {
			ingame.refreshSelection(fleet);
		} else {
			ingame.refreshSelection(ship);
		}
	}

	deleteCourse() {
		$.ajax({
			url: 'ship/navigation?shipId=' + this.shipId,
			type: 'DELETE',
			contentType: 'application/json',
			success: () => {
				this.originDestinationX = -1;
				this.originDestinationY = -1;
				this.courseModeActive = false;
				this.refreshSelection();
				ingame.clearShipTravelLine(this.shipId);
				ingame.clearShipDestinationMarker(this.shipId);
			}
		});
	}
}

export const shipNavigation = new ShipNavigation();
window.shipNavigation = shipNavigation;
