import { ingame } from '../ingame.js';
import { tutorial } from '../tutorial.js';
import { RangeLine } from '../range-line.js';

class ShipTransporter extends RangeLine {
	request;

	constructor() {
		super('#skr-game-ship-transporter');

		$(document).on('click', '#skr-colonist-warning-confirm-button', () => {
			this.doTransport(this.request);
		});
	}

	leftInputChanged(event) {
		super.leftInputChanged(event);
		this.updateTotalsLabels();
	}

	rightInputChanged(event) {
		super.rightInputChanged(event);
		this.updateTotalsLabels();
	}

	leftMaxButtonClicked(event) {
		const fieldName = this.getFieldName($(event.target));
		const slider = $('#slider-' + fieldName);
		const maxAvailable = parseInt(slider.prop('max'));

		if (fieldName === 'fuel') {
			const maxFuel = this.getMaxFuel();
			const newValue = maxAvailable - Math.min(maxAvailable, maxFuel);
			slider.val(newValue).trigger('input');
		} else if (fieldName === 'money') {
			slider.val(0).trigger('input');
		} else {
			const maxStorage = this.getMaxStorage();
			const currentTotalGoods = this.getStorageSum(this.createRequest());

			if (maxStorage > currentTotalGoods) {
				let factor = 1;

				if (fieldName === 'colonists') {
					factor = 0.01;
				} else if (fieldName === 'lightgroundunits') {
					factor = 0.3;
				} else if (fieldName === 'heavygroundunits') {
					factor = 1.5;
				}

				const currentValue = Math.round(parseInt(slider.val()) * factor);
				const maxPossibleDiff = maxStorage - currentTotalGoods;
				const maxTransportableDiff = Math.round(maxAvailable * factor);
				const nextValue = Math.round(currentValue - Math.min(maxPossibleDiff, maxTransportableDiff));
				const newValue = Math.round(nextValue / factor);

				slider.val(newValue).trigger('input');
			}
		}
	}

	updateLabels(elem) {
		super.updateLabels(elem);
		this.updateTotalsLabels();
	}

	updateTotalsLabels() {
		const request = this.createRequest();

		const maxFuel = this.getMaxFuel();
		const fuelTextElem = $('#skr-game-ship-transporter-fuel');
		fuelTextElem.text(request.fuel);

		if (maxFuel < request.fuel) {
			fuelTextElem.addClass('text-danger');
		} else {
			fuelTextElem.removeClass('text-danger');
		}

		const maxStorage = this.getMaxStorage();
		const totalGoods = this.getStorageSum(request);

		const goodsTextElem = $('#skr-game-ship-transporter-totalgoods');
		goodsTextElem.text(totalGoods);

		if (maxStorage < totalGoods) {
			goodsTextElem.addClass('text-danger');
		} else {
			goodsTextElem.removeClass('text-danger');
		}
	}

	getFieldName(elem) {
		const parts = elem.prop('id').split('-');
		return parts[parts.length - 1];
	}

	getMaxFuel() {
		return parseInt($('#skr-game-ship-transporter-fuelcapacity').text());
	}

	getMaxStorage() {
		return parseInt($('#skr-game-ship-transporter-storagespace').text());
	}

	createRequest() {
		const request = {
			fuel: $('#slider-fuel').prop('max') - $('#slider-fuel').val(),
			colonists: $('#slider-colonists').prop('max') - $('#slider-colonists').val(),
			money: $('#slider-money').prop('max') - $('#slider-money').val(),
			supplies: $('#slider-supplies').prop('max') - $('#slider-supplies').val(),
			mineral1: $('#slider-mineral1').prop('max') - $('#slider-mineral1').val(),
			mineral2: $('#slider-mineral2').prop('max') - $('#slider-mineral2').val(),
			mineral3: $('#slider-mineral3').prop('max') - $('#slider-mineral3').val(),
			lightGroundUnits: $('#slider-lightgroundunits').prop('max') - $('#slider-lightgroundunits').val(),
			heavyGroundUnits: $('#slider-heavygroundunits').prop('max') - $('#slider-heavygroundunits').val()
		};

		return request;
	}

	getStorageSum(request) {
		return Math.round(request.colonists / 100 + request.supplies + request.mineral1 + request.mineral2 + request.mineral3 + request.lightGroundUnits * 0.3 + request.heavyGroundUnits * 1.5);
	}

	finishTransport() {
		const request = this.createRequest();

		const maxFuel = this.getMaxFuel();
		const maxStorage = this.getMaxStorage();

		const sum = this.getStorageSum(request);

		const fuelDiff = request.fuel - maxFuel;
		const storageDiff = sum - maxStorage;

		if (fuelDiff > 0) {
			let msg = $('#skr-game-ship-transporter-fuelmessage').val();
			msg = msg.replace('{0}', fuelDiff);
			window.alert(msg);
		} else if (storageDiff > 0) {
			let msg = $('#skr-game-ship-transporter-storagemessage').val();
			msg = msg.replace('{0}', storageDiff);
			window.alert(msg);
		} else if (this.showColonistWarning(request)) {
			this.request = request;
			$('#skr-ingame-colonist-warning-modal').modal('show');
		} else {
			this.doTransport(request);
		}
	}

	showColonistWarning(request) {
		const ownPlanet = $('#skr-game-ship-transporter-is-own-planet').val() === 'true';

		if (ownPlanet) {
			const slider = $('#slider-colonists');
			const max = parseInt(slider.prop('max'));

			return max === request.colonists;
		}

		return false;
	}

	doTransport(request) {
		const planetId = parseInt($('#skr-game-ship-transporter-planet-id').val());

		$.ajax({
			url: 'ship/transporter?shipId=' + ingame.getSelectedId(),
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(request),
			success: function(response) {
				$('#skr-game-ship-transporter').replaceWith(response);

				ingame.updatePlanetMouseOver(planetId);
				ingame.updateShipMouseOver(ingame.getSelectedId());
				ingame.updateShipStatsDetails(ingame.getSelectedId());

				if (tutorial) {
					if (tutorial.getStage() === 'TRANSPORT_FOR_FREIGHTER' && request.fuel === 10 && request.colonists === 1000 && request.money === 100 && request.supplies === 10) {
						tutorial.next('TRANSPORT_FOR_FREIGHTER');
					} else if (tutorial.getStage() === 'TRANSPORT_TO_NEW_COLONY' && request.fuel === 0 && request.colonists === 0 && request.money === 0 && request.supplies === 0) {
						tutorial.next('TRANSPORT_TO_NEW_COLONY');
					} else if (tutorial.getStage() === 'TRANSPORT_BOMBER_FIRST' && request.fuel === 15) {
						tutorial.next('TRANSPORT_BOMBER_FIRST');
					}
				}
			}
		});
	}
}

export const shipTransporter = new ShipTransporter();
window.shipTransporter = shipTransporter;
