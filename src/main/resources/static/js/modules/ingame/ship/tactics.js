import { ingame } from '../ingame.js';

class Tactics {
	constructor() {
		$(document).on('input', '#skr-ingame-aggressiveness-slider', event => {
			const agg = parseInt($(event.target).val());
			$('#skr-ingame-ship-tactics-aggressiveness-label').text(agg + '%');
		});
	}

	save() {
		const request = {
			shipId: ingame.getSelectedId(),
			aggressiveness: parseInt($('#skr-ingame-aggressiveness-slider').val()),
			tactics: $('#skr-ingame-tactics-select').val()
		};

		$.ajax({
			url: 'ship/tactics',
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(request),
			success: function(response) {
				$('#skr-game-tactics-content').replaceWith(response);
			}
		});
	}
}

const tactics = new Tactics();
window.tactics = tactics;
