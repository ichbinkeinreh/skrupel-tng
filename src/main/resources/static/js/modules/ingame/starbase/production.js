import { ingame } from '../ingame.js';
import { tutorial } from '../tutorial.js';

class StarbaseProduction {
	produce(templateId, type) {
		const quantity = parseInt($('#skr-starbase-production-quantity-' + templateId).val());

		if (tutorial) {
			const stage = tutorial.getStage();

			if (stage !== 'BUILD_HULLS_FREIGHTER' && stage !== 'BUILD_PROPULSION_FREIGHTER' && stage !== 'BUILD_HULLS_BOMBER' && stage !== 'BUILD_PROPULSION_BOMBER' && stage !== 'BUILD_ENERGY_BOMBER') {
				return;
			}
			if (stage === 'BUILD_HULLS_FREIGHTER' && (templateId !== 'orion_2' || quantity !== 1)) {
				return;
			}
			if (stage === 'BUILD_PROPULSION_FREIGHTER' && (templateId !== 'solarisplasmotan' || quantity !== 1)) {
				return;
			}
			if (stage === 'BUILD_HULLS_BOMBER' && (templateId !== 'orion_14' || quantity !== 1)) {
				return;
			}
			if (stage === 'BUILD_PROPULSION_BOMBER' && (templateId !== 'solarisplasmotan' || quantity !== 2)) {
				return;
			}
			if (stage === 'BUILD_ENERGY_BOMBER' && quantity !== 4) {
				return;
			}
		}

		const request = {
			templateId: templateId,
			type: type,
			quantity: quantity
		};

		$.ajax({
			url: 'starbase/produce?starbaseId=' + ingame.getSelectedId(),
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(request),
			success: function() {
				ingame.refreshSelection(starbase);
				
				const planetId = parseInt($('#skr-ingame-starbase-production-planet-id').val());
				ingame.updatePlanetMouseOver(planetId);

				if (tutorial) {
					const stage = tutorial.getStage();

					if (stage === 'BUILD_HULLS_FREIGHTER' && templateId === 'orion_2') {
						tutorial.next('BUILD_HULLS_FREIGHTER');
					} else if (stage === 'BUILD_PROPULSION_FREIGHTER' && templateId === 'solarisplasmotan') {
						tutorial.next('BUILD_PROPULSION_FREIGHTER');
					} else if (stage === 'BUILD_HULLS_BOMBER' && templateId === 'orion_14') {
						tutorial.next('BUILD_HULLS_BOMBER');
					} else if (stage === 'BUILD_PROPULSION_BOMBER' && templateId === 'solarisplasmotan') {
						tutorial.next('BUILD_PROPULSION_BOMBER');
					} else if (stage === 'BUILD_ENERGY_BOMBER') {
						tutorial.next('BUILD_ENERGY_BOMBER');
					}
				}
			},
		});
	}
}

export const starbaseProduction = new StarbaseProduction();
window.starbaseProduction = starbaseProduction;
