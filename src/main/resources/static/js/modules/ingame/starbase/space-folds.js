import { ingame } from '../ingame.js';
import { RangeLine } from '../range-line.js';

class SpaceFolds extends RangeLine {
	selectionActive = false;

	constructor() {
		super('#skr-ingame-space-folds-content');

		$(document).on('click', '.skr-game-planet', event => {
			if (this.selectionActive) {
				event.stopPropagation();
				const planetId = parseInt($(event.target).attr('planetid'));
				this.updateSelectedItem('planet_' + planetId);
			}
		});

		$(document).on('click', '.ship-in-space', event => {
			if (this.selectionActive) {
				event.stopPropagation();

				if ($(event.target).hasClass('owned-ship')) {
					const shipId = parseInt(
						$(event.target)
							.parent()
							.attr('shipid')
					);
					this.updateSelectedItem('ship_' + shipId);
				}
			}
		});

		$(document).on('click', '.ship', event => {
			if (this.selectionActive) {
				event.stopPropagation();
				const shipId = parseInt($(event.target).attr('shipid'));
				this.updateSelectedItem('ship_' + shipId);
			}
		});

		$(document).on('click', '.skr-ingame-ship-mouse-over-ship-item', event => {
			if (this.selectionActive) {
				event.stopPropagation();
				const shipId = parseInt(
					$(event.target)
						.prop('id')
						.replace('skr-ingame-ship-mouseover-ship-id-', '')
				);
				this.updateSelectedItem('ship_' + shipId);
				$('.skr-game-details-mouseover').hide();
			}
		});

		$(document).on('change', '#skr-ingame-spacefolds-target-select', function() {
			const id = $(this).val();
			const image = $('#' + id).attr('image');

			$('#skr-ingame-spacefolds-target-image').attr('src', image);
		});
	}

	updateLabels(elem) {
		super.updateLabels(elem);

		const costs = this.getCosts(this.createRequest());
		$('#skr-ingame-spacefolds-money-costs-label').text(costs);

		if (costs > this.getAvailableMoney()) {
			$('#skr-ingame-spacefolds-money-costs-wrapper').addClass('text-danger');
			$('#skr-ingame-starbase-init-space-fold-button').attr('disabled', true);
		} else {
			$('#skr-ingame-spacefolds-money-costs-wrapper').removeClass('text-danger');
			$('#skr-ingame-starbase-init-space-fold-button').attr('disabled', false);
		}
	}

	selectByGalaxyMap() {
		if (this.selectionActive) {
			this.selectionActive = false;
			this.deactivateSelection();
		} else {
			this.selectionActive = true;
			ingame.hideDetailsOfSmallDisplay();
			$('#skr-space-fold-select-by-map-button').addClass('btn-danger');
		}
	}

	updateSelectedItem(id) {
		$('#skr-ingame-spacefolds-target-select')
			.val(id)
			.trigger('change');
		this.deactivateSelection();
	}

	deactivateSelection() {
		this.selectionActive = false;
		$('#skr-space-fold-select-by-map-button').removeClass('btn-danger');
	}

	initSpaceFold() {
		const request = this.createRequest();
		const availableMoney = this.getAvailableMoney();
		const costs = this.getCosts(request);

		if (costs > availableMoney - request.money) {
			let msg = $('#skr-ingame-spacefolds-costs-message').val();
			msg = msg.replace('{0}', costs);
			window.alert(msg);
		} else {
			const targetValue = $('#skr-ingame-spacefolds-target-select').val();
			const parts = targetValue.split('_');

			if (parts.length !== 2) {
				const msg = $('#skr-ingame-spacefolds-select-target-message').val();
				window.alert(msg);
			} else {
				const targetId = parseInt(parts[1]);

				if (parts[0] === 'ship') {
					request.shipId = targetId;
				} else {
					request.planetId = targetId;
				}

				$.ajax({
					url: 'starbase/space-folds?starbaseId=' + ingame.getSelectedId(),
					type: 'POST',
					contentType: 'application/json',
					data: JSON.stringify(request),
					success: function(response) {
						const planetId = parseInt($('#skr-ingame-spacefolds-planet-id').val());
						ingame.updatePlanetMouseOver(planetId);

						ingame.updateStarbaseStatsDetails(ingame.getSelectedId());
						$('#skr-ingame-space-folds-content').replaceWith(response);
					}
				});
			}
		}
	}

	getAvailableMoney() {
		return $('#skr-ingame-spacefolds-available-money').val();
	}

	createRequest() {
		return {
			money: parseInt($('#slider-money').val()),
			supplies: parseInt($('#slider-supplies').val()),
			fuel: parseInt($('#slider-fuel').val()),
			mineral1: parseInt($('#slider-mineral1').val()),
			mineral2: parseInt($('#slider-mineral2').val()),
			mineral3: parseInt($('#slider-mineral3').val())
		};
	}

	getCosts(request) {
		return (request.supplies + request.fuel + request.mineral1 + request.mineral2 + request.mineral3) * 8 + 75;
	}
}

export const spaceFolds = new SpaceFolds();
window.spaceFolds = spaceFolds;
