import { skrupel } from '../../skrupel_tng.js';
import { shipNavigation } from './ship/navigation.js';
import { route } from './ship/route.js';
import { spaceFolds } from './starbase/space-folds.js';
import { tutorial } from './tutorial.js';
import { coloniesTable, shipsTable, starbasesTable } from './overview-table.js';

const gameId = new URLSearchParams(window.location.search).get('id');

class Ingame {
	keepShipsMouseOver = false;
	detailsVisible = false;

	shipIdForFleetCreation;

	selectedPage;
	selectedId;

	secondHashAlreadyLoaded = false;

	constructor() {
		$(window).resize(() => {
			this.resizeGalaxy();

			if (this.smallDisplay()) {
				this.updateDetailsContent();
			}
		});

		$(document).on('click', '#skr-ingame-expand-details-button', () => {
			this.detailsVisible = !this.detailsVisible;
			this.updateDetailsContent();
		});

		$(document).on('click', '#skr-ingame-navbar-toggle-button', () => {
			this.hideDetailsOfSmallDisplay();
		});

		$(document).on('click', '.skr-ingame-ship-template-details-button', event => {
			const shipTemplateId = $(event.target)
				.prop('id')
				.replace('skr-ingame-ship-template-details-button-', '');

			$.ajax({
				url: 'ship/ship-template?shipTemplateId=' + shipTemplateId,
				type: 'GET',
				contentType: 'application/json',
				success: function(response) {
					const content = $('#skr-ingame-ship-template-details-' + shipTemplateId + ' .skr-ingame-ship-template-details-content');
					content.empty();
					content.append(response);
					showCopyOfModal('#skr-ingame-ship-template-details-' + shipTemplateId);

					if (tutorial && tutorial.getStage() === 'OPEN_BOMBER_DETAILS' && shipTemplateId === 'orion_14') {
						tutorial.next('OPEN_BOMBER_DETAILS');
					}
				}
			});
		});

		$(document).on('click', '#skr-ingame-overview-news-tab', () => {
			$.ajax({
				url: 'news-viewed?gameId=' + new URLSearchParams(window.location.search).get('id'),
				type: 'POST',
				contentType: 'application/json',
				success: function() {
					$('#skr-ingame-overview-news-tab-badge').hide();
				}
			});
		});

		$(document).on('mouseenter', '.skr-game-planet-mouse', event => {
			const planetId = $(event.target).attr('planetid');
			const modal = $('#skr-ingame-planet-details-' + planetId);
			modal.show();
			this.correctMouseOverPosition(modal);
		});

		$(document).on('mouseleave', '.skr-game-planet-mouse', event => {
			const planetId = $(event.target).attr('planetid');
			$('#skr-ingame-planet-details-' + planetId).hide();
		});

		$(document).on('mouseenter', '.skr-game-ship-mouse-space', event => {
			const shipId = $(event.target)
				.parent()
				.attr('shipid');
			const modal = $('#skr-ingame-ship-details-' + shipId);
			modal.show();
			this.correctMouseOverPosition(modal);
		});

		$(document).on('mouseleave', '.skr-game-ship-mouse-space', event => {
			const shipId = $(event.target)
				.parent()
				.attr('shipid');
			$('#skr-ingame-ship-details-' + shipId).hide();
		});

		$(document).on('mouseenter', '.skr-game-ship-mouse-planet', event => {
			const shipId = $(event.target).attr('shipid');
			const modal = $('#skr-ingame-ship-details-' + shipId);
			modal.show();
			this.correctMouseOverPosition(modal);
		});

		$(document).on('mouseleave', '.skr-game-ship-mouse-planet', event => {
			const shipId = $(event.target).attr('shipid');
			$('#skr-ingame-ship-details-' + shipId).hide();
		});

		$(document).on('contextmenu', '.skr-game-ships-mouse, .skr-game-ship-cluster-mouse', event => {
			this.keepShipsMouseOver = true;
			return false;
		});

		$(document).on('click', () => {
			if (this.keepShipsMouseOver) {
				$('.skr-game-details-mouseover').hide();
			}

			this.keepShipsMouseOver = false;
		});

		$(document).on('click', '.skr-ingame-ship-mouse-over-ship-item', event => {
			if (!spaceFolds || !spaceFolds.selectionActive) {
				const eventTarget = $(event.target);
				const idTarget = eventTarget.hasClass('col-12') ? eventTarget.parent() : eventTarget.parent().parent();

				const shipId = parseInt(idTarget.prop('id').replace('skr-ingame-ship-mouseover-ship-id-', ''));
				this.select('ship', shipId);
			}
		});

		$(document).on('mouseenter', '.skr-game-ships-mouse', event => {
			const planetId = $(event.target).attr('planetid');
			const modal = $('#skr-ingame-ships-details-' + planetId);
			modal.show();
			this.correctMouseOverPosition(modal);
		});

		$(document).on('mouseleave', '.skr-game-ships-mouse', event => {
			if (!this.keepShipsMouseOver) {
				const planetId = $(event.target).attr('planetid');
				$('#skr-ingame-ships-details-' + planetId).hide();
			}
		});

		$(document).on('mouseenter', '.skr-game-starbase-mouse', event => {
			const starbaseId = $(event.target).attr('starbaseid');
			const modal = $('#skr-ingame-starbase-details-' + starbaseId);
			modal.show();
			this.correctMouseOverPosition(modal);
		});

		$(document).on('mouseleave', '.skr-game-starbase-mouse', event => {
			const starbaseId = $(event.target).attr('starbaseid');
			$('#skr-ingame-starbase-details-' + starbaseId).hide();
		});

		$(document).on('mouseenter', '.skr-game-ship-cluster-mouse', event => {
			const coordId = $(event.target)
				.parent()
				.attr('coordId');
			const modal = $('#skr-ingame-ships-details-' + coordId);
			modal.show();
			this.correctMouseOverPosition(modal);
		});

		$(document).on('mouseleave', '.skr-game-ship-cluster-mouse', event => {
			if (!this.keepShipsMouseOver) {
				const coordId = $(event.target)
					.parent()
					.attr('coordId');
				$('#skr-ingame-ships-details-' + coordId).hide();
			}
		});

		$(document).on('click', '#skr-game-mini-map', event => {
			const galaxySize = parseInt($('#skr-ingame-galaxy-size').val());
			const parentWidth = $(event.target).width();
			const factor = galaxySize / parentWidth;

			const x = event.offsetX * factor;
			const y = event.offsetY * factor;

			const wrapper = $('#skr-game-galaxy-wrapper');
			wrapper.scrollLeft(x - wrapper.innerWidth() / 2);
			wrapper.scrollTop(y - wrapper.innerHeight() / 2);

			$('#skr-ingame-overview-modal').modal('hide');
		});
	}

	resizeGalaxy() {
		skrupel.resize('#skr-game-galaxy-wrapper', ['#header', '#skr-game-selection-wrapper']);
	}

	getSelectedId() {
		return parseInt(this.selectedId);
	}

	updateDetailsContent() {
		const iconElem = $('#skr-ingame-expand-details-button i');

		if (this.detailsVisible) {
			iconElem.removeClass('fa-angle-up');
			iconElem.addClass('fa-angle-down');
			$('#skr-ingame-expand-details-button #collapse').show();
			$('#skr-ingame-expand-details-button #expand').hide();

			$('#skr-game-selection').show();

			const headerHeight = parseInt($('#header').outerHeight());
			const maxHeight = window.innerHeight - headerHeight;
			$('#skr-game-selection-wrapper').height(maxHeight + 'px');
			$('.skr-ingame-selection-panel').height(maxHeight - 33 + 'px');
		} else {
			iconElem.removeClass('fa-angle-down');
			iconElem.addClass('fa-angle-up');
			$('#skr-ingame-expand-details-button #collapse').hide();
			$('#skr-ingame-expand-details-button #expand').show();

			if (this.smallDisplay()) {
				$('#skr-game-selection').hide();
				$('#skr-game-selection-wrapper').height('33px');
			} else {
				$('#skr-game-selection-wrapper').height('163px');
				$('.skr-ingame-selection-panel').height('130px');
			}
		}

		this.resizeGalaxy();
	}

	hideDetailsOfSmallDisplay() {
		if (this.smallDisplay()) {
			this.detailsVisible = false;
			this.updateDetailsContent();
		}
	}

	smallDisplay() {
		return $('#skr-ingame-navbar-toggle-button').is(':visible');
	}

	collapseNavbar() {
		if (this.smallDisplay() && $('#skr-ingame-nav-bar').is(':visible')) {
			$('#skr-ingame-navbar-toggle-button').click();
		}
	}

	showOverview() {
		$('#skr-ingame-overview-modal-wrapper').load('overview?gameId=' + gameId, () => {
			$('#skr-ingame-overview-modal').modal('show');
			this.collapseNavbar();
		});
	}

	showInfos() {
		$('#skr-ingame-infos-modal-wrapper').load('infos', () => {
			$('#skr-ingame-infos-modal').modal('show');
		});
	}

	finishTurn() {
		$.ajax({
			url: 'turn?gameId=' + gameId,
			type: 'POST',
			contentType: 'application/json',
			success: response => {
				$('#skr-ingame-end-turn-button').hide();
				$('#skr-ingame-turn-finished-button').show();

				if (response) {
					$('#skr-ingame-round-calculation-modal').modal('show');
					$.ajax({
						url: 'round?gameId=' + gameId,
						type: 'POST',
						contentType: 'application/json',
						success: function() {
							window.location.reload();
						}
					});
				} else {
					window.location.hash = '';
					this.select('turn-finished', 0);
				}
			}
		});
	}

	selectionIsNotActive() {
		return (!shipNavigation || !shipNavigation.courseModeActive) && (!route || !route.planetSelectionActive) && (!spaceFolds || !spaceFolds.selectionActive);
	}

	select(page, id, updateHash = true, ingamePageForRefresh) {
		this.collapseNavbar();
		const oldPage = this.selectedPage;

		if (this.selectionIsNotActive()) {
			this.selectedPage = page;
			this.selectedId = id;

			let url = page + '?' + page + 'Id=' + id;

			if (page === 'turn-finished') {
				url = 'turn-finished';
			}

			$('#skr-game-selection').load(url, () => {
				if (!ingamePageForRefresh) {
					this.updateScrollAndPing();
				}

				if (updateHash === true) {
					window.location.hash = this.getMainUrlHash();
				} else if (ingamePageForRefresh) {
					ingamePageForRefresh.show(ingame.getSecondUrlHash());
				}

				if (this.smallDisplay()) {
					this.detailsVisible = true;
					this.updateDetailsContent();
				}
			});

			if (tutorial && (tutorial.getStage() !== 'INITIAL_OPEN_STARBASE' || (page !== 'starbase' && oldPage === 'planet'))) {
				tutorial.checkStage();
			}
		}
	}

	showShipSelection(x, y) {
		if ((!shipNavigation || !shipNavigation.courseModeActive) && (!route || !route.planetSelectionActive) && (!spaceFolds || !spaceFolds.selectionActive)) {
			const url = 'ship-selection?gameId=' + gameId + '&x=' + x + '&y=' + y;

			$('#skr-game-selection').load(url, () => {
				window.location.hash = 'ship-selection';
				this.updateScrollAndPing();
			});
		}
	}

	updateScrollAndPing() {
		const wrapper = $('#skr-game-galaxy-wrapper');

		const x = parseInt($('#skr-ingame-current-x').val());
		const y = parseInt($('#skr-ingame-current-y').val());

		wrapper.scrollLeft(x - wrapper.innerWidth() / 2);

		const maxScrollY = parseInt($('#skr-game-galaxy').innerHeight()) - 260;

		let wrapperY = y - wrapper.innerHeight() / 2;

		if (wrapperY > maxScrollY) {
			wrapperY = maxScrollY;
		}

		wrapper.scrollTop(wrapperY);

		$('#skr-ingame-selection-ping-wrapper').css({ left: x, top: y, display: 'block' });
	}

	setSecondUrlHash(page) {
		window.location.hash = this.getMainUrlHash() + ';' + page;

		if (tutorial) {
			tutorial.checkStage();
		}
	}

	getMainUrlHash() {
		return this.selectedPage + '=' + this.selectedId;
	}

	getSecondUrlHash() {
		const hash = window.location.hash;

		if (hash) {
			const parts = hash.replace('#', '').split(';');

			if (parts.length == 2) {
				return parts[1].split('=');
			}
		}

		return null;
	}

	refreshSelection(ingamePage) {
		this.select(this.selectedPage, this.selectedId, false, ingamePage);
	}

	unselect() {
		$('#skr-game-selection').empty();
		this.selectedId = null;
		this.selectedPage = null;
		window.location.hash = '';
	}

	openModal(modalName) {
		this.collapseNavbar();
		const modalId = '#skr-ingame-' + modalName + '-modal';

		if ($(modalId).html().length > 0) {
			$(modalId).modal('show');
		} else {
			$(modalId).load(modalName + '?gameId=' + gameId, () => {
				$(modalId).modal('show');
			});
		}
	}

	drawShipTravelLine(shipId, fuelTooLow) {
		const destinationX = parseInt($('#skr-game-ship-destinationx-' + shipId).val());
		const destinationY = parseInt($('#skr-game-ship-destinationy-' + shipId).val());
		const x = parseInt($('#skr-game-ship-x-' + shipId).val());
		const y = parseInt($('#skr-game-ship-y-' + shipId).val());
		const color = '#' + (fuelTooLow ? 'bebebe' : $('#skr-game-playercolor').val());

		this.drawShipTravelLineWithParams(shipId, destinationX, destinationY, x, y, color, '.skr-game-ship-travel-line-wrapper', 'dotted');
		this.drawShipDestinationMarker(shipId);
	}

	drawShipOriginLine(shipId) {
		const lastX = parseInt($('#skr-game-ship-lastx-' + shipId).val());
		const lastY = parseInt($('#skr-game-ship-lasty-' + shipId).val());
		const x = parseInt($('#skr-game-ship-x-' + shipId).val());
		const y = parseInt($('#skr-game-ship-y-' + shipId).val());

		if ((lastX > 0 || lastY > 0) && (lastX !== x || lastY !== y)) {
			this.drawShipTravelLineWithParams(shipId, lastX, lastY, x, y, '#474747', '.skr-game-ship-origin-line-wrapper', 'dashed');
		}
	}

	drawShipTravelLineWithParams(shipId, destinationX, destinationY, x, y, color, wrapperClass, style) {
		if (destinationX >= 0 || destinationY >= 0) {
			const elem = this.createLine(x, y, destinationX, destinationY, color, style);

			const wrapper = $('#skr-game-ship-' + shipId + ' ' + wrapperClass);
			wrapper.empty();
			wrapper.append(elem);
		} else {
			this.clearShipTravelLine(shipId);
		}
	}

	drawShipDestinationMarker(shipId) {
		const destinationX = parseInt($('#skr-game-ship-destinationx-' + shipId).val());
		const destinationY = parseInt($('#skr-game-ship-destinationy-' + shipId).val());
		const color = '#' + $('#skr-game-playercolor').val();

		const elem = document.createElement('div');
		elem.className = 'ship-destination-marker';
		elem.style.left = destinationX - 11 + 'px';
		elem.style.top = destinationY - 11 + 'px';
		elem.style.borderColor = color;
		const wrapperClass = '.skr-game-ship-destination-marker-wrapper';

		const wrapper = $('#skr-game-ship-' + shipId + ' ' + wrapperClass);

		if (destinationX >= 0 || destinationY >= 0) {
			const wrapper = $('#skr-game-ship-' + shipId + ' ' + wrapperClass);
			wrapper.empty();
			wrapper.append(elem);
		} else {
			wrapper.empty();
		}
	}

	drawShipRouting(shipId) {
		const routeEntries = $('#skr-game-ship-' + shipId + ' .skr-game-ship-route-entry');

		if (routeEntries.length > 1) {
			let lastX = -1;
			let lastY = -1;

			let firstX = -1;
			let firstY = -1;
			let firstRouteId = null;

			for (let routeEntry of routeEntries) {
				const coords = $(routeEntry)
					.val()
					.split('_');
				const x = parseInt(coords[0]);
				const y = parseInt(coords[1]);
				const routeId = $(routeEntry)
					.prop('id')
					.replace('skr-game-ship-route-', '');

				if (firstX === -1) {
					firstX = x;
					firstY = y;
					firstRouteId = routeId;
				} else {
					this.drawShipTravelLineWithParams(shipId, x, y, lastX, lastY, 'gray', '.skr-game-ship-route-wrapper-' + routeId, 'dotted');
				}

				lastX = x;
				lastY = y;
			}

			this.drawShipTravelLineWithParams(shipId, firstX, firstY, lastX, lastY, 'gray', '.skr-game-ship-route-wrapper-' + firstRouteId, 'dotted');
		}
	}

	clearShipDestinationMarker(shipId) {
		const wrapperClass = '.skr-game-ship-destination-marker-wrapper';
		const wrapper = $('#skr-game-ship-' + shipId + ' ' + wrapperClass);
		wrapper.empty();
	}

	clearShipTravelLine(shipId) {
		const wrapper = $('#skr-game-ship-' + shipId + ' .skr-game-ship-travel-line-wrapper');
		wrapper.empty();
	}

	createLine(x1, y1, x2, y2, color, style) {
		const a = x1 - x2;
		const b = y1 - y2;
		const c = Math.sqrt(a * a + b * b);

		const sx = (x1 + x2) / 2;
		const sy = (y1 + y2) / 2;

		const x = sx - c / 2;
		const y = sy;

		const alpha = Math.PI - Math.atan2(-b, a);

		return this.createLineElement(x, y, c, alpha, color, style);
	}

	createLineElement(x, y, length, angle, color, style) {
		const line = document.createElement('div');
		const styles =
			'border: 1px ' +
			style +
			' ' +
			color +
			'; ' +
			'width: ' +
			length +
			'px; ' +
			'height: 0px; ' +
			'-moz-transform: rotate(' +
			angle +
			'rad); ' +
			'-webkit-transform: rotate(' +
			angle +
			'rad); ' +
			'-o-transform: rotate(' +
			angle +
			'rad); ' +
			'-ms-transform: rotate(' +
			angle +
			'rad); ' +
			'position: absolute; ' +
			'top: ' +
			y +
			'px; ' +
			'left: ' +
			x +
			'px; ' +
			'z-index: 50; ';

		line.setAttribute('style', styles);
		return line;
	}

	toggleNewsDelete(newsEntryId) {
		$.ajax({
			url: 'overview/toggle-news-delete?newsEntryId=' + newsEntryId,
			type: 'POST',
			contentType: 'application/json',
			success: function(newValue) {
				const id = newValue ? '#skr-overview-will-be-deleted-text' : '#skr-overview-will-not-be-deleted-text';
				const text = $(id).val();
				$('#skr-overview-toggle-news-delete-button-' + newsEntryId).html(text);
			}
		});
	}

	performPlayerRelationAction(playerId, action) {
		const request = {
			gameId,
			playerId,
			action
		};

		$.ajax({
			url: 'overview/player-relation',
			type: 'POST',
			data: JSON.stringify(request),
			contentType: 'application/json',
			success: function(newContent) {
				$('#skr-overview-politics-content').replaceWith(newContent);
			}
		});
	}

	acceptPlayerRelation(id) {
		this.processPlayerRelation(id, 'PUT');
	}

	rejectPlayerRelation(id) {
		this.processPlayerRelation(id, 'DELETE');
	}

	processPlayerRelation(id, type) {
		$.ajax({
			url: 'overview/player-relation?id=' + id,
			type: type,
			contentType: 'application/json',
			success: function(newContent) {
				$('#skr-overview-politics-content').replaceWith(newContent);
			}
		});
	}

	updatePlanetMouseOver(planetId) {
		if ($('#skr-ingame-planet-mouseover-content-' + planetId).length > 0) {
			const container = $('#skr-ingame-planet-details-' + planetId);
			container.empty();
			container.load('/ingame/planet-mouse-over?planetId=' + planetId);

			coloniesTable.updateTableContent();
		}
	}

	updateShipMouseOver(shipId) {
		let container = $('#skr-ingame-ship-details-' + shipId);

		if (container.length) {
			container.empty();
			container.load('/ingame/ship-mouse-over?shipId=' + shipId);
		} else {
			let container = $('#skr-ingame-ship-mouseover-ship-id-' + shipId);
			container.empty();
			container.load('/ingame/ships-mouse-over-item?shipId=' + shipId);
		}

		shipsTable.updateTableContent();
	}

	updateStarbaseMouseOver(starbaseId) {
		const container = $('#skr-ingame-starbase-details-' + starbaseId);
		container.empty();
		container.load('/ingame/starbase-mouse-over?starbaseId=' + starbaseId);

		starbasesTable.updateTableContent();
	}

	updateShipStatsDetails(shipId) {
		const container = $('#skr-ingame-ship-stats-details');
		container.empty();
		container.load('ship/stats-details?shipId=' + shipId);
	}

	updateStarbaseStatsDetails(starbaseId) {
		const container = $('#skr-ingame-starbase-stats-details');
		container.empty();
		container.load('starbase/stats-details?starbaseId=' + starbaseId);
		1;
	}

	updateShipTravelData(shipId) {
		if (!shipId) {
			shipId = this.getSelectedId();
		}

		$.ajax({
			url: 'ship/travel-data?shipId=' + shipId,
			type: 'GET',
			contentType: 'application/json',
			success: response => {
				this.drawShipTravelLineWithParams(shipId, response.destinationX, response.destinationY, response.x, response.y, response.color, '.skr-game-ship-travel-line-wrapper', 'dotted');
				this.updateShipMouseOver(shipId);
				this.updateShipStatsDetails(shipId);

				$('#skr-game-ship-route-container-' + shipId).load('ship/route-data?shipId=' + shipId, () => {
					this.drawShipRouting(shipId);
				});
			}
		});
	}

	messageToPlayer(recipientId, recipientName) {
		if ((!shipNavigation || !shipNavigation.courseModeActive) && (!route || !route.planetSelectionActive) && (!spaceFolds || !spaceFolds.selectionActive)) {
			$('#skr-ingame-send-message-modal-player-name-title').text(recipientName);
			$('#skr-ingame-send-message-recipient-id-input').val(recipientId);
			$('#skr-ingame-send-message-modal-message-input').val('');
			$('#skr-ingame-send-message-modal').modal('show');
		}
	}

	sendMessage() {
		const recipientId = parseInt($('#skr-ingame-send-message-recipient-id-input').val());
		const message = $('#skr-ingame-send-message-modal-message-input').val();

		const request = {
			recipientId: recipientId,
			message: message,
			gameId: gameId
		};

		$.ajax({
			url: 'overview/message',
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(request),
			success: function() {
				$('#skr-ingame-send-message-modal').modal('hide');
			}
		});
	}

	correctMouseOverPosition(modal) {
		if (modal && modal.parent() && modal.parent().offset()) {
			const map = $('#skr-game-galaxy-wrapper');
			const modalHeight = modal.height();
			const modalWidth = modal.width();

			if (map.height() - modal.parent().offset().top < modalHeight) {
				modal.css('top', '-' + (modalHeight + 15) + 'px');
			} else {
				modal.css('top', '');
			}

			if (map.width() - modal.parent().offset().left < modalWidth) {
				modal.css('left', '-' + modalWidth + 'px');
			} else {
				modal.css('left', '');
			}
		}
	}

	createNewFleet(useShipId) {
		this.shipIdForFleetCreation = null;

		if (useShipId) {
			this.shipIdForFleetCreation = this.getSelectedId();
		}

		$('#skr-ingame-fleet-overview-modal').modal('hide');

		$('#skr-game-selection').load('fleet', () => {});
	}
}

export const ingame = new Ingame();
window.ingame = ingame;

export class IngamePage {
	name;

	constructor(name) {
		this.name = name;

		if (!ingame.secondHashAlreadyLoaded) {
			const value = ingame.getSecondUrlHash();

			if (value) {
				this.show(value);
				ingame.secondHashAlreadyLoaded = true;
			}
		}
	}

	show(page) {
		if (ingame.selectionIsNotActive()) {
			const url = this.name + '/' + page + '?' + this.name + 'Id=' + ingame.getSelectedId();

			$('#skr-' + this.name + '-selection').load(url, () => {
				ingame.setSecondUrlHash(page);
			});
		}
	}
}

$(document).ready(function() {
	const hash = window.location.hash;

	$('.skr-game-ship').each(function() {
		const shipId = parseInt(
			$(this)
				.prop('id')
				.replace('skr-game-ship-', '')
		);
		ingame.drawShipTravelLine(shipId);
		ingame.drawShipOriginLine(shipId);
		ingame.drawShipDestinationMarker(shipId);
		ingame.drawShipRouting(shipId);
	});

	ingame.resizeGalaxy();

	if (hash) {
		const parts = hash.replace('#', '').split(';');
		const first = parts[0].split('=');

		setTimeout(function() {
			ingame.select(first[0], first[1], false);
		}, 150);
	} else {
		const initX = parseInt($('#skr-ingame-init-x').val());
		const initY = parseInt($('#skr-ingame-init-y').val());

		const wrapper = $('#skr-game-galaxy-wrapper');

		setTimeout(function() {
			wrapper.scrollLeft(initX - wrapper.innerWidth() / 2);
			wrapper.scrollTop(initY - wrapper.innerHeight() / 2);
		}, 150);
	}

	if ($('#skr-ingame-overview-viewed').val() === 'false' && (!tutorial || (tutorial.getStage() !== 'SELECT_FREIGHTER_COLONIZATION' && tutorial.getStage() !== 'OPEN_NEW_COLONY'))) {
		ingame.showOverview();

		$.ajax({
			url: 'overview-viewed?gameId=' + new URLSearchParams(window.location.search).get('id'),
			type: 'POST',
			contentType: 'application/json'
		});
	}

	if (ingame.smallDisplay()) {
		ingame.updateDetailsContent();
	}
});
