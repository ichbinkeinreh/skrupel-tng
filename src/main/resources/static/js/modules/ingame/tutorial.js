class Tutorial {
	initialized = false;
	nextStageElem;
	nextStageClickHandler;

	constructor() {}

	init() {
		$.ajax({
			url: '/ingame/tutorial-stage',
			type: 'GET',
			success: response => {
				this.initialized = true;
				this.applyStageContent(response);
			},
		});
	}

	next(expectedStage) {
		const currentStage = this.getStage();

		if ((currentStage || expectedStage !== 'null') && currentStage !== expectedStage) {
			return;
		}

		$('.tutorial-element-stage-' + currentStage).popover('dispose');

		$.ajax({
			url: '/ingame/tutorial-stage?page=' + this.buildPageString(),
			type: 'POST',
			success: response => {
				$.ajax({
					url: '/ingame/tutorial-stage-value',
					type: 'GET',
					success: stageValue => {
						if (stageValue === 'END') {
							$('#skr-ingame-overview-modal').modal('hide');
							$('#skr-ingame-tutorial-end-modal').modal('show');
						} else {
							$('#skr-ingame-tutorial-stage')
								.val(stageValue)
								.trigger('change');

							setTimeout(() => {
								this.applyStageContent(response);
							}, 200);
						}
					},
				});
			},
		});
	}

	getStage() {
		return $('#skr-ingame-tutorial-stage').val();
	}

	applyStageContent(htmlContent) {
		$('.popover').remove();

		const stage = this.getStage();
		const elem = $('.tutorial-element-stage-' + stage);

		elem.popover({
			content: htmlContent,
			trigger: 'manual',
			html: true,
		});

		elem.popover('show');

		const newNextStageElem = '.tutorial-click-element-stage-' + stage;

		if (this.nextStageElem === newNextStageElem) {
			return;
		}

		if (this.nextStageClickHandler) {
			$(document).off('click', this.nextStageElem, this.nextStageClickHandler);
		}

		this.nextStageElem = newNextStageElem;
		this.nextStageClickHandler = () => this.next(stage);

		$(document).on('click', this.nextStageElem, this.nextStageClickHandler);
	}

	checkStage() {
		if (!this.initialized) {
			return;
		}

		const currentStage = this.getStage();

		$.ajax({
			url: '/ingame/check-tutorial-stage?page=' + this.buildPageString(),
			type: 'GET',
			success: revertedStage => {
				if (revertedStage && revertedStage !== currentStage) {
					$('.tutorial-element-stage-' + currentStage).popover('dispose');
					$('#skr-ingame-tutorial-max-reached-stage')
						.val(currentStage)
						.trigger('change');
					$('#skr-ingame-tutorial-stage')
						.val(revertedStage)
						.trigger('change');
					this.init();
				}
			},
		});
	}

	revert(stage) {
		$.ajax({
			url: '/ingame/revert-to-tutorial-stage?stage=' + stage,
			type: 'POST',
			success: response => {
				$('#skr-ingame-tutorial-stage')
					.val(stage)
					.trigger('change');

				setTimeout(() => {
					this.applyStageContent(response);
				}, 200);
			},
		});
	}

	buildPageString() {
		const second = ingame.getSecondUrlHash();

		if (second) {
			return ingame.selectedPage + ';' + second;
		}

		return ingame.selectedPage;
	}

	finish() {
		$.ajax({
			url: '/ingame/tutorial',
			type: 'DELETE',
			success: () => {
				window.location.href = '/open-games?page=0&sortField=EXISTING_GAMES_CREATED&sortDirection=false';
			},
		});
	}

	restart() {
		$.ajax({
			url: '/ingame/tutorial',
			type: 'POST',
			contentType: 'application/json',
			success: function(gameId) {
				window.location.href = '/ingame/game?id=' + gameId;
			},
		});
	}
}

export let tutorial;
const tutorialActive = $('#skr-ingame-tutorial-active').val() === 'true';

if (tutorialActive) {
	tutorial = new Tutorial();
	window.tutorial = tutorial;
} else {
	tutorial = null;
}

$(document).ready(() => {
	const stage = $('#skr-ingame-tutorial-stage').val();

	if (tutorialActive) {
		if (!stage) {
			$('#skr-ingame-tutorial-start-modal').modal('show');
		} else if (stage === 'END') {
			$('#skr-ingame-tutorial-end-modal').modal('show');
		} else {
			setTimeout(() => tutorial.init(), 500);
		}

		$(document).on('hide.bs.modal', '#skr-ingame-overview-modal', () => {
			if (tutorial.getStage() === 'OVERIVEW_MODAL') {
				tutorial.next('OVERIVEW_MODAL');
			}
		});

		$(document).on('hide.bs.modal', '#skr-ingame-tutorial-start-modal', () => {
			if (!tutorial.getStage()) {
				tutorial.next('null');
				tutorial.initialized = true;
			}
		});

		$(document).on('hide.bs.modal', '#skr-ingame-ship-template-details-orion_14', () => {
			if (tutorial.getStage() === 'BOMBER_DETAILS') {
				tutorial.next('BOMBER_DETAILS');
			}
		});
	}
});
