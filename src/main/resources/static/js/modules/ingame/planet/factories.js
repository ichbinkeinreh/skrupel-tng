import { ingame } from '../ingame.js';
import { tutorial } from '../tutorial.js';

class PlanetFactories {
	constructor() {
		$(document).on('click', '#skr-planet-factories-build-factories', function() {
			const quantity = parseInt($('#skr-planet-factories-quantity-select').val());

			if (tutorial) {
				const stage = tutorial.getStage();
				if (stage !== 'BUILD_FACTORIES_FIRST' && stage !== 'BUILD_FACTORIES_SECOND') {
					return;
				}

				const max = parseInt($('#skr-ingame-factories-max-buildable').val());
				if (quantity != max) {
					return;
				}
			}

			$.ajax({
				url: 'planet/factories?planetId=' + ingame.getSelectedId() + '&quantity=' + quantity,
				type: 'POST',
				contentType: 'application/json',
				success: function() {
					ingame.refreshSelection(planet);
					ingame.updatePlanetMouseOver(ingame.getSelectedId());

					if (tutorial) {
						const stage = tutorial.getStage();

						if (stage === 'BUILD_FACTORIES_FIRST' || stage === 'BUILD_FACTORIES_SECOND') {
							tutorial.next(stage);
						}
					}
				}
			});
		});

		if (!tutorial) {
			$(document).on('click', '#skr-planet-factories-automated-checkbox', function() {
				$.ajax({
					url: 'planet/automated-factories?planetId=' + ingame.getSelectedId(),
					type: 'POST',
					contentType: 'application/json',
					success: function() {
						ingame.updatePlanetMouseOver(ingame.getSelectedId());
					}
				});
			});

			$(document).on('click', '#skr-planet-sell-supplies-automated-checkbox', function() {
				$.ajax({
					url: 'planet/automated-sell-supplies?planetId=' + ingame.getSelectedId(),
					type: 'POST',
					contentType: 'application/json'
				});
			});

			$(document).on('click', '#skr-planet-factories-sell-supplies', function() {
				const availableSupplies = parseInt($('#skr-ingame-supplies').val());
				const quantity = parseInt($('#skr-planet-supplies-quantity-input').val());

				if (!quantity || availableSupplies < quantity || quantity <= 0) {
					$('#skr-ingame-sell-supplies-error').show();
					return;
				}

				$('#skr-ingame-sell-supplies-error').hide();

				$.ajax({
					url: 'planet/sell-supplies?planetId=' + ingame.getSelectedId() + '&quantity=' + quantity,
					type: 'POST',
					contentType: 'application/json',
					success: function() {
						ingame.refreshSelection(planet);
						ingame.updatePlanetMouseOver(ingame.getSelectedId());
					}
				});
			});
		}
	}
}

const factories = new PlanetFactories();
