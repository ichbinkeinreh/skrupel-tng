import { ingame } from '../ingame.js';
import { skrupel } from '../../../skrupel_tng.js';

class StarbaseConstruction {
	constructor() {
		$(document).on('click', '.skr-ingame-starbase-type-item', function() {
			const starbaseType = $(this)
				.prop('id')
				.replace('skr-ingame-starbase-type-', '');
			showCopyOfModal('#skr-ingame-starbase-type-modal-' + starbaseType);
		});
	}

	toNameSelect(starbaseType) {
		$.ajax({
			url: 'planet/starbase-construction-name-select?planetId=' + ingame.getSelectedId() + '&type=' + starbaseType,
			type: 'GET',
			success: function(response) {
				$('#skr-planet-starbasepconstruction-content').replaceWith(response);
			},
		});
	}

	construct() {
		const type = $('#skr-planet-starbaseconstruction-type').val();
		const name = $('#skr-planet-starbaseconstruction-name-input').val();

		$.ajax({
			url: 'planet/starbase?planetId=' + ingame.getSelectedId() + '&type=' + type + '&name=' + name,
			type: 'POST',
			success: function(response) {
				$('#skr-planet-starbasepconstruction-content').replaceWith(response);
				ingame.refreshSelection(planet);
				ingame.updatePlanetMouseOver(ingame.getSelectedId());
			},
		});
	}
}

const starbaseConstruction = new StarbaseConstruction();
window.starbaseConstruction = starbaseConstruction;
