import { IngamePage } from '../ingame.js';
import { ingame } from '../ingame.js';
import { fleetsTable } from '../../ingame/overview-table.js';

export const fleet = new IngamePage('fleet');
window.fleet = fleet;

class FleetDetails {
	constructor() {}

	updateStats(callback) {
		const container = $('#skr-ingame-fleet-stats-details');
		container.empty();
		container.load('fleet/stats-details?fleetId=' + ingame.getSelectedId(), callback);
	}

	updateTravelData() {
		const shipIds = $('#skr-ingame-fleet-ship-ids')
			.val()
			.split(',');

		shipIds.forEach(shipId => {
			ingame.updateShipMouseOver(shipId);
			ingame.updateShipTravelData(shipId);
		});
	}

	save() {
		const name = $('#skr-ingame-fleet-name-input').val();
		const gameId = new URLSearchParams(window.location.search).get('id');

		let url = 'fleet?name=' + name + '&gameId=' + gameId;

		if (ingame.shipIdForFleetCreation) {
			url += '&shipId=' + ingame.shipIdForFleetCreation;
		}

		$.ajax({
			type: 'PUT',
			url: url,
			success: fleetId => {
				ingame.select('fleet', fleetId);
				fleetsTable.updateTableContent();

				if (ingame.shipIdForFleetCreation) {
					ingame.updateShipMouseOver(ingame.shipIdForFleetCreation);
				}
			}
		});
	}

	addShipToFleet(fleetId) {
		const shipId = ingame.getSelectedId();

		$.ajax({
			type: 'POST',
			url: 'fleet/ship?shipId=' + shipId + '&fleetId=' + fleetId,
			success: () => {
				ingame.updateShipMouseOver(ingame.getSelectedId());
				ingame.updateShipStatsDetails(ingame.getSelectedId());
				ingame.updateShipTravelData();
			}
		});
	}

	removeFromFleet() {
		const shipId = ingame.getSelectedId();

		$.ajax({
			type: 'DELETE',
			url: 'fleet/ship?shipId=' + shipId,
			success: () => {
				ingame.updateShipMouseOver(ingame.getSelectedId());
				ingame.updateShipStatsDetails(ingame.getSelectedId());
				ingame.updateShipTravelData();
			}
		});
	}

	removeShipFromFleet(shipId) {
		$.ajax({
			type: 'DELETE',
			url: 'fleet/ship?shipId=' + shipId,
			success: () => {
				ingame.refreshSelection(fleet);
				ingame.updateShipMouseOver(shipId);

				if (ingame.getSelectedId() === shipId) {
					ingame.updateShipTravelData();
				}
			}
		});
	}
}

export const fleetDetails = new FleetDetails();
window.fleetDetails = fleetDetails;
