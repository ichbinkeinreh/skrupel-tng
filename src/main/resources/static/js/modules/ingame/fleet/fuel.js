import { fleetDetails } from './fleet.js';
import { ingame } from '../ingame.js';

class FleetFuel {
	constructor() {
		$(document).on('input', '#slider-fuel-percentage', event => {
			const percentage = $(event.target).val();

			$('#skr-ingame-fleet-fuel-percentage').text(percentage + '%');
		});
	}

	fillTanks() {
		const fleetId = ingame.getSelectedId();
		const percentage = parseFloat($('#slider-fuel-percentage').val()) / 100.0;

		$.ajax({
			type: 'POST',
			url: 'fleet/fill-tanks?fleetId=' + fleetId + '&percentage=' + percentage,
			success: planetIds => {
				fleetDetails.updateStats(() => {
					fleetDetails.updateTravelData();

					planetIds.forEach(planetId => {
						ingame.updatePlanetMouseOver(planetId);
					});
				});
			}
		});
	}
}

const fleetFuel = new FleetFuel();
window.fleetFuel = fleetFuel;
