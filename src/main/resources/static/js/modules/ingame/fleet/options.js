import { ingame } from '../ingame.js';
import { fleetsTable } from '../../ingame/overview-table.js';
import { fleetDetails } from './fleet.js';

class FleetOptions {
	changeName() {
		const fleetId = ingame.getSelectedId();
		const name = $('#skr-ingame-fleet-name-input').val();

		if (!name) {
			$('#skr-ingame-fleet-name-blank').show();
			return;
		}

		$('#skr-ingame-fleet-name-blank').hide();

		$.ajax({
			type: 'POST',
			url: 'fleet/name?name=' + name + '&fleetId=' + fleetId,
			success: response => {
				$('#skr-game-options-content').replaceWith(response);

				fleetDetails.updateStats();
				fleetDetails.updateTravelData();

				fleetsTable.updateTableContent();
			}
		});
	}

	showDeleteFleetModal() {
		$('#skr-ingame-delete-fleet-modal').modal('show');
	}

	deleteFleet() {
		const fleetId = ingame.getSelectedId();

		$.ajax({
			type: 'DELETE',
			url: 'fleet?fleetId=' + fleetId,
			success: () => {
				fleetDetails.updateTravelData();
				ingame.unselect();
				fleetsTable.updateTableContent();
			}
		});
	}
}

const fleetOptions = new FleetOptions();
window.fleetOptions = fleetOptions;
