import { FullpageSearchTable } from './fullpage-search-table.js';

export const searchTable = new FullpageSearchTable();
window.searchTable = searchTable;

export function joinGame(gameId) {
	$.ajax({
		url: 'game/player?gameId=' + gameId,
		type: 'POST',
		success: function(errorMessage) {
			if (errorMessage) {
				window.alert(errorMessage);
			} else {
				window.location.href = 'game?id=' + gameId;
			}
		},
	});
}
window.joinGame = joinGame;
