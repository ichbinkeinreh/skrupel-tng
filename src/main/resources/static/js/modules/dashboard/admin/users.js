import { FullpageSearchTable } from '../fullpage-search-table.js';

export const searchTable = new FullpageSearchTable();
window.searchTable = searchTable;

class Users {
	loginIdToBeDeleted;

	constructor() {}

	showDeleteUserModal(loginId) {
		this.loginIdToBeDeleted = loginId;
		$('#skr-admin-delete-user-modal').modal('show');
	}

	deleteUser() {
		$.ajax({
			url: '/admin/user?loginId=' + this.loginIdToBeDeleted,
			type: 'DELETE',
			success: () => {
				window.location.reload();
			},
		});
	}
}

export const users = new Users();
window.users = users;
