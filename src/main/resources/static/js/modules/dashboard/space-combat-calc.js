window.doRequest = function() {
	const request = {
		hull1: $("#hull1").val(),
		energy1: $("#energy1").val(),
		projectile1: $("#projectile1").val(),
		damage1: $("#damage1").val(),
		shield1: $("#shield1").val(),
		projectile1cnt: $("#projectile1cnt").val(),
		
		hull2: $("#hull2").val(),
		energy2: $("#energy2").val(),
		projectile2: $("#projectile2").val(),
		damage2: $("#damage2").val(),
		shield2: $("#shield2").val(),
		projectile2cnt: $("#projectile2cnt").val()
	};

	$.ajax({
		url: '/infos/combat-calculator/space',
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(request),
		success: function(response) {
			$("#result1").text(" " + response.me);
			$("#result2").text(" " + response.enemy);
		},
	});
}