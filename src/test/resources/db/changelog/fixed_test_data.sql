SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;
SET SESSION AUTHORIZATION DEFAULT;

UPDATE planet SET player_id = 1 WHERE id = 3; -- PlanetRepositoryDatabaseTest.shouldCheckRevealingFaction
UPDATE planet SET scan_radius = 116 WHERE id = 1; -- PlanetRepositoryDatabaseTest.shouldResetScanRadius
UPDATE starbase SET TYPE = 'WAR_BASE' WHERE id = 4; -- PlanetRepositoryDatabaseTest.shouldSetExtendedScanRadius
UPDATE orbital_system SET TYPE = 'PSY_CORPS' WHERE id = 1; -- PlanetRepositoryDatabaseTest.shouldSetPsyCorpsScanRadius
INSERT INTO ship_route_entry (ship_id, planet_id) VALUES (8, 92); -- PlanetRepositoryDatabaseTest.shouldReturnPlanetsWithoutRoute
update player set turn_finished = false where id = 2; -- PlayerRepositoryDatabaseTest.shouldUpdateTurnValues
insert into space_fold (game_id, x, y) values (1, 80, 80), (1, 500, 500); -- SpaceFoldRepositoryDatabaseTest.shouldFindSpaceFoldsInRadius
insert into worm_hole (game_id, x, y) values (1, 180, 180), (1, 500, 500); -- WormHoleRepositoryDatabaseTest.shouldFindWormHolesInRadius
insert into mine_field (id, game_id, player_id, x, y, mines, level) values (1, 1, 1, 50, 100, 10, 2), (2, 1, 4, 100, 200, 8, 3), (3, 1, 5, 300, 400, 5, 1); -- MineFieldRepositoryDatabaseTest
update ship set propulsion_system_template_name = 'antigrav_engine' where id = 7; -- ShipRepositoryDatabaseTest.shouldCloakAntiGravEngineShips
update ship set active_ability_id = 8 where id = 6; -- ShipRepositoryDatabaseTest.shouldScanRadiusToCorrectValue
