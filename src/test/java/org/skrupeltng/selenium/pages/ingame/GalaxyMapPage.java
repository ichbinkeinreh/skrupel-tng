package org.skrupeltng.selenium.pages.ingame;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.selenium.pages.AbstractPage;

public class GalaxyMapPage extends AbstractPage {

	private final int STARBASE_SELECTION_OFFSET = 11;
	private final int SHIP_SELECTION_OFFSET = 7;

	public GalaxyMapPage(WebDriver driver) {
		super(driver);
	}

	public void selectHomePlanet() {
		By owned = By.className("owned");
		scrollToElement(owned);

		driver.findElement(owned).click();

		System.out.println("Selecting home planet from galaxy map...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#planet="));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-planet-mines-button")));
	}

	public void selectShipInSpace() {
		By owned = By.className("owned-ship");
		scrollToElement(owned);

		WebElement element = driver.findElement(owned);
		element.click();
	}

	public void selectHomeStarbase() {
		By owned = By.className("starbase");
		scrollToElement(owned);

		new Actions(driver).moveToElement(driver.findElement(owned), STARBASE_SELECTION_OFFSET, STARBASE_SELECTION_OFFSET).click().build().perform();

		System.out.println("Selecting home starbase from galaxy map...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#starbase="));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-starbase-upgrade-button")));
	}

	public void selectShipOnPlanet(String className) {
		By owned = By.className(className);
		scrollToElement(owned);

		new Actions(driver).moveToElement(driver.findElement(owned), SHIP_SELECTION_OFFSET, SHIP_SELECTION_OFFSET).click().build().perform();

		System.out.println("Selecting ship on planet from galaxy map...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#ship="));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-ship-transporter-button")));
	}

	public void selectMultipleShipOnPlanet() {
		By owned = By.className("ships-around-planet");
		scrollToElement(owned);

		new Actions(driver).moveToElement(driver.findElement(owned), SHIP_SELECTION_OFFSET, SHIP_SELECTION_OFFSET).click().build().perform();

		System.out.println("Selecting multiple ship on planet from galaxy map...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#ship-selection"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.className("skr-ingame-selection-item")));
	}
}