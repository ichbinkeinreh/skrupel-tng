package org.skrupeltng.selenium.pages.ingame;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.selenium.pages.AbstractPage;

public class StarbaseOverviewPage extends AbstractPage {

	public StarbaseOverviewPage(WebDriver driver) {
		super(driver);
	}

	public void openStarbaseOverview() {
		driver.findElement(By.id("skr-ingame-starbases-button")).click();

		System.out.println("Loading starbases list...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.elementToBeClickable(By.className("skr-table-row")));

		driver.findElements(By.className("skr-table-row")).get(0).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.not(ExpectedConditions.elementToBeClickable(By.className("skr-table-row"))));

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-starbase-selection")));
	}
}