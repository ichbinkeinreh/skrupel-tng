package org.skrupeltng.selenium.pages.ingame;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.selenium.pages.AbstractPage;

public class StarbasePage extends AbstractPage {

	public StarbasePage(WebDriver driver) {
		super(driver);
	}

	public void upgradeTechlevelFirst() {
		System.out.println("Upgrading starbase techlevels first...");

		new Select(driver.findElement(By.id("skr-starbase-upgrade-hull-select"))).selectByIndex(3);
		new Select(driver.findElement(By.id("skr-starbase-upgrade-propulsion-select"))).selectByIndex(7);

		driver.findElement(By.id("skr-ingame-starbase-performupgrade-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-starbase-hullproduction-button")));

		waitShortly();
	}

	public void upgradeTechlevelSecond() {
		System.out.println("Upgrading starbase techlevels second...");

		new Select(driver.findElement(By.id("skr-starbase-upgrade-hull-select"))).selectByIndex(2);
		new Select(driver.findElement(By.id("skr-starbase-upgrade-energy-select"))).selectByIndex(2);
		new Select(driver.findElement(By.id("skr-starbase-upgrade-projectile-select"))).selectByIndex(2);

		driver.findElement(By.id("skr-ingame-starbase-performupgrade-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-starbase-hullproduction-button")));

		waitShortly();
	}

	public void productShipParts(ShipConstructionData data) {
		productShipPart("hull", data.getHullName(), data.getExpectedHullQuantity(), data.getHullQuantity());
		productShipPart("propulsion", data.getPropulsionName(), data.getExpectedPropulsionQuantity(), data.getPropulsionQuantity());

		if (data.getEnergyWeaponQuantity() > 0) {
			productShipPart("energyweapon", data.getEnergyWeaponName(), data.getExpectedEnergyWeaponQuantity(), data.getEnergyWeaponQuantity());
		}

		if (data.getProjectileWeaponQuantity() > 0) {
			productShipPart("projectileweapon", data.getProjectileWeaponName(), data.getExpectedProjectileWeaponQuantity(), data.getProjectileWeaponQuantity());
		}
	}

	public void productShipPart(String type, String productName, int expectedItems, int quantity) {
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.className("skr-ingame-starbase-production-item")));

		List<WebElement> items = driver.findElements(By.className("skr-ingame-starbase-production-item"));
		assertEquals(expectedItems, items.size());

		WebElement constructButton = null;

		for (WebElement item : items) {
			String text = item.findElement(By.className("skr-ingame-starbase-production-item-name")).getText();

			if (text.equals(productName)) {
				new Select(item.findElement(By.className("skr-starbase-production-quantity-select"))).selectByValue("" + quantity);
				constructButton = item.findElement(By.className("skr-ingame-starbase-produce-button"));
				break;
			}
		}

		assertNotNull(constructButton);

		constructButton.click();
	}

	public void buildShip() {
		driver.findElement(By.id("skr-ingame-starbase-shipconstruction-hull-select-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-starbase-shipconstruction-shipname-input")));

		driver.findElement(By.id("skr-starbase-shipconstruction-shipname-input")).sendKeys("test");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(new ExpectedCondition<Boolean>() {
					@Override
					public Boolean apply(WebDriver input) {
						return input.findElement(By.id("skr-starbase-shipconstruction-build-button")).isEnabled();
					}
				});

		driver.findElement(By.id("skr-starbase-shipconstruction-build-button")).click();

		System.out.println("Building ship...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-starbase-shipconstruction-success-message")));
	}

	public void closeShipTemplateModal(String shipTemplateId) {
		String modalId = "current-open-modal";

		(new WebDriverWait(driver, STANDARD_TIMEOUT)).until(ExpectedConditions.visibilityOfElementLocated(By.className(modalId)));

		driver.findElement(By.className(modalId)).findElement(By.id("skr-close-ship-template-modal-button-" + shipTemplateId)).click();
	}

	public void openSpaceFolds() {
		driver.findElement(By.id("skr-ingame-starbase-spacefold-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-starbase-spacefolds-unavailable-message")));
	}
}
