package org.skrupeltng.selenium.pages.ingame;

public class ShipConstructionData {

	private String hullName;
	private int expectedHullQuantity;
	private int hullQuantity;

	private String propulsionName;
	private int expectedPropulsionQuantity;
	private int propulsionQuantity;

	private String energyWeaponName;
	private int expectedEnergyWeaponQuantity;
	private int energyWeaponQuantity;

	private String projectileWeaponName;
	private int expectedProjectileWeaponQuantity;
	private int projectileWeaponQuantity;

	public String getHullName() {
		return hullName;
	}

	public void setHullName(String hullName) {
		this.hullName = hullName;
	}

	public int getExpectedHullQuantity() {
		return expectedHullQuantity;
	}

	public void setExpectedHullQuantity(int expectedHullQuantity) {
		this.expectedHullQuantity = expectedHullQuantity;
	}

	public int getHullQuantity() {
		return hullQuantity;
	}

	public void setHullQuantity(int hullQuantity) {
		this.hullQuantity = hullQuantity;
	}

	public String getPropulsionName() {
		return propulsionName;
	}

	public void setPropulsionName(String propulsionName) {
		this.propulsionName = propulsionName;
	}

	public int getExpectedPropulsionQuantity() {
		return expectedPropulsionQuantity;
	}

	public void setExpectedPropulsionQuantity(int expectedPropulsionQuantity) {
		this.expectedPropulsionQuantity = expectedPropulsionQuantity;
	}

	public int getPropulsionQuantity() {
		return propulsionQuantity;
	}

	public void setPropulsionQuantity(int propulsionQuantity) {
		this.propulsionQuantity = propulsionQuantity;
	}

	public String getEnergyWeaponName() {
		return energyWeaponName;
	}

	public void setEnergyWeaponName(String energyWeaponName) {
		this.energyWeaponName = energyWeaponName;
	}

	public int getExpectedEnergyWeaponQuantity() {
		return expectedEnergyWeaponQuantity;
	}

	public void setExpectedEnergyWeaponQuantity(int expectedEnergyWeaponQuantity) {
		this.expectedEnergyWeaponQuantity = expectedEnergyWeaponQuantity;
	}

	public int getEnergyWeaponQuantity() {
		return energyWeaponQuantity;
	}

	public void setEnergyWeaponQuantity(int energyWeaponQuantity) {
		this.energyWeaponQuantity = energyWeaponQuantity;
	}

	public String getProjectileWeaponName() {
		return projectileWeaponName;
	}

	public void setProjectileWeaponName(String projectileWeaponName) {
		this.projectileWeaponName = projectileWeaponName;
	}

	public int getExpectedProjectileWeaponQuantity() {
		return expectedProjectileWeaponQuantity;
	}

	public void setExpectedProjectileWeaponQuantity(int expectedProjectileWeaponQuantity) {
		this.expectedProjectileWeaponQuantity = expectedProjectileWeaponQuantity;
	}

	public int getProjectileWeaponQuantity() {
		return projectileWeaponQuantity;
	}

	public void setProjectileWeaponQuantity(int projectileWeaponQuantity) {
		this.projectileWeaponQuantity = projectileWeaponQuantity;
	}
}