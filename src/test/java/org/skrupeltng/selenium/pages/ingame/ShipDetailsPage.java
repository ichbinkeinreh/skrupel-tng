package org.skrupeltng.selenium.pages.ingame;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.modules.ingame.database.TutorialStage;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.selenium.pages.AbstractPage;

public class ShipDetailsPage extends AbstractPage {

	public ShipDetailsPage(WebDriver driver) {
		super(driver);
	}

	public void transportToShipForColonization() {
		System.out.println("Transporting cargo onto ship...");

		driver.findElement(By.id("skr-ingame-ship-transporter-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";transporter"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("slider-fuel")));

		moveCargoToShip("fuel", 10);
		moveCargoToShip("colonists", 1000);
		moveCargoToShip("money", 100);
		moveCargoToShip("supplies", 10);

		driver.findElement(By.id("skr-ingame-ship-finish-transport-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-ship-transport-success")));
	}

	public void transportToShipForBomber() {
		System.out.println("Transporting fuel onto ship...");

		driver.findElement(By.id("skr-ingame-ship-transporter-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";transporter"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("slider-fuel")));

		moveCargoToShip("fuel", 15);

		driver.findElement(By.id("skr-ingame-ship-finish-transport-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-ship-transport-success")));
	}

	private void moveCargoToShip(String fieldName, int amount) {
		String id = "left-input-" + fieldName;

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.elementToBeClickable(By.id(id)));

		WebElement elem = driver.findElement(By.id(id));
		elem.clear();
		elem.sendKeys(amount + "");
	}

	public void navigateShipToEnemyPlanet() {
		System.out.println("Navigating ship to enemy planet...");

		driver.findElement(By.id("skr-ingame-ship-navigation-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";navigation"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ship-courseselect-checkbox")));

		driver.findElement(By.id("skr-ship-courseselect-checkbox")).click();

		driver.findElement(By.className("tutorial-element-stage-ENEMY_COLONY")).click();

		waitShortly();

		driver.findElement(By.id("skr-ingame-ship-save-course-button")).click();
		waitShortly();

		String coordsText = driver.findElement(By.id("skr-ship-courseselect-coordinates")).getText();
		assertTrue(!coordsText.contains("-"));

		String distanceText = driver.findElement(By.id("skr-ship-courseselect-distance")).getText();
		assertTrue(!distanceText.contains("-"));

		String targetText = driver.findElement(By.id("skr-ship-courseselect-name")).getText();
		assertTrue(!targetText.contains("-"));

		String durationText = driver.findElement(By.id("skr-ship-courseselect-duration")).getText();
		assertTrue(!durationText.contains("-"));

		String fuelConsumptionText = driver.findElement(By.id("skr-ship-courseselect-fuelconsumption")).getText();
		assertFalse(fuelConsumptionText.equals("0"));

		assertTrue(driver.findElement(By.id("skr-ship-courseselect-delete-course")).isEnabled());
	}

	public void activateShipAbility(ShipAbilityType ability) {
		System.out.println("Activating " + ability + " ability...");

		driver.findElement(By.id("skr-ingame-ship-task-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";task"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id(ability.name())));

		driver.findElement(By.id(ability.name())).click();
		driver.findElement(By.id("skr-ingame-ship-change-task-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-ship-task-success")));
		driver.findElement(By.id("skr-ingame-ship-task-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";task"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id(ability.name())));

		assertTrue(driver.findElement(By.id(ability.name())).isSelected());
	}

	public void scanNearbyPlanet() {
		System.out.println("Loading scanner view...");

		driver.findElement(By.id("skr-ingame-ship-scanner-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";scanner"));
		// (new WebDriverWait(driver, STANDARD_TIMEOUT))
		// .until(ExpectedConditions.presenceOfElementLocated(By.className("skr-ingame-native-species-details-button")));
		//
		// driver.findElement(By.className("skr-ingame-native-species-details-button")).click();
		//
		// (new WebDriverWait(driver, STANDARD_TIMEOUT))
		// .until(ExpectedConditions.presenceOfElementLocated(By.className("skr-ingame-native-species-details-modal")));
		//
		// (new WebDriverWait(driver, STANDARD_TIMEOUT))
		// .until(ExpectedConditions.presenceOfElementLocated(By.id("skr-close-native-species-modal-button")));
		//
		// driver.findElement(By.id("skr-close-native-species-modal-button")).click();

		driver.findElement(By.className("skr-ingame-scanner-planet-details-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-scanner-planet-details")));
	}

	public void colonisePlanet() {
		System.out.println("Colonising planet...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";transporter"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("slider-fuel")));

		moveCargoToShip("colonists", 0);
		moveCargoToShip("money", 0);
		moveCargoToShip("supplies", 0);
		moveCargoToShip("fuel", 0);

		driver.findElement(By.id("skr-ingame-ship-finish-transport-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-ship-transport-success")));
	}

	public void addHomePlanetToRoute() {
		new Select(driver.findElement(By.id("route-entry-planet-select-0"))).selectByVisibleText("Beteigeuze");
		checkTutorialStage(TutorialStage.ADD_ROUTE_PLANET_ONE);

		driver.findElement(By.id("skr-ingame-route-add-leave")).click();
		checkTutorialStage(TutorialStage.SELECT_ROUTE_PLANET_TWO);
	}

	public void addColonyToRoute() {
		driver.findElement(By.id("skr-route-select-by-map-button")).click();
		waitShortly();

		driver.findElement(By.className("tutorial-click-element-stage-OPEN_NEW_COLONY")).click();
		checkTutorialStage(TutorialStage.ADD_ROUTE_PLANET_TWO);

		driver.findElement(By.id("skr-ingame-route-add-take")).click();
		checkTutorialStage(TutorialStage.SET_MIN_ROUTE_FUEL);
	}
}