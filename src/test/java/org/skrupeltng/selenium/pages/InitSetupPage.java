package org.skrupeltng.selenium.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InitSetupPage extends AbstractPage {

	public InitSetupPage(WebDriver driver) {
		super(driver);
	}

	public void init() {
		driver.findElement(By.id("username")).sendKeys(USERNAME);
		driver.findElement(By.id("password")).sendKeys(PASSWORD);
		driver.findElement(By.id("dataPrivacyStatementEnglish")).sendKeys("Data privacy statement en");
		driver.findElement(By.id("dataPrivacyStatementGerman")).sendKeys("Data privacy statement de");
		driver.findElement(By.id("domainUrl")).sendKeys("http://localhost:8080");
		driver.findElement(By.id("contactEmail")).sendKeys("test@test.comde");

		List<WebElement> emailElements = driver.findElements(By.id("email"));

		if (emailElements.size() == 1) {
			emailElements.get(0).sendKeys("test@test.comde");
		}

		driver.findElement(By.id("skr-init-setup-form-submit-button")).submit();

		System.out.println("Performing init setup...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-dashboard-start-tutorial")));
	}
}