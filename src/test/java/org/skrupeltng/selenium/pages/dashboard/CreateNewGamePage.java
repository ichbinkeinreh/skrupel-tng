package org.skrupeltng.selenium.pages.dashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.selenium.pages.AbstractPage;

public class CreateNewGamePage extends AbstractPage {

	public CreateNewGamePage(WebDriver driver) {
		super(driver);
	}

	public void createNewGame(String name, WinCondition winCondition, long expectedGameId) {
		System.out.println("Creating new game...");

		driver.findElement(By.id("name")).sendKeys(name);
		driver.findElement(By.id("gala_1")).click();
		new Select(driver.findElement(By.id("winCondition"))).selectByValue(WinCondition.NONE.name());

		driver.findElement(By.id("skr-game-options-wormholes-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.elementToBeClickable(By.id("unstableWormholeCount")));

		driver.findElement(By.id("unstableWormholeCount")).sendKeys("4");

		driver.findElement(By.id("skr-dashboard-newgame-create-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("game?id=" + expectedGameId));
	}
}