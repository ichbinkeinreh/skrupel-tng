package org.skrupeltng.selenium.pages.dashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.selenium.pages.AbstractPage;

public class RegistrationPage extends AbstractPage {

	public RegistrationPage(WebDriver driver) {
		super(driver);
	}

	public void register() {
		System.out.println("Opening registration page...");

		driver.get("http://localhost:8080/register");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("username")));

		driver.findElement(By.id("username")).sendKeys("testuser");
		driver.findElement(By.id("password")).sendKeys(PASSWORD);
		driver.findElement(By.id("passwordRepeat")).sendKeys(PASSWORD);
		driver.findElement(By.id("dataPrivacyStatementReadAndAccepted1")).click();

		System.out.println("Registering new user...");

		driver.findElement(By.id("skr-register-submit-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-registration-success-page")));
	}
}