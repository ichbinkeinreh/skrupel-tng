package org.skrupeltng.selenium.pages.dashboard;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.selenium.pages.AbstractPage;

public class GameDetailsPage extends AbstractPage {

	public GameDetailsPage(WebDriver driver) {
		super(driver);
	}

	public void deleteGame(long gameId) {
		driver.get("http://localhost:8080/game?id=" + gameId);

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-play-button")));

		driver.findElement(By.id("skr-game-tab-options")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-dashboard-show-game-delete-modal-button")));

		driver.findElement(By.id("skr-dashboard-show-game-delete-modal-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-game-confirm-delete-button")));

		driver.findElement(By.id("skr-game-confirm-delete-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("my-games"));
	}

	public void checkGameDetails() {
		System.out.println("Checking game details..");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-play-button")));

		List<WebElement> playerNameElements = driver.findElements(By.className("skr-player-name"));
		boolean hasAdminPlayer = playerNameElements.stream().anyMatch(w -> w.getText().equals(USERNAME));
		assertTrue(hasAdminPlayer);
	}

	public void startGame() {
		waitShortly();
		driver.findElement(By.id("skr-start-game-button")).click();
	}

	public void playGame() {
		waitShortly();
		driver.findElement(By.id("skr-play-button")).click();
	}

	public void selectFactionForPlayer(long playerId) {
		new Select(driver.findElement(By.id("skr-game-faction-select-" + playerId))).selectByIndex(1);
	}
}