package org.skrupeltng.selenium.pages.dashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.selenium.pages.AbstractPage;

public class UserDetailsPage extends AbstractPage {

	public UserDetailsPage(WebDriver driver) {
		super(driver);
	}

	public void openUserDetails() {
		System.out.println("Opening user details page...");

		driver.findElement(By.id("skr-dashboard-account-menu-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("skr-dashboard-account-details-link")));

		driver.findElement(By.id("skr-dashboard-account-details-link")).click();
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-dashboard-delete-user-button")));
	}

	public void deleteUser() {
		System.out.println("Deleting user...");
		driver.findElement(By.id("skr-dashboard-delete-user-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("delete-user-button")));

		driver.findElement(By.id("delete-user-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-landingpage-create-account-button")));
	}
}