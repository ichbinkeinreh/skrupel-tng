package org.skrupeltng.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.TutorialStage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractPage {

	private final Logger log = LoggerFactory.getLogger(getClass());

	public static final int STANDARD_TIMEOUT = 15;
	protected static final String USERNAME = "admin";
	protected static final String PASSWORD = "$adminPassw0rd";

	protected final WebDriver driver;

	public AbstractPage(WebDriver driver) {
		this.driver = driver;
	}

	protected void waitShortly() {
		try {
			Thread.sleep(500L);
		} catch (InterruptedException e) {
			log.error("Error while sleeping: ", e);
		}
	}

	public void login(String path) {
		driver.get("http://localhost:8080" + path);

		driver.findElement(By.id("username")).sendKeys(USERNAME);
		driver.findElement(By.id("password")).sendKeys(PASSWORD);
		driver.findElement(By.id("skr-login-button")).submit();

		System.out.println("Logging in to " + path);
	}

	public void continueTutorial(TutorialStage stage) {
		driver.get("http://localhost:8080/ingame/game?id=1");

		driver.findElement(By.id("username")).sendKeys(USERNAME);
		driver.findElement(By.id("password")).sendKeys(PASSWORD);
		driver.findElement(By.id("skr-login-button")).submit();

		checkTutorialStage(stage);
	}

	protected void scrollToBottom() {
		((JavascriptExecutor)driver)
				.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		waitShortly();
	}

	public void scrollToElement(By by) {
		WebElement element = driver.findElement(by);
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", element);
		waitShortly();
	}

	protected CoordinateImpl getCoords(By by) {
		return getCoordsOfParent(driver.findElement(by));
	}

	protected CoordinateImpl getCoordsOfParent(WebElement elem) {
		WebElement shipParent = elem.findElement(By.xpath(".."));
		return getCoords(shipParent);
	}

	protected CoordinateImpl getCoords(WebElement elem) {
		Integer x = Integer.valueOf(elem.getCssValue("left").replace("px", ""));
		Integer y = Integer.valueOf(elem.getCssValue("top").replace("px", ""));
		return new CoordinateImpl(x, y, 0);
	}

	public void checkTutorialStage(TutorialStage stage) {
		System.out.println("Checking tutorial stage " + stage + "...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("tutorial-text-stage-" + stage.name())));
	}

	public void clickOnContinueTutorial(TutorialStage stage) {
		driver.findElements(By.className("tutorial-click-element-stage-" + stage.name())).get(0).click();
	}
}