package org.skrupeltng.modules.ingame.database.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;
import org.skrupeltng.AbstractDbTest;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationContainer;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class PlayerReleationRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Test
	public void shouldReturnContainers() {
		List<PlayerRelationContainer> containers = playerRelationRepository.getContainers(4L, 1L);
		assertEquals(8, containers.size());

		Set<Long> playerIds = containers.stream().filter(c -> c.getRelationType() != null).map(c -> c.getPlayerId()).collect(Collectors.toSet());
		assertEquals(2, playerIds.size());
		assertTrue(playerIds.contains(3L));
		assertTrue(playerIds.contains(5L));
	}
}