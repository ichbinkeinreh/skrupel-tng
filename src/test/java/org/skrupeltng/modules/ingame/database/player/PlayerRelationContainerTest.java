package org.skrupeltng.modules.ingame.database.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationContainer;
import org.springframework.context.MessageSource;

public class PlayerRelationContainerTest {

	private PlayerRelationContainer subject;
	private MessageSource messageSource;

	@Before
	public void setup() {
		subject = new PlayerRelationContainer();

		messageSource = Mockito.mock(MessageSource.class);
		subject.offerMessageSource(messageSource);
	}

	@Test
	public void shouldReturnNoActions() {
		subject.setRequestedRelationType(PlayerRelationType.ALLIANCE.name());

		List<PlayerRelationAction> result = subject.retrieveActions();
		assertEquals(0, result.size());

		subject.setRequestedRelationType(null);
		subject.setRoundsLeft(1);

		result = subject.retrieveActions();
		assertEquals(0, result.size());
	}

	@Test
	public void shouldReturnAllPossibleActions() {
		List<PlayerRelationAction> result = subject.retrieveActions();
		assertEquals(PlayerRelationType.values().length, result.size());
	}

	@Test
	public void shouldReturnOfferPeace() {
		subject.setRelationType(PlayerRelationType.WAR.name());
		List<PlayerRelationAction> result = subject.retrieveActions();
		assertEquals(PlayerRelationAction.OFFER_PEACE, result.get(0));
	}

	@Test
	public void shouldReturnCancelAlliance() {
		subject.setRelationType(PlayerRelationType.ALLIANCE.name());
		List<PlayerRelationAction> result = subject.retrieveActions();
		assertEquals(PlayerRelationAction.CANCEL_ALLIANCE, result.get(0));
	}

	@Test
	public void shouldReturnCancelNonAggressionTreaty() {
		subject.setRelationType(PlayerRelationType.NON_AGGRESSION_TREATY.name());
		List<PlayerRelationAction> result = subject.retrieveActions();
		assertEquals(PlayerRelationAction.CANCEL_NON_AGGRESSION_TREATY, result.get(0));
	}

	@Test
	public void shouldReturnCancelTradeAgreement() {
		subject.setRelationType(PlayerRelationType.TRADE_AGREEMENT.name());
		List<PlayerRelationAction> result = subject.retrieveActions();
		assertEquals(PlayerRelationAction.CANCEL_TRADE_AGREEMENT, result.get(0));
	}

	@Test
	public void shouldReturnPeaceRequested() {
		subject.setRequestId(1L);
		Mockito.when(messageSource.getMessage(Mockito.eq("peace_requested"), Mockito.eq(null), Mockito.any())).thenReturn("peace requested");

		String result = subject.retrieveDescription();
		assertEquals("peace requested", result);
	}

	@Test
	public void shouldReturnAllianceRequested() {
		subject.setRequestId(1L);
		subject.setRequestedRelationType(PlayerRelationType.ALLIANCE.name());
		Mockito.when(messageSource.getMessage(Mockito.eq("player_relation_type_ALLIANCE"), Mockito.eq(null), Mockito.any())).thenReturn("alliance");
		Mockito.when(messageSource.getMessage(Mockito.eq("requested"), Mockito.eq(null), Mockito.any())).thenReturn("requested");

		String result = subject.retrieveDescription();
		assertEquals("alliance requested", result);
	}

	@Test
	public void shouldReturnAlliance() {
		subject.setRelationType(PlayerRelationType.ALLIANCE.name());
		Mockito.when(messageSource.getMessage(Mockito.eq("player_relation_type_ALLIANCE"), Mockito.eq(null), Mockito.any())).thenReturn("alliance");

		String result = subject.retrieveDescription();
		assertEquals("alliance", result);
	}

	@Test
	public void shouldReturnAllianceEndsInNineMonths() {
		subject.setRelationType(PlayerRelationType.ALLIANCE.name());
		subject.setRoundsLeft(9);
		Object[] args = new Object[] { 9 };
		Mockito.when(messageSource.getMessage(Mockito.eq("player_relation_type_ALLIANCE"), Mockito.eq(null), Mockito.any())).thenReturn("alliance");
		Mockito.when(messageSource.getMessage(Mockito.eq("player_relation_ends"), Mockito.eq(args), Mockito.eq(null), Mockito.any()))
				.thenReturn("ends in 9 months");

		String result = subject.retrieveDescription();
		assertEquals("alliance ends in 9 months", result);
	}

	@Test
	public void shouldReturnAllianceEndsInOneMonths() {
		subject.setRelationType(PlayerRelationType.ALLIANCE.name());
		subject.setRoundsLeft(1);
		Mockito.when(messageSource.getMessage(Mockito.eq("player_relation_type_ALLIANCE"), Mockito.eq(null), Mockito.any())).thenReturn("alliance");
		Mockito.when(messageSource.getMessage(Mockito.eq("player_relation_ends_in_one_month"), Mockito.eq(null), Mockito.any()))
				.thenReturn("ends in one months");

		String result = subject.retrieveDescription();
		assertEquals("alliance ends in one months", result);
	}

	@Test
	public void shouldReturnMinus() {
		String result = subject.retrieveDescription();
		assertEquals("-", result);
	}

	@Test
	public void shouldNullCheckTypeWithIcon() {
		String result = subject.retrieveTypeIcon();
		assertNull(result);
	}

	@Test
	public void shouldReturnCorrectIcon() {
		subject.setRelationType(PlayerRelationType.ALLIANCE.name());
		String result = subject.retrieveTypeIcon();
		assertEquals(PlayerRelationType.ALLIANCE.getIcon(), result);
	}

	@Test
	public void shouldUsePlayerNameField() {
		String testUser = "testuser";
		subject.setPlayerName(testUser);
		Mockito.when(messageSource.getMessage(Mockito.eq(testUser), Mockito.eq(null), Mockito.eq(testUser), Mockito.any())).thenReturn("testuser_translated");

		String result = subject.retrievePlayerName();

		assertEquals("testuser_translated", result);
	}
}