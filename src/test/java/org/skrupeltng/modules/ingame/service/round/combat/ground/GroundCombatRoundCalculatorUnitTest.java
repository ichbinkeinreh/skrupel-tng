package org.skrupeltng.modules.ingame.service.round.combat.ground;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpecies;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesEffect;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.testcontainers.shaded.com.google.common.collect.Lists;

public class GroundCombatRoundCalculatorUnitTest {

	@Spy
	@InjectMocks
	private final GroundCombatRoundCalculator subject = new GroundCombatRoundCalculator();

	@Mock
	private PlanetRepository planetRepository;

	@Mock
	private NewsService newsService;

	@Mock
	private StatsUpdater statsUpdater;

	@Before
	public void setup() {
		MasterDataService.RANDOM.setSeed(1L);

		MockitoAnnotations.initMocks(this);
	}

	private Planet createPlanet(int colonists1, int light1, int heavy1, float defense, int colonists2, int light2, int heavy2, float offense) {
		Planet planet = new Planet(1L);
		Player player = new Player(2L);
		Faction faction1 = new Faction("faction1");
		faction1.setGroundCombatDefenseRate(defense);
		player.setFaction(faction1);
		planet.setPlayer(player);
		planet.setColonists(colonists1);
		planet.setLightGroundUnits(light1);
		planet.setHeavyGroundUnits(heavy1);

		Player newPlayer = new Player(3L);
		Faction faction2 = new Faction("faction2");
		faction2.setGroundCombatAttackRate(offense);
		newPlayer.setFaction(faction2);
		planet.setNewPlayer(newPlayer);
		planet.setNewColonists(colonists2);
		planet.setNewLightGroundUnits(light2);
		planet.setNewHeavyGroundUnits(heavy2);
		return planet;
	}

	@Test
	public void shouldCheckForNativeSpecies() {
		Planet planet = createPlanet(1000, 0, 10, 1f, 1000, 0, 20, 1f);
		NativeSpecies nativeSpecies = new NativeSpecies();
		nativeSpecies.setEffect(NativeSpeciesEffect.CHANGE_EFFECTIVENESS_GROUND_COMBAT);
		nativeSpecies.setEffectValue(-67f);
		planet.setNativeSpecies(nativeSpecies);
		planet.setNativeSpeciesCount(1000);
		float result = subject.getDefenderStrength(planet);
		assertEquals(0.33f, result, 0f);

		planet.setNativeSpeciesCount(0);
		result = subject.getDefenderStrength(planet);
		assertEquals(1f, result, 0f);
	}

	@Test
	public void shouldCheckForEqualHeavyGroundUnitStrength() {
		Planet planet = createPlanet(1000, 0, 10, 1f, 1000, 0, 10, 0.8f);

		subject.heavyAgainstHeavy(planet);

		assertEquals(0, planet.getHeavyGroundUnits());
		assertEquals(0, planet.getNewHeavyGroundUnits());
	}

	@Test
	public void shouldCheckForHigherAttackerHeavyGroundUnitStrength() {
		Planet planet = createPlanet(1000, 0, 20, 1f, 1000, 0, 20, 1f);

		subject.heavyAgainstHeavy(planet);

		assertEquals(4, planet.getNewHeavyGroundUnits());
		assertEquals(0, planet.getHeavyGroundUnits());
	}

	@Test
	public void shouldCheckForLowerAttackerHeavyGroundUnitStrength() {
		Planet planet = createPlanet(1000, 0, 40, 1f, 1000, 0, 20, 1f);

		subject.heavyAgainstHeavy(planet);

		assertEquals(0, planet.getNewHeavyGroundUnits());
		assertEquals(15, planet.getHeavyGroundUnits());
	}

	@Test
	public void shouldCheckForEqualLightGroundUnitStrength() {
		Planet planet = createPlanet(1000, 10, 0, 1f, 1000, 20, 0, 0.5f);

		subject.lightAgainstLight(planet);

		assertEquals(0, planet.getLightGroundUnits());
		assertEquals(0, planet.getNewLightGroundUnits());
	}

	@Test
	public void shouldCheckForHigherAttackerLightGroundUnitStrength() {
		Planet planet = createPlanet(1000, 20, 0, 1f, 1000, 40, 0, 1f);

		subject.lightAgainstLight(planet);

		assertEquals(20, planet.getNewLightGroundUnits());
		assertEquals(0, planet.getLightGroundUnits());
	}

	@Test
	public void shouldCheckForLowerAttackerLightGroundUnitStrength() {
		Planet planet = createPlanet(1000, 40, 0, 1f, 1000, 20, 0, 1f);

		subject.lightAgainstLight(planet);

		assertEquals(0, planet.getNewLightGroundUnits());
		assertEquals(20, planet.getLightGroundUnits());
	}

	@Test
	public void shouldCheckForEqualHeavyOffenseAndLightDefense() {
		Planet planet = createPlanet(1000, 100, 0, 1f, 1000, 0, 16, 1f);

		subject.heavyOffenseAgainstLightDefense(planet);

		assertEquals(0, planet.getLightGroundUnits());
		assertEquals(0, planet.getNewHeavyGroundUnits());
	}

	@Test
	public void shouldCheckForHigherHeavyOffenseAndLowerLightDefense() {
		Planet planet = createPlanet(1000, 50, 0, 1f, 1000, 0, 16, 1f);

		subject.heavyOffenseAgainstLightDefense(planet);

		assertEquals(0, planet.getLightGroundUnits());
		assertEquals(8, planet.getNewHeavyGroundUnits());
	}

	@Test
	public void shouldCheckForLowerHeavyOffenseAndHigherLightDefense() {
		Planet planet = createPlanet(1000, 200, 0, 1f, 1000, 0, 16, 1f);

		subject.heavyOffenseAgainstLightDefense(planet);

		assertEquals(100, planet.getLightGroundUnits());
		assertEquals(0, planet.getNewHeavyGroundUnits());
	}

	@Test
	public void shouldCheckForEqualLightOffenseAndHeavyDefense() {
		Planet planet = createPlanet(1000, 0, 2, 1f, 1000, 10, 0, 1f);

		subject.lightOffenseAgainstHeavyDefense(planet);

		assertEquals(0, planet.getHeavyGroundUnits());
		assertEquals(0, planet.getNewLightGroundUnits());
	}

	@Test
	public void shouldCheckForHigherLightOffenseAndLowerHeavyDefense() {
		Planet planet = createPlanet(1000, 0, 1, 1f, 1000, 10, 0, 1f);

		subject.lightOffenseAgainstHeavyDefense(planet);

		assertEquals(0, planet.getHeavyGroundUnits());
		assertEquals(5, planet.getNewLightGroundUnits());
	}

	@Test
	public void shouldCheckForLowerLightOffenseAndHigherHeavyDefense() {
		Planet planet = createPlanet(1000, 0, 2, 1f, 1000, 5, 0, 1f);

		subject.lightOffenseAgainstHeavyDefense(planet);

		assertEquals(1, planet.getHeavyGroundUnits());
		assertEquals(0, planet.getNewLightGroundUnits());
	}

	@Test
	public void shouldCheckForEqualColonistDefenseAndHeavyOffense() {
		Planet planet = createPlanet(1000, 0, 0, 1f, 1000, 0, 10, 1f);

		subject.heavyOffenseAgainstColonistDefense(planet);

		assertEquals(0, planet.getColonists());
		assertEquals(0, planet.getNewHeavyGroundUnits());
	}

	@Test
	public void shouldCheckForHigherColonistDefenseAndHeavyOffense() {
		Planet planet = createPlanet(2000, 0, 0, 1f, 1000, 0, 10, 1f);

		subject.heavyOffenseAgainstColonistDefense(planet);

		assertEquals(1000, planet.getColonists());
		assertEquals(0, planet.getNewHeavyGroundUnits());
	}

	@Test
	public void shouldCheckForLowerColonistDefenseAndHeavyOffense() {
		Planet planet = createPlanet(500, 0, 0, 1f, 1000, 0, 10, 1f);

		subject.heavyOffenseAgainstColonistDefense(planet);

		assertEquals(0, planet.getColonists());
		assertEquals(5, planet.getNewHeavyGroundUnits());
	}

	@Test
	public void shouldCheckForEqualColonistOffenseAndHeavyDefense() {
		Planet planet = createPlanet(1000, 0, 10, 1f, 800, 0, 0, 1f);

		subject.colonistOffenseAgainstHeavyDefense(planet);

		assertEquals(0, planet.getNewColonists());
		assertEquals(0, planet.getHeavyGroundUnits());
	}

	@Test
	public void shouldCheckForHigherColonistOffenseAndHeavyDefense() {
		Planet planet = createPlanet(1000, 0, 10, 1f, 1600, 0, 0, 1f);

		subject.colonistOffenseAgainstHeavyDefense(planet);

		assertEquals(800, planet.getNewColonists());
		assertEquals(0, planet.getHeavyGroundUnits());
	}

	@Test
	public void shouldCheckForLowerColonistOffenseAndHeavyDefense() {
		Planet planet = createPlanet(1000, 0, 10, 1f, 400, 0, 0, 1f);

		subject.colonistOffenseAgainstHeavyDefense(planet);

		assertEquals(0, planet.getNewColonists());
		assertEquals(5, planet.getHeavyGroundUnits());
	}

	@Test
	public void shouldCheckForEqualColonistDefenseAndLightOffense() {
		Planet planet = createPlanet(1600, 0, 0, 1f, 1000, 100, 0, 1f);

		subject.lightOffenseAgainstColonistDefense(planet);

		assertEquals(0, planet.getColonists());
		assertEquals(0, planet.getNewLightGroundUnits());
	}

	@Test
	public void shouldCheckForHigherColonistDefenseAndLightOffense() {
		Planet planet = createPlanet(1600, 0, 0, 1f, 1000, 50, 0, 1f);

		subject.lightOffenseAgainstColonistDefense(planet);

		assertEquals(800, planet.getColonists());
		assertEquals(0, planet.getNewLightGroundUnits());
	}

	@Test
	public void shouldCheckForLowerColonistDefenseAndLightOffense() {
		Planet planet = createPlanet(800, 0, 0, 1f, 1000, 100, 0, 1f);

		subject.lightOffenseAgainstColonistDefense(planet);

		assertEquals(0, planet.getColonists());
		assertEquals(50, planet.getNewLightGroundUnits());
	}

	@Test
	public void shouldCheckForEqualColonistOffenseAndLightDefense() {
		Planet planet = createPlanet(1000, 100, 0, 1f, 1600, 0, 0, 1f);

		subject.colonistOffenseAgainstLightDefense(planet);

		assertEquals(0, planet.getNewColonists());
		assertEquals(0, planet.getLightGroundUnits());
	}

	@Test
	public void shouldCheckForHigherColonistOffenseAndLightDefense() {
		Planet planet = createPlanet(1000, 50, 0, 1f, 1600, 0, 0, 1f);

		subject.colonistOffenseAgainstLightDefense(planet);

		assertEquals(800, planet.getNewColonists());
		assertEquals(0, planet.getLightGroundUnits());
	}

	@Test
	public void shouldCheckForLowerColonistOffenseAndLightDefense() {
		Planet planet = createPlanet(1000, 200, 0, 1f, 1600, 0, 0, 1f);

		subject.colonistOffenseAgainstLightDefense(planet);

		assertEquals(0, planet.getNewColonists());
		assertEquals(100, planet.getLightGroundUnits());
	}

	@Test
	public void shouldCheckForEqualColonistOffenseAndColonistDefense() {
		Planet planet = createPlanet(1000, 0, 0, 1f, 1000, 0, 0, 1f);

		subject.colonistOffenseAgainstColonistDefense(planet);

		assertEquals(0, planet.getNewColonists());
		assertEquals(0, planet.getColonists());
	}

	@Test
	public void shouldCheckForHigherColonistOffenseAndColonistDefense() {
		Planet planet = createPlanet(500, 0, 0, 1f, 1000, 0, 0, 1f);

		subject.colonistOffenseAgainstColonistDefense(planet);

		assertEquals(500, planet.getNewColonists());
		assertEquals(0, planet.getColonists());
	}

	@Test
	public void shouldCheckForLowerColonistOffenseAndColonistDefense() {
		Planet planet = createPlanet(1000, 0, 0, 1f, 500, 0, 0, 1f);

		subject.colonistOffenseAgainstColonistDefense(planet);

		assertEquals(0, planet.getNewColonists());
		assertEquals(500, planet.getColonists());
	}

	@Test
	public void shouldHandleSuccessfullDefense() {
		Planet planet = createPlanet(1000, 0, 0, 0, 0, 0, 0, 0);
		Player attackingPlayer = planet.getNewPlayer();
		Player player = planet.getPlayer();

		subject.processResult(planet);

		assertNull(planet.getNewPlayer());

		Mockito.verify(newsService).add(player, NewsEntryConstants.news_entry_ground_combat_defense_success, planet.createFullImagePath(), planet.getId(),
				NewsEntryClickTargetType.planet, planet.getName());
		Mockito.verify(newsService).add(attackingPlayer, NewsEntryConstants.news_entry_ground_combat_offense_failed, planet.createFullImagePath(), null, null,
				planet.getName());
	}

	@Test
	public void shouldHandleSuccessfullOffense() {
		Planet planet = createPlanet(0, 0, 0, 0, 1000, 0, 0, 0);
		Player attackingPlayer = planet.getNewPlayer();
		Player player = planet.getPlayer();

		subject.processResult(planet);

		assertNull(planet.getNewPlayer());
		assertEquals(attackingPlayer, planet.getPlayer());
		assertEquals(1000, planet.getColonists());
		assertEquals(0, planet.getNewColonists());

		Mockito.verify(newsService).add(player, NewsEntryConstants.news_entry_ground_combat_defense_failed, planet.createFullImagePath(), null, null,
				planet.getName());
		Mockito.verify(newsService).add(attackingPlayer, NewsEntryConstants.news_entry_ground_combat_offense_success, planet.createFullImagePath(),
				planet.getId(), NewsEntryClickTargetType.planet, planet.getName());
	}

	@Test
	public void shouldHandleMutualDefeat() {
		Planet planet = createPlanet(0, 0, 0, 0, 0, 0, 0, 0);
		Player attackingPlayer = planet.getNewPlayer();
		Player player = planet.getPlayer();

		subject.processResult(planet);

		assertNull(planet.getNewPlayer());
		assertNull(planet.getPlayer());

		Mockito.verify(newsService).add(player, NewsEntryConstants.news_entry_ground_combat_defense_failed, planet.createFullImagePath(), null, null,
				planet.getName());
		Mockito.verify(newsService).add(attackingPlayer, NewsEntryConstants.news_entry_ground_combat_offense_failed, planet.createFullImagePath(), null, null,
				planet.getName());
	}

	@Test
	public void shouldDefendSuccessfully() {
		Planet planet = createPlanet(1000, 0, 0, 1f, 500, 0, 0, 1f);
		Mockito.when(planetRepository.findPlanetsForGroundCombat(1L)).thenReturn(Lists.newArrayList(planet));

		subject.processGroundCombat(1L);

		Mockito.verify(planetRepository).save(planet);

		assertEquals(500, planet.getColonists());
		assertEquals(0, planet.getNewColonists());
	}

	@Test
	public void shouldAttackSuccessfully() {
		Planet planet = createPlanet(1000, 0, 0, 1f, 0, 0, 20, 1f);
		Mockito.when(planetRepository.findPlanetsForGroundCombat(1L)).thenReturn(Lists.newArrayList(planet));

		subject.processGroundCombat(1L);

		Mockito.verify(planetRepository).save(planet);

		assertEquals(0, planet.getColonists());
		assertEquals(10, planet.getHeavyGroundUnits());
		assertEquals(0, planet.getNewHeavyGroundUnits());
	}
}