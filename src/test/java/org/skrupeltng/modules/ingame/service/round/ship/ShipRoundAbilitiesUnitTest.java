package org.skrupeltng.modules.ingame.service.round.ship;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFoldRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpecies;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.data.util.Pair;

import com.google.common.collect.Lists;

public class ShipRoundAbilitiesUnitTest {

	@Spy
	@InjectMocks
	private final ShipRoundAbilities subject = new ShipRoundAbilities();

	@Mock
	private ConfigProperties configProperties;

	@Mock
	private NewsService newsService;

	@Mock
	private OrbitalSystemRepository orbitalSystemRepository;

	@Mock
	private PlanetRepository planetRepository;

	@Mock
	private PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Mock
	private PlayerRelationRepository playerRelationRepository;

	@Mock
	private ShipRepository shipRepository;

	@Mock
	private ShipService shipService;

	@Mock
	private SpaceFoldRepository spaceFoldRepository;

	@Mock
	private StarbaseService starbaseService;

	@Mock
	private WormHoleRepository wormHoleRepository;

	@Mock
	private StatsUpdater statsUpdater;

	@Mock
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		MasterDataService.RANDOM.setSeed(1L);

		Mockito.when(shipService.transportWithoutPermissionCheck(Mockito.any(), Mockito.any(), Mockito.any())).thenAnswer(new Answer<Pair<Ship, Planet>>() {
			@Override
			public Pair<Ship, Planet> answer(InvocationOnMock invocation) throws Throwable {
				return Pair.of(invocation.getArgumentAt(1, Ship.class), invocation.getArgumentAt(2, Planet.class));
			}
		});

		Mockito.when(shipRepository.save(Mockito.any())).thenAnswer(new Answer<Ship>() {
			@Override
			public Ship answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgumentAt(0, Ship.class);
			}
		});

		Mockito.when(wormHoleRepository.save(Mockito.any())).thenAnswer(new Answer<WormHole>() {
			@Override
			public WormHole answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgumentAt(0, WormHole.class);
			}
		});
	}

	private Ship createShipWithAbility(ShipAbilityType type, Map<String, String> values) {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		PropulsionSystemTemplate propulsionSystemTemplate = new PropulsionSystemTemplate();
		propulsionSystemTemplate.setTechLevel(5);
		ship.setPropulsionSystemTemplate(propulsionSystemTemplate);
		ShipAbility ability = new ShipAbility(type);
		ability.setValues(values);
		ship.getShipTemplate().setShipAbilities(Lists.newArrayList(ability));
		Mockito.when(shipRepository.getShipsWithActiveAbility(1L, type)).thenReturn(Lists.newArrayList(ship));
		return ship;
	}

	private Ship createSubParticleClusterShip() {
		Map<String, String> values = new HashMap<>(4);
		values.put(ShipAbilityConstants.SUB_PARTICLE_CLUSTER_SUPPLIES, "1");
		values.put(ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL1, "1");
		values.put(ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL2, "1");
		values.put(ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL3, "1");
		Ship ship = createShipWithAbility(ShipAbilityType.SUB_PARTICLE_CLUSTER, values);
		ship.getShipTemplate().setStorageSpace(2700);
		ship.setMineral1(0);
		ship.setMineral2(0);
		ship.setMineral3(0);
		return ship;
	}

	private Ship createQuarkReorganizerShip() {
		Map<String, String> values = new HashMap<>(4);
		values.put(ShipAbilityConstants.QUARK_REORGANIZER_SUPPLIES, "1");
		values.put(ShipAbilityConstants.QUARK_REORGANIZER_MINERAL1, "1");
		values.put(ShipAbilityConstants.QUARK_REORGANIZER_MINERAL2, "1");
		values.put(ShipAbilityConstants.QUARK_REORGANIZER_MINERAL3, "0");
		Ship ship = createShipWithAbility(ShipAbilityType.QUARK_REORGANIZER, values);
		ship.setMineral1(0);
		ship.setMineral2(0);
		ship.setMineral3(0);
		ship.getShipTemplate().setFuelCapacity(500);
		return ship;
	}

	private Ship createCybernrittnikkShip() {
		Map<String, String> values = new HashMap<>(1);
		values.put(ShipAbilityConstants.CYBERNRITTNIKK_VALUE, "100");
		return createShipWithAbility(ShipAbilityType.CYBERNRITTNIKK, values);
	}

	private Ship createJumpEngineShip() {
		Map<String, String> values = new HashMap<>(3);
		values.put(ShipAbilityConstants.JUMP_ENGINE_COSTS, "20");
		values.put(ShipAbilityConstants.JUMP_ENGINE_MIN, "500");
		values.put(ShipAbilityConstants.JUMP_ENGINE_MAX, "1000");
		return createShipWithAbility(ShipAbilityType.JUMP_ENGINE, values);
	}

	private Ship createGravityWaveGeneratorShip() {
		Map<String, String> values = new HashMap<>(1);
		values.put(ShipAbilityConstants.GRAVITY_WAVE_GENERATOR_COSTS, "100");
		return createShipWithAbility(ShipAbilityType.GRAVITY_WAVE_GENERATOR, values);
	}

	private Ship createJumpPortalShip() {
		Map<String, String> values = new HashMap<>(4);
		values.put(ShipAbilityConstants.JUMP_PORTAL_FUEL, "10");
		values.put(ShipAbilityConstants.JUMP_PORTAL_MINERAL1, "20");
		values.put(ShipAbilityConstants.JUMP_PORTAL_MINERAL2, "30");
		values.put(ShipAbilityConstants.JUMP_PORTAL_MINERAL3, "40");
		return createShipWithAbility(ShipAbilityType.JUMP_PORTAL, values);
	}

	private Ship createViralInvasionShip() {
		Map<String, String> values = new HashMap<>(2);
		values.put(ShipAbilityConstants.VIRAL_INVASION_MIN, "80");
		values.put(ShipAbilityConstants.VIRAL_INVASION_MAX, "90");
		return createShipWithAbility(ShipAbilityType.VIRAL_INVASION, values);
	}

	private Ship createDestabilizerShip() {
		Map<String, String> values = new HashMap<>(1);
		values.put(ShipAbilityConstants.DESTABILIZER_EFFICIENCY, "0");
		return createShipWithAbility(ShipAbilityType.DESTABILIZER, values);
	}

	@Test
	public void shouldProcessSensors() {
		subject.processSensors(1L);

		Mockito.verify(shipRepository).resetShipScanRadius(1L);
		Mockito.verify(shipRepository).updateScanRadius(1L, 116, ShipAbilityType.ASTRO_PHYSICS_LAB);
		Mockito.verify(shipRepository).updateScanRadius(1L, 85, ShipAbilityType.EXTENDED_SENSORS);
	}

	@Test
	public void shouldNotAutoTransportWithSubParticleCluster() {
		Ship ship = createSubParticleClusterShip();

		subject.processSubParticleCluster(1L);

		Mockito.verify(shipService, Mockito.never()).transportWithoutPermissionCheck(Mockito.any(), Mockito.any(), Mockito.any());

		Planet planet = new Planet(1L);
		ship.setPlanet(planet);

		subject.processSubParticleCluster(1L);

		Mockito.verify(shipService, Mockito.never()).transportWithoutPermissionCheck(Mockito.any(), Mockito.any(), Mockito.any());
	}

	@Test
	public void shouldAutoTransportWithSubParticleCluster() {
		Ship ship = createSubParticleClusterShip();
		ship.setTaskValue("true");

		Planet planet = new Planet(1L);
		ship.setPlanet(planet);

		subject.processSubParticleCluster(1L);

		ArgumentCaptor<ShipTransportRequest> requestArg = ArgumentCaptor.forClass(ShipTransportRequest.class);
		Mockito.verify(shipService).transportWithoutPermissionCheck(requestArg.capture(), Mockito.eq(ship), Mockito.eq(planet));

		ShipTransportRequest request = requestArg.getValue();
		assertEquals(0, request.getColonists());
		assertEquals(0, request.getFuel());
		assertEquals(0, request.getHeavyGroundUnits());
		assertEquals(0, request.getLightGroundUnits());
		assertEquals(0, request.getSupplies());
		assertEquals(0, request.getMineral1());
		assertEquals(0, request.getMineral2());
		assertEquals(0, request.getMineral3());
	}

	@Test
	public void shouldCheckSuppliesWithSubParticleCluster() {
		Ship ship = createSubParticleClusterShip();
		subject.processSubParticleCluster(1L);
		Mockito.verify(shipRepository, Mockito.never()).save(ship);

		ship.setSupplies(100);
		subject.processSubParticleCluster(1L);
		Mockito.verify(shipRepository).save(ship);

		assertEquals(0, ship.getSupplies());
		assertEquals(100, ship.getMineral1());
		assertEquals(100, ship.getMineral2());
		assertEquals(100, ship.getMineral3());
	}

	@Test
	public void shouldCheckMaximumSuppliesWithSubParticleCluster() {
		Ship ship = createSubParticleClusterShip();

		ship.setSupplies(300);
		subject.processSubParticleCluster(1L);
		Mockito.verify(shipRepository).save(ship);

		assertEquals(13, ship.getSupplies());
		assertEquals(287, ship.getMineral1());
		assertEquals(287, ship.getMineral2());
		assertEquals(287, ship.getMineral3());
	}

	@Test
	public void shouldCheckStorageCapacityWithSubParticleCluster() {
		Ship ship = createSubParticleClusterShip();
		ship.setMineral1(800);
		ship.setMineral2(800);
		ship.setMineral3(800);

		ship.setSupplies(287);
		subject.processSubParticleCluster(1L);
		Mockito.verify(shipRepository).save(ship);

		assertEquals(283, ship.getSupplies());
		assertEquals(804, ship.getMineral1());
		assertEquals(804, ship.getMineral2());
		assertEquals(804, ship.getMineral3());
	}

	@Test
	public void shouldCheckPlanetPresenceWithTerraFormer() {
		createShipWithAbility(ShipAbilityType.TERRA_FORMER_COLD, null);

		subject.processTerraformer(1L, ShipAbilityType.TERRA_FORMER_COLD);

		Mockito.verify(planetRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldProcessColdTerraFormer() {
		Ship ship = createShipWithAbility(ShipAbilityType.TERRA_FORMER_COLD, null);
		Planet planet = new Planet();
		planet.setTemperature(40);
		ship.setPlanet(planet);

		subject.processTerraformer(1L, ShipAbilityType.TERRA_FORMER_COLD);

		Mockito.verify(planetRepository).save(planet);
		assertEquals(39, planet.getTemperature());
	}

	@Test
	public void shouldProcessWarmTerraFormer() {
		Ship ship = createShipWithAbility(ShipAbilityType.TERRA_FORMER_WARM, null);
		Planet planet = new Planet();
		planet.setTemperature(40);
		ship.setPlanet(planet);

		subject.processTerraformer(1L, ShipAbilityType.TERRA_FORMER_WARM);

		Mockito.verify(planetRepository).save(planet);
		assertEquals(41, planet.getTemperature());
	}

	@Test
	public void shouldNotAutoTransportWithQuarkReorganizer() {
		Ship ship = createQuarkReorganizerShip();

		subject.processQuarkReorganizer(1L);

		Mockito.verify(shipService, Mockito.never()).transportWithoutPermissionCheck(Mockito.any(), Mockito.any(), Mockito.any());

		Planet planet = new Planet(1L);
		ship.setPlanet(planet);

		subject.processQuarkReorganizer(1L);

		Mockito.verify(shipService, Mockito.never()).transportWithoutPermissionCheck(Mockito.any(), Mockito.any(), Mockito.any());
	}

	@Test
	public void shouldAutoTransportWithQuarkReorganizer() {
		Ship ship = createQuarkReorganizerShip();
		ship.setTaskValue("true");

		Planet planet = new Planet(1L);
		ship.setPlanet(planet);

		subject.processQuarkReorganizer(1L);

		ArgumentCaptor<ShipTransportRequest> requestArg = ArgumentCaptor.forClass(ShipTransportRequest.class);
		Mockito.verify(shipService).transportWithoutPermissionCheck(requestArg.capture(), Mockito.eq(ship), Mockito.eq(planet));

		ShipTransportRequest request = requestArg.getValue();
		assertEquals(0, request.getColonists());
		assertEquals(0, request.getFuel());
		assertEquals(0, request.getHeavyGroundUnits());
		assertEquals(0, request.getLightGroundUnits());
		assertEquals(0, request.getSupplies());
		assertEquals(0, request.getMineral1());
		assertEquals(0, request.getMineral2());
		assertEquals(0, request.getMineral3());
	}

	@Test
	public void shouldCheckSuppliesWithQuarkReorganizer() {
		Ship ship = createQuarkReorganizerShip();
		subject.processQuarkReorganizer(1L);
		Mockito.verify(shipRepository, Mockito.never()).save(ship);

		ship.setSupplies(100);
		ship.setMineral1(100);
		ship.setMineral2(100);
		subject.processQuarkReorganizer(1L);
		Mockito.verify(shipRepository).save(ship);

		assertEquals(0, ship.getSupplies());
		assertEquals(0, ship.getMineral1());
		assertEquals(0, ship.getMineral2());
		assertEquals(100, ship.getFuel());
	}

	@Test
	public void shouldCheckMaximumSuppliesWithQuarkReorganizer() {
		Ship ship = createQuarkReorganizerShip();

		ship.setSupplies(200);
		ship.setMineral1(200);
		ship.setMineral2(200);
		subject.processQuarkReorganizer(1L);
		Mockito.verify(shipRepository).save(ship);

		assertEquals(87, ship.getSupplies());
		assertEquals(87, ship.getMineral1());
		assertEquals(87, ship.getMineral2());
		assertEquals(113, ship.getFuel());
	}

	@Test
	public void shouldNotAutoTransportWithCybernrittnikk() {
		Ship ship = createCybernrittnikkShip();

		subject.processCybernrittnikk(1L);

		Mockito.verify(shipService, Mockito.never()).transportWithoutPermissionCheck(Mockito.any(), Mockito.any(), Mockito.any());

		Planet planet = new Planet(1L);
		ship.setPlanet(planet);

		subject.processCybernrittnikk(1L);

		Mockito.verify(shipService, Mockito.never()).transportWithoutPermissionCheck(Mockito.any(), Mockito.any(), Mockito.any());
	}

	@Test
	public void shouldAutoTransportWithCybernrittnikk() {
		Ship ship = createCybernrittnikkShip();
		ship.setTaskValue("true");

		Planet planet = new Planet(1L);
		ship.setPlanet(planet);

		subject.processCybernrittnikk(1L);

		ArgumentCaptor<ShipTransportRequest> requestArg = ArgumentCaptor.forClass(ShipTransportRequest.class);
		Mockito.verify(shipService).transportWithoutPermissionCheck(requestArg.capture(), Mockito.eq(ship), Mockito.eq(planet));

		ShipTransportRequest request = requestArg.getValue();
		assertEquals(0, request.getColonists());
		assertEquals(0, request.getFuel());
		assertEquals(0, request.getHeavyGroundUnits());
		assertEquals(0, request.getLightGroundUnits());
		assertEquals(0, request.getSupplies());
		assertEquals(0, request.getMineral1());
		assertEquals(0, request.getMineral2());
		assertEquals(0, request.getMineral3());
	}

	@Test
	public void shouldCheckSuppliesWithCybernrittnikk() {
		Ship ship = createCybernrittnikkShip();
		subject.processCybernrittnikk(1L);
		Mockito.verify(shipRepository, Mockito.never()).save(ship);

		ship.setSupplies(220);
		subject.processCybernrittnikk(1L);
		Mockito.verify(shipRepository).save(ship);

		assertEquals(0, ship.getSupplies());
		assertEquals(100, ship.getColonists());
	}

	@Test
	public void shouldCheckMaximumSuppliesWithCybernrittnikk() {
		Ship ship = createCybernrittnikkShip();

		ship.setSupplies(300);
		subject.processCybernrittnikk(1L);
		Mockito.verify(shipRepository).save(ship);

		assertEquals(80, ship.getSupplies());
		assertEquals(100, ship.getColonists());
	}

	@Test
	public void shouldCheckFuelCostsWithJumpEngine() {
		Ship ship = createJumpEngineShip();

		subject.processJumpEngine(1L);
		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_engine_not_enough_fuel, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());
	}

	@Test
	public void shouldCorrectlyUseJumpEngine() {
		Ship ship = createJumpEngineShip();
		ship.setFuel(100);
		ship.setDestinationX(800);
		ship.setDestinationY(800);

		Mockito.when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.GRAVITY_WAVE_GENERATOR)).thenReturn(Lists.newArrayList());
		Mockito.when(planetRepository.findByGameIdAndXAndY(Mockito.anyLong(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(Optional.empty());

		subject.processJumpEngine(1L);

		assertSuccessfullJumpEngine(ship);
	}

	@Test
	public void shouldCheckGravityWaveShipOwnerWithJumpEngine() {
		Ship ship = createJumpEngineShip();
		ship.setFuel(100);
		ship.setDestinationX(800);
		ship.setDestinationY(800);

		Ship gravityWaveShip = createShipWithAbility(ShipAbilityType.GRAVITY_WAVE_GENERATOR, null);
		gravityWaveShip.setPlayer(ship.getPlayer());
		Mockito.when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.GRAVITY_WAVE_GENERATOR)).thenReturn(Lists.newArrayList(gravityWaveShip));
		Mockito.when(planetRepository.findByGameIdAndXAndY(Mockito.anyLong(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(Optional.empty());

		subject.processJumpEngine(1L);

		assertSuccessfullJumpEngine(ship);
	}

	private void assertSuccessfullJumpEngine(Ship ship) {
		assertEquals(649, ship.getX());
		assertEquals(649, ship.getY());

		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_engine_success, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), 919);
	}

	@Test
	public void shouldCheckGravityWaveShipRelationWithJumpEngine() {
		Ship ship = createJumpEngineShip();
		ship.setFuel(100);
		ship.setDestinationX(800);
		ship.setDestinationY(800);

		Ship gravityWaveShip = createShipWithAbility(ShipAbilityType.GRAVITY_WAVE_GENERATOR, null);
		gravityWaveShip.getPlayer().setId(2L);
		Mockito.when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.GRAVITY_WAVE_GENERATOR)).thenReturn(Lists.newArrayList(gravityWaveShip));
		Mockito.when(planetRepository.findByGameIdAndXAndY(Mockito.anyLong(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(Optional.empty());
		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.ALLIANCE);
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Lists.newArrayList(relation));

		subject.processJumpEngine(1L);

		assertSuccessfullJumpEngine(ship);
	}

	@Test
	public void shouldNotInterceptJumpEngineWithGravityWaveGenerator() {
		Ship ship = createJumpEngineShip();
		ship.setFuel(100);
		ship.setDestinationX(800);
		ship.setDestinationY(800);

		Ship gravityWaveShip = createShipWithAbility(ShipAbilityType.GRAVITY_WAVE_GENERATOR, null);
		gravityWaveShip.setX(1000);
		gravityWaveShip.setY(500);
		gravityWaveShip.getPlayer().setId(2L);
		Mockito.when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.GRAVITY_WAVE_GENERATOR)).thenReturn(Lists.newArrayList(gravityWaveShip));
		Mockito.when(planetRepository.findByGameIdAndXAndY(Mockito.anyLong(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(Optional.empty());
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());

		subject.processJumpEngine(1L);

		assertSuccessfullJumpEngine(ship);
	}

	@Test
	public void shouldInterceptJumpEngineWithGravityWaveGenerator() {
		Ship ship = createJumpEngineShip();
		ship.setFuel(100);
		ship.setDestinationX(800);
		ship.setDestinationY(800);

		Ship gravityWaveShip = createShipWithAbility(ShipAbilityType.GRAVITY_WAVE_GENERATOR, null);
		gravityWaveShip.setX(500);
		gravityWaveShip.setY(500);
		gravityWaveShip.getPlayer().setId(2L);
		Mockito.when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.GRAVITY_WAVE_GENERATOR)).thenReturn(Lists.newArrayList(gravityWaveShip));
		Mockito.when(planetRepository.findByGameIdAndXAndY(Mockito.anyLong(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(Optional.empty());
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());

		subject.processJumpEngine(1L);

		assertEquals(499, ship.getX());
		assertEquals(499, ship.getY());

		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_engine_stopped_mid_flight, ship.createFullImagePath(),
				ship.getId(), NewsEntryClickTargetType.ship, ship.getName(), 706);
	}

	@Test
	public void shouldCheckMapBoundariesWithJumpEngine() {
		Ship ship = createJumpEngineShip();
		ship.setFuel(100);
		ship.setDestinationX(-800);
		ship.setDestinationY(-800);

		Mockito.when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.GRAVITY_WAVE_GENERATOR)).thenReturn(Lists.newArrayList());
		Mockito.when(planetRepository.findByGameIdAndXAndY(Mockito.anyLong(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(Optional.empty());
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());

		subject.processJumpEngine(1L);

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(shipService).deleteShip(ship, null);
	}

	@Test
	public void shouldCheckGravityWaveCosts() {
		Ship ship = createGravityWaveGeneratorShip();

		subject.processGravityWaveGenerator(1L);

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_gravity_wave_generator_not_enough_mineral3, ship.createFullImagePath(),
				ship.getId(), NewsEntryClickTargetType.ship, ship.getName());
	}

	private void assertNoGravityWaveGeneratorInteractions() {
		Mockito.verify(shipRepository).save(Mockito.any());
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}

	private void assertGravityWaveGeneratorActivatedSuccessfully(Ship ship) {
		Mockito.verify(shipRepository).save(ship);
		assertEquals(20, ship.getMineral3());
	}

	@Test
	public void shouldIgnoreEmptyShipsWithGravityWave() {
		Ship ship = createGravityWaveGeneratorShip();
		ship.setMineral3(120);

		subject.processGravityWaveGenerator(1L);

		assertGravityWaveGeneratorActivatedSuccessfully(ship);
		assertNoGravityWaveGeneratorInteractions();
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldIgnoreSlowShipsWithGravityWave() {
		Ship ship = createGravityWaveGeneratorShip();
		ship.setMineral3(120);
		ship.setTravelSpeed(7);

		Mockito.when(shipRepository.findByIds(Mockito.anyList())).thenReturn(Lists.newArrayList(ship));
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 1L)).thenReturn(Collections.emptyList());

		subject.processGravityWaveGenerator(1L);

		assertGravityWaveGeneratorActivatedSuccessfully(ship);
		assertNoGravityWaveGeneratorInteractions();
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldSlowDownSelfWithGravityWave() {
		Ship ship = createGravityWaveGeneratorShip();
		ship.setMineral3(120);
		ship.setTravelSpeed(8);

		Mockito.when(shipRepository.findByIds(Mockito.anyList())).thenReturn(Lists.newArrayList(ship));
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 1L)).thenReturn(Collections.emptyList());

		subject.processGravityWaveGenerator(1L);

		Mockito.verify(shipRepository, Mockito.times(2)).save(ship);
		assertEquals(20, ship.getMineral3());
		assertEquals(7, ship.getTravelSpeed());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_gravity_wave_generator_slowed, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());
		Mockito.verify(newsService).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any(), Mockito.any());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldSlowDownOwnShipsWithGravityWave() {
		Ship ship = createGravityWaveGeneratorShip();
		ship.setMineral3(120);

		Ship ownShip = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		ownShip.setTravelSpeed(8);
		Mockito.when(shipRepository.findByIds(Mockito.anyList())).thenReturn(Lists.newArrayList(ownShip));
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 1L)).thenReturn(Collections.emptyList());

		subject.processGravityWaveGenerator(1L);

		assertGravityWaveGeneratorActivatedSuccessfully(ship);
		Mockito.verify(shipRepository).save(ownShip);
		assertEquals(7, ownShip.getTravelSpeed());
		Mockito.verify(newsService).add(ownShip.getPlayer(), NewsEntryConstants.news_entry_gravity_wave_generator_slowed, ownShip.createFullImagePath(),
				ownShip.getId(), NewsEntryClickTargetType.ship, ownShip.getName());
		Mockito.verify(newsService).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any(), Mockito.any());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldSlowDownAlliedShipsWithGravityWave() {
		Ship ship = createGravityWaveGeneratorShip();
		ship.setMineral3(120);

		Ship alliedShip = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		alliedShip.setTravelSpeed(8);
		Mockito.when(shipRepository.findByIds(Mockito.anyList())).thenReturn(Lists.newArrayList(alliedShip));
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 1L)).thenReturn(Collections.emptyList());

		subject.processGravityWaveGenerator(1L);

		assertGravityWaveGeneratorActivatedSuccessfully(ship);
		Mockito.verify(shipRepository).save(alliedShip);
		assertEquals(7, alliedShip.getTravelSpeed());
		Mockito.verify(newsService).add(alliedShip.getPlayer(), NewsEntryConstants.news_entry_gravity_wave_generator_slowed, alliedShip.createFullImagePath(),
				alliedShip.getId(), NewsEntryClickTargetType.ship, alliedShip.getName());
		Mockito.verify(newsService).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any(), Mockito.any());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldInterceptShipsWithGravityWave() {
		Ship ship = createGravityWaveGeneratorShip();
		ship.setMineral3(120);

		Ship otherShip = createJumpEngineShip();
		otherShip.setActiveAbility(otherShip.getAbility(ShipAbilityType.JUMP_ENGINE).get());
		otherShip.setPlayer(new Player(2L));
		Mockito.when(shipRepository.findByIds(Mockito.anyList())).thenReturn(Lists.newArrayList(otherShip));
		Mockito.when(playerRelationRepository.findByPlayerIds(2L, 1L)).thenReturn(Collections.emptyList());

		subject.processGravityWaveGenerator(1L);

		assertGravityWaveGeneratorActivatedSuccessfully(ship);
		Mockito.verify(shipRepository).save(otherShip);
		Mockito.verify(newsService).add(otherShip.getPlayer(), NewsEntryConstants.news_entry_jump_engine_stopped, otherShip.createFullImagePath(),
				otherShip.getId(), NewsEntryClickTargetType.ship, otherShip.getName());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_gravity_wave_generator_stopped_jumps, ship.createFullImagePath(),
				ship.getId(), NewsEntryClickTargetType.ship, ship.getName());
		Mockito.verify(newsService, Mockito.times(2)).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any(),
				Mockito.any());
	}

	@Test
	public void shouldCheckIfShipCloaked() {
		Ship ship = ShipTestFactory.createShip(100, 1000, 0, 0, 100, 1L);
		ship.setMineral2(1);

		subject.updateCloak(ship);

		assertEquals(false, ship.isCloaked());
		assertEquals(1, ship.getMineral2());

		ship.setMineral2(20);

		subject.updateCloak(ship);

		assertEquals(true, ship.isCloaked());
		assertEquals(10, ship.getMineral2());
	}

	@Test
	public void shouldCheckPropulsionSystem() {
		MasterDataService.RANDOM.setSeed(3L);

		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		ship.setCloaked(true);

		PropulsionSystemTemplate propulsion = new PropulsionSystemTemplate();
		propulsion.setTechLevel(7);
		ship.setPropulsionSystemTemplate(propulsion);

		subject.finishCloakingChecks(ship);

		assertEquals(true, ship.isCloaked());
		Mockito.verify(subject).checkCloakingPlasmaDischarge(Mockito.eq(ship), Mockito.anyFloat());
		Mockito.verify(shipRepository).clearDestinationShip(Lists.newArrayList(3L));
	}

	@Test
	public void shouldTriggerPlasmaDischarge() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		ship.setCloaked(true);

		PropulsionSystemTemplate propulsion = new PropulsionSystemTemplate();
		propulsion.setTechLevel(7);
		ship.setPropulsionSystemTemplate(propulsion);

		subject.checkCloakingPlasmaDischarge(ship, 0.1f);

		assertEquals(false, ship.isCloaked());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_cloaking_failed_plasma_discharge, ship.createFullImagePath(),
				ship.getId(), NewsEntryClickTargetType.ship, ship.getName());
	}

	@Test
	public void shouldProcessPerfectCloaking() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		Mockito.when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.CLOAKING_PERFECT)).thenReturn(Lists.newArrayList(ship));

		subject.processPerfectCloaking(1L);

		Mockito.verify(subject).updateCloak(ship);
		Mockito.verify(subject).finishCloakingChecks(ship);
		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(shipRepository).cloakAntigravPropulsion(1L);
	}

	@Test
	public void shouldProcessReliableCloaking() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		ship.setPropulsionSystemTemplate(new PropulsionSystemTemplate());
		Mockito.when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.CLOAKING_RELIABLE)).thenReturn(Lists.newArrayList(ship));

		subject.processReliableCloaking(1L);

		Mockito.verify(subject).updateCloak(ship);
		Mockito.verify(subject).finishCloakingChecks(ship);
		Mockito.verify(shipRepository).save(ship);
	}

	@Test
	public void shouldProcessUnreliableCloaking() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		ship.setPropulsionSystemTemplate(new PropulsionSystemTemplate());
		Mockito.when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.CLOAKING_UNRELIABLE)).thenReturn(Lists.newArrayList(ship));
		Mockito.when(configProperties.getVisibilityRadius()).thenReturn(83);

		subject.processUnreliableCloaking(1L);

		Mockito.verify(subject).updateCloak(ship);
		Mockito.verify(subject).finishCloakingChecks(ship);
		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(shipRepository).getShipIdsInRadius(1L, 0, 0, 83);
		Mockito.verify(planetRepository).getPlanetIdsInRadius(1L, 0, 0, 83, false);
	}

	@Test
	public void shouldCheckForActiveSubspaceDistortion() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setActiveAbility(new ShipAbility(ShipAbilityType.SUB_SPACE_DISTORTION));

		Mockito.when(shipRepository.findByIds(Mockito.any())).thenReturn(Lists.newArrayList(ship));

		subject.damageShipsBySubSpaceDistortion(1L, ship, 50);

		assertEquals(0, ship.getDamage());

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(shipService, Mockito.never()).deleteShip(Mockito.any(), Mockito.any());
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}

	@Test
	public void shouldDamageShipBySubspaceDistortion() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);

		Mockito.when(shipRepository.findByIds(Mockito.any())).thenReturn(Lists.newArrayList(ship));

		subject.damageShipsBySubSpaceDistortion(1L, ship, 50);

		assertEquals(33, ship.getDamage());

		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_damaged_by_subspace_distortion, ship.createFullImagePath(),
				ship.getId(), NewsEntryClickTargetType.ship, ship.getName(), 33);

		Mockito.verify(shipService, Mockito.never()).deleteShip(Mockito.any(), Mockito.any());
	}

	@Test
	public void shouldDestroyShipBySubspaceDistortion() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);

		Mockito.when(shipRepository.findByIds(Mockito.any())).thenReturn(Lists.newArrayList(ship));

		subject.damageShipsBySubSpaceDistortion(1L, ship, 1000);

		Mockito.verify(shipRepository, Mockito.never()).save(ship);
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_destroyed_by_subspace_distortion, ship.createFullImagePath(), null,
				null, ship.getName());

		Mockito.verify(shipService).deleteShip(ship, ship.getPlayer());
	}

	@Test
	public void shouldNotDestroySpaceFoldBySubspaceDistortion() {
		MasterDataService.RANDOM.setSeed(2L);

		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);

		Mockito.when(spaceFoldRepository.getSpaceFoldIdsInRadius(1L, 0, 0, 0)).thenReturn(Lists.newArrayList(123L));

		subject.destroySpaceFoldsBySubSpaceDistortion(1L, ship, 1);

		Mockito.verify(spaceFoldRepository, Mockito.never()).deleteById(Mockito.anyLong());
	}

	@Test
	public void shouldDestroySpaceFoldBySubspaceDistortion() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);

		Mockito.when(spaceFoldRepository.getSpaceFoldIdsInRadius(1L, 0, 0, 0)).thenReturn(Lists.newArrayList(123L));

		subject.destroySpaceFoldsBySubSpaceDistortion(1L, ship, 10);

		Mockito.verify(spaceFoldRepository).deleteById(123L);
	}

	@Test
	public void shouldProcessSubspaceDistortions() {
		Map<String, String> values = new HashMap<>(1);
		values.put(ShipAbilityConstants.SUB_SPACE_DISTORTION_LEVEL, "1");
		Ship ship = createShipWithAbility(ShipAbilityType.SUB_SPACE_DISTORTION, values);

		subject.processSubSpaceDistortion(1L);

		Mockito.verify(subject).damageShipsBySubSpaceDistortion(1L, ship, 50);
		Mockito.verify(subject).destroySpaceFoldsBySubSpaceDistortion(1L, ship, 1);

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_created_subspace_distortion, ship.createFullImagePath(), null, null,
				ship.getName());

		Mockito.verify(shipService).deleteShip(ship, null, false);
	}

	@Test
	public void shouldCompleteUnfinishedJumpPortal() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		WormHole wormHole = new WormHole();
		Optional<WormHole> unfinishedJumpPortalOpt = Optional.of(wormHole);
		subject.buildJumpPortal(ship, unfinishedJumpPortalOpt);

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_portals_completed, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());

		ArgumentCaptor<WormHole> wormHoleArgs = ArgumentCaptor.forClass(WormHole.class);
		Mockito.verify(wormHoleRepository, Mockito.times(2)).save(wormHoleArgs.capture());

		List<WormHole> wormHoles = wormHoleArgs.getAllValues();
		WormHole secondPortal = wormHoles.get(0);
		WormHole firstPortal = wormHoles.get(1);

		assertEquals(WormHoleType.JUMP_PORTAL, secondPortal.getType());

		assertEquals(firstPortal, secondPortal.getConnection());
		assertEquals(secondPortal, firstPortal.getConnection());
	}

	@Test
	public void shouldStartNewJumpPortal() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		Optional<WormHole> unfinishedJumpPortalOpt = Optional.empty();
		subject.buildJumpPortal(ship, unfinishedJumpPortalOpt);

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_portal_build, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());

		ArgumentCaptor<WormHole> wormHoleArgs = ArgumentCaptor.forClass(WormHole.class);
		Mockito.verify(wormHoleRepository).save(wormHoleArgs.capture());

		WormHole newWormHole = wormHoleArgs.getValue();
		assertNull(newWormHole.getConnection());
		assertEquals(WormHoleType.JUMP_PORTAL, newWormHole.getType());
	}

	@Test
	public void shouldCheckTravelSpeedOfJumpPortalShip() {
		Ship ship = createJumpPortalShip();
		ship.setTravelSpeed(5);

		subject.processJumpPortalConstruction(1L);

		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
		Mockito.verify(subject, Mockito.never()).buildJumpPortal(Mockito.any(), Mockito.any());
	}

	@Test
	public void shouldCheckNearPlanetsOfJumpPortalShip() {
		Ship ship = createJumpPortalShip();

		Mockito.when(planetRepository.getPlanetIdsInRadius(1L, 0, 0, 0, false)).thenReturn(Lists.newArrayList(1L));

		subject.processJumpPortalConstruction(1L);

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_portal_planets_too_near, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());
		Mockito.verify(subject, Mockito.never()).buildJumpPortal(Mockito.any(), Mockito.any());
	}

	@Test
	public void shouldCheckNearWormHolesOfJumpPortalShip() {
		Ship ship = createJumpPortalShip();

		Mockito.when(wormHoleRepository.getWormHoleIdsInRadius(1L, 0, 0, 0)).thenReturn(Lists.newArrayList(1L));

		subject.processJumpPortalConstruction(1L);

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_portal_anomalies_too_near, ship.createFullImagePath(),
				ship.getId(), NewsEntryClickTargetType.ship, ship.getName());
		Mockito.verify(subject, Mockito.never()).buildJumpPortal(Mockito.any(), Mockito.any());
	}

	@Test
	public void shouldCheckResourcesOfJumpPortalShip() {
		Ship ship = createJumpPortalShip();

		subject.processJumpPortalConstruction(1L);

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_portal_not_enough_resources, ship.createFullImagePath(),
				ship.getId(), NewsEntryClickTargetType.ship, ship.getName());
		Mockito.verify(subject, Mockito.never()).buildJumpPortal(Mockito.any(), Mockito.any());
	}

	@Test
	public void shouldBuildJumpPortal() {
		Ship ship = createJumpPortalShip();
		ship.setFuel(100);
		ship.setMineral1(100);
		ship.setMineral2(100);
		ship.setMineral3(100);

		Optional<WormHole> wormHoleOpt = Optional.empty();
		Mockito.when(wormHoleRepository.findUnfinishedJumpPortal(3L)).thenReturn(wormHoleOpt);

		subject.processJumpPortalConstruction(1L);

		Mockito.verify(subject).buildJumpPortal(ship, wormHoleOpt);

		assertEquals(90, ship.getFuel());
		assertEquals(80, ship.getMineral1());
		assertEquals(70, ship.getMineral2());
		assertEquals(60, ship.getMineral3());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldCheckForEmptyShipListWithPerfectEvade() {
		subject.processPerfectEvade(1L);

		Mockito.verify(shipRepository, Mockito.never()).saveAll(Mockito.any());
		Mockito.verify(shipRepository, Mockito.never()).clearDestinationShip(Mockito.anyList());
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}

	@Test
	public void shouldPerformPerfectEvade() {
		Ship ship = createShipWithAbility(ShipAbilityType.EVADE_PERFECT, Collections.emptyMap());

		Mockito.when(planetRepository.findByGameIdAndXAndY(1L, 0, 0)).thenReturn(Optional.empty());

		subject.processPerfectEvade(1L);

		Mockito.verify(shipRepository).saveAll(Lists.newArrayList(ship));
		Mockito.verify(shipRepository).clearDestinationShip(Lists.newArrayList(3L));
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_evade_perfect, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldCheckForEmptyShipListWithUnreliableEvade() {
		subject.processUnreliableEvade(1L);

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(shipService, Mockito.never()).deleteShip(Mockito.any(), Mockito.any());
		Mockito.verify(shipRepository, Mockito.never()).clearDestinationShip(Mockito.anyList());
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldGetDestroyedByEvadeDamage() {
		Map<String, String> values = new HashMap<>(1);
		values.put(ShipAbilityConstants.EVADE_DAMAGE_MAX, "5000");
		Ship ship = createShipWithAbility(ShipAbilityType.EVADE_UNRELIABLE, values);

		Mockito.when(planetRepository.findByGameIdAndXAndY(1L, 0, 0)).thenReturn(Optional.empty());

		subject.processUnreliableEvade(1L);

		Mockito.verify(shipService).deleteShip(ship, null);
		Mockito.verify(newsService).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any(), Mockito.any());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_evade_destroyed, ship.createFullImagePath(), null, null,
				ship.getName());

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(shipRepository, Mockito.never()).clearDestinationShip(Mockito.anyList());
	}

	@Test
	public void shouldPerformUnreliableEvade() {
		Map<String, String> values = new HashMap<>(1);
		values.put(ShipAbilityConstants.EVADE_DAMAGE_MAX, "5");
		Ship ship = createShipWithAbility(ShipAbilityType.EVADE_UNRELIABLE, values);

		Mockito.when(planetRepository.findByGameIdAndXAndY(1L, 0, 0)).thenReturn(Optional.empty());

		subject.processUnreliableEvade(1L);

		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(shipRepository).clearDestinationShip(Lists.newArrayList(3L));
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_evade_damaged, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), 5);

		Mockito.verify(shipService, Mockito.never()).deleteShip(Mockito.any(), Mockito.any());
	}

	@Test
	public void shouldCheckViralInvasionTypeColonistsWithMedicalCenter() {
		Ship ship = createViralInvasionShip();
		Planet planet = new Planet(1L);
		planet.setPlayer(new Player(4L));
		ship.setPlanet(planet);

		subject.processMedicalCenter(ship, true, false);

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_natives_unsuccessfull, ImageConstants.news_epidemie,
				ship.getId(), NewsEntryClickTargetType.ship, ship.getName(), planet.getName());
		Mockito.verify(newsService).add(planet.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_natives_prevented_colonists,
				ImageConstants.news_epidemie, planet.getId(), NewsEntryClickTargetType.planet, planet.getName());
	}

	@Test
	public void shouldCheckViralInvasionTypeNativesWithMedicalCenter() {
		Ship ship = createViralInvasionShip();
		Planet planet = new Planet(1L);
		planet.setPlayer(new Player(4L));
		ship.setPlanet(planet);

		subject.processMedicalCenter(ship, false, true);

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_natives_unsuccessfull, ImageConstants.news_epidemie,
				ship.getId(), NewsEntryClickTargetType.ship, ship.getName(), planet.getName());
		Mockito.verify(newsService).add(planet.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_natives_prevented_natives,
				ImageConstants.news_epidemie, planet.getId(), NewsEntryClickTargetType.planet, planet.getName());
	}

	@Test
	public void shouldCheckViralInvasionTypeColonistsWithViralInvasion() {
		Ship ship = createViralInvasionShip();
		Planet planet = new Planet(1L);
		planet.setColonists(1000);
		planet.setNativeSpecies(new NativeSpecies());
		planet.setNativeSpeciesCount(2000);
		planet.setPlayer(new Player(4L));
		ship.setPlanet(planet);

		subject.performViralInvasion(ship, true, false);

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_killed_colonists, ImageConstants.news_epidemie,
				ship.getId(), NewsEntryClickTargetType.ship, ship.getName(), planet.getName(), 890, 89);
		Mockito.verify(newsService).add(planet.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_colonists_got_killed, ImageConstants.news_epidemie,
				planet.getId(), NewsEntryClickTargetType.planet, planet.getName(), 890, 89);

		assertEquals(110, planet.getColonists());
		assertEquals(2000, planet.getNativeSpeciesCount());
	}

	@Test
	public void shouldCheckViralInvasionTypeNativesWithViralInvasion() {
		Ship ship = createViralInvasionShip();
		Planet planet = new Planet(1L);
		planet.setColonists(1000);
		planet.setNativeSpecies(new NativeSpecies());
		planet.setNativeSpeciesCount(2000);
		planet.setPlayer(new Player(4L));
		ship.setPlanet(planet);

		subject.performViralInvasion(ship, false, true);

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_killed_natives, ImageConstants.news_epidemie,
				ship.getId(), NewsEntryClickTargetType.ship, ship.getName(), planet.getName(), 1780, 89);
		Mockito.verify(newsService).add(planet.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_natives_got_killed, ImageConstants.news_epidemie,
				planet.getId(), NewsEntryClickTargetType.planet, planet.getName(), 1780, 89);

		assertEquals(1000, planet.getColonists());
		assertEquals(220, planet.getNativeSpeciesCount());
	}

	@Test
	public void shouldCheckPlanetPlayerPresentWithNativeViralInvasion() {
		Ship ship = createViralInvasionShip();
		Planet planet = new Planet(1L);
		planet.setColonists(1000);
		planet.setNativeSpecies(new NativeSpecies());
		planet.setNativeSpeciesCount(2000);
		ship.setPlanet(planet);

		subject.performViralInvasion(ship, false, true);

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_killed_natives, ImageConstants.news_epidemie,
				ship.getId(), NewsEntryClickTargetType.ship, ship.getName(), planet.getName(), 1780, 89);
		Mockito.verify(newsService, Mockito.never()).add(planet.getPlayer(), NewsEntryConstants.news_entry_viral_invasion_natives_got_killed,
				ImageConstants.news_epidemie, planet.getId(), NewsEntryClickTargetType.planet, planet.getName(), 1780, 89);

		assertEquals(1000, planet.getColonists());
		assertEquals(220, planet.getNativeSpeciesCount());
	}

	@Test
	public void shouldCheckPlanetPresentWithViralInvasion() {
		createViralInvasionShip();

		subject.processViralInvasion(1L);

		Mockito.verify(subject, Mockito.never()).performViralInvasion(Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean());
		Mockito.verify(subject, Mockito.never()).processMedicalCenter(Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean());
	}

	@Test
	public void shouldCheckPlanetPlayerAgainstShipPlayerWithViralInvasion() {
		Ship ship = createViralInvasionShip();
		Planet planet = new Planet(1L);
		planet.setPlayer(ship.getPlayer());
		ship.setPlanet(planet);

		subject.processViralInvasion(1L);

		Mockito.verify(subject, Mockito.never()).performViralInvasion(Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean());
		Mockito.verify(subject, Mockito.never()).processMedicalCenter(Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean());
	}

	@Test
	public void shouldCheckPlayerRelationWithViralInvasion() {
		Ship ship = createViralInvasionShip();
		Planet planet = new Planet(1L);
		planet.setPlayer(new Player(4L));
		ship.setPlanet(planet);

		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.ALLIANCE);
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 4L)).thenReturn(Lists.newArrayList(relation));

		subject.processViralInvasion(1L);

		Mockito.verify(subject, Mockito.never()).performViralInvasion(Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean());
		Mockito.verify(subject, Mockito.never()).processMedicalCenter(Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean());
	}

	@Test
	public void shouldCheckMedicalCenterWithViralInvasion() {
		Ship ship = createViralInvasionShip();
		Planet planet = new Planet(1L);
		planet.setPlayer(new Player(4L));
		OrbitalSystem medicalCenter = new OrbitalSystem();
		medicalCenter.setType(OrbitalSystemType.MEDICAL_CENTER);
		planet.setOrbitalSystems(Lists.newArrayList(medicalCenter));
		ship.setPlanet(planet);
		ship.setTaskValue(ShipAbilityConstants.VIRAL_INVASION_COLONISTS);

		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.WAR);
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 4L)).thenReturn(Lists.newArrayList(relation));

		subject.processViralInvasion(1L);

		Mockito.verify(subject).processMedicalCenter(ship, true, false);
		Mockito.verify(subject, Mockito.never()).performViralInvasion(Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean());
	}

	@Test
	public void shouldPerformViralInvasion() {
		Ship ship = createViralInvasionShip();
		Planet planet = new Planet(1L);
		planet.setPlayer(new Player(4L));
		ship.setPlanet(planet);
		ship.setTaskValue(ShipAbilityConstants.VIRAL_INVASION_NATIVES);

		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 4L)).thenReturn(Collections.emptyList());

		subject.processViralInvasion(1L);

		Mockito.verify(subject).performViralInvasion(ship, false, true);
		Mockito.verify(subject, Mockito.never()).processMedicalCenter(Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean());
	}

	@Test
	public void shouldCheckPlanetPresentWithDestabilizer() {
		createDestabilizerShip();

		subject.processDestabilizer(1L);

		Mockito.verify(subject, Mockito.never()).destroyPlanet(Mockito.anyLong(), Mockito.any(), Mockito.any());
	}

	@Test
	public void shouldCheckPlayerWithDestabilizer() {
		Ship ship = createDestabilizerShip();
		Planet planet = new Planet(1L);
		planet.setPlayer(ship.getPlayer());
		ship.setPlanet(planet);

		subject.processDestabilizer(1L);

		Mockito.verify(subject, Mockito.never()).destroyPlanet(Mockito.anyLong(), Mockito.any(), Mockito.any());
	}

	@Test
	public void shouldCheckAlliancesWithDestabilizer() {
		Ship ship = createDestabilizerShip();
		Planet planet = new Planet(1L);
		Player alliedPlayer = new Player(2L);
		planet.setPlayer(alliedPlayer);
		ship.setPlanet(planet);

		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.ALLIANCE);
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Lists.newArrayList(relation));

		subject.processDestabilizer(1L);

		Mockito.verify(subject, Mockito.never()).destroyPlanet(Mockito.anyLong(), Mockito.any(), Mockito.any());
	}

	@Test
	public void shouldCheckPlanetaryShieldWithDestabilizer() {
		Ship ship = createDestabilizerShip();
		Planet planet = new Planet(1L);
		Player alliedPlayer = new Player(2L);
		planet.setPlayer(alliedPlayer);
		OrbitalSystem planetaryShield = new OrbitalSystem();
		planetaryShield.setType(OrbitalSystemType.PLANETARY_SHIELDS);
		planet.setOrbitalSystems(Lists.newArrayList(planetaryShield));
		ship.setPlanet(planet);

		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.TRADE_AGREEMENT);
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Lists.newArrayList(relation));

		subject.processDestabilizer(1L);

		Mockito.verify(subject, Mockito.never()).destroyPlanet(Mockito.anyLong(), Mockito.any(), Mockito.any());
	}

	@Test
	public void shouldCheckEfficiencyWithDestabilizer() {
		Ship ship = createDestabilizerShip();
		Planet planet = new Planet(1L);
		Player otherPlayer = new Player(2L);
		planet.setPlayer(otherPlayer);
		ship.setPlanet(planet);

		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());

		subject.processDestabilizer(1L);

		Mockito.verify(subject, Mockito.never()).destroyPlanet(Mockito.anyLong(), Mockito.any(), Mockito.any());
	}

	@Test
	public void shouldUseDestabilizer() {
		Ship ship = createDestabilizerShip();
		ship.getAbility(ShipAbilityType.DESTABILIZER).get().getValues().put(ShipAbilityConstants.DESTABILIZER_EFFICIENCY, "100");
		Planet planet = new Planet(1L);
		Player otherPlayer = new Player(2L);
		planet.setPlayer(otherPlayer);
		ship.setPlanet(planet);

		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());
		Player player = ship.getPlayer();
		player.getGame().setPlayers(Collections.emptyList());

		subject.processDestabilizer(1L);

		Mockito.verify(subject).destroyPlanet(1L, player, planet);
	}

	@Test
	public void shouldCheckPlayerEqualWithDestabilizer() {
		Ship ship = createDestabilizerShip();
		Planet planet = new Planet(1L);
		Player otherPlayer = new Player(2L);
		planet.setPlayer(otherPlayer);
		ship.setPlanet(planet);

		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());
		Player player = ship.getPlayer();
		player.getGame().setPlayers(Lists.newArrayList(player, otherPlayer));

		subject.destroyPlanet(1L, player, planet);

		Mockito.verify(newsService).add(player, NewsEntryConstants.news_entry_planet_destroyed, ImageConstants.news_explode, null, null, planet.getName(),
				"A1");
		Mockito.verify(newsService).add(otherPlayer, NewsEntryConstants.news_entry_our_planet_destroyed, ImageConstants.news_explode, null, null,
				planet.getName(), "A1");
		Mockito.verify(planetRepository).delete(planet);
		Mockito.verify(starbaseService, Mockito.never()).delete(Mockito.any());
	}

	@Test
	public void shouldCheckStarbasePresentWithDestabilizer() {
		Ship ship = createDestabilizerShip();
		Planet planet = new Planet(1L);
		Player otherPlayer = new Player(2L);
		planet.setPlayer(otherPlayer);
		ship.setPlanet(planet);
		Starbase starbase = new Starbase();
		planet.setStarbase(starbase);

		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());
		Player player = ship.getPlayer();
		player.getGame().setPlayers(Collections.emptyList());

		subject.destroyPlanet(1L, player, planet);

		Mockito.verify(planetRepository).delete(planet);
		Mockito.verify(starbaseService).delete(starbase);
	}
}