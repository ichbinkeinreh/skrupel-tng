package org.skrupeltng.modules.ingame.service.round.combat.orbit;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;

public class OrbitalCombatRoundCalculatorUnitTest {

	@Spy
	@InjectMocks
	private final OrbitalCombatRoundCalculator subject = new OrbitalCombatRoundCalculator();

	@Mock
	private OrbitalCombat orbitalCombat;

	@Mock
	private ShipRepository shipRepository;

	@Mock
	private ShipService shipService;

	@Mock
	private PoliticsService politicsService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldCheckOrbitalShield() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.getShipTemplate().getShipAbilities().add(new ShipAbility(ShipAbilityType.ORBITAL_SHIELD));

		Set<Ship> destroyedShips = new HashSet<>();
		Map<Long, Integer> totalPlanetaryDefenses = new HashMap<>();
		subject.processShip(destroyedShips, ship, totalPlanetaryDefenses);

		Mockito.verify(orbitalCombat, Mockito.never()).checkStructurScanner(Mockito.any());
		Mockito.verify(orbitalCombat, Mockito.never()).processCombat(Mockito.any(), Mockito.anyInt());
		Mockito.verify(orbitalCombat, Mockito.never()).handlePlanetDamage(Mockito.anyLong(), Mockito.anyMap(), Mockito.anyInt());
		Mockito.verify(orbitalCombat, Mockito.never()).handleShipDamage(Mockito.anySet(), Mockito.any());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldCheckAlliedPlayer() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		Planet planet = new Planet(1L);
		planet.setPlayer(new Player(2L));
		ship.setPlanet(planet);

		Mockito.when(politicsService.isAlly(1L, 2L)).thenReturn(true);

		Set<Ship> destroyedShips = new HashSet<>();
		Map<Long, Integer> totalPlanetaryDefenses = new HashMap<>();
		totalPlanetaryDefenses.put(1L, 100);
		subject.processShip(destroyedShips, ship, totalPlanetaryDefenses);

		Mockito.verify(orbitalCombat, Mockito.never()).checkStructurScanner(Mockito.any());
		Mockito.verify(orbitalCombat, Mockito.never()).processCombat(Mockito.any(), Mockito.anyInt());
		Mockito.verify(orbitalCombat, Mockito.never()).handlePlanetDamage(Mockito.anyLong(), Mockito.anyMap(), Mockito.anyInt());
		Mockito.verify(orbitalCombat, Mockito.never()).handleShipDamage(Mockito.anySet(), Mockito.any());
	}

	@Test
	public void shouldCallOrbitalCombat() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		Planet planet = new Planet(1L);
		planet.setPlayer(new Player(2L));
		ship.setPlanet(planet);

		Set<Ship> destroyedShips = new HashSet<>();
		Map<Long, Integer> totalPlanetaryDefenses = new HashMap<>();
		totalPlanetaryDefenses.put(1L, 100);
		subject.processShip(destroyedShips, ship, totalPlanetaryDefenses);

		Mockito.verify(orbitalCombat).checkStructurScanner(ship);
		Mockito.verify(orbitalCombat).processCombat(ship, 100);
		Mockito.verify(orbitalCombat).handlePlanetDamage(1L, totalPlanetaryDefenses, 0);
		Mockito.verify(orbitalCombat).handleShipDamage(destroyedShips, ship);
	}
}