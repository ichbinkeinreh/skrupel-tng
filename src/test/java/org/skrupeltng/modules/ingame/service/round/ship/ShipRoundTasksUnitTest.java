package org.skrupeltng.modules.ingame.service.round.ship;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineField;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineFieldRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.springframework.data.util.Pair;

import com.google.common.collect.Lists;

public class ShipRoundTasksUnitTest {

	@Spy
	@InjectMocks
	private final ShipRoundTasks subject = new ShipRoundTasks();

	@Mock
	private ConfigProperties configProperties;

	@Mock
	private NewsService newsService;

	@Mock
	private PlanetRepository planetRepository;

	@Mock
	private PlayerRelationRepository playerRelationRepository;

	@Mock
	private ShipRepository shipRepository;

	@Mock
	private ShipService shipService;

	@Mock
	private MineFieldRepository mineFieldRepository;

	@Mock
	private GameRepository gameRepository;

	@Mock
	private StatsUpdater statsUpdater;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		Mockito.when(shipService.transportWithoutPermissionCheck(Mockito.any(), Mockito.any(), Mockito.any())).then(new Answer<Pair<Ship, Planet>>() {
			@Override
			public Pair<Ship, Planet> answer(InvocationOnMock invocation) throws Throwable {
				Ship ship = invocation.getArgumentAt(1, Ship.class);
				Planet planet = invocation.getArgumentAt(2, Planet.class);
				return Pair.of(ship, planet);
			}
		});

		Mockito.when(planetRepository.save(Mockito.any())).thenAnswer(new Answer<Planet>() {
			@Override
			public Planet answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgumentAt(0, Planet.class);
			}
		});
	}

	private Ship createShipWithTask(ShipTaskType task) {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setTaskType(task);

		Mockito.when(shipRepository.getShipsWithTask(1L, task)).thenReturn(Lists.newArrayList(ship));

		return ship;
	}

	private Ship createCreateMineFieldShip(int projectiles) {
		Mockito.when(gameRepository.mineFieldsEnabled(1L)).thenReturn(true);
		Ship ship = createShipWithTask(ShipTaskType.CREATE_MINE_FIELD);
		ship.setTaskValue(projectiles + "");
		return ship;
	}

	private Ship createAutoRefuelShip() {
		Ship ship = createShipWithTask(ShipTaskType.AUTOREFUEL);
		return ship;
	}

	private Ship createRecycleShip() {
		Ship ship = createShipWithTask(ShipTaskType.SHIP_RECYCLE);
		ShipTemplate template = ship.getShipTemplate();
		template.setCostMineral1(100);
		template.setCostMineral2(1000);
		template.setCostMineral3(10000);
		return ship;
	}

	private Ship createAutoDestructionShip() {
		Ship ship = createShipWithTask(ShipTaskType.AUTO_DESTRUCTION);
		ship.getShipTemplate().setTechLevel(1);
		return ship;
	}

	@Test
	public void shouldResetShipTasksBecauseNoValueForTractorBeam() {
		Ship ship = createShipWithTask(ShipTaskType.TRACTOR_BEAM);

		subject.precheckTractorBeam(1L);

		Mockito.verify(shipRepository).save(ship);
		assertEquals(ShipTaskType.NONE, ship.getTaskType());
	}

	@Test
	public void shouldResetShipTasksBecauseTargetNotOnSamePosition() {
		Ship ship = createShipWithTask(ShipTaskType.TRACTOR_BEAM);
		ship.setTaskValue("2");

		Ship targetShip = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		targetShip.setX(2);
		Mockito.when(shipRepository.getOne(2L)).thenReturn(targetShip);

		subject.precheckTractorBeam(1L);

		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(shipRepository, Mockito.never()).save(targetShip);
		assertEquals(ShipTaskType.NONE, ship.getTaskType());
	}

	@Test
	public void shouldReduceWarpSpeedWithTractorBeam() {
		Ship ship = createShipWithTask(ShipTaskType.TRACTOR_BEAM);
		ship.setTaskValue("2");
		ship.setTravelSpeed(8);

		Ship targetShip = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		targetShip.setTaskType(ShipTaskType.AUTOREFUEL);
		targetShip.setTravelSpeed(5);
		Mockito.when(shipRepository.getOne(2L)).thenReturn(targetShip);

		subject.precheckTractorBeam(1L);

		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(shipRepository).save(targetShip);
		assertEquals(ShipTaskType.TRACTOR_BEAM, ship.getTaskType());
		assertEquals(7, ship.getTravelSpeed());
		assertEquals(0, targetShip.getTravelSpeed());
		assertEquals(ShipTaskType.NONE, targetShip.getTaskType());
	}

	@Test
	public void shouldNotReduceWarpSpeedWithTractorBeam() {
		Ship ship = createShipWithTask(ShipTaskType.TRACTOR_BEAM);
		ship.setTaskValue("2");
		ship.setTravelSpeed(6);

		Ship targetShip = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		targetShip.setTaskType(ShipTaskType.AUTOREFUEL);
		targetShip.setTravelSpeed(5);
		Mockito.when(shipRepository.getOne(2L)).thenReturn(targetShip);

		subject.precheckTractorBeam(1L);

		Mockito.verify(shipRepository, Mockito.never()).save(ship);
		Mockito.verify(shipRepository).save(targetShip);
		assertEquals(ShipTaskType.TRACTOR_BEAM, ship.getTaskType());
		assertEquals(6, ship.getTravelSpeed());
		assertEquals(0, targetShip.getTravelSpeed());
		assertEquals(ShipTaskType.NONE, targetShip.getTaskType());
	}

	@Test
	public void shouldCheckMineFieldsEnabled() {
		Mockito.when(gameRepository.mineFieldsEnabled(1L)).thenReturn(false);

		subject.processCreateMineField(1L);

		Mockito.verify(shipRepository, Mockito.never()).getShipsWithTask(Mockito.anyLong(), Mockito.any());
	}

	@Test
	public void shouldCheckForPlanetWithCreateMineField() {
		Ship ship = createCreateMineFieldShip(2);
		ship.setPlanet(new Planet());

		subject.processCreateMineField(1L);

		assertEquals(5, ship.getProjectiles());
		assertEquals(ShipTaskType.NONE, ship.getTaskType());
		assertNull(ship.getTaskValue());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_not_created, ImageConstants.news_mine_field, ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());
		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(mineFieldRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldCheckForProjectilesPresentWithCreateMineField() {
		Ship ship = createCreateMineFieldShip(2);
		ship.setProjectiles(0);

		subject.processCreateMineField(1L);

		assertEquals(0, ship.getProjectiles());
		assertEquals(ShipTaskType.NONE, ship.getTaskType());
		assertNull(ship.getTaskValue());
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(mineFieldRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldCreateMineField() {
		Ship ship = createCreateMineFieldShip(2);
		ship.setProjectiles(5);
		ship.setX(50);
		ship.setY(100);

		subject.processCreateMineField(1L);

		assertEquals(3, ship.getProjectiles());
		assertEquals(ShipTaskType.NONE, ship.getTaskType());
		assertNull(ship.getTaskValue());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_created, ImageConstants.news_mine_field, ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), 2);
		Mockito.verify(shipRepository).save(ship);

		ArgumentCaptor<MineField> mineFieldArg = ArgumentCaptor.forClass(MineField.class);
		Mockito.verify(mineFieldRepository).save(mineFieldArg.capture());

		MineField mineField = mineFieldArg.getValue();
		assertEquals(ship.getX(), mineField.getX());
		assertEquals(ship.getY(), mineField.getY());
		assertEquals(ship.getPlayer().getGame(), mineField.getGame());
		assertEquals(2, mineField.getMines());
		assertEquals(ship.getProjectileWeaponTemplate().getDamageIndex(), mineField.getLevel());
	}

	@Test
	public void shouldCheckPlanetPresentWithAutoRefuel() {
		createAutoRefuelShip();

		subject.processAutorefuel(1L);

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(planetRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldCheckShipNotFullAndPlanetHasFuelWithAutoRefuel() {
		Ship ship = createAutoRefuelShip();
		Planet planet = new Planet();
		ship.setPlanet(planet);
		ship.setFuel(ship.getShipTemplate().getFuelCapacity());

		subject.processAutorefuel(1L);

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(planetRepository, Mockito.never()).save(Mockito.any());

		ship.setFuel(0);
		subject.processAutorefuel(1L);

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(planetRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldCheckBunkerWithAutoReful() {
		Ship ship = createAutoRefuelShip();
		Planet planet = new Planet();
		planet.setFuel(100);
		OrbitalSystem bunker = new OrbitalSystem();
		bunker.setType(OrbitalSystemType.BUNKER);
		planet.setOrbitalSystems(Lists.newArrayList(bunker));
		ship.setPlanet(planet);

		subject.processAutorefuel(1L);

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(planetRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldRefuelWithBunker() {
		Ship ship = createAutoRefuelShip();
		Planet planet = new Planet();
		planet.setFuel(120);
		OrbitalSystem bunker = new OrbitalSystem();
		bunker.setType(OrbitalSystemType.BUNKER);
		planet.setOrbitalSystems(Lists.newArrayList(bunker));
		ship.setPlanet(planet);

		subject.processAutorefuel(1L);

		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(planetRepository).save(planet);
		assertEquals(20, ship.getFuel());
		assertEquals(100, planet.getFuel());
	}

	@Test
	public void shouldRefuelWithoutBunker() {
		Ship ship = createAutoRefuelShip();
		Planet planet = new Planet();
		planet.setFuel(60);
		ship.setPlanet(planet);

		subject.processAutorefuel(1L);

		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(planetRepository).save(planet);
		assertEquals(50, ship.getFuel());
		assertEquals(10, planet.getFuel());
	}

	@Test
	public void shouldNotRecycleBecauseNoPlanet() {
		createRecycleShip();

		subject.processShipRecycle(1L);
		assertNoShipRecycle();
	}

	@Test
	public void shouldNotRecycleBecauseNotOwnPlanet() {
		Ship ship = createRecycleShip();
		ship.setPlanet(new Planet());

		subject.processShipRecycle(1L);
		assertNoShipRecycle();
	}

	@Test
	public void shouldNotRecycleBecauseNorStarbaseNeitherRecyclingFacility() {
		Ship ship = createRecycleShip();
		Planet planet = new Planet();
		planet.setPlayer(ship.getPlayer());
		ship.setPlanet(planet);

		subject.processShipRecycle(1L);
		assertNoShipRecycle();
	}

	@Test
	public void shouldRecycleBecauseStarbasePresent() {
		Ship ship = createRecycleShip();
		Planet planet = new Planet();
		planet.setPlayer(ship.getPlayer());
		planet.setStarbase(new Starbase());
		ship.setPlanet(planet);

		subject.processShipRecycle(1L);
		assertShipRecycle(ship);
	}

	@Test
	public void shouldRecycleBecauseRecycleFacilityPresent() {
		Ship ship = createRecycleShip();
		Planet planet = new Planet();
		planet.setPlayer(ship.getPlayer());
		OrbitalSystem recycleFacility = new OrbitalSystem(planet);
		recycleFacility.setType(OrbitalSystemType.RECYCLING_FACILITY);
		planet.setOrbitalSystems(Lists.newArrayList(recycleFacility));
		ship.setPlanet(planet);

		subject.processShipRecycle(1L);
		assertShipRecycle(ship);
	}

	private void assertNoShipRecycle() {
		Mockito.verify(shipService, Mockito.never()).transportWithoutPermissionCheck(Mockito.any(), Mockito.any(), Mockito.any());
		Mockito.verify(shipService, Mockito.never()).deleteShip(Mockito.any(), Mockito.any(), Mockito.anyBoolean());
		Mockito.verify(planetRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}

	private void assertShipRecycle(Ship ship) {
		Mockito.verify(shipService).transportWithoutPermissionCheck(new ShipTransportRequest(), ship, ship.getPlanet());
		Mockito.verify(shipService).deleteShip(ship, null, false);
		Mockito.verify(planetRepository).save(ship.getPlanet());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_recycled, ship.createFullImagePath(), ship.getPlanet().getId(),
				NewsEntryClickTargetType.planet, ship.getName(), 85, 850, 8500);
	}

	@Test
	public void shouldCheckTaskTypeWithAutoDestruction() {
		Ship ship = createAutoDestructionShip();

		Mockito.when(shipRepository.getShipIdsInRadius(1L, 0, 0, 83)).thenReturn(Lists.newArrayList(2L));
		Ship otherShip = ShipTestFactory.createShip(100, 100, 1, 1, 100, 2L);
		otherShip.setTaskType(ShipTaskType.AUTO_DESTRUCTION);
		Mockito.when(shipRepository.getOne(2L)).thenReturn(otherShip);

		subject.processAutoDestruction(1L);

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_auto_destructed, ship.createFullImagePath(), null, null,
				ship.getName());

		assertEquals(0, otherShip.getDamage());
		Mockito.verify(newsService, Mockito.never()).add(otherShip.getPlayer(), NewsEntryConstants.news_entry_ship_damaged_by_auto_destruction,
				otherShip.createFullImagePath(), otherShip.getId(), NewsEntryClickTargetType.ship, ship.getName(), 15);
		Mockito.verify(newsService, Mockito.never()).add(otherShip.getPlayer(), NewsEntryConstants.news_entry_ship_destroyed_by_auto_destruction,
				otherShip.createFullImagePath(), null, null, ship.getName());
		Mockito.verify(shipRepository, Mockito.never()).save(otherShip);
		Mockito.verify(shipService, Mockito.never()).deleteShip(Mockito.eq(otherShip), Mockito.any());

		Mockito.verify(shipService).deleteShip(ship, null, false);
	}

	@Test
	public void shouldCheckDamagedShipsWithAutoDestruction() {
		Ship ship = createAutoDestructionShip();

		Mockito.when(shipRepository.getShipIdsInRadius(1L, 0, 0, 83)).thenReturn(Lists.newArrayList(2L));
		Ship otherShip = ShipTestFactory.createShip(100, 100, 1, 1, 100, 2L);
		Mockito.when(shipRepository.getOne(2L)).thenReturn(otherShip);

		subject.processAutoDestruction(1L);

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_auto_destructed, ship.createFullImagePath(), null, null,
				ship.getName());

		assertEquals(26, otherShip.getDamage());
		Mockito.verify(newsService).add(otherShip.getPlayer(), NewsEntryConstants.news_entry_ship_damaged_by_auto_destruction, otherShip.createFullImagePath(),
				otherShip.getId(), NewsEntryClickTargetType.ship, otherShip.getName(), 26);
		Mockito.verify(shipRepository).save(otherShip);
		Mockito.verify(shipService, Mockito.never()).deleteShip(Mockito.eq(otherShip), Mockito.any());

		Mockito.verify(shipService).deleteShip(ship, null, false);
	}

	@Test
	public void shouldCheckDestroyedShipsWithAutoDestruction() {
		Ship ship = createAutoDestructionShip();

		Mockito.when(shipRepository.getShipIdsInRadius(1L, 0, 0, 83)).thenReturn(Lists.newArrayList(2L));
		Ship otherShip = ShipTestFactory.createShip(100, 100, 1, 1, 100, 2L);
		otherShip.setDamage(90);
		Mockito.when(shipRepository.getOne(2L)).thenReturn(otherShip);

		subject.processAutoDestruction(1L);

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_auto_destructed, ship.createFullImagePath(), null, null,
				ship.getName());

		assertEquals(100, otherShip.getDamage());
		Mockito.verify(newsService).add(otherShip.getPlayer(), NewsEntryConstants.news_entry_ship_destroyed_by_auto_destruction,
				otherShip.createFullImagePath(), null, null, otherShip.getName());
		Mockito.verify(shipRepository, Mockito.never()).save(otherShip);
		Mockito.verify(shipService).deleteShip(otherShip, ship.getPlayer());

		Mockito.verify(shipService).deleteShip(ship, null, false);
	}
}