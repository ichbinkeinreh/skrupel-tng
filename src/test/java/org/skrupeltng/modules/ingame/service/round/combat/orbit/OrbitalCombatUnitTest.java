package org.skrupeltng.modules.ingame.service.round.combat.orbit;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.testcontainers.shaded.com.google.common.collect.Lists;

public class OrbitalCombatUnitTest {

	@Spy
	@InjectMocks
	private final OrbitalCombat orbitalCombat = new OrbitalCombat();

	@Mock
	private ShipRepository shipRepository;

	@Mock
	private ShipService shipService;

	@Mock
	private NewsService newsService;

	@Mock
	private StatsUpdater statsUpdater;

	@Before
	public void setup() {
		MasterDataService.RANDOM.setSeed(1L);

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldAddMutliplePlanetsToMap() {
		List<Ship> ships = new ArrayList<>();

		Ship ship = ShipTestFactory.createShip(0, 0, 0, 0, 0, 1);
		ship.setPlanet(new Planet(1L));
		ships.add(ship);

		ship = ShipTestFactory.createShip(0, 0, 0, 0, 0, 2);
		ship.setPlanet(new Planet(2L));
		ships.add(ship);

		ship = ShipTestFactory.createShip(0, 0, 0, 0, 0, 2);
		ship.setPlanet(new Planet(2L));
		ships.add(ship);

		Map<Long, Integer> result = orbitalCombat.getTotalPlanetaryDefenses(ships);
		assertEquals(2, result.size());
		assertEquals(100, result.get(1L).intValue());
		assertEquals(100, result.get(2L).intValue());
	}

	@Test
	public void shouldCheckStructureScannerPresence() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.setPlanet(new Planet(3L));

		orbitalCombat.checkStructurScanner(ship);

		Mockito.verify(shipService, Mockito.never()).checkStructurScanner(Mockito.any(), Mockito.any());

		Planet planet = new Planet();
		planet.setPlayer(new Player(3L));
		ship.setPlanet(planet);

		OrbitalSystem orbitalSystem = new OrbitalSystem();
		orbitalSystem.setType(OrbitalSystemType.HOBAN_BUD);
		planet.setOrbitalSystems(Lists.newArrayList(orbitalSystem));

		orbitalCombat.checkStructurScanner(ship);

		Mockito.verify(shipService).checkStructurScanner(planet.getPlayer(), ship);
	}

	@Test
	public void shouldDestroyShip() {
		Ship ship = ShipTestFactory.createShip(100, 10, 1, 1, 1, 1L);
		Planet planet = new Planet(1L);
		ship.setPlanet(planet);
		planet.setColonists(100000);
		planet.setMoney(1000);
		planet.setSupplies(100);
		planet.setPlanetaryDefense(20);
		int totalPlanetaryDefense = 120;

		int damageToPlanetResult = orbitalCombat.processCombat(ship, totalPlanetaryDefense);

		assertEquals(35, damageToPlanetResult);
		assertEquals(0, ship.getShield());
		assertEquals(100, ship.getDamage());
	}

	@Test
	public void shouldNotDestroyShip() {
		Ship ship = ShipTestFactory.createShip(100, 1000, 10, 10, 100, 1L);
		Planet planet = new Planet(1L);
		ship.setPlanet(planet);
		planet.setColonists(100000);
		planet.setMoney(1000);
		planet.setSupplies(100);
		planet.setPlanetaryDefense(20);
		int totalPlanetaryDefense = 120;

		int damageToPlanetResult = orbitalCombat.processCombat(ship, totalPlanetaryDefense);

		assertEquals(127, damageToPlanetResult);
		assertEquals(68, ship.getShield());
		assertEquals(0, ship.getDamage());
	}

	@Test
	public void shouldCheckBattleStationType() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		Planet planet = new Planet(1L);
		ship.setPlanet(planet);
		Starbase starbase = new Starbase();
		starbase.setType(StarbaseType.BATTLE_STATION);
		planet.setStarbase(starbase);
		int totalPlanetaryDefense = 100;

		int damageToPlanetResult = orbitalCombat.processCombat(ship, totalPlanetaryDefense);

		assertEquals(101, damageToPlanetResult);
		assertEquals(4, ship.getShield());
		assertEquals(0, ship.getDamage());
	}

	@Test
	public void shouldUpdatePlanetaryDefenses() {
		long planetId = 1L;
		Map<Long, Integer> totalPlanetaryDefenses = new HashMap<>();
		totalPlanetaryDefenses.put(1L, 100);
		int damageToPlanet = 80;

		orbitalCombat.handlePlanetDamage(planetId, totalPlanetaryDefenses, damageToPlanet);
		assertEquals(20, totalPlanetaryDefenses.get(1L).intValue());

		orbitalCombat.handlePlanetDamage(planetId, totalPlanetaryDefenses, damageToPlanet);
		assertEquals(0, totalPlanetaryDefenses.get(1L).intValue());
	}

	@Test
	public void shouldHandleNotDestroyedShip() {
		Set<Ship> destroyedShips = new HashSet<>();
		Ship ship = ShipTestFactory.createShip(10, 10, 1, 1, 1, 1l);
		Planet planet = new Planet(1L);
		planet.setName("planetname");
		Player player = new Player(2L);
		planet.setPlayer(player);
		ship.setPlanet(planet);

		orbitalCombat.handleShipDamage(destroyedShips, ship);

		assertEquals(0, destroyedShips.size());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_entered_enemy_orbit, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), "planetname");
		Mockito.verify(newsService).add(player, NewsEntryConstants.news_entry_enemy_ship_entered_orbit, planet.createFullImagePath(), planet.getId(),
				NewsEntryClickTargetType.planet, ship.getName(), "planetname");
	}

	@Test
	public void shouldHandleDestroyedShip() {
		Set<Ship> destroyedShips = new HashSet<>();
		Ship ship = ShipTestFactory.createShip(10, 10, 1, 1, 1, 1l);
		ship.setDamage(100);
		Planet planet = new Planet(1L);
		planet.setName("planetname");
		Player player = new Player(2L);
		planet.setPlayer(player);
		ship.setPlanet(planet);

		orbitalCombat.handleShipDamage(destroyedShips, ship);

		assertEquals(1, destroyedShips.size());
		assertEquals(ship, destroyedShips.iterator().next());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_destroyed_ship_by_planet, ship.createFullImagePath(), null, null,
				ship.getName(), "planetname");
		Mockito.verify(newsService).add(player, NewsEntryConstants.news_entry_planet_defended_enemy_ship, planet.createFullImagePath(), planet.getId(),
				NewsEntryClickTargetType.planet, "planetname", ship.getName());
	}
}