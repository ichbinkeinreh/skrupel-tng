package org.skrupeltng.modules.ingame.service.round;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpecies;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.testcontainers.shaded.com.google.common.collect.Lists;

public class MeteorRoundCalculatorUnitTest {

	@Spy
	@InjectMocks
	private final MeteorRoundCalculator subject = new MeteorRoundCalculator();

	@Mock
	private PlanetRepository planetRepository;

	@Mock
	private PlayerRepository playerRepository;

	@Mock
	private NewsService newsService;

	@Mock
	private StatsUpdater statsUpdater;

	@Before
	public void setup() {
		MasterDataService.RANDOM.setSeed(1L);

		MockitoAnnotations.initMocks(this);

		Mockito.when(planetRepository.save(Mockito.any())).thenAnswer(new Answer<Planet>() {
			@Override
			public Planet answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgumentAt(0, Planet.class);
			}
		});
	}

	@Test
	public void shouldNotCreateBigMeteor() {
		subject.processBigMeteors(1L, 44);

		Mockito.verify(planetRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}

	@Test
	public void shouldCreateBigMeteorOnUninhabitedPlanet() {
		Planet planet = new Planet(1L);
		planet.setName("planetname1");
		planet.setX(100);
		planet.setY(200);
		planet.setUntappedMineral1(100);
		planet.setUntappedMineral2(200);
		planet.setUntappedMineral3(300);
		planet.setUntappedFuel(400);
		planet.setNativeSpecies(new NativeSpecies());
		planet.setNativeSpeciesCount(10000);

		Mockito.when(planetRepository.getOne(0L)).thenReturn(planet);

		List<Player> players = Lists.newArrayList(new Player(1L), new Player(2L));
		Mockito.when(playerRepository.findByGameId(1L)).thenReturn(players);

		subject.processBigMeteors(1L, 0);

		Mockito.verify(planetRepository).save(planet);

		for (Player player : players) {
			Mockito.verify(newsService).add(player, NewsEntryConstants.news_entry_big_meteor_hit_planet, "/images/news/meteor_gross.jpg", null, null,
					planet.getName(), planet.getX(), planet.getY(), 7753, 77, 48, 541);
		}

		assertEquals(null, planet.getNativeSpecies());
		assertEquals(0, planet.getNativeSpeciesCount());
		assertEquals(8153, planet.getUntappedFuel());
		assertEquals(177, planet.getUntappedMineral1());
		assertEquals(248, planet.getUntappedMineral2());
		assertEquals(841, planet.getUntappedMineral3());
	}

	@Test
	public void shouldCreateBigMeteorOnInhabitedPlanet() {
		Planet planet = new Planet(1L);
		planet.setName("planetname1");
		planet.setX(100);
		planet.setY(200);
		planet.setUntappedMineral1(100);
		planet.setUntappedMineral2(200);
		planet.setUntappedMineral3(300);
		planet.setUntappedFuel(400);
		planet.setNativeSpecies(new NativeSpecies());
		planet.setNativeSpeciesCount(10000);

		Player owner = new Player(1L);
		planet.setPlayer(owner);

		planet.setColonists(10000);
		planet.setMoney(100000);
		planet.setSupplies(1000);
		planet.setMines(10);
		planet.setFactories(20);
		planet.setPlanetaryDefense(30);

		Mockito.when(planetRepository.getOne(0L)).thenReturn(planet);

		Player player2 = new Player(2L);
		List<Player> players = Lists.newArrayList(owner, player2);
		Mockito.when(playerRepository.findByGameId(1L)).thenReturn(players);

		subject.processBigMeteors(1L, 0);

		Mockito.verify(planetRepository).save(planet);

		Mockito.verify(newsService).add(player2, NewsEntryConstants.news_entry_big_meteor_hit_planet, "/images/news/meteor_gross.jpg", null, null,
				planet.getName(), planet.getX(), planet.getY(), 7753, 77, 48, 541);

		Mockito.verify(newsService).add(owner, NewsEntryConstants.news_entry_big_meteor_hit_colony, "/images/news/meteor_gross.jpg", null, null,
				planet.getName(), planet.getX(), planet.getY(), 7753, 77, 48, 541);

		assertEquals(null, planet.getPlayer());
		assertEquals(0, planet.getColonists());
		assertEquals(0, planet.getMines());
		assertEquals(0, planet.getFactories());
		assertEquals(0, planet.getPlanetaryDefense());

		assertEquals(null, planet.getNativeSpecies());
		assertEquals(0, planet.getNativeSpeciesCount());
		assertEquals(8153, planet.getUntappedFuel());
		assertEquals(177, planet.getUntappedMineral1());
		assertEquals(248, planet.getUntappedMineral2());
		assertEquals(841, planet.getUntappedMineral3());
	}

	@Test
	public void shouldCreateNoSmallMeteors() {
		subject.processSmallMeteors(1L, 0);

		Mockito.verify(planetRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}

	@Test
	public void shouldCreateOneSmallMeteorsOnInhabitetPlanet() {
		Planet planet = new Planet(1L);
		planet.setName("planet1");
		planet.setMineral1(10);
		planet.setMineral2(20);
		planet.setMineral3(30);
		planet.setFuel(40);

		Player player = new Player(1L);
		planet.setPlayer(player);

		Mockito.when(planetRepository.getOne(0L)).thenReturn(planet);

		subject.processSmallMeteors(1L, 1);

		Mockito.verify(planetRepository).save(planet);
		Mockito.verify(newsService).add(player, NewsEntryConstants.news_entry_small_meteor_hit_colony, "/images/news/meteor_klein.jpg", planet.getId(),
				NewsEntryClickTargetType.planet, planet.getName(), 0, 28, 27, 64);

		assertEquals(player, planet.getPlayer());
		assertEquals(40, planet.getFuel());
		assertEquals(38, planet.getMineral1());
		assertEquals(47, planet.getMineral2());
		assertEquals(94, planet.getMineral3());
	}

	@Test
	public void shouldCreateOneSmallMeteorsOnUninhabitetPlanet() {
		Planet planet = new Planet(1L);
		planet.setName("planet1");
		planet.setMineral1(10);
		planet.setMineral2(20);
		planet.setMineral3(30);
		planet.setFuel(40);

		Mockito.when(planetRepository.getOne(0L)).thenReturn(planet);

		subject.processSmallMeteors(1L, 1);

		Mockito.verify(planetRepository).save(planet);
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());

		assertEquals(null, planet.getPlayer());
		assertEquals(40, planet.getFuel());
		assertEquals(38, planet.getMineral1());
		assertEquals(47, planet.getMineral2());
		assertEquals(94, planet.getMineral3());
	}
}