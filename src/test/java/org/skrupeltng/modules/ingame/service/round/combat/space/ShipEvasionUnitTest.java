package org.skrupeltng.modules.ingame.service.round.combat.space;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.masterdata.service.MasterDataService;

public class ShipEvasionUnitTest {

	@Spy
	@InjectMocks
	private final ShipEvasion shipEvasion = new ShipEvasion();

	@Mock
	private NewsService newsService;

	@Mock
	private PlanetRepository planetRepository;

	@Mock
	private ConfigProperties configProperties;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		MasterDataService.RANDOM.setSeed(1L);

		Mockito.when(planetRepository.findByGameIdAndXAndY(Mockito.anyLong(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(Optional.empty());

		Mockito.when(configProperties.getEvadeDistance()).thenReturn(20);
	}

	@Test
	public void shouldAddNewsEntry() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 2, 1L);
		ship.setX(50);
		ship.setY(50);

		Ship enemyShip = ShipTestFactory.createShip(100, 100, 1, 1, 2, 2L);

		shipEvasion.evade(ship, enemyShip, true);

		Mockito.verify(planetRepository).findByGameIdAndXAndY(1L, 63, 65);
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_evaded_successfull, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), enemyShip.getName(), "A1");
	}

	@Test
	public void shouldNotAddNewsEntry() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 2, 1L);
		ship.setX(50);
		ship.setY(50);

		Ship enemyShip = ShipTestFactory.createShip(100, 100, 1, 1, 2, 2L);

		shipEvasion.evade(ship, enemyShip, false);

		Mockito.verify(planetRepository).findByGameIdAndXAndY(1L, 63, 65);
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}
}