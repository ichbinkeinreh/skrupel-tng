package org.skrupeltng.modules.ingame.service.round;

import static org.junit.Assert.assertEquals;

import java.util.Collections;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.dashboard.service.AchievementService;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineField;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineFieldRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.masterdata.service.MasterDataService;

import com.google.common.collect.Lists;

public class MineFieldRoundCalculatorUnitTest {

	@Spy
	@InjectMocks
	private final MineFieldRoundCalculator subject = new MineFieldRoundCalculator();

	@Mock
	private MineFieldRepository mineFieldRepository;

	@Mock
	private ShipRepository shipRepository;

	@Mock
	private ShipService shipService;

	@Mock
	private NewsService newsService;

	@Mock
	private PlayerRelationRepository playerRelationRepository;

	@Mock
	private GameRepository gameRepository;

	@Mock
	private StatsUpdater statsUpdater;

	@Mock
	private AchievementService achievementService;

	@Before
	public void setup() {
		MasterDataService.RANDOM.setSeed(1L);

		MockitoAnnotations.initMocks(this);

		Mockito.when(gameRepository.mineFieldsEnabled(Mockito.anyLong())).thenReturn(true);
	}

	private Ship createMineFieldClearingShip(int hangar) {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.getShipTemplate().setHangarCapacity(hangar);
		ship.setTaskType(ShipTaskType.CLEAR_MINE_FIELD);
		Mockito.when(shipRepository.getShipsWithTask(Mockito.anyLong(), Mockito.eq(ShipTaskType.CLEAR_MINE_FIELD))).thenReturn(Lists.newArrayList(ship));
		return ship;
	}

	@Test
	public void shouldCheckMineFieldsEnabledBeforeDeplete() {
		MineField mineField = new MineField();
		Mockito.when(mineFieldRepository.findByGameId(1L)).thenReturn(Lists.newArrayList(mineField));

		Mockito.when(gameRepository.mineFieldsEnabled(Mockito.anyLong())).thenReturn(false);

		subject.processMineFieldDepletion(1L);

		Mockito.verify(subject, Mockito.never()).depleteMineField(Mockito.any());
	}

	@Test
	public void shouldDeplete() {
		MineField mineField = new MineField();
		Mockito.when(mineFieldRepository.findByGameId(1L)).thenReturn(Lists.newArrayList(mineField));

		subject.processMineFieldDepletion(1L);
		Mockito.verify(subject).depleteMineField(mineField);
	}

	@Test
	public void shouldReduceMines() {
		MineField mineField = new MineField();
		mineField.setMines(3);

		subject.depleteMineField(mineField);

		assertEquals(2, mineField.getMines());
		Mockito.verify(mineFieldRepository).save(mineField);
		Mockito.verify(mineFieldRepository, Mockito.never()).delete(Mockito.any());
	}

	@Test
	public void shouldDeleteMineField() {
		MineField mineField = new MineField();
		mineField.setMines(1);

		subject.depleteMineField(mineField);

		assertEquals(0, mineField.getMines());
		Mockito.verify(mineFieldRepository).delete(mineField);
		Mockito.verify(mineFieldRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldDamageShipAndDeleteMineField() {
		MineField mineField = new MineField();
		mineField.setMines(2);
		mineField.setLevel(2);

		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);

		subject.damageShipByMineField(ship, mineField, 2);

		assertEquals(83, ship.getDamage());
		assertEquals(80, ship.getCrew());
		assertEquals(0, mineField.getMines());

		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(shipService, Mockito.never()).deleteShip(Mockito.any(), Mockito.any());

		Mockito.verify(mineFieldRepository).delete(mineField);
		Mockito.verify(mineFieldRepository, Mockito.never()).save(Mockito.any());

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_damaged_ship, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), "A1", 2, 83, 20);
	}

	@Test
	public void shouldDestroyShipAndReduceMines() {
		MineField mineField = new MineField();
		mineField.setMines(40);
		mineField.setLevel(10);
		mineField.setPlayer(new Player());

		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);

		subject.damageShipByMineField(ship, mineField, 20);

		assertEquals(100, ship.getDamage());
		assertEquals(20, mineField.getMines());

		Mockito.verify(shipService).deleteShip(ship, mineField.getPlayer());
		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());

		Mockito.verify(mineFieldRepository).save(mineField);
		Mockito.verify(mineFieldRepository, Mockito.never()).delete(Mockito.any());

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_destroyed_ship, ship.createFullImagePath(), null, null,
				ship.getName(), "A1");
	}

	@Test
	public void shouldCheckOwnMineField() {
		MineField mineField = new MineField();
		mineField.setLevel(1);
		mineField.setMines(30);
		Player player = new Player(1L);
		mineField.setPlayer(player);
		Mockito.when(mineFieldRepository.getOne(1L)).thenReturn(mineField);

		Pair<MineField, Integer> result = subject.getMostDenseMineField(1L, Lists.newArrayList(1L));

		assertEquals(null, result.getLeft());
		assertEquals(0, result.getRight().intValue());
	}

	@Test
	public void shouldCheckPlayerRelation() {
		MineField mineField = new MineField();
		mineField.setLevel(1);
		mineField.setMines(30);
		Player player = new Player(2L);
		mineField.setPlayer(player);
		Mockito.when(mineFieldRepository.getOne(1L)).thenReturn(mineField);

		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.ALLIANCE);
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Lists.newArrayList(relation));

		Pair<MineField, Integer> result = subject.getMostDenseMineField(1L, Lists.newArrayList(1L));

		assertEquals(null, result.getLeft());
		assertEquals(0, result.getRight().intValue());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldCheckIfMineFieldsEnabledBeforeHits() {
		Mockito.when(gameRepository.mineFieldsEnabled(Mockito.anyLong())).thenReturn(false);

		subject.processMineFieldHits(1L);

		Mockito.verify(subject, Mockito.never()).getMostDenseMineField(Mockito.anyLong(), Mockito.anyList());
		Mockito.verify(subject, Mockito.never()).damageShipByMineField(Mockito.any(), Mockito.any(), Mockito.anyInt());
	}

	@Test
	public void shouldCheckForMinesResult() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setX(50);
		ship.setY(100);
		Mockito.when(shipRepository.getOne(1L)).thenReturn(ship);

		Mockito.when(shipRepository.findShipsInFreeSpace(1L)).thenReturn(Lists.newArrayList(1L));

		Mockito.when(mineFieldRepository.getMineFieldIdsInRadius(1L, 50, 100, 85)).thenReturn(Lists.newArrayList(1L));
		MineField mineField = new MineField();
		mineField.setLevel(1);
		mineField.setMines(1);
		Player player = new Player(2L);
		mineField.setPlayer(player);
		Mockito.when(mineFieldRepository.getOne(1L)).thenReturn(mineField);

		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.NON_AGGRESSION_TREATY);
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Lists.newArrayList(relation));

		subject.processMineFieldHits(1L);

		Mockito.verify(subject, Mockito.never()).damageShipByMineField(Mockito.any(), Mockito.any(), Mockito.anyInt());
	}

	@Test
	public void shouldCheckForSingleMine() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setX(50);
		ship.setY(100);
		Mockito.when(shipRepository.getOne(1L)).thenReturn(ship);

		Mockito.when(shipRepository.findShipsInFreeSpace(1L)).thenReturn(Lists.newArrayList(1L));

		Mockito.when(mineFieldRepository.getMineFieldIdsInRadius(1L, 50, 100, 85)).thenReturn(Lists.newArrayList(1L));
		MineField mineField = new MineField();
		mineField.setLevel(1);
		mineField.setMines(1);
		Player player = new Player(2L);
		mineField.setPlayer(player);
		Mockito.when(mineFieldRepository.getOne(1L)).thenReturn(mineField);

		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());

		subject.processMineFieldHits(1L);

		Mockito.verify(subject).damageShipByMineField(ship, mineField, 1);
	}

	@Test
	public void shouldDamageEnemyShip() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setX(50);
		ship.setY(100);
		Mockito.when(shipRepository.getOne(1L)).thenReturn(ship);

		Mockito.when(shipRepository.findShipsInFreeSpace(1L)).thenReturn(Lists.newArrayList(1L));

		Mockito.when(mineFieldRepository.getMineFieldIdsInRadius(1L, 50, 100, 85)).thenReturn(Lists.newArrayList(1L));
		MineField mineField = new MineField();
		mineField.setLevel(1);
		mineField.setMines(30);
		Player player = new Player(2L);
		mineField.setPlayer(player);
		Mockito.when(mineFieldRepository.getOne(1L)).thenReturn(mineField);

		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.TRADE_AGREEMENT);
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Lists.newArrayList(relation));

		subject.processMineFieldHits(1L);

		Mockito.verify(subject).damageShipByMineField(ship, mineField, 6);
	}

	@Test
	public void shouldFullyClearMineField() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		MineField mineField = new MineField();
		mineField.setPlayer(new Player(2L));
		mineField.setMines(2);

		subject.clearMineField(ship, mineField, 2);

		Mockito.verify(mineFieldRepository).delete(mineField);
		Mockito.verify(mineFieldRepository, Mockito.never()).save(Mockito.any());

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_cleared_fully, ImageConstants.news_mine_field, ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());
		Mockito.verify(newsService).add(mineField.getPlayer(), NewsEntryConstants.news_entry_mine_field_cleared_by_enemy, ImageConstants.news_mine_field, null,
				null);
	}

	@Test
	public void shouldPartiallyClearMineField() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		MineField mineField = new MineField();
		mineField.setMines(10);

		subject.clearMineField(ship, mineField, 2);

		Mockito.verify(mineFieldRepository, Mockito.never()).delete(Mockito.any());
		Mockito.verify(mineFieldRepository).save(mineField);
		assertEquals(8, mineField.getMines());

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_cleared, ImageConstants.news_mine_field, ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), 2);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldCheckIfMineFieldsEnabledWithMineFieldClearing() {
		createMineFieldClearingShip(2);

		Mockito.when(gameRepository.mineFieldsEnabled(Mockito.anyLong())).thenReturn(false);

		subject.processClearMineField(1L);

		Mockito.verify(shipRepository, Mockito.never()).getShipsWithTask(Mockito.anyLong(), Mockito.any());
		Mockito.verify(subject, Mockito.never()).getMostDenseMineField(Mockito.anyLong(), Mockito.anyList());
		Mockito.verify(subject, Mockito.never()).clearMineField(Mockito.any(), Mockito.any(), Mockito.anyInt());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldCheckPlanetPresentWithMineFieldClearing() {
		Ship ship = createMineFieldClearingShip(2);
		ship.setPlanet(new Planet(1L));

		subject.processClearMineField(1L);

		Mockito.verify(subject, Mockito.never()).clearMineField(Mockito.any(), Mockito.any(), Mockito.anyInt());
		Mockito.verify(subject, Mockito.never()).getMostDenseMineField(Mockito.anyLong(), Mockito.anyList());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_not_cleared, ImageConstants.news_mine_field, ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldCheckHangarCapacityPresent() {
		createMineFieldClearingShip(0);

		subject.processClearMineField(1L);

		Mockito.verify(subject, Mockito.never()).getMostDenseMineField(Mockito.anyLong(), Mockito.anyList());
		Mockito.verify(subject, Mockito.never()).clearMineField(Mockito.any(), Mockito.any(), Mockito.anyInt());
	}

	@Test
	public void shouldSetClearedMinesToOne() {
		Ship ship = createMineFieldClearingShip(1);

		MineField mineField = new MineField();
		mineField.setMines(10);
		mineField.setPlayer(new Player(2L));
		Mockito.when(mineFieldRepository.getOne(2L)).thenReturn(mineField);

		Mockito.when(mineFieldRepository.getMineFieldIdsInRadius(1L, 0, 0, 100)).thenReturn(Lists.newArrayList(2L));
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());

		subject.processClearMineField(1L);

		Mockito.verify(subject).getMostDenseMineField(1L, Lists.newArrayList(2L));
		Mockito.verify(subject).clearMineField(ship, mineField, 1);
	}

	@Test
	public void shouldClearMinesDependingOnHangarCapacity() {
		Ship ship = createMineFieldClearingShip(10);

		MineField mineField = new MineField();
		mineField.setMines(10);
		mineField.setPlayer(new Player(2L));
		Mockito.when(mineFieldRepository.getOne(2L)).thenReturn(mineField);

		Mockito.when(mineFieldRepository.getMineFieldIdsInRadius(1L, 0, 0, 100)).thenReturn(Lists.newArrayList(2L));
		Mockito.when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());

		subject.processClearMineField(1L);

		Mockito.verify(subject).getMostDenseMineField(1L, Lists.newArrayList(2L));
		Mockito.verify(subject).clearMineField(ship, mineField, 9);
	}
}