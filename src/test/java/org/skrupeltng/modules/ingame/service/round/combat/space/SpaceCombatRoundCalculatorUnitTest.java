package org.skrupeltng.modules.ingame.service.round.combat.space;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTactics;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.testcontainers.shaded.com.google.common.collect.Lists;
import org.testcontainers.shaded.com.google.common.collect.Sets;

@SuppressWarnings("unchecked")
public class SpaceCombatRoundCalculatorUnitTest {

	@Spy
	@InjectMocks
	private final SpaceCombatRoundCalculator subject = new SpaceCombatRoundCalculator();

	@Mock
	private ShipRepository shipRepository;

	@Mock
	private ShipService shipService;

	@Mock
	private SpaceCombat spaceCombat;

	@Mock
	private PoliticsService politicsService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldCheckDestroyedShips() {
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip.setId(10L);
		List<Ship> enemyShips = Lists.newArrayList(enemyShip);

		Set<Ship> destroyedShips = new HashSet<>();
		destroyedShips.add(enemyShip);
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);

		Set<Set<Long>> processedShipPairs = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		subject.processCombat(enemyShips, destroyedShips, ship, processedShipPairs, capturedShips);

		assertEquals(1, destroyedShips.size());
		Mockito.verifyZeroInteractions(spaceCombat);
	}

	@Test
	public void shouldCheckProcessedPairs() {
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip.setId(10L);
		List<Ship> enemyShips = Lists.newArrayList(enemyShip);

		Set<Ship> destroyedShips = new HashSet<>();
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);

		Set<Set<Long>> processedShipPairs = new HashSet<>();
		processedShipPairs.add(Sets.newHashSet(enemyShip.getId(), ship.getId()));
		Set<Ship> capturedShips = new HashSet<>();

		subject.processCombat(enemyShips, destroyedShips, ship, processedShipPairs, capturedShips);

		assertEquals(0, destroyedShips.size());
		Mockito.verifyZeroInteractions(spaceCombat);
	}

	@Test
	public void shouldCheckIfAlly() {
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip.setId(10L);
		List<Ship> enemyShips = Lists.newArrayList(enemyShip);

		Set<Ship> destroyedShips = new HashSet<>();
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		Set<Set<Long>> processedShipPairs = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		Mockito.when(politicsService.isAlly(1L, 2L)).thenReturn(true);

		subject.processCombat(enemyShips, destroyedShips, ship, processedShipPairs, capturedShips);

		assertEquals(0, destroyedShips.size());
		Mockito.verify(spaceCombat, Mockito.never()).checkStructurScanner(Mockito.any(), Mockito.any());
		Mockito.verify(spaceCombat, Mockito.never()).checkCombatWithoutWeapons(Mockito.any(), Mockito.any());
		Mockito.verify(spaceCombat, Mockito.never()).processCombat(Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyInt(), Mockito.anyInt());
		Mockito.verify(spaceCombat, Mockito.never()).handleShipDamage(Mockito.anySet(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anySet());
	}

	@Test
	public void shouldCheckIfBothEvade() {
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip.setId(10L);
		enemyShip.setTactics(ShipTactics.DEFENSIVE);
		List<Ship> enemyShips = Lists.newArrayList(enemyShip);

		Set<Ship> destroyedShips = new HashSet<>();
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.setTactics(ShipTactics.DEFENSIVE);
		ship.getPlayer().getGame().setEnableTactialCombat(true);
		Set<Set<Long>> processedShipPairs = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		Mockito.when(spaceCombat.bothEvade(ship, enemyShip)).thenReturn(true);

		subject.processCombat(enemyShips, destroyedShips, ship, processedShipPairs, capturedShips);

		assertEquals(0, destroyedShips.size());
		Mockito.verify(spaceCombat, Mockito.never()).checkStructurScanner(Mockito.any(), Mockito.any());
		Mockito.verify(spaceCombat, Mockito.never()).checkCombatWithoutWeapons(Mockito.any(), Mockito.any());
		Mockito.verify(spaceCombat, Mockito.never()).processCombat(Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyInt(), Mockito.anyInt());
		Mockito.verify(spaceCombat, Mockito.never()).handleShipDamage(Mockito.anySet(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anySet());
	}

	@Test
	public void shouldCheckLuckyShots() {
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip.setId(10L);
		List<Ship> enemyShips = Lists.newArrayList(enemyShip);

		Set<Ship> destroyedShips = new HashSet<>();
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		Set<Set<Long>> processedShipPairs = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		Mockito.when(spaceCombat.checkLuckyShot(ship, enemyShip, destroyedShips)).thenReturn(true);

		subject.processCombat(enemyShips, destroyedShips, ship, processedShipPairs, capturedShips);

		assertEquals(0, destroyedShips.size());
		Mockito.verify(spaceCombat).checkStructurScanner(ship, enemyShip);
		Mockito.verify(spaceCombat, Mockito.never()).checkCombatWithoutWeapons(Mockito.any(), Mockito.any());
		Mockito.verify(spaceCombat, Mockito.never()).processCombat(Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyInt(), Mockito.anyInt());
		Mockito.verify(spaceCombat, Mockito.never()).handleShipDamage(Mockito.anySet(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anySet());

		Mockito.when(spaceCombat.checkLuckyShot(ship, enemyShip, destroyedShips)).thenReturn(false);
		Mockito.when(spaceCombat.checkLuckyShot(enemyShip, ship, destroyedShips)).thenReturn(true);
		processedShipPairs.clear();

		subject.processCombat(enemyShips, destroyedShips, ship, processedShipPairs, capturedShips);

		assertEquals(0, destroyedShips.size());
		Mockito.verify(spaceCombat, Mockito.never()).checkCombatWithoutWeapons(Mockito.any(), Mockito.any());
		Mockito.verify(spaceCombat, Mockito.never()).processCombat(Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyInt(), Mockito.anyInt());
		Mockito.verify(spaceCombat, Mockito.never()).handleShipDamage(Mockito.anySet(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anySet());
	}

	@Test
	public void shouldCheckCombatWithoutWeapons() {
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip.setId(10L);
		List<Ship> enemyShips = Lists.newArrayList(enemyShip);

		Set<Ship> destroyedShips = new HashSet<>();
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		Set<Set<Long>> processedShipPairs = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		Mockito.when(spaceCombat.checkCombatWithoutWeapons(ship, enemyShip)).thenReturn(true);

		subject.processCombat(enemyShips, destroyedShips, ship, processedShipPairs, capturedShips);

		assertEquals(0, destroyedShips.size());
		Mockito.verify(spaceCombat).checkStructurScanner(ship, enemyShip);
		Mockito.verify(spaceCombat, Mockito.never()).processCombat(Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyInt(), Mockito.anyInt());

		Mockito.verify(spaceCombat).handleShipDamage(destroyedShips, ship, enemyShip, false, capturedShips);
		Mockito.verify(spaceCombat).handleShipDamage(destroyedShips, enemyShip, ship, false, capturedShips);
	}

	@Test
	public void shouldProcessCombat() {
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip.setId(10L);
		List<Ship> enemyShips = Lists.newArrayList(enemyShip);

		Set<Ship> destroyedShips = new HashSet<>();
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		Set<Set<Long>> processedShipPairs = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		Mockito.when(spaceCombat.evadeAfterFirstCombatRound(ship, enemyShip)).thenReturn(true);

		subject.processCombat(enemyShips, destroyedShips, ship, processedShipPairs, capturedShips);

		assertEquals(0, destroyedShips.size());
		Mockito.verify(spaceCombat).checkStructurScanner(ship, enemyShip);
		Mockito.verify(spaceCombat).checkShieldDamper(ship);
		Mockito.verify(spaceCombat).checkShieldDamper(enemyShip);
		Mockito.verify(spaceCombat).processCombat(ship, enemyShip, true, 0, 0);

		Mockito.verify(spaceCombat).handleShipDamage(destroyedShips, ship, enemyShip, true, capturedShips);
		Mockito.verify(spaceCombat).handleShipDamage(destroyedShips, enemyShip, ship, true, capturedShips);
	}

	@Test
	public void shouldAbortWhenShipDestroyed() {
		Ship enemyShip1 = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip1.setId(10L);

		Ship enemyShip2 = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip2.setId(11L);
		List<Ship> enemyShips = Lists.newArrayList(enemyShip1, enemyShip2);

		Set<Ship> destroyedShips = new HashSet<>();
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		Set<Set<Long>> processedShipPairs = Mockito.spy(new HashSet<>());

		destroyedShips.add(ship);
		Set<Ship> capturedShips = new HashSet<>();

		subject.processCombat(enemyShips, destroyedShips, ship, processedShipPairs, capturedShips);

		Mockito.verify(processedShipPairs, Mockito.never()).add(Mockito.any());
	}

	@Test
	public void shouldBeDestroyedByFirstShip() {
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip.setId(10L);

		long gameId = 1L;
		long playerId = 1L;
		Ship ship1 = ShipTestFactory.createShip(100, 100, 1, 1, 1, playerId);
		Ship ship2 = ShipTestFactory.createShip(100, 100, 1, 1, 1, playerId);

		List<Ship> allShips = Lists.newArrayList(ship1, ship2, enemyShip);
		Mockito.when(shipRepository.findForSpaceCombat(gameId)).thenReturn(allShips);

		List<Ship> enemyShips = Lists.newArrayList(enemyShip);
		Mockito.when(shipRepository.findEnemyShipsOnSamePosition(gameId, playerId, ship1.getX(), ship1.getY())).thenReturn(enemyShips);

		Set<Ship> capturedShips = new HashSet<>();

		subject.processCombat(gameId);

		Mockito.verify(spaceCombat).handleShipDamage(Mockito.anySet(), Mockito.eq(ship1), Mockito.eq(enemyShip), Mockito.eq(false), Mockito.eq(capturedShips));
		Mockito.verify(spaceCombat).handleShipDamage(Mockito.anySet(), Mockito.eq(enemyShip), Mockito.eq(ship1), Mockito.eq(false), Mockito.eq(capturedShips));

		Mockito.verify(shipService).deleteShips(Mockito.anyCollection());
		Mockito.verify(shipRepository).restoreShields(gameId);
	}
}