package org.skrupeltng.modules.ingame.service.round.combat.space;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTactics;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.testcontainers.shaded.com.google.common.collect.Lists;

public class SpaceCombatUnitTest {

	@Spy
	@InjectMocks
	private final SpaceCombat spaceCombat = new SpaceCombat();

	@Mock
	private PlayerRelationRepository playerRelationRepository;

	@Mock
	private ShipRepository shipRepository;

	@Mock
	private NewsService newsService;

	@Mock
	private ConfigProperties configProperties;

	@Mock
	private ShipEvasion shipEvasion;

	@Mock
	private FleetService fleetService;

	@Mock
	private StatsUpdater statsUpdater;

	@Mock
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Mock
	private ShipService shipService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		MasterDataService.RANDOM.setSeed(1L);

		Mockito.when(configProperties.getMaxExperience()).thenReturn(5);
	}

	@Test
	public void shouldModuleEnabledForMutualEvasion() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.setTactics(ShipTactics.DEFENSIVE);
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip.setTactics(ShipTactics.DEFENSIVE);
		ship.getPlayer().getGame().setEnableTactialCombat(false);

		boolean result = spaceCombat.bothEvade(ship, enemyShip);
		assertEquals(false, result);
		Mockito.verify(shipEvasion, Mockito.never()).evade(Mockito.any(), Mockito.any(), Mockito.anyBoolean());
		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldCheckTacticsForMutualEvasion() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.setTactics(ShipTactics.OFFENSIVE);
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip.setTactics(ShipTactics.OFFENSIVE);
		ship.getPlayer().getGame().setEnableTactialCombat(true);

		boolean result = spaceCombat.bothEvade(ship, enemyShip);
		assertEquals(false, result);
		Mockito.verify(shipEvasion, Mockito.never()).evade(Mockito.any(), Mockito.any(), Mockito.anyBoolean());
		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());

		ship.setTactics(ShipTactics.DEFENSIVE);
		enemyShip.setTactics(ShipTactics.OFFENSIVE);

		result = spaceCombat.bothEvade(ship, enemyShip);
		assertEquals(false, result);
		Mockito.verify(shipEvasion, Mockito.never()).evade(Mockito.any(), Mockito.any(), Mockito.anyBoolean());
		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldExecuteMutualEvasion() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.setTactics(ShipTactics.DEFENSIVE);
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip.setTactics(ShipTactics.DEFENSIVE);
		ship.getPlayer().getGame().setEnableTactialCombat(true);

		boolean result = spaceCombat.bothEvade(ship, enemyShip);
		assertEquals(true, result);

		Mockito.verify(shipEvasion).evade(ship, enemyShip, true);
		Mockito.verify(shipEvasion).evade(enemyShip, ship, true);
		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(shipRepository).save(enemyShip);
	}

	@Test
	public void shouldCheckStructureScannerPresence() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);

		spaceCombat.checkStructurScanner(ship, enemyShip);

		Mockito.verify(shipService, Mockito.never()).checkStructurScanner(Mockito.any(), Mockito.any());

		ship.getShipTemplate().setShipAbilities(Lists.newArrayList(new ShipAbility(ShipAbilityType.STRUCTUR_SCANNER)));

		spaceCombat.checkStructurScanner(ship, enemyShip);

		Mockito.verify(shipService).checkStructurScanner(ship.getPlayer(), enemyShip);
	}

	@Test
	public void shouldCheckTacticsActivated() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);

		boolean result = spaceCombat.evadeAfterFirstCombatRound(ship, enemyShip);
		assertEquals(false, result);
	}

	@Test
	public void shouldNotEvade() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.setTactics(ShipTactics.OFFENSIVE);
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip.setTactics(ShipTactics.STANDARD);
		ship.getPlayer().getGame().setEnableTactialCombat(true);

		boolean result = spaceCombat.evadeAfterFirstCombatRound(ship, enemyShip);
		assertEquals(false, result);

		ship.setTactics(ShipTactics.STANDARD);
		enemyShip.setTactics(ShipTactics.STANDARD);

		result = spaceCombat.evadeAfterFirstCombatRound(ship, enemyShip);
		assertEquals(false, result);
	}

	@Test
	public void shouldEvade() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.setTactics(ShipTactics.DEFENSIVE);
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		enemyShip.setTactics(ShipTactics.STANDARD);
		ship.getPlayer().getGame().setEnableTactialCombat(true);

		boolean result = spaceCombat.evadeAfterFirstCombatRound(ship, enemyShip);
		assertEquals(true, result);

		ship.setTactics(ShipTactics.STANDARD);
		enemyShip.setTactics(ShipTactics.DEFENSIVE);

		result = spaceCombat.evadeAfterFirstCombatRound(ship, enemyShip);
		assertEquals(true, result);
	}

	@Test
	public void shouldCheckLuckyShotPresence() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		Set<Ship> destroyedShips = new HashSet<>();

		boolean result = spaceCombat.checkLuckyShot(ship, enemyShip, destroyedShips);

		assertEquals(false, result);
		assertEquals(0, destroyedShips.size());
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}

	@Test
	public void shouldMissLuckyShot() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);

		ShipAbility shipAbility = new ShipAbility(ShipAbilityType.LUCKY_SHOT);
		Map<String, String> values = new HashMap<>();
		values.put(ShipAbilityConstants.LUCKY_SHOT_PROBABILTY, "0");
		shipAbility.setValues(values);
		ship.getShipTemplate().getShipAbilities().add(shipAbility);

		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		Set<Ship> destroyedShips = new HashSet<>();

		boolean result = spaceCombat.checkLuckyShot(ship, enemyShip, destroyedShips);
		assertEquals(false, result);
		assertEquals(0, destroyedShips.size());
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}

	@Test
	public void shouldHitLuckyShot() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);

		ShipAbility shipAbility = new ShipAbility(ShipAbilityType.LUCKY_SHOT);
		Map<String, String> values = new HashMap<>();
		values.put(ShipAbilityConstants.LUCKY_SHOT_PROBABILTY, "100");
		shipAbility.setValues(values);
		ship.getShipTemplate().getShipAbilities().add(shipAbility);

		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);
		Set<Ship> destroyedShips = new HashSet<>();

		boolean result = spaceCombat.checkLuckyShot(ship, enemyShip, destroyedShips);

		assertEquals(true, result);
		assertEquals(1, destroyedShips.size());
		assertEquals(enemyShip, destroyedShips.iterator().next());
		String sector = "A1";
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_lucky_shot_success, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), sector, enemyShip.getName());
		Mockito.verify(newsService).add(enemyShip.getPlayer(), NewsEntryConstants.news_entry_lucky_shot_victim, enemyShip.createFullImagePath(), null, null,
				enemyShip.getName(), sector, ship.getName());
	}

	@Test
	public void shouldCheckAllWeaponCounts() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		Ship enemyShip = ShipTestFactory.createShip(1, 1, 1, 1, 1, 2L);

		boolean result = spaceCombat.checkCombatWithoutWeapons(ship, enemyShip);
		assertEquals(false, result);
	}

	@Test
	public void shouldCheckMassForEquality() {
		Ship ship = ShipTestFactory.createShip(1, 100, 0, 0, 1, 1L);
		ship.getShipTemplate().setHangarCapacity(0);
		Ship enemyShip = ShipTestFactory.createShip(1, 100, 0, 0, 1, 2L);
		enemyShip.getShipTemplate().setHangarCapacity(0);

		boolean result = spaceCombat.checkCombatWithoutWeapons(ship, enemyShip);
		assertEquals(true, result);
		assertEquals(100, ship.getDamage());
		assertEquals(100, enemyShip.getDamage());
	}

	@Test
	public void shouldCheckMassForInequality() {
		Ship ship = ShipTestFactory.createShip(1, 200, 0, 0, 1, 1L);
		ship.getShipTemplate().setHangarCapacity(0);
		Ship enemyShip = ShipTestFactory.createShip(1, 100, 0, 0, 1, 2L);
		enemyShip.getShipTemplate().setHangarCapacity(0);

		boolean result = spaceCombat.checkCombatWithoutWeapons(ship, enemyShip);
		assertEquals(true, result);
		assertEquals(50, ship.getDamage());
		assertEquals(0, ship.getShield());
		assertEquals(100, enemyShip.getDamage());

		ship.getShipTemplate().setMass(100);
		enemyShip.getShipTemplate().setMass(200);

		result = spaceCombat.checkCombatWithoutWeapons(ship, enemyShip);
		assertEquals(true, result);
		assertEquals(150, enemyShip.getDamage());
		assertEquals(0, enemyShip.getShield());
		assertEquals(100, ship.getDamage());
	}

	@Test
	public void shouldNullCheckShieldDamperAbility() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.setMineral2(100);

		spaceCombat.checkShieldDamper(ship);

		assertEquals(100, ship.getMineral2());
	}

	@Test
	public void shouldCheckShieldDamperAbility() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.setActiveAbility(new ShipAbility(ShipAbilityType.ASTRO_PHYSICS_LAB));
		ship.setMineral2(100);

		spaceCombat.checkShieldDamper(ship);

		assertEquals(100, ship.getMineral2());
	}

	@Test
	public void shouldCheckShieldDamperCosts() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ShipAbility ability = new ShipAbility(ShipAbilityType.SHIELD_DAMPER);
		Map<String, String> values = new HashMap<>();
		values.put(ShipAbilityConstants.SHIELD_DAMPER_COSTS, "110");
		ability.setValues(values);
		ship.getShipTemplate().getShipAbilities().add(ability);
		ship.setActiveAbility(ability);
		ship.setMineral2(100);

		spaceCombat.checkShieldDamper(ship);

		assertEquals(null, ship.getActiveAbility());
		assertEquals(100, ship.getMineral2());
	}

	@Test
	public void shouldActivateShieldDamper() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ShipAbility ability = new ShipAbility(ShipAbilityType.SHIELD_DAMPER);
		Map<String, String> values = new HashMap<>();
		values.put(ShipAbilityConstants.SHIELD_DAMPER_COSTS, "90");
		ability.setValues(values);
		ship.getShipTemplate().getShipAbilities().add(ability);
		ship.setActiveAbility(ability);
		ship.setMineral2(100);

		spaceCombat.checkShieldDamper(ship);

		assertEquals(ability, ship.getActiveAbility());
		assertEquals(10, ship.getMineral2());
	}

	@Test
	public void shouldCheckMinTechLevel() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 6, 1, 1L);
		ship.getShipTemplate().setTechLevel(1);

		Mockito.when(shipRepository.getSupportingShipCount(1L, 0, 0, 3, 1)).thenReturn(1L);
		Mockito.when(shipRepository.getCommunicationCenterShipCount(1L, 0, 0, 3L)).thenReturn(0L);

		int result = spaceCombat.getSupport(ship);

		assertEquals(1, result);
		Mockito.verify(shipRepository).getSupportingShipCount(1L, 0, 0, 3, 1);
		Mockito.verify(shipRepository).getCommunicationCenterShipCount(1L, 0, 0, 3L);
	}

	@Test
	public void shouldCheckSupportingShipCount() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 6, 1, 1L);
		ship.getShipTemplate().setTechLevel(3);

		Mockito.when(shipRepository.getSupportingShipCount(1L, 0, 0, 3, 1)).thenReturn(7L);
		Mockito.when(shipRepository.getCommunicationCenterShipCount(1L, 0, 0, 3L)).thenReturn(0L);

		int result = spaceCombat.getSupport(ship);

		assertEquals(6, result);
		Mockito.verify(shipRepository).getSupportingShipCount(1L, 0, 0, 3, 1);
		Mockito.verify(shipRepository).getCommunicationCenterShipCount(1L, 0, 0, 3L);
	}

	@Test
	public void shouldCheckCommunicationCenterCount() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 6, 1, 1L);
		ship.getShipTemplate().setTechLevel(1);

		Mockito.when(shipRepository.getSupportingShipCount(1L, 0, 0, 3, 1)).thenReturn(1L);
		Mockito.when(shipRepository.getCommunicationCenterShipCount(1L, 0, 0, 3L)).thenReturn(1L);

		int result = spaceCombat.getSupport(ship);

		assertEquals(2, result);
		Mockito.verify(shipRepository).getSupportingShipCount(1L, 0, 0, 3, 1);
		Mockito.verify(shipRepository).getCommunicationCenterShipCount(1L, 0, 0, 3L);
	}

	@Test
	public void shouldEvadeAfterFirstRound() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		Ship enemyShip = ShipTestFactory.createShip(100, 100, 1, 1, 100, 2L);

		spaceCombat.processCombat(ship, enemyShip, true, 1, 1);

		assertEquals(34, ship.getShield());
		assertEquals(0, ship.getDamage());

		assertEquals(34, enemyShip.getShield());
		assertEquals(0, enemyShip.getDamage());
	}

	@Test
	public void shouldUseShieldDamper() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setActiveAbility(new ShipAbility(ShipAbilityType.SHIELD_DAMPER));
		Ship enemyShip = ShipTestFactory.createShip(100, 100, 1, 1, 100, 2L);

		spaceCombat.processCombat(ship, enemyShip, false, 1, 1);

		assertEquals(50, ship.getShield());
		assertEquals(0, ship.getDamage());

		assertEquals(100, enemyShip.getShield());
		assertEquals(100, enemyShip.getDamage());

		ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		enemyShip = ShipTestFactory.createShip(100, 100, 1, 1, 100, 2L);
		enemyShip.setActiveAbility(new ShipAbility(ShipAbilityType.SHIELD_DAMPER));

		spaceCombat.processCombat(ship, enemyShip, false, 1, 1);

		assertEquals(100, ship.getShield());
		assertEquals(100, ship.getDamage());

		assertEquals(50, enemyShip.getShield());
		assertEquals(0, enemyShip.getDamage());
	}

	@Test
	public void shouldDestroyEnemyShip() {
		Ship ship = ShipTestFactory.createShip(100, 100, 5, 1, 100, 1L);
		Ship enemyShip = ShipTestFactory.createShip(100, 100, 1, 1, 100, 2L);

		spaceCombat.processCombat(ship, enemyShip, false, 1, 1);

		assertEquals(34, ship.getShield());
		assertEquals(0, ship.getDamage());

		assertEquals(0, enemyShip.getShield());
		assertEquals(100, enemyShip.getDamage());
	}

	@Test
	public void shouldGetDestroyed() {
		Ship ship = ShipTestFactory.createShip(100, 100, 2, 2, 100, 1L);
		Ship enemyShip = ShipTestFactory.createShip(100, 1000, 3, 3, 100, 2L);

		spaceCombat.processCombat(ship, enemyShip, false, 1, 1);

		assertEquals(0, ship.getShield());
		assertEquals(100, ship.getDamage());

		assertEquals(2, enemyShip.getShield());
		assertEquals(0, enemyShip.getDamage());
	}

	@Test
	public void shouldCheckMutuallyDestructionForCrew() {
		Ship ship = ShipTestFactory.createShip(0, 100, 2, 2, 0, 1L);
		Ship enemyShip = ShipTestFactory.createShip(0, 100, 2, 2, 0, 2L);
		Set<Ship> destroyedShips = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		spaceCombat.handleShipDamage(destroyedShips, ship, enemyShip, false, capturedShips);

		assertEquals(1, destroyedShips.size());
		assertEquals(ship, destroyedShips.iterator().next());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_mutually_destroyed, ship.createFullImagePath(), null, null,
				ship.getName(), enemyShip.getName(), "A1");
		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldCheckMutuallyDestruction() {
		Ship ship = ShipTestFactory.createShip(0, 100, 2, 2, 10, 1L);
		ship.setDamage(100);
		Ship enemyShip = ShipTestFactory.createShip(0, 100, 2, 2, 10, 2L);
		enemyShip.setDamage(100);
		Set<Ship> destroyedShips = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		spaceCombat.handleShipDamage(destroyedShips, ship, enemyShip, false, capturedShips);
		spaceCombat.handleShipDamage(destroyedShips, enemyShip, ship, false, capturedShips);

		assertEquals(2, destroyedShips.size());
		assertEquals(true, destroyedShips.contains(ship));
		assertEquals(true, destroyedShips.contains(enemyShip));

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_mutually_destroyed, ship.createFullImagePath(), null, null,
				ship.getName(), enemyShip.getName(), "A1");
		Mockito.verify(newsService).add(enemyShip.getPlayer(), NewsEntryConstants.news_entry_ship_mutually_destroyed, enemyShip.createFullImagePath(), null,
				null, enemyShip.getName(), ship.getName(), "A1");

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldCheckSimpleDestruction() {
		Ship ship = ShipTestFactory.createShip(0, 100, 2, 2, 10, 1L);
		ship.setDamage(100);
		Ship enemyShip = ShipTestFactory.createShip(0, 100, 2, 2, 10, 2L);
		Set<Ship> destroyedShips = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		spaceCombat.handleShipDamage(destroyedShips, ship, enemyShip, false, capturedShips);
		spaceCombat.handleShipDamage(destroyedShips, enemyShip, ship, false, capturedShips);

		assertEquals(1, destroyedShips.size());
		assertEquals(ship, destroyedShips.iterator().next());

		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_destroyed_ship_by_ship, ship.createFullImagePath(), null, null,
				ship.getName(), enemyShip.getName(), "A1");
		Mockito.verify(newsService).add(enemyShip.getPlayer(), NewsEntryConstants.news_entry_ship_destroyed_enemy_ship, enemyShip.createFullImagePath(),
				enemyShip.getId(), NewsEntryClickTargetType.ship, enemyShip.getName(), ship.getName(), "A1");

		Mockito.verify(shipRepository, Mockito.never()).save(ship);
		Mockito.verify(shipRepository).save(enemyShip);
	}

	@Test
	public void shouldCheckCapture() {
		Ship ship = ShipTestFactory.createShip(0, 100, 2, 2, 0, 1L);
		ship.setExperience(2);
		ship.setTravelSpeed(0);
		ship.setActiveAbility(new ShipAbility(ShipAbilityType.ASTRO_PHYSICS_LAB));
		Ship enemyShip = ShipTestFactory.createShip(0, 100, 2, 2, 10, 2L);
		Set<Ship> destroyedShips = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		Player player1 = ship.getPlayer();
		Player player2 = enemyShip.getPlayer();

		spaceCombat.handleShipDamage(destroyedShips, ship, enemyShip, false, capturedShips);

		assertEquals(0, destroyedShips.size());
		assertEquals(enemyShip.getPlayer(), ship.getPlayer());
		assertEquals(0, ship.getExperience());
		assertEquals(0, ship.getTravelSpeed());
		assertEquals(null, ship.getActiveAbility());
		assertEquals(1, capturedShips.size());
		assertTrue(capturedShips.contains(ship));

		Mockito.verify(shipEvasion).evade(ship, enemyShip, false);
		Mockito.verify(newsService).add(player1, NewsEntryConstants.news_entry_ship_captured_by_enemy, ship.createFullImagePath(), null, null, ship.getName(),
				enemyShip.getName(), "A1");
		Mockito.verify(newsService).add(player2, NewsEntryConstants.news_entry_ship_captured, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, enemyShip.getName(), ship.getName(), "A1");
		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(fleetService).clearFleetOfShipWithoutPermissionCheck(ship.getId());
	}

	@Test
	public void shouldCheckMaxExperience() {
		Ship ship = ShipTestFactory.createShip(0, 100, 2, 2, 10, 1L);
		ship.setExperience(5);
		Ship enemyShip = ShipTestFactory.createShip(0, 100, 2, 2, 0, 2L);
		Set<Ship> destroyedShips = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		spaceCombat.handleShipDamage(destroyedShips, ship, enemyShip, false, capturedShips);

		assertEquals(5, ship.getExperience());
		Mockito.verify(shipRepository).save(ship);
	}

	@Test
	public void shouldNotGainExperience() {
		Ship ship = ShipTestFactory.createShip(0, 100, 2, 2, 10, 1L);
		ship.getShipTemplate().setTechLevel(5);
		Ship enemyShip = ShipTestFactory.createShip(0, 100, 2, 2, 0, 2L);
		Set<Ship> destroyedShips = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		spaceCombat.handleShipDamage(destroyedShips, ship, enemyShip, false, capturedShips);

		assertEquals(0, ship.getExperience());
		Mockito.verify(shipRepository).save(ship);
	}

	@Test
	public void shouldGainExperience() {
		MasterDataService.RANDOM.setSeed(4L);

		Ship ship = ShipTestFactory.createShip(0, 100, 2, 2, 10, 1L);
		ship.getShipTemplate().setTechLevel(5);
		Ship enemyShip = ShipTestFactory.createShip(0, 100, 2, 2, 0, 2L);
		enemyShip.getShipTemplate().setTechLevel(5);
		Set<Ship> destroyedShips = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		spaceCombat.handleShipDamage(destroyedShips, ship, enemyShip, false, capturedShips);

		assertEquals(1, ship.getExperience());
		Mockito.verify(shipRepository).save(ship);
	}

	@Test
	public void shouldCheckEvasionFlag() {
		Ship ship = ShipTestFactory.createShip(0, 100, 2, 2, 10, 1L);
		Ship enemyShip = ShipTestFactory.createShip(0, 100, 2, 2, 0, 2L);
		Set<Ship> destroyedShips = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		spaceCombat.handleShipDamage(destroyedShips, ship, enemyShip, false, capturedShips);

		Mockito.verify(shipEvasion, Mockito.never()).evade(Mockito.any(), Mockito.any(), Mockito.anyBoolean());
		Mockito.verify(shipRepository).save(ship);
	}

	@Test
	public void shouldCheckEvasionCrew() {
		Ship ship = ShipTestFactory.createShip(0, 100, 2, 2, 0, 1L);
		Ship enemyShip = ShipTestFactory.createShip(0, 100, 2, 2, 10, 2L);
		enemyShip.setDamage(100);
		Set<Ship> destroyedShips = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		spaceCombat.handleShipDamage(destroyedShips, ship, enemyShip, true, capturedShips);

		Mockito.verify(shipEvasion, Mockito.never()).evade(Mockito.any(), Mockito.any(), Mockito.anyBoolean());
		Mockito.verify(shipRepository).save(ship);
	}

	@Test
	public void shouldCallEvasion() {
		Ship ship = ShipTestFactory.createShip(0, 100, 2, 2, 10, 1L);
		Ship enemyShip = ShipTestFactory.createShip(0, 100, 2, 2, 10, 2L);
		Set<Ship> destroyedShips = new HashSet<>();
		Set<Ship> capturedShips = new HashSet<>();

		spaceCombat.handleShipDamage(destroyedShips, ship, enemyShip, true, capturedShips);

		Mockito.verify(shipEvasion).evade(ship, enemyShip, true);
		Mockito.verify(shipRepository).save(ship);
	}
}