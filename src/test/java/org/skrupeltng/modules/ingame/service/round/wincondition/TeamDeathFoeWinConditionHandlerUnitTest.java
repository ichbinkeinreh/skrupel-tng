package org.skrupeltng.modules.ingame.service.round.wincondition;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoeRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;

import com.google.common.collect.Lists;

public class TeamDeathFoeWinConditionHandlerUnitTest {

	private TeamDeathFoeWinConditionHandler subject;
	private GameRepository gameRepository;
	private PlayerDeathFoeRepository playerDeathFoeRepository;
	private PlayerRelationRepository playerRelationRepository;
	private PlayerRepository playerRepository;

	@Before
	public void setup() {
		subject = new TeamDeathFoeWinConditionHandler();

		gameRepository = Mockito.mock(GameRepository.class);
		subject.setGameRepository(gameRepository);

		playerDeathFoeRepository = Mockito.mock(PlayerDeathFoeRepository.class);
		subject.setPlayerDeathFoeRepository(playerDeathFoeRepository);

		playerRelationRepository = Mockito.mock(PlayerRelationRepository.class);
		subject.setPlayerRelationRepository(playerRelationRepository);

		playerRepository = Mockito.mock(PlayerRepository.class);
		subject.setPlayerRepository(playerRepository);
	}

	@Test
	public void shouldCheckForSurvivingDeathFoes() {
		Player player = new Player(2L);
		Mockito.when(playerRepository.getNotLostPlayers(1L)).thenReturn(Lists.newArrayList(player));

		Player deathFoe = new Player(3L);
		Mockito.when(playerDeathFoeRepository.getDeathFoesByPlayerId(2L)).thenReturn(Lists.newArrayList(deathFoe));

		subject.checkWinCondition(1L);

		Mockito.verify(gameRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(playerRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldLetAllAlliesWin() {
		Player player = new Player(2L);
		Mockito.when(playerRepository.getNotLostPlayers(1L)).thenReturn(Lists.newArrayList(player));

		Player deathFoe = new Player(3L);
		deathFoe.setHasLost(true);
		Mockito.when(playerDeathFoeRepository.getDeathFoesByPlayerId(2L)).thenReturn(Lists.newArrayList(deathFoe));

		Player ally = new Player(4L);

		PlayerRelation relation = new PlayerRelation();
		relation.setPlayer1(player);
		relation.setPlayer2(ally);
		relation.setType(PlayerRelationType.ALLIANCE);
		Mockito.when(playerRelationRepository.findByPlayerIdAndType(2L, PlayerRelationType.ALLIANCE)).thenReturn(Lists.newArrayList(relation));

		Player otherParty = new Player(5L);

		Game game = new Game(1L);
		game.setPlayers(Lists.newArrayList(player, ally, deathFoe, otherParty));
		Mockito.when(gameRepository.getOne(1L)).thenReturn(game);

		subject.checkWinCondition(1L);

		Mockito.verify(gameRepository).save(game);

		ArgumentCaptor<Player> savedPlayers = ArgumentCaptor.forClass(Player.class);
		Mockito.verify(playerRepository, Mockito.times(2)).save(savedPlayers.capture());

		List<Player> losers = savedPlayers.getAllValues();
		assertTrue(losers.contains(deathFoe));
		assertTrue(losers.contains(otherParty));

		for (Player loser : losers) {
			assertEquals(true, loser.isHasLost());
		}

		assertEquals(true, game.isFinished());
	}
}