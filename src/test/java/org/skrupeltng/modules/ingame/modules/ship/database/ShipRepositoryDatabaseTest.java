package org.skrupeltng.modules.ingame.modules.ship.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.skrupeltng.AbstractDbTest;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.springframework.beans.factory.annotation.Autowired;

public class ShipRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Test
	public void shouldAcceptOwnedShipAsOwnedByPlayer() {
		List<Ship> ships = shipRepository.findByGameId(1L);
		assertTrue(!ships.isEmpty());

		Ship ship = ships.get(0);

		boolean loginOwnsShip = shipRepository.loginOwnsShip(ship.getId(), ship.getPlayer().getLogin().getId());
		assertTrue(loginOwnsShip);
	}

	@Test
	public void shouldNotAcceptEnemyShipAsOwnedByPlayer() {
		List<Ship> ships = shipRepository.findByGameId(1L);
		assertTrue(!ships.isEmpty());

		Ship ship = ships.get(0);
		long shipLoginId = ship.getPlayer().getLogin().getId();

		Player enemyPlayer = playerRepository.findByGameId(1L).stream().filter(p -> p.getLogin().getId() != shipLoginId).findFirst().get();

		boolean loginOwnsShip = shipRepository.loginOwnsShip(ship.getId(), enemyPlayer.getLogin().getId());
		assertFalse(loginOwnsShip);
	}

	@Test
	public void shoulddFindShipsInRadius() {
		List<Long> shipIds = shipRepository.getShipIdsInRadius(1L, 398, 281, 500);
		assertEquals(8, shipIds.size());
	}

	@Test
	public void shoulddFindNoShipsInRadius() {
		List<Long> shipIds = shipRepository.getShipIdsInRadius(1L, 150, 100, 5);
		assertEquals(0, shipIds.size());
	}

	@Test
	public void shouldResetScanRadius() {
		shipRepository.resetShipScanRadius(1L);

		Ship ship = shipRepository.getOne(8L);
		assertEquals(47, ship.getScanRadius());
	}

	@Test
	public void shouldScanRadiusToCorrectValue() {
		shipRepository.updateScanRadius(1L, 116, ShipAbilityType.ASTRO_PHYSICS_LAB);
	}

	@Test
	public void shouldCloakAntiGravEngineShips() {
		shipRepository.cloakAntigravPropulsion(1L);

		Ship ship = shipRepository.getOne(7L);
		assertTrue(ship.isCloaked());
	}

	@Test
	public void shouldFindSupportingShips() {
		long count = shipRepository.getSupportingShipCount(1L, 741, 844, 0, 1);
		assertEquals(0L, count);
	}

	@Test
	public void shouldNotFindCommunicationCenters() {
		long count = shipRepository.getCommunicationCenterShipCount(1L, 398, 281, 14L);
		assertEquals(0L, count);
	}

	@Test
	public void shouldGetEnemyShipCountAtDestination() {
		long count = shipRepository.getEnemyShipCountByPlanetDestination(391, 142, 1L, 1L);
		assertEquals(0L, count);
	}

	@Test
	public void shouldGetHighestSupportingTechLevel() {
		int result = shipRepository.getHighestSupportingTechLevel(1L, 1L, 0, 0);
		assertEquals(0, result);
	}

	@Test
	public void shouldProcessesShipsInPlasmaStorms() {
		shipRepository.processShipsInPlasmaStorms(1L);
	}
}