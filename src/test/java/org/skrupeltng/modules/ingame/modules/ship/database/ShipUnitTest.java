package org.skrupeltng.modules.ingame.modules.ship.database;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;

public class ShipUnitTest {

	@Test
	public void shouldNotTowShipBecauseCoordinateX() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		ship.setPropulsionSystemTemplate(new PropulsionSystemTemplate());
		Ship s = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1);

		ship.setX(1);

		boolean result = ship.canTowShip(s);
		assertEquals(false, result);
	}

	@Test
	public void shouldNotTowShipBecauseCoordinateY() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		ship.setPropulsionSystemTemplate(new PropulsionSystemTemplate());
		Ship s = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1);

		ship.setY(1);

		boolean result = ship.canTowShip(s);
		assertEquals(false, result);
	}

	@Test
	public void shouldNotTowShipBecauseMassTooHigh() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		ship.getShipTemplate().setPropulsionSystemsCount(1);
		PropulsionSystemTemplate propulsionSystemTemplate = new PropulsionSystemTemplate();
		propulsionSystemTemplate.setTechLevel(1);
		ship.setPropulsionSystemTemplate(propulsionSystemTemplate);
		Ship s = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1);

		boolean result = ship.canTowShip(s);
		assertEquals(false, result);
	}

	@Test
	public void shouldTowShip() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		ship.getShipTemplate().setPropulsionSystemsCount(2);
		PropulsionSystemTemplate propulsionSystemTemplate = new PropulsionSystemTemplate();
		propulsionSystemTemplate.setTechLevel(5);
		ship.setPropulsionSystemTemplate(propulsionSystemTemplate);
		Ship s = ShipTestFactory.createShip(100, 10, 1, 1, 1, 1);

		boolean result = ship.canTowShip(s);
		assertEquals(true, result);
	}
}