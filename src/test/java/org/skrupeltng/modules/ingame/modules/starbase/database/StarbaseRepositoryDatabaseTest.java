package org.skrupeltng.modules.ingame.modules.starbase.database;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.skrupeltng.AbstractDbTest;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class StarbaseRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Test
	public void shouldAcceptHomePlanetStarbaseAsOwnedByPlayer() {
		Player player = playerRepository.getOne(1L);
		Planet homePlanet = player.getHomePlanet();
		boolean loginOwnsStarbase = starbaseRepository.loginOwnsStarbase(homePlanet.getStarbase().getId(), player.getLogin().getId());
		assertTrue(loginOwnsStarbase);
	}

	@Test
	public void shouldNotAcceptEnemyStarbaseAsOwnedByPlayer() {
		Player player = playerRepository.getOne(1L);
		Player other = playerRepository.getOne(2L);
		Planet otherHomePlanet = other.getHomePlanet();
		boolean loginOwnsStarbase = starbaseRepository.loginOwnsStarbase(otherHomePlanet.getStarbase().getId(), player.getLogin().getId());
		assertFalse(loginOwnsStarbase);
	}
}