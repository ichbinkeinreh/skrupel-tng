package org.skrupeltng.modules.ingame.modules.planet.database;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.skrupeltng.AbstractDbTest;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class OrbitalSystemRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Test
	public void shouldAcceptHomePlanetOrbitalSystemsAsOwnedByPlayer() {
		Player player = playerRepository.getOne(1L);
		Planet homePlanet = player.getHomePlanet();

		long loginId = player.getLogin().getId();
		List<OrbitalSystem> orbitalSystems = homePlanet.getOrbitalSystems();

		for (OrbitalSystem orbitalSystem : orbitalSystems) {
			boolean loginOwnsOrbitalSystem = orbitalSystemRepository.loginOwnsOrbitalSystem(orbitalSystem.getId(), loginId);
			assertTrue(loginOwnsOrbitalSystem);
		}
	}

	@Test
	public void shouldNotAcceptEnemyPlanetAsOwnedByPlayer() {
		Player player = playerRepository.getOne(1L);
		Player other = playerRepository.getOne(2L);
		Planet otherHomePlanet = other.getHomePlanet();

		long loginId = player.getLogin().getId();
		List<OrbitalSystem> orbitalSystems = otherHomePlanet.getOrbitalSystems();

		for (OrbitalSystem orbitalSystem : orbitalSystems) {
			boolean loginOwnsPlanet = orbitalSystemRepository.loginOwnsOrbitalSystem(orbitalSystem.getId(), loginId);
			assertFalse(loginOwnsPlanet);
		}
	}

	@Test
	public void shouldNotAcceptUninhabitedPlanetAsOwnedByPlayer() {
		Player player = playerRepository.getOne(1L);

		long loginId = player.getLogin().getId();
		Planet uninhabitedPlanet = planetRepository.findAll().stream().filter(p -> p.getPlayer() == null).findFirst().get();
		List<OrbitalSystem> orbitalSystems = uninhabitedPlanet.getOrbitalSystems();

		for (OrbitalSystem orbitalSystem : orbitalSystems) {
			boolean loginOwnsPlanet = orbitalSystemRepository.loginOwnsOrbitalSystem(orbitalSystem.getId(), loginId);
			assertFalse(loginOwnsPlanet);
		}
	}
}