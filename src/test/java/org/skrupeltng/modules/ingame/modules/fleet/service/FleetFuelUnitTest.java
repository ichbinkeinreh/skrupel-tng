package org.skrupeltng.modules.ingame.modules.fleet.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.testcontainers.shaded.com.google.common.collect.Lists;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Sets;

public class FleetFuelUnitTest {

	@Spy
	@InjectMocks
	private final FleetFuel subject = new FleetFuel();

	@Mock
	private FleetRepository fleetRepository;

	@Mock
	private ShipService shipService;

	private List<Ship> ships;

	private Planet planet1;
	private Planet planet2;

	private Ship ship1;
	private Ship ship2;
	private Ship ship3;
	private Ship ship4;
	private Ship ship5;
	private Ship ship6;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		Fleet fleet = new Fleet();

		planet1 = new Planet(1L);
		planet1.setFuel(500);

		planet2 = new Planet(2L);
		planet2.setFuel(300);

		ship1 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship1.setId(1L);
		ship1.getShipTemplate().setFuelCapacity(200);
		ship1.setFuel(50);
		ship1.setPlanet(planet1);

		ship2 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship2.setId(2L);
		ship2.getShipTemplate().setFuelCapacity(200);
		ship2.setFuel(50);
		ship2.setPlanet(planet1);

		ship3 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship3.setId(3L);
		ship3.getShipTemplate().setFuelCapacity(200);
		ship3.setFuel(50);

		ship4 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship4.setId(4L);
		ship4.getShipTemplate().setFuelCapacity(200);
		ship4.setFuel(50);
		ship4.setPlanet(planet2);

		ship5 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship5.setId(5L);
		ship5.getShipTemplate().setFuelCapacity(200);
		ship5.setFuel(50);
		ship5.setPlanet(planet2);

		ship6 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship6.setId(6L);
		ship6.getShipTemplate().setFuelCapacity(50);
		ship6.setFuel(20);
		ship6.setPlanet(planet2);

		ships = Lists.newArrayList(ship1, ship2, ship3, ship4, ship5, ship6);
		fleet.setShips(ships);

		Mockito.when(fleetRepository.getOne(Mockito.anyLong())).thenReturn(fleet);
	}

	@Test
	public void shouldCreateMap() {
		HashMultimap<Planet, Ship> result = subject.createMap(1L);

		assertNotNull(result);
		assertEquals(2, result.keySet().size());

		assertEquals(Sets.newHashSet(ship1, ship2), result.get(planet1));
		assertEquals(Sets.newHashSet(ship4, ship5, ship6), result.get(planet2));
	}

	@Test
	public void shouldTransportAllFuelToPlanets() {
		subject.transportFuelToPlanets(1L);

		ShipTransportRequest request = new ShipTransportRequest(ship1);
		request.setFuel(0);

		Mockito.verify(shipService).transport(1L, request);
		Mockito.verify(shipService).transport(2L, request);
		Mockito.verify(shipService, Mockito.never()).transport(Mockito.eq(3L), Mockito.any());
		Mockito.verify(shipService).transport(4L, request);
		Mockito.verify(shipService).transport(5L, request);
		Mockito.verify(shipService).transport(6L, request);
	}

	@Test
	public void shouldDistributeFuelEvenlyFull() {
		subject.distributeFuel(1L, 1f);

		ShipTransportRequest request = new ShipTransportRequest(ship1);
		request.setFuel(200);

		Mockito.verify(shipService).transport(1L, request);
		Mockito.verify(shipService).transport(2L, request);

		Mockito.verify(shipService, Mockito.never()).transport(Mockito.eq(3L), Mockito.any());

		request = new ShipTransportRequest(ship4);
		request.setFuel(133);
		Mockito.verify(shipService).transport(4L, request);

		request = new ShipTransportRequest(ship5);
		request.setFuel(133);
		Mockito.verify(shipService).transport(5L, request);

		request = new ShipTransportRequest(ship6);
		request.setFuel(33);
		Mockito.verify(shipService).transport(6L, request);
	}

	@Test
	public void shouldDistributeFuelEvenlyHalf() {
		subject.distributeFuel(1L, 0.5f);

		ShipTransportRequest request = new ShipTransportRequest(ship1);
		request.setFuel(100);

		Mockito.verify(shipService).transport(1L, request);
		Mockito.verify(shipService).transport(2L, request);

		Mockito.verify(shipService, Mockito.never()).transport(Mockito.eq(3L), Mockito.any());

		request = new ShipTransportRequest(ship4);
		request.setFuel(100);
		Mockito.verify(shipService).transport(4L, request);

		request = new ShipTransportRequest(ship5);
		request.setFuel(100);
		Mockito.verify(shipService).transport(5L, request);

		request = new ShipTransportRequest(ship6);
		request.setFuel(25);
		Mockito.verify(shipService).transport(6L, request);
	}
}