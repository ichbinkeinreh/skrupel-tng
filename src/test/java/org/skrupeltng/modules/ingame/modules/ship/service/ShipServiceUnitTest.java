package org.skrupeltng.modules.ingame.modules.ship.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFoldRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplate;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplateRepository;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.data.util.Pair;
import org.testcontainers.shaded.com.google.common.collect.Lists;

public class ShipServiceUnitTest {

	@Spy
	@InjectMocks
	private final ShipService subject = new ShipService();

	@Mock
	private ShipRepository shipRepository;

	@Mock
	private PlanetRepository planetRepository;

	@Mock
	private GameRepository gameRepository;

	@Mock
	private PlayerRepository playerRepository;

	@Mock
	private PlayerRelationRepository playerRelationRepository;

	@Mock
	private WormHoleRepository wormHoleRepository;

	@Mock
	private SpaceFoldRepository spaceFoldRepository;

	@Mock
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Mock
	private AquiredShipTemplateRepository aquiredShipTemplateRepository;

	@Mock
	private FleetRepository fleetRepository;

	@Mock
	private VisibleObjects visibleObjects;

	@Before
	public void setup() {
		MasterDataService.RANDOM.setSeed(1L);

		MockitoAnnotations.initMocks(this);

		Mockito.when(shipRepository.save(Mockito.any())).thenAnswer(new Answer<Ship>() {
			@Override
			public Ship answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgumentAt(0, Ship.class);
			}
		});

		Mockito.when(planetRepository.save(Mockito.any())).thenAnswer(new Answer<Planet>() {
			@Override
			public Planet answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgumentAt(0, Planet.class);
			}
		});
	}

	@Test
	public void shouldCheckAquiredShipTemplateExistence() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.getShipTemplate().getShipAbilities().add(new ShipAbility(ShipAbilityType.STRUCTUR_SCANNER));
		Player player = ShipTestFactory.createPlayer(2L);

		Mockito.when(aquiredShipTemplateRepository.findByGameIdAndShipTemplateId(Mockito.anyLong(), Mockito.anyString()))
				.thenReturn(Optional.of(new AquiredShipTemplate()));

		subject.checkStructurScanner(player, ship);

		Mockito.verify(aquiredShipTemplateRepository).findByGameIdAndShipTemplateId(player.getGame().getId(), "1");
		Mockito.verify(aquiredShipTemplateRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldInsertAquiredShipTemplate() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.getShipTemplate().getShipAbilities().add(new ShipAbility(ShipAbilityType.STRUCTUR_SCANNER));
		Player player = ShipTestFactory.createPlayer(2L);

		Mockito.when(aquiredShipTemplateRepository.findByGameIdAndShipTemplateId(Mockito.anyLong(), Mockito.anyString())).thenReturn(Optional.empty());

		subject.checkStructurScanner(player, ship);

		Mockito.verify(aquiredShipTemplateRepository).findByGameIdAndShipTemplateId(player.getGame().getId(), "1");
		Mockito.verify(aquiredShipTemplateRepository).save(Mockito.any());
	}

	private void assertNoTransport() {
		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(planetRepository, Mockito.never()).save(Mockito.any());
	}

	private void assertTransport(Ship ship, Planet planet) {
		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(planetRepository).save(planet);
	}

	@Test
	public void shouldCheckPlanetsPresence() {
		ShipTransportRequest request = new ShipTransportRequest();
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		Planet planet = null;

		try {
			subject.transportWithoutPermissionCheck(request, ship, planet);
			fail("Exception expected!");
		} catch (IllegalArgumentException e) {
			assertEquals("Ship " + ship.getId() + " is not in a planet's orbit!", e.getMessage());
		}

		assertNoTransport();
	}

	@Test
	public void shouldCheckInfantryTransport() {
		ShipTransportRequest request = new ShipTransportRequest();
		request.setMineral1(100);
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		ship.getShipTemplate().setShipAbilities(Lists.newArrayList(new ShipAbility(ShipAbilityType.INFANTRY_TRANSPORT)));
		Planet planet = new Planet(2L);

		try {
			subject.transportWithoutPermissionCheck(request, ship, planet);
			fail("Exception expected!");
		} catch (IllegalArgumentException e) {
			assertEquals("Infantry transport can only carry fuel, supplies and light ground units!", e.getMessage());
		}

		assertNoTransport();
	}

	@Test
	public void shouldCheckForNegativeValues() {
		ShipTransportRequest request = new ShipTransportRequest();
		request.setFuel(-1);
		checkForNegativeValues(request);

		request = new ShipTransportRequest();
		request.setColonists(-1);
		checkForNegativeValues(request);

		request = new ShipTransportRequest();
		request.setHeavyGroundUnits(-1);
		checkForNegativeValues(request);

		request = new ShipTransportRequest();
		request.setLightGroundUnits(-1);
		checkForNegativeValues(request);

		request = new ShipTransportRequest();
		request.setMineral1(-1);
		checkForNegativeValues(request);

		request = new ShipTransportRequest();
		request.setMineral2(-1);
		checkForNegativeValues(request);

		request = new ShipTransportRequest();
		request.setMineral3(-1);
		checkForNegativeValues(request);

		request = new ShipTransportRequest();
		request.setMoney(-1);
		checkForNegativeValues(request);

		request = new ShipTransportRequest();
		request.setSupplies(-1);
		checkForNegativeValues(request);
	}

	private void checkForNegativeValues(ShipTransportRequest request) {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		Planet planet = new Planet(2L);

		try {
			subject.transportWithoutPermissionCheck(request, ship, planet);
			fail("Exception expected!");
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().startsWith("Invalid change ship storage request for ship " + ship.getId() + ": "));
		}

		assertNoTransport();
	}

	@Test
	public void shouldCheckForNonExistingValues() {
		ShipTransportRequest request = new ShipTransportRequest();
		request.setColonists(1);
		checkForNonExistingValues(request);

		request = new ShipTransportRequest();
		request.setHeavyGroundUnits(1);
		checkForNonExistingValues(request);

		request = new ShipTransportRequest();
		request.setLightGroundUnits(1);
		checkForNonExistingValues(request);
	}

	private void checkForNonExistingValues(ShipTransportRequest request) {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		Planet planet = new Planet(2L);

		try {
			subject.transportWithoutPermissionCheck(request, ship, planet);
			fail("Exception expected!");
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().startsWith("Invalid transport request!"));
		}

		assertNoTransport();
	}

	@Test
	public void shouldCheckForOddNumberOfIntegerOverflow() {
		ShipTransportRequest request = new ShipTransportRequest();
		request.setMineral1(1);
		request.setMineral2(2147483647);

		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		Planet planet = new Planet(2L);

		try {
			subject.transportWithoutPermissionCheck(request, ship, planet);
			fail("Exception expected!");
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().startsWith("Invalid change ship storage request for ship " + ship.getId() + ": "));
		}

		assertNoTransport();
	}

	@Test
	public void shouldCheckForEvenNumberOfIntegerOverflow() {
		ShipTransportRequest request = new ShipTransportRequest();
		request.setMineral1(2);
		request.setMineral2(2147483647);
		request.setMineral3(2147483647);

		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		Planet planet = new Planet(2L);
		planet.setMineral3(300);

		try {
			subject.transportWithoutPermissionCheck(request, ship, planet);
			fail("Exception expected!");
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().equals("Invalid transport request!"));
		}

		assertNoTransport();
	}

	@Test
	public void shouldCheckFuelCapacity() {
		ShipTransportRequest request = new ShipTransportRequest();
		request.setFuel(150);
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		Planet planet = new Planet(2L);

		try {
			subject.transportWithoutPermissionCheck(request, ship, planet);
			fail("Exception expected!");
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().startsWith("Invalid change ship storage request for ship " + ship.getId() + ": "));
		}

		assertNoTransport();
	}

	@Test
	public void shouldCheckStorageSpace() {
		ShipTransportRequest request = new ShipTransportRequest();
		request.setMineral1(10000);
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		Planet planet = new Planet(2L);

		try {
			subject.transportWithoutPermissionCheck(request, ship, planet);
			fail("Exception expected!");
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().startsWith("Invalid change ship storage request for ship " + ship.getId() + ": "));
		}

		assertNoTransport();
	}

	private Planet createBunkerPlanet() {
		Planet planet = new Planet(2L);
		planet.setOrbitalSystems(Lists.newArrayList(new OrbitalSystem(OrbitalSystemType.BUNKER)));
		return planet;
	}

	@Test
	public void shouldCheckTransportableFuel() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		ship.getShipTemplate().setFuelCapacity(200);
		ShipTransportRequest request = new ShipTransportRequest(ship);
		request.setFuel(150);
		Planet planet = createBunkerPlanet();
		planet.setFuel(150);

		Pair<Ship, Planet> result = subject.transportWithoutPermissionCheck(request, ship, planet);

		assertNotNull(result);
		assertEquals(100, planet.getFuel());
		assertEquals(50, ship.getFuel());

		assertTransport(ship, planet);
	}

	@Test
	public void shouldCheckTransportableMoney() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		ShipTransportRequest request = new ShipTransportRequest(ship);
		request.setMoney(550);
		Planet planet = createBunkerPlanet();
		planet.setMoney(550);

		Pair<Ship, Planet> result = subject.transportWithoutPermissionCheck(request, ship, planet);

		assertNotNull(result);
		assertEquals(500, planet.getMoney());
		assertEquals(50, ship.getMoney());

		assertTransport(ship, planet);
	}

	@Test
	public void shouldCheckTransportableSupplies() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		ship.getShipTemplate().setStorageSpace(200);
		ShipTransportRequest request = new ShipTransportRequest(ship);
		request.setSupplies(150);
		Planet planet = createBunkerPlanet();
		planet.setSupplies(150);

		Pair<Ship, Planet> result = subject.transportWithoutPermissionCheck(request, ship, planet);

		assertNotNull(result);
		assertEquals(100, planet.getSupplies());
		assertEquals(50, ship.getSupplies());

		assertTransport(ship, planet);
	}

	@Test
	public void shouldCheckTransportableMineral1() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		ship.getShipTemplate().setStorageSpace(200);
		ShipTransportRequest request = new ShipTransportRequest(ship);
		request.setMineral1(150);
		Planet planet = createBunkerPlanet();
		planet.setMineral1(150);

		Pair<Ship, Planet> result = subject.transportWithoutPermissionCheck(request, ship, planet);

		assertNotNull(result);
		assertEquals(100, planet.getMineral1());
		assertEquals(50, ship.getMineral1());

		assertTransport(ship, planet);
	}

	@Test
	public void shouldCheckTransportableMineral2() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		ship.getShipTemplate().setStorageSpace(200);
		ShipTransportRequest request = new ShipTransportRequest(ship);
		request.setMineral2(150);
		Planet planet = createBunkerPlanet();
		planet.setMineral2(150);

		Pair<Ship, Planet> result = subject.transportWithoutPermissionCheck(request, ship, planet);

		assertNotNull(result);
		assertEquals(100, planet.getMineral2());
		assertEquals(50, ship.getMineral2());

		assertTransport(ship, planet);
	}

	@Test
	public void shouldCheckTransportableMineral3() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		ship.getShipTemplate().setStorageSpace(200);
		ShipTransportRequest request = new ShipTransportRequest(ship);
		request.setMineral3(150);
		Planet planet = createBunkerPlanet();
		planet.setMineral3(150);

		Pair<Ship, Planet> result = subject.transportWithoutPermissionCheck(request, ship, planet);

		assertNotNull(result);
		assertEquals(100, planet.getMineral3());
		assertEquals(50, ship.getMineral3());

		assertTransport(ship, planet);
	}

	@Test
	public void shouldCheckForNoPlayer() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		ship.setColonists(1500);
		ShipTransportRequest request = new ShipTransportRequest();
		request.setColonists(500);
		Planet planet = new Planet(2L);

		Pair<Ship, Planet> result = subject.transportWithoutPermissionCheck(request, ship, planet);

		assertNotNull(result);
		assertEquals(0, planet.getColonists());
		assertEquals(1000, planet.getNewColonists());
		assertEquals(500, ship.getColonists());
		assertEquals(ship.getPlayer(), planet.getNewPlayer());
		assertEquals(null, planet.getPlayer());

		assertTransport(ship, planet);
	}

	@Test
	public void shouldCheckForOwnPlayer() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		ship.setColonists(1500);
		ShipTransportRequest request = new ShipTransportRequest();
		request.setColonists(500);
		Planet planet = new Planet(2L);
		planet.setPlayer(ship.getPlayer());

		Pair<Ship, Planet> result = subject.transportWithoutPermissionCheck(request, ship, planet);

		assertNotNull(result);
		assertEquals(1000, planet.getColonists());
		assertEquals(0, planet.getNewColonists());
		assertEquals(500, ship.getColonists());
		assertEquals(null, planet.getNewPlayer());
		assertEquals(ship.getPlayer(), planet.getPlayer());

		assertTransport(ship, planet);
	}

	@Test
	public void shouldSetNewPlayer() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		ship.setColonists(1500);
		ShipTransportRequest request = new ShipTransportRequest();
		request.setColonists(500);
		Planet planet = new Planet(2L);

		Pair<Ship, Planet> result = subject.transportWithoutPermissionCheck(request, ship, planet);

		assertNotNull(result);
		assertEquals(0, planet.getColonists());
		assertEquals(1000, planet.getNewColonists());
		assertEquals(500, ship.getColonists());
		assertEquals(ship.getPlayer(), planet.getNewPlayer());
		assertEquals(null, planet.getPlayer());

		assertTransport(ship, planet);
	}

	@Test
	public void shouldCheckPlayerInGame() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 4L);

		Player player = new Player(2L);
		player.setGame(new Game(5L));
		Mockito.when(playerRepository.getOne(2L)).thenReturn(player);

		try {
			subject.giveShipToAlly(ship, 2L);
			fail("IllegalArgumentException expected!");
		} catch (IllegalArgumentException e) {
			assertEquals("Player 2 is not part of game 1!", e.getMessage());
		}

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldCheckPlayerHasRelation() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 4L);

		Player player = new Player(2L);
		player.setGame(new Game(1L));
		Mockito.when(playerRepository.getOne(2L)).thenReturn(player);

		try {
			subject.giveShipToAlly(ship, 2L);
			fail("IllegalArgumentException expected!");
		} catch (IllegalArgumentException e) {
			assertEquals("Player 2 is not in an Alliance with player 4!", e.getMessage());
		}

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldCheckPlayerIsInAlliance() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 4L);

		Player player = new Player(2L);
		player.setGame(new Game(1L));
		Mockito.when(playerRepository.getOne(2L)).thenReturn(player);

		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.NON_AGGRESSION_TREATY);
		Mockito.when(playerRelationRepository.findByPlayerIds(4L, 2L)).thenReturn(Lists.newArrayList(relation));

		try {
			subject.giveShipToAlly(ship, 2L);
			fail("IllegalArgumentException expected!");
		} catch (IllegalArgumentException e) {
			assertEquals("Player 2 is not in an Alliance with player 4!", e.getMessage());
		}

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void shouldGiveShipToPlayer() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 4L);

		Player player = new Player(2L);
		player.setGame(new Game(1L));
		Mockito.when(playerRepository.getOne(2L)).thenReturn(player);

		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.ALLIANCE);
		Mockito.when(playerRelationRepository.findByPlayerIds(4L, 2L)).thenReturn(Lists.newArrayList(relation));

		subject.giveShipToAlly(ship, 2L);

		assertEquals(player, ship.getPlayer());

		Mockito.verify(shipRepository).save(ship);
	}

	@Test
	public void shouldResetTravelBecauseOnlyOneRouteEntryLeft() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);

		ShipRouteEntry entry1 = new ShipRouteEntry(1L, ship, new Planet(1L, 1, 1), 0);
		ShipRouteEntry entry2 = new ShipRouteEntry(2L, ship, new Planet(2L, 2, 2), 1);
		List<ShipRouteEntry> route = Lists.newArrayList(entry1, entry2);

		ship.setRoute(route);
		ship.setCurrentRouteEntry(entry2);

		Mockito.when(shipRouteEntryRepository.getOne(1L)).thenReturn(entry1);
		Mockito.when(shipRouteEntryRepository.findByShipId(ship.getId())).thenReturn(route);

		subject.deleteRouteEntry(1L);

		assertEquals(null, ship.getCurrentRouteEntry());
		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(ship).resetTravel();
		Mockito.verify(shipRouteEntryRepository).delete(entry1);
	}

	@Test
	public void shouldNotRedirectToNextRouteEntryBecauseNotDeletingCurrrent() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		ship.setDestinationX(1);
		ship.setDestinationY(1);

		ShipRouteEntry entry1 = new ShipRouteEntry(1L, ship, new Planet(1L, 1, 1), 0);
		ShipRouteEntry entry2 = new ShipRouteEntry(2L, ship, new Planet(2L, 2, 2), 1);
		ShipRouteEntry entry3 = new ShipRouteEntry(3L, ship, new Planet(3L, 3, 3), 2);

		List<ShipRouteEntry> route = Lists.newArrayList(entry1, entry2, entry3);
		ship.setRoute(route);
		ship.setCurrentRouteEntry(entry1);

		Mockito.when(shipRouteEntryRepository.getOne(2L)).thenReturn(entry2);
		Mockito.when(shipRouteEntryRepository.findByShipId(ship.getId())).thenReturn(route);

		subject.deleteRouteEntry(2L);

		assertEquals(entry1, ship.getCurrentRouteEntry());
		Mockito.verify(shipRepository, Mockito.never()).save(ship);
		Mockito.verify(ship, Mockito.never()).resetTravel();
		Mockito.verify(shipRouteEntryRepository).delete(entry2);

		assertEquals(1, ship.getDestinationX());
		assertEquals(1, ship.getDestinationY());
	}

	@Test
	public void shouldRedirectToNextRouteEntry() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);

		ShipRouteEntry entry1 = new ShipRouteEntry(1L, ship, new Planet(1L, 1, 1), 0);
		ShipRouteEntry entry2 = new ShipRouteEntry(2L, ship, new Planet(2L, 2, 2), 1);
		ShipRouteEntry entry3 = new ShipRouteEntry(3L, ship, new Planet(3L, 3, 3), 2);

		List<ShipRouteEntry> route = Lists.newArrayList(entry1, entry2, entry3);
		ship.setRoute(route);
		ship.setCurrentRouteEntry(entry1);

		Mockito.when(shipRouteEntryRepository.getOne(1L)).thenReturn(entry1);
		Mockito.when(shipRouteEntryRepository.findByShipId(ship.getId())).thenReturn(route);

		subject.deleteRouteEntry(1L);

		assertEquals(entry2, ship.getCurrentRouteEntry());
		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(ship, Mockito.never()).resetTravel();
		Mockito.verify(shipRouteEntryRepository).delete(entry1);

		assertEquals(2, ship.getDestinationX());
		assertEquals(2, ship.getDestinationY());
	}
}