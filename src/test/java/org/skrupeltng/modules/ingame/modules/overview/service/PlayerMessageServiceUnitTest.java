package org.skrupeltng.modules.ingame.modules.overview.service;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.controller.SendPlayerMessageRequest;
import org.springframework.context.MessageSource;

public class PlayerMessageServiceUnitTest {

	@Spy
	@InjectMocks
	private final PlayerMessageService subject = new PlayerMessageService();

	@Mock
	private NewsService newsService;

	@Mock
	private UserDetailServiceImpl userService;

	@Mock
	private PlayerRepository playerRepository;

	@Mock
	private MessageSource messageSource;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldCheckRecipientPresent() {
		SendPlayerMessageRequest request = new SendPlayerMessageRequest();
		request.setGameId(1L);
		request.setRecipientId(2L);

		Mockito.when(userService.getLoginId()).thenReturn(3L);

		Mockito.when(playerRepository.findById(2L)).thenReturn(Optional.empty());

		subject.sendMessage(request);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldCheckRecipientInSameGame() {
		SendPlayerMessageRequest request = new SendPlayerMessageRequest();
		request.setGameId(1L);
		request.setRecipientId(2L);

		Mockito.when(userService.getLoginId()).thenReturn(3L);

		Player recipient = new Player(2L);
		recipient.setGame(new Game(2L));
		Mockito.when(playerRepository.findById(2L)).thenReturn(Optional.of(recipient));

		subject.sendMessage(request);
	}

	@Test
	public void shouldSendMessage() {
		SendPlayerMessageRequest request = new SendPlayerMessageRequest();
		request.setGameId(1L);
		request.setRecipientId(2L);
		request.setMessage("message1");

		Mockito.when(userService.getLoginId()).thenReturn(3L);

		Player recipient = new Player(2L);
		recipient.setGame(new Game(1L));
		Mockito.when(playerRepository.findById(2L)).thenReturn(Optional.of(recipient));

		Player sender = new Player(4L);
		Login login = new Login();
		login.setUsername("sendername");
		sender.setLogin(login);
		Mockito.when(playerRepository.findByGameIdAndLoginId(1L, 3L)).thenReturn(Optional.of(sender));

		subject.sendMessage(request);

		Mockito.verify(newsService).add(recipient, NewsEntryConstants.news_entry_player_message, ImageConstants.news_message, null, null,
				sender.retrieveDisplayNameWithColor(null), request.getMessage(), sender.getId(), "'" + sender.retrieveDisplayName(null) + "'");
	}
}