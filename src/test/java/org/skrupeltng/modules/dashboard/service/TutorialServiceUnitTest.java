package org.skrupeltng.modules.dashboard.service;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.ingame.database.Tutorial;
import org.skrupeltng.modules.ingame.database.TutorialRepository;
import org.skrupeltng.modules.ingame.database.TutorialStage;
import org.skrupeltng.modules.ingame.service.round.RoundCalculationService;

public class TutorialServiceUnitTest {

	@Spy
	@InjectMocks
	private final TutorialService subject = new TutorialService();

	@Mock
	private TutorialRepository tutorialRepository;

	@Mock
	private LoginRepository loginRepository;

	@Mock
	private UserDetailServiceImpl userService;

	@Mock
	private TutorialGenerator tutorialGenerator;

	@Mock
	private RoundCalculationService roundCalculationService;

	@Mock
	private GameRemoval gameRemoval;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldRevertBecauseDifferentMainPage() {
		boolean result = subject.revertNecessary("planet", "planet", TutorialStage.OPEN_TECHLEVELS_FIRST);
		assertEquals(true, result);
	}

	@Test
	public void shouldNotRevertBecauseSameMainPage() {
		boolean result = subject.revertNecessary("planet", "planet", TutorialStage.COLONISTS);
		assertEquals(false, result);
	}

	@Test
	public void shouldNotRevertBecauseSameMainPageAndNoExpectedSubpage() {
		boolean result = subject.revertNecessary("planet;mines", "planet", TutorialStage.OPEN_FACTORIES_FIRST);
		assertEquals(false, result);
	}

	@Test
	public void shouldRevertBecauseSameMainPageAndDifferentSubpage() {
		boolean result = subject.revertNecessary("planet;mines", "planet", TutorialStage.BUILD_FACTORIES_FIRST);
		assertEquals(true, result);
	}

	private Tutorial setupTutorial(TutorialStage stage) {
		Tutorial tutorial = new Tutorial();
		tutorial.setStage(stage);
		Mockito.when(tutorialRepository.findByLoginId(Mockito.anyLong())).thenReturn(Optional.of(tutorial));
		return tutorial;
	}

	@Test
	public void shouldRevertToMines() {
		Tutorial tutorial = setupTutorial(TutorialStage.BUILD_FACTORIES_FIRST);
		subject.checkTutorialStage("planet;mines");
		assertEquals(TutorialStage.OPEN_FACTORIES_FIRST, tutorial.getStage());
	}

	@Test
	public void shouldRevertToHomePlanetSelection() {
		Tutorial tutorial = setupTutorial(TutorialStage.BUILD_FACTORIES_FIRST);
		subject.checkTutorialStage("starbase");
		assertEquals(TutorialStage.INITIAL_SELECT_HOME_PLANET, tutorial.getStage());
	}

	@Test
	public void shouldRevertToOpenStarbase() {
		Tutorial tutorial = setupTutorial(TutorialStage.UPGRADE_TECHLEVELS_FIRST);
		subject.checkTutorialStage("planet");
		assertEquals(TutorialStage.INITIAL_OPEN_STARBASE, tutorial.getStage());
	}

	@Test
	public void shouldRevertFromSelectToOpenShipConstructionFreighter() {
		Tutorial tutorial = setupTutorial(TutorialStage.SELECT_SHIP_CONSTRUCTON_FREIGHTER);
		subject.checkTutorialStage("starbase;production-projectileweapon");
		assertEquals(TutorialStage.OPEN_SHIP_CONSTRUCTON_FREIGHTER, tutorial.getStage());
	}

	@Test
	public void shouldRevertFromBuildToOpenShipConstructionFreighter() {
		Tutorial tutorial = setupTutorial(TutorialStage.BUILD_SHIP_CONSTRUCTON_FREIGHTER);
		subject.checkTutorialStage("starbase;production-projectileweapon");
		assertEquals(TutorialStage.OPEN_SHIP_CONSTRUCTON_FREIGHTER, tutorial.getStage());
	}
}