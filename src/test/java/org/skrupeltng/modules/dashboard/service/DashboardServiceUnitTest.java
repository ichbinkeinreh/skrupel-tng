package org.skrupeltng.modules.dashboard.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;

public class DashboardServiceUnitTest {

	@Spy
	@InjectMocks
	private final DashboardService subject = new DashboardService();

	@Mock
	private UserDetailServiceImpl userService;

	@Mock
	private GameRepository gameRepository;

	@Mock
	private GameRemoval gameRemoval;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotDeleteGameBecauseUserIsNotAdminAndNotCreator() {
		Game game = new Game(1L);
		game.setCreator(new Login(2L));
		Mockito.when(gameRepository.getOne(1L)).thenReturn(game);

		subject.deleteGame(1L);
	}

	@Test
	public void shouldDeleteGameBecauseUserIsCreator() {
		Game game = new Game(1L);
		game.setCreator(new Login(2L));
		Mockito.when(gameRepository.getOne(1L)).thenReturn(game);

		Mockito.when(userService.getLoginId()).thenReturn(2L);

		subject.deleteGame(1L);

		Mockito.verify(gameRemoval).delete(game);
	}

	@Test
	public void shouldDeleteGameBecauseUserIsAdmin() {
		Game game = new Game(1L);
		game.setCreator(new Login(2L));
		Mockito.when(gameRepository.getOne(1L)).thenReturn(game);

		Mockito.when(userService.isAdmin()).thenReturn(true);

		subject.deleteGame(1L);

		Mockito.verify(gameRemoval).delete(game);
	}
}