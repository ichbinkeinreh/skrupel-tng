package org.skrupeltng.modules.dashboard.service;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.StartPositionSetup;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoeRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetTypeRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.testcontainers.shaded.com.google.common.collect.Sets;

import com.google.common.collect.Lists;

public class GameStartingHelperUnitTest {

	@Spy
	@InjectMocks
	private final GameStartingHelper subject = new GameStartingHelper();

	@Mock
	private GameRepository gameRepository;

	@Mock
	private PlayerRepository playerRepository;

	@Mock
	private PlanetRepository planetRepository;

	@Mock
	private WormHoleRepository wormHoleRepository;

	@Mock
	private StarbaseRepository starbaseRepository;

	@Mock
	private PlanetTypeRepository planetTypeRepository;

	@Mock
	private OrbitalSystemRepository orbitalSystemRepository;

	@Mock
	private PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Mock
	private PlayerDeathFoeRepository playerDeathFoeRepository;

	@Mock
	private MasterDataService masterDataService;

	@Mock
	private DashboardService dashboardService;

	@Mock
	private StatsUpdater statsUpdater;

	@Mock
	private PlanetGenerator planetGenerator;

	@Mock
	private StartPositionHelper startPositionHelper;

	@Mock
	private ConfigProperties configProperties;

	private Game game;
	private Player player1;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		game = new Game(1L);

		List<Player> players = Lists.newArrayList(player1);
		game.setPlayers(players);

		game.setPlanets(Sets.newHashSet(new Planet(1L)));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldCallEqualDistanceMethod() {
		game.setStartPositionSetup(StartPositionSetup.EQUAL_DISTANCE);

		subject.assignStartPositions(game);

		Mockito.verify(startPositionHelper).assignStartPositionsEqualDistance(Mockito.eq(game), Mockito.eq(game.getPlayers()), Mockito.anyList());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldRandomMethod() {
		game.setStartPositionSetup(StartPositionSetup.RANDOM);

		subject.assignStartPositions(game);

		Mockito.verify(startPositionHelper).assignStartPositionsRandom(Mockito.eq(game), Mockito.eq(game.getPlayers()), Mockito.anyList());
	}
}
