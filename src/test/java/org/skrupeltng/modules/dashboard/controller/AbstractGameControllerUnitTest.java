package org.skrupeltng.modules.dashboard.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.service.DashboardService;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoe;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class AbstractGameControllerUnitTest {

	@Spy
	@InjectMocks
	private final AbstractGameController subject = new DashboardController();

	@Mock
	private MessageSource messageSource;

	@Mock
	private DashboardService dashboardService;

	@Mock
	private Model model;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		Mockito.when(messageSource.getMessage(Mockito.eq("and"), Mockito.eq(null), Mockito.any())).thenReturn("and");
	}

	@Test
	public void shouldCreateNoOneSurvivedText() {
		WinCondition winCondition = WinCondition.SURVIVE;

		Player player1 = new Player(1L);
		player1.setHasLost(true);
		Player player2 = new Player(2L);
		player2.setHasLost(true);

		subject.addGameFinishedText(model, Lists.newArrayList(player1, player2), winCondition);

		Mockito.verify(messageSource).getMessage(Mockito.eq("no_one_survived"), Mockito.eq(null), Mockito.any());
	}

	@Test
	public void shouldCreateSurviveText() {
		WinCondition winCondition = WinCondition.SURVIVE;

		Player player1 = new Player(1L);
		Login login = new Login(1L);
		login.setUsername("player1");
		player1.setLogin(login);
		Player player2 = new Player(2L);
		player2.setHasLost(true);

		subject.addGameFinishedText(model, Lists.newArrayList(player1, player2), winCondition);

		ArgumentCaptor<Object[]> args = ArgumentCaptor.forClass(Object[].class);
		Mockito.verify(messageSource).getMessage(Mockito.eq("game_finished_text_SURVIVE"), args.capture(), Mockito.any());

		Object[] array = args.getValue();
		assertEquals(1, array.length);
		assertTrue(array[0].toString().contains("player1"));
	}

	@Test
	public void shouldCreateMutuallyDestroyedDeathFoeText() {
		WinCondition winCondition = WinCondition.DEATH_FOE;

		Player player1 = new Player(1L);
		Login login = new Login(1L);
		login.setUsername("player1");
		player1.setLogin(login);
		player1.setHasLost(true);

		Player player2 = new Player(2L);
		Login login2 = new Login(2L);
		login2.setUsername("player2");
		player2.setLogin(login2);
		player2.setHasLost(true);

		PlayerDeathFoe deathFoe = new PlayerDeathFoe();
		deathFoe.setDeathFoe(player2);
		player1.setDeathFoes(Sets.newHashSet(deathFoe));

		subject.addGameFinishedText(model, Lists.newArrayList(player1, player2), winCondition);

		Mockito.verify(messageSource).getMessage(Mockito.eq("mutually_destroyed_death_foes"), Mockito.eq(null), Mockito.any());
	}

	@Test
	public void shouldCreateSingleDeathFoeText() {
		WinCondition winCondition = WinCondition.DEATH_FOE;

		Player player1 = new Player(1L);
		Login login = new Login(1L);
		login.setUsername("player1");
		player1.setLogin(login);

		Player player2 = new Player(2L);
		Login login2 = new Login(2L);
		login2.setUsername("player2");
		player2.setLogin(login2);
		player2.setHasLost(true);

		PlayerDeathFoe deathFoe = new PlayerDeathFoe();
		deathFoe.setDeathFoe(player2);
		player1.setDeathFoes(Sets.newHashSet(deathFoe));

		subject.addGameFinishedText(model, Lists.newArrayList(player1, player2), winCondition);

		ArgumentCaptor<Object[]> args = ArgumentCaptor.forClass(Object[].class);
		Mockito.verify(messageSource).getMessage(Mockito.eq("game_finished_text_DEATH_FOE"), args.capture(), Mockito.any());

		Object[] array = args.getValue();
		assertEquals(2, array.length);
		assertTrue(array[0].toString().contains("player1"));
		assertTrue(array[1].toString().contains("player2"));
	}

	@Test
	public void shouldCreateMultipleDeathFoeText() {
		WinCondition winCondition = WinCondition.DEATH_FOE;

		Player player1 = new Player(1L);
		Login login1 = new Login(1L);
		login1.setUsername("player1");
		player1.setLogin(login1);

		Player player2 = new Player(2L);
		Login login2 = new Login(2L);
		login2.setUsername("player2");
		player2.setLogin(login2);
		player2.setHasLost(true);

		PlayerDeathFoe deathFoe1 = new PlayerDeathFoe();
		deathFoe1.setDeathFoe(player2);
		player1.setDeathFoes(Sets.newHashSet(deathFoe1));

		Player player3 = new Player(3L);
		Login login3 = new Login(3L);
		login3.setUsername("player3");
		player3.setLogin(login3);

		Player player4 = new Player(2L);
		Login login4 = new Login(4L);
		login4.setUsername("player4");
		player4.setLogin(login4);
		player4.setHasLost(true);

		PlayerDeathFoe deathFoe2 = new PlayerDeathFoe();
		deathFoe2.setDeathFoe(player4);
		player3.setDeathFoes(Sets.newHashSet(deathFoe2));

		subject.addGameFinishedText(model, Lists.newArrayList(player1, player2, player3, player4), winCondition);

		ArgumentCaptor<Object[]> args = ArgumentCaptor.forClass(Object[].class);
		Mockito.verify(messageSource).getMessage(Mockito.eq("game_finished_text_multiple_DEATH_FOE"), args.capture(), Mockito.any());

		Object[] array = args.getValue();
		assertEquals(1, array.length);
		assertTrue(array[0].toString().contains("player1") && array[0].toString().contains("player3"));
	}

	@Test
	public void shouldCreateSingleTeamDeathFoeText() {
		WinCondition winCondition = WinCondition.TEAM_DEATH_FOE;

		Player player1 = new Player(1L);
		Login login1 = new Login(1L);
		login1.setUsername("player1");
		player1.setLogin(login1);

		Player player2 = new Player(2L);
		Login login2 = new Login(2L);
		login2.setUsername("player2");
		player2.setLogin(login2);
		player2.setHasLost(true);

		Player player3 = new Player(3L);
		Login login3 = new Login(3L);
		login3.setUsername("player3");
		player3.setLogin(login3);
		player3.setHasLost(true);

		Player player4 = new Player(2L);
		Login login4 = new Login(4L);
		login4.setUsername("player4");
		player4.setLogin(login4);
		player4.setHasLost(true);

		PlayerDeathFoe deathFoe1 = new PlayerDeathFoe(1L);
		deathFoe1.setDeathFoe(player3);
		PlayerDeathFoe deathFoe2 = new PlayerDeathFoe(2L);
		deathFoe2.setDeathFoe(player4);

		Set<PlayerDeathFoe> deathFoes = Sets.newHashSet(deathFoe1, deathFoe2);
		player1.setDeathFoes(deathFoes);
		player2.setDeathFoes(deathFoes);

		subject.addGameFinishedText(model, Lists.newArrayList(player1, player2, player3, player4), winCondition);

		ArgumentCaptor<Object[]> args = ArgumentCaptor.forClass(Object[].class);
		Mockito.verify(messageSource).getMessage(Mockito.eq("game_finished_text_single_TEAM_DEATH_FOE"), args.capture(), Mockito.any());

		Object[] array = args.getValue();
		assertEquals(2, array.length);
		assertTrue(array[0].toString().contains("player1"));
		assertTrue(array[1].toString().contains("player3") && array[1].toString().contains("player4"));
	}

	@Test
	public void shouldCreateMultipleTeamDeathFoeText() {
		WinCondition winCondition = WinCondition.TEAM_DEATH_FOE;

		Player player1 = new Player(1L);
		Login login1 = new Login(1L);
		login1.setUsername("player1");
		player1.setLogin(login1);

		Player player2 = new Player(2L);
		Login login2 = new Login(2L);
		login2.setUsername("player2");
		player2.setLogin(login2);

		Player player3 = new Player(3L);
		Login login3 = new Login(3L);
		login3.setUsername("player3");
		player3.setLogin(login3);
		player3.setHasLost(true);

		Player player4 = new Player(2L);
		Login login4 = new Login(4L);
		login4.setUsername("player4");
		player4.setLogin(login4);
		player4.setHasLost(true);

		PlayerDeathFoe deathFoe1 = new PlayerDeathFoe(1L);
		deathFoe1.setDeathFoe(player3);
		PlayerDeathFoe deathFoe2 = new PlayerDeathFoe(2L);
		deathFoe2.setDeathFoe(player4);

		Set<PlayerDeathFoe> deathFoes = Sets.newHashSet(deathFoe1, deathFoe2);
		player1.setDeathFoes(deathFoes);
		player2.setDeathFoes(deathFoes);

		subject.addGameFinishedText(model, Lists.newArrayList(player1, player2, player3, player4), winCondition);

		ArgumentCaptor<Object[]> args = ArgumentCaptor.forClass(Object[].class);
		Mockito.verify(messageSource).getMessage(Mockito.eq("game_finished_text_TEAM_DEATH_FOE"), args.capture(), Mockito.any());

		Object[] array = args.getValue();
		assertEquals(2, array.length);
		assertTrue(array[0].toString().contains("player1") && array[0].toString().contains("player2"));
		assertTrue(array[1].toString().contains("player3") && array[1].toString().contains("player4"));
	}

	@Test
	public void shouldCreateSingleLostAllyText() {
		Player winner = new Player(1L);

		Player lostAlly = new Player(2L);
		Login login = new Login(2L);
		login.setUsername("player2");
		lostAlly.setLogin(login);
		Mockito.when(dashboardService.getLostAllies(winner)).thenReturn(Lists.newArrayList(lostAlly));

		subject.createAdditionalText(winner);

		ArgumentCaptor<Object[]> args = ArgumentCaptor.forClass(Object[].class);
		Mockito.verify(messageSource).getMessage(Mockito.eq("lost_ally"), args.capture(), Mockito.any());

		Object[] array = args.getValue();
		assertEquals(1, array.length);
		assertTrue(array[0].toString().contains("player2"));
	}

	@Test
	public void shouldCreateMultipleLostAlliesText() {
		Player winner = new Player(1L);

		Player lostAlly1 = new Player(2L);
		Login login1 = new Login(2L);
		login1.setUsername("player2");
		lostAlly1.setLogin(login1);

		Player lostAlly2 = new Player(2L);
		Login login2 = new Login(2L);
		login2.setUsername("player3");
		lostAlly2.setLogin(login2);

		Mockito.when(dashboardService.getLostAllies(winner)).thenReturn(Lists.newArrayList(lostAlly1, lostAlly2));

		subject.createAdditionalText(winner);

		ArgumentCaptor<Object[]> args = ArgumentCaptor.forClass(Object[].class);
		Mockito.verify(messageSource).getMessage(Mockito.eq("lost_allies"), args.capture(), Mockito.any());

		Object[] array = args.getValue();
		assertEquals(1, array.length);
		assertTrue(array[0].toString().contains("player2") && array[0].toString().contains("player3"));
	}
}